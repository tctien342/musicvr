﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneS : MonoBehaviour {
	public new GameObject gameObjectA;
	private AudioProcess AudioP;
	private float scale = 0.9f;
	private Transform Trans;
	// Use this for initialization
	void Start () {
		AudioP =  gameObjectA.GetComponent<AudioProcess>();
		Trans = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		float scaleP = Remap(AudioP.OutPutSpectrum[3], 50.0f, 500.0f, 0.3f, 1.0f);
		if (scale > scaleP)
        {
            scale -= Time.deltaTime;
        }else{
			scale += Time.deltaTime;
		}
		Trans.localScale = new Vector3(scale, scale, scale);
	}

	public float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}
