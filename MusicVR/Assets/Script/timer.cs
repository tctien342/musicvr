﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class timer : MonoBehaviour {

	//GET OBJECT
	public new GameObject AudioObject;
	public Shader shaderMaterial;


	//GET AUDIO PROS-VALUE
	private AudioProcess AudioP; 

	//DELAY TIME PRE-LOAD
	private float DelayTime = 0f;
	public float DelayTimeValue = 1;

	///NEW EFFECT CALLCULATE DATA
    //TIME
        private float timeBass = 0f;
        private float timeMid = 0f;
        private float timeTreble = 0f;
		public float timeUpdate = 0.1f;
    //DATA FREG
        private float BassAvg = 0f;
        private float MidAvg = 0f;
        private float TrebleAvg = 0f;

	//INIT STATE
	void Start () {
		AudioP = AudioObject.GetComponent<AudioProcess>();
	}
	
	//UPDATE STATE
	void Update () {
		DelayTime += Time.deltaTime;
		if (DelayTime > DelayTimeValue){
			// TIME COUNT
				timeBass += Time.deltaTime;
				timeMid += Time.deltaTime;
				timeTreble += Time.deltaTime;
			// FREG CHECK
				//BASS
				BassAvg = AudioP.bassAvgF;
				//VOCAL - MID
				if (AudioP.OutPutSpectrum[3]/3 > MidAvg) MidAvg = AudioP.OutPutSpectrum[3]/3;
				//TREBLE
				if (AudioP.OutPutSpectrum[4]/3 > TrebleAvg) TrebleAvg = AudioP.OutPutSpectrum[4]/3;
			// DO CREATE SHAPE
				if (AudioP.OutPutSpectrum[0] > BassAvg && timeBass > timeUpdate){
					//RE-STATE
					timeBass = 0;
					//MAKE
					MakeBasS();
				}
				if (AudioP.OutPutSpectrum[3] > MidAvg && timeMid > timeUpdate){
					timeMid = 0;
					MakeVocal();
				}
				if (AudioP.OutPutSpectrum[4] > TrebleAvg && timeTreble > timeUpdate){
					timeTreble = 0;
					MakeTreblE();
				}

			// // Instantiate(gameObject, new Vector3(0,0,20.0f), Quaternion.Euler(new Vector3(90, 0f, 0f)));
			// var shapeObject = new GameObject("Shape");
			// var ShapeDraw = shapeObject.AddComponent<CircleShape>();
			// var lineObject = shapeObject.GetComponent<LineRenderer>();
			// // Set material
			// var material = new Material(shaderMaterial);
			// material.color = new Color(Random.Range(0.5f,1.0f),Random.Range(0.5f,1.0f),Random.Range(0.5f,1.0f));
			// lineObject.material = material;
			// lineObject.SetWidth(0.1f,0.1f);
			// ShapeDraw.segments = 4;
			// ShapeDraw.gameObjectA = AudioObject;
			// ShapeDraw.spectrumSel = bandSel;
			// // ShapeDraw.speed = Mathf.RoundToInt(AudioP.speedSong/2);
			// var bonusRange = Mathf.RoundToInt(AudioP.speedSong);
			// shapeObject.transform.position = new Vector3(bonusRange*Random.Range(-20f,20f),Random.Range(-100f,100f),860f);
			// shapeObject.transform.rotation = Quaternion.Euler(new Vector3(90f, 0f, 0f));
			// bandSel = Mathf.RoundToInt(Random.Range(0,5));
		}
	}

	//MAKE RING-SHAPE FOR BASS
	void MakeBasS(){
		//INIT SHAPE
		var shapeObject = new GameObject("Shape");
		var ShapeDraw = shapeObject.AddComponent<CircleShape>();
		var lineObject = shapeObject.GetComponent<LineRenderer>();
		// Set material
		var material = new Material(shaderMaterial);
		material.color = new Color(Remap(AudioP.OutPutSpectrum[0],100f,300f,0.0f,1.0f),Remap(AudioP.OutPutSpectrum[1],10f,1000f,0.0f,1.0f),Remap(AudioP.OutPutSpectrum[2],0f,300f,1.0f,0.0f));
		lineObject.material = material;
		//SET SHAPE WITH
		lineObject.SetWidth(2f,2f);
		//SHAPE TRIGLE
		ShapeDraw.segments = 50;
		//SHAPE SETUP
		ShapeDraw.timeSurvite = Remap(AudioP.OutPutSpectrum[0],100,1000,3f,20f);
		ShapeDraw.AudioP = AudioP;
		ShapeDraw.spectrumSel = 0;
		ShapeDraw.scaleThres = BassAvg;
		ShapeDraw.speed = Remap(AudioP.OutPutSpectrum[0],100,1000,100f,80f);
		//SHAPE-RANGE RANDOM
		var bonusRange = Mathf.RoundToInt(AudioP.speedSong);
		shapeObject.transform.position = new Vector3(bonusRange*Random.Range(-20f,20f),Random.Range(-100f,100f),860f);
		shapeObject.transform.rotation = Quaternion.Euler(new Vector3(90f, 0f, 0f));
	}
	//MAKE HEXA-SHAPE FOR TREBLE
	void MakeTreblE(){
		var shapeObject = new GameObject("Shape");
		var ShapeDraw = shapeObject.AddComponent<CircleShape>();
		var lineObject = shapeObject.GetComponent<LineRenderer>();
		// Set material
		var material = new Material(shaderMaterial);
		material.color = new Color(Remap(AudioP.OutPutSpectrum[3],0f,50f,0.0f,1.0f),Remap(AudioP.OutPutSpectrum[4],0f,10f,0.0f,1.0f),Remap(AudioP.OutPutSpectrum[5],0f,5f,0.0f,1.0f));
		material.color += Color.white/2.0f;
		lineObject.material = material;
		lineObject.SetWidth(1f,1f);
		ShapeDraw.segments = 6;
		ShapeDraw.timeSurvite = Remap(AudioP.OutPutSpectrum[4],0f,10f,3f,20f);
		ShapeDraw.AudioP = AudioP;
		ShapeDraw.spectrumSel = 4;
		ShapeDraw.scaleThres = TrebleAvg;
		ShapeDraw.speed = Remap(AudioP.OutPutSpectrum[4],0f,10f,30f,70f);
		//RANGE RANDOM
		var bonusRange = Mathf.RoundToInt(AudioP.speedSong);
		shapeObject.transform.position = new Vector3(bonusRange*Random.Range(-20f,20f),Random.Range(-100f,100f),860f);
		shapeObject.transform.rotation = Quaternion.Euler(new Vector3(90f, 0f, 0f));
	}
	//MAKE SQUARE-SHAPE FOR VOICE
	void MakeVocal(){
		var shapeObject = new GameObject("Shape");
		var ShapeDraw = shapeObject.AddComponent<CircleShape>();
		var lineObject = shapeObject.GetComponent<LineRenderer>();
		// Set material
		var material = new Material(shaderMaterial);
		material.color = new Color(Remap(AudioP.OutPutSpectrum[2],0f,400f,0.0f,1.0f),Remap(AudioP.OutPutSpectrum[3],0f,300f,0.3f,1.0f),Remap(AudioP.OutPutSpectrum[4],0f,100f,0.3f,1.0f));
		lineObject.material = material;
		lineObject.SetWidth(1.4f,1.4f);
		ShapeDraw.segments = 4;
		ShapeDraw.AudioP = AudioP;
		ShapeDraw.spectrumSel = 3;
		ShapeDraw.timeSurvite = Remap(AudioP.OutPutSpectrum[3],10f,200f,3f,20f);
		ShapeDraw.scaleThres = MidAvg;
		ShapeDraw.speed = Remap(AudioP.OutPutSpectrum[3],10f,200f,30f,80f);
		//RANGE RANDOM
		var bonusRange = Mathf.RoundToInt(AudioP.speedSong);
		shapeObject.transform.position = new Vector3(bonusRange*Random.Range(-20f,20f),Random.Range(-100f,100f),860f);
		shapeObject.transform.rotation = Quaternion.Euler(new Vector3(90f, 0f, 0f));
	}

	public float Remap(float value, float from1, float to1, float from2, float to2)
    {
		if(from1 < from2){
			if (value < from1) value = from1;
			if (value > from2) value = from2;
		}else{
			if (value > from1) value = from1;
			if (value < from2) value = from2;
		}
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}
