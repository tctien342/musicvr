﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioProcess : MonoBehaviour
{
    public int FromSpec = 0;
    public int ToSpec = 0;
    public int Clarity = 10;
    public Material m;
    // private Camera cam;

    private AudioVisualiser AudioV;
    private float[] SpectrumArray;
    private Color color;
    public float[] OutPutSpectrum;
    public float speedSong = 100.0f;

    public int bassCollectNumber = 100;
    public float bassAvgF = 0;
    public float bassAvg = 0;

    private float[] bassCollect;
    private int bassPos = 0;
    private float r = 0.1f;
    private float g = 0.1f;
    private float b = 0.1f;
    // Use this for initialization
    void Start()
    {
        AudioV = GetComponent<AudioVisualiser>();
        SpectrumArray = AudioV.spectrum;
        if (ToSpec == 0) ToSpec = AudioV.SAMPLE_SIZE;
        OutPutSpectrum = new float[Clarity];
        color = new Color(1, 1, 1);
        bassCollect = new float[bassCollectNumber];
    }

    // Update is called once per frame
    void Update()
    {   
        //SpeedSong Value
        speedSong = AudioV.pitchValue;
        //Make output spectrum
        specTrumCal();
        //Bass AvgCal
        if (bassAvgF > bassAvg){
            bassAvg = bassAvgF;
        }
    }

    void specTrumCal()
    {
        SpectrumArray = AudioV.spectrum;
        int pos = FromSpec;
        int clarityPart = ToSpec / Clarity;
        int mul = 1;
        while (pos < ToSpec && mul <= Clarity)
        {
            float sum = 0;
            for (int i = pos; i < clarityPart * mul; i++)
            {
                if (i == ToSpec) break;
                sum += SpectrumArray[i];
            }
            OutPutSpectrum[mul - 1] = (float)sum * 100000 / clarityPart;
            if (mul == 1)
            {
                if (bassPos >= bassCollectNumber) bassPos = 0;
                bassCollect[bassPos] = (float)sum * 100000 / clarityPart;
                int count = 0;
                float bassSum = 0;
                for (int j = 0; j < bassCollectNumber; j++)
                {
                    if (bassCollect[j]>0)
                    {
                        count++;
                        bassSum += bassCollect[j];
                    }
                }
                if (count > 0)
                {
                    bassAvgF = bassSum / count;
                }
                bassPos++;
            }
            pos = clarityPart * mul;
            mul++;
        }
        
    }

    public float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}
