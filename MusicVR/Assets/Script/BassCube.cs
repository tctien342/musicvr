﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BassCube : MonoBehaviour {

    public new GameObject gameObject;
    public int sel = 0;

    private AudioProcess AudioV;
    private AudioVisualiser AudioVisu;
    private Transform Trans;
    private Light lt;
    private float scale = 5;

    public Transform target;
	public float orbitDistance = 30.0f;
	public float orbitDegreesPerSec = 180.0f;

	// Use this for initialization
	void Start () {
        AudioV = gameObject.GetComponent<AudioProcess>();
        AudioVisu = gameObject.GetComponent<AudioVisualiser>();
        Trans = GetComponent<Transform>();
        lt = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
		if (scale * 100 > AudioV.bassAvg && scale > 5)
        {
            scale -= Time.deltaTime * 5;  
        }
        if (AudioVisu.pitchValue < 100)
        {
            if (sel == 0) transform.Rotate(Vector3.right * Time.deltaTime* AudioVisu.pitchValue * 2);
            if (sel == 1) transform.Rotate(Vector3.up * Time.deltaTime * AudioVisu.pitchValue * 2);
            if (sel == 2) transform.Rotate(Vector3.forward * Time.deltaTime * AudioVisu.pitchValue * 2);
        }
        if (AudioV.OutPutSpectrum[0] > AudioV.bassAvg)
        {
            scale += Time.deltaTime * 10;
            if (scale > 20) scale = 5;
            if (lt.color.a < 0 || lt.color.b < 0 || lt.color.g < 0) lt.color = new Color(AudioV.OutPutSpectrum[0]%1, AudioV.OutPutSpectrum[1] % 1, AudioV.OutPutSpectrum[2] % 1);
        }
        lt.range = scale * 4 + 10;
        lt.color -= Color.white / 2.0F * Time.deltaTime;
        Trans.localScale = new Vector3(scale, scale, scale);
        orbitDegreesPerSec = Remap(AudioVisu.pitchValue, 10.0f, 1000.0f, 10.0f, 160.0f);
	}

    void LateUpdate () {

		Orbit();

	}

    void Orbit()
	{
        if (target != null)
		{
			// Keep us at orbitDistance from target
			transform.position = target.position + (transform.position - target.position).normalized * orbitDistance;
			transform.RotateAround(target.position, Vector3.up, orbitDegreesPerSec * Time.deltaTime);
		}
	}

    public float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}
