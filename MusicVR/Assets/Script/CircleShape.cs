﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class CircleShape : MonoBehaviour {

    //SHAPE VALUE
	[Range(0,50)]
    public int segments = 50;
    [Range(0,20)]
    public float xradius = 15;
    [Range(0,20)]
    public float yradius = 15;
    //OBJECT TRACKING
    LineRenderer line;
    public AudioProcess AudioP;
    
    //OBJECT SETUP
    public float speed = 30.0f;
    public float scaleThres = 0f;
    public int spectrumSel = 0;
    public float timeSurvite = 0f;

    //PRIVATE STATE
    private Transform Trans;
    private float scale = 5;
    

	// Use this for initialization
	void Start () {
		line = GetComponent<LineRenderer>();
        // AudioP = gameObjectA.GetComponent<AudioProcess>();
        Trans = GetComponent<Transform>();
        line.SetVertexCount (segments + 1);
        line.useWorldSpace = false;
        CreatePoints ();
	}
	
	// Update is called once per frame
	void Update () {
        //speed = speed < Remap(AudioP.speedSong,10,500,30,120)? speed+Time.deltaTime*20.0f : speed-Time.deltaTime*10.0f;
		// renderBelowPlane();
        updateThisShape();
	}

    // Setup shape state
    void updateThisShape(){
        //Survite time
        timeSurvite -= Time.deltaTime;

        //SCALE UPDATE
        if (AudioP.OutPutSpectrum[spectrumSel] > scaleThres){
            scale += Time.deltaTime * Remap(AudioP.speedSong,50,500,30,10);
        }else{
            scale -= Time.deltaTime * Remap(AudioP.speedSong,50,500,30,10);
        }
        if (scale < 5) scale = 5;
        if (scale > 20) scale = 20;
        Trans.localScale = new Vector3(scale, scale, scale);
        //COLOR UPDATE
        line.material.color -= (Color.white/100.0f)*timeSurvite;
        //POS UPDATE
        Trans.position -= new Vector3(0,0,Time.deltaTime*speed);
        if (Trans.position.z < -600.0f || timeSurvite < 0) {
            Destroy(gameObject);
        }
    }

    void renderBelowPlane(){
        // if (AudioV.bassAvg/5 > AudioV.OutPutSpectrum[speedTrumSel] && scale > 5)
        // {
        //     scale -= Time.deltaTime * bassSpeed/2;
        // }else if (AudioV.bassAvg/5 < AudioV.OutPutSpectrum[speedTrumSel] ){
        //     scale += Time.deltaTime * bassSpeed;
        // }
        // if (AudioV.OutPutSpectrum[0] > AudioV.bassAvg)
        // {
        //     scale += Time.deltaTime * bassSpeed;
        // }
        // if (scale > 10) scale = 10;
        // Trans.localScale = new Vector3(scale/6, scale/6, scale/6);
        // Trans.position += new Vector3(0,0,Time.deltaTime*speed);
        // if (Trans.position.z < -600.0f ) {
        //     Destroy(gameObject);
        // }
    }


	void CreatePoints ()
    {
        float x;
        float y;
        float z;

        float angle = 20f;

        for (int i = 0; i < (segments + 1); i++)
        {
            x = Mathf.Sin (Mathf.Deg2Rad * angle) * xradius;
            z = Mathf.Cos (Mathf.Deg2Rad * angle) * yradius;

            line.SetPosition (i,new Vector3(x,0,z) );

            angle += (360f / segments);
        }
    }

    public float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}
