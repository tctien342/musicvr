﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateByTime : MonoBehaviour {
	public Transform target;
	public float orbitDistance = 10.0f;
	public float orbitDegreesPerSec = 180.0f;
    public GameObject Camera;
	public Renderer rend;
	public Light lt;
    public float debug;
    
	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer>();
		lt = GetComponent<Light>();
	}

	void Orbit()
	{
        if (target != null)
		{
			// Keep us at orbitDistance from target
			transform.position = target.position + (transform.position - target.position).normalized * orbitDistance;
			transform.RotateAround(target.position, Vector3.up, orbitDegreesPerSec * Time.deltaTime);
		}
	}

	// Update is called once per frame
	void Update () {
        var musicVO = Camera.GetComponent< AudioVisualiser>();
        debug = musicVO.pitchValue;
        
        orbitDegreesPerSec = Remap(musicVO.pitchValue, 5.0f, 500.0f, 0.0f, 180.0f);
        var newV = Remap(musicVO.pitchValue, 5.0f, 500.0f, 0.0f, 1000.0f)/1000;
        if (orbitDegreesPerSec > 100.0f) orbitDegreesPerSec = 100.0f;
        if (lt.color.a < 0 || lt.color.b < 0 || lt.color.g < 0 ) lt.color = new Color (newV*Random.value, newV * Random.value, newV * Random.value);
		lt.color -= Color.white / 2.0F * Time.deltaTime;
		Debug.Log(lt.color);
//		rend.material.color = new Color(255,255,1);
		// Call from LateUpdate instead...
		// Orbit();

	}
	// Call from LateUpdate if you want to be sure your
	// target is done with it's move.
	void LateUpdate () {

		Orbit();

	}

    public float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}
