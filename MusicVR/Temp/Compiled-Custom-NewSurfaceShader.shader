// Compiled shader for Android

//////////////////////////////////////////////////////////////////////////
// 
// NOTE: This is *not* a valid shader file, the contents are provided just
// for information and for debugging purposes only.
// 
//////////////////////////////////////////////////////////////////////////
// Skipping shader variants that would not be included into build of current scene.

Shader "Custom/NewSurfaceShader" {
Properties {
 _Color ("Color", Color) = (1.000000,1.000000,1.000000,1.000000)
 _MainTex ("Albedo (RGB)", 2D) = "white" { }
 _Glossiness ("Smoothness", Range(0.000000,1.000000)) = 0.500000
 _Metallic ("Metallic", Range(0.000000,1.000000)) = 0.000000
}
SubShader { 
 LOD 200
 Tags { "RenderType"="Opaque" }


 // Stats for Vertex shader:
 //         gles: 75 avg math (50..95), 2 avg texture (2..4)
 Pass {
  Name "FORWARD"
  Tags { "LIGHTMODE"="FORWARDBASE" "SHADOWSUPPORT"="true" "RenderType"="Opaque" }
  //////////////////////////////////
  //                              //
  //      Compiled programs       //
  //                              //
  //////////////////////////////////
//////////////////////////////////////////////////////
Keywords set in this variant: DIRECTIONAL 
-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 50 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  highp vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_4;
  tmpvar_4[0] = unity_WorldToObject[0].xyz;
  tmpvar_4[1] = unity_WorldToObject[1].xyz;
  tmpvar_4[2] = unity_WorldToObject[2].xyz;
  tmpvar_1.xyz = (unity_ObjectToWorld * _glesVertex).xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_4));
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD6 = tmpvar_2;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
void main ()
{
  mediump vec3 tmpvar_1;
  highp vec4 tmpvar_2;
  mediump vec3 tmpvar_3;
  mediump vec3 tmpvar_4;
  lowp vec4 c_5;
  lowp vec3 tmpvar_6;
  lowp float tmpvar_7;
  highp vec3 worldViewDir_8;
  lowp vec3 lightDir_9;
  mediump vec3 tmpvar_10;
  tmpvar_10 = _WorldSpaceLightPos0.xyz;
  lightDir_9 = tmpvar_10;
  worldViewDir_8 = normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz));
  lowp vec4 tmpvar_11;
  tmpvar_11 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_6 = tmpvar_11.xyz;
  tmpvar_7 = tmpvar_11.w;
  tmpvar_3 = _LightColor0.xyz;
  tmpvar_4 = lightDir_9;
  tmpvar_1 = worldViewDir_8;
  tmpvar_2 = unity_SpecCube0_HDR;
  mediump vec3 Normal_12;
  Normal_12 = xlv_TEXCOORD1;
  mediump float tmpvar_13;
  highp float tmpvar_14;
  highp float smoothness_15;
  smoothness_15 = _Glossiness;
  tmpvar_14 = (1.0 - smoothness_15);
  tmpvar_13 = tmpvar_14;
  mediump vec3 I_16;
  I_16 = -(tmpvar_1);
  mediump vec4 hdr_17;
  hdr_17 = tmpvar_2;
  mediump vec4 tmpvar_18;
  tmpvar_18.xyz = (I_16 - (2.0 * (
    dot (Normal_12, I_16)
   * Normal_12)));
  tmpvar_18.w = ((tmpvar_13 * (1.7 - 
    (0.7 * tmpvar_13)
  )) * 6.0);
  lowp vec4 tmpvar_19;
  tmpvar_19 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_18.xyz, tmpvar_18.w);
  mediump vec4 tmpvar_20;
  tmpvar_20 = tmpvar_19;
  lowp vec3 tmpvar_21;
  mediump vec4 c_22;
  highp vec3 tmpvar_23;
  tmpvar_23 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_24;
  mediump vec3 albedo_25;
  albedo_25 = tmpvar_6;
  mediump vec3 tmpvar_26;
  tmpvar_26 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_25, vec3(_Metallic));
  mediump float tmpvar_27;
  tmpvar_27 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_24 = (albedo_25 * tmpvar_27);
  tmpvar_21 = tmpvar_24;
  mediump vec3 diffColor_28;
  diffColor_28 = tmpvar_21;
  mediump float alpha_29;
  alpha_29 = tmpvar_7;
  tmpvar_21 = diffColor_28;
  mediump vec3 diffColor_30;
  diffColor_30 = tmpvar_21;
  mediump vec3 color_31;
  mediump vec2 rlPow4AndFresnelTerm_32;
  mediump float tmpvar_33;
  highp float tmpvar_34;
  tmpvar_34 = clamp (dot (tmpvar_23, tmpvar_4), 0.0, 1.0);
  tmpvar_33 = tmpvar_34;
  mediump float tmpvar_35;
  highp float tmpvar_36;
  tmpvar_36 = clamp (dot (tmpvar_23, worldViewDir_8), 0.0, 1.0);
  tmpvar_35 = tmpvar_36;
  highp vec2 tmpvar_37;
  tmpvar_37.x = dot ((worldViewDir_8 - (2.0 * 
    (dot (tmpvar_23, worldViewDir_8) * tmpvar_23)
  )), tmpvar_4);
  tmpvar_37.y = (1.0 - tmpvar_35);
  highp vec2 tmpvar_38;
  tmpvar_38 = ((tmpvar_37 * tmpvar_37) * (tmpvar_37 * tmpvar_37));
  rlPow4AndFresnelTerm_32 = tmpvar_38;
  mediump float tmpvar_39;
  tmpvar_39 = rlPow4AndFresnelTerm_32.x;
  mediump float specular_40;
  highp float smoothness_41;
  smoothness_41 = _Glossiness;
  highp vec2 tmpvar_42;
  tmpvar_42.x = tmpvar_39;
  tmpvar_42.y = (1.0 - smoothness_41);
  highp float tmpvar_43;
  tmpvar_43 = (texture2D (unity_NHxRoughness, tmpvar_42).w * 16.0);
  specular_40 = tmpvar_43;
  color_31 = ((diffColor_30 + (specular_40 * tmpvar_26)) * (tmpvar_3 * tmpvar_33));
  color_31 = (color_31 + ((
    (hdr_17.x * ((hdr_17.w * (tmpvar_20.w - 1.0)) + 1.0))
   * tmpvar_20.xyz) * mix (tmpvar_26, vec3(
    clamp ((_Glossiness + (1.0 - tmpvar_27)), 0.0, 1.0)
  ), rlPow4AndFresnelTerm_32.yyy)));
  mediump vec4 tmpvar_44;
  tmpvar_44.w = 1.0;
  tmpvar_44.xyz = color_31;
  c_22.xyz = tmpvar_44.xyz;
  c_22.w = alpha_29;
  c_5.xyz = c_22.xyz;
  c_5.w = 1.0;
  gl_FragData[0] = c_5;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 70 math, 2 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  highp vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_4;
  tmpvar_4[0] = unity_WorldToObject[0].xyz;
  tmpvar_4[1] = unity_WorldToObject[1].xyz;
  tmpvar_4[2] = unity_WorldToObject[2].xyz;
  tmpvar_1.xyz = (unity_ObjectToWorld * _glesVertex).xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_4));
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD6 = tmpvar_2;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
void main ()
{
  mediump vec3 tmpvar_1;
  highp vec4 tmpvar_2;
  mediump vec3 tmpvar_3;
  mediump vec3 tmpvar_4;
  lowp vec4 c_5;
  lowp vec3 tmpvar_6;
  lowp float tmpvar_7;
  highp vec3 worldViewDir_8;
  lowp vec3 lightDir_9;
  mediump vec3 tmpvar_10;
  tmpvar_10 = _WorldSpaceLightPos0.xyz;
  lightDir_9 = tmpvar_10;
  worldViewDir_8 = normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz));
  lowp vec4 tmpvar_11;
  tmpvar_11 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_6 = tmpvar_11.xyz;
  tmpvar_7 = tmpvar_11.w;
  tmpvar_3 = _LightColor0.xyz;
  tmpvar_4 = lightDir_9;
  tmpvar_1 = worldViewDir_8;
  tmpvar_2 = unity_SpecCube0_HDR;
  mediump vec3 Normal_12;
  Normal_12 = xlv_TEXCOORD1;
  mediump float tmpvar_13;
  highp float tmpvar_14;
  highp float smoothness_15;
  smoothness_15 = _Glossiness;
  tmpvar_14 = (1.0 - smoothness_15);
  tmpvar_13 = tmpvar_14;
  mediump vec3 I_16;
  I_16 = -(tmpvar_1);
  mediump vec3 tmpvar_17;
  mediump vec4 hdr_18;
  hdr_18 = tmpvar_2;
  mediump vec4 tmpvar_19;
  tmpvar_19.xyz = (I_16 - (2.0 * (
    dot (Normal_12, I_16)
   * Normal_12)));
  tmpvar_19.w = ((tmpvar_13 * (1.7 - 
    (0.7 * tmpvar_13)
  )) * 6.0);
  lowp vec4 tmpvar_20;
  tmpvar_20 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_19.xyz, tmpvar_19.w);
  mediump vec4 tmpvar_21;
  tmpvar_21 = tmpvar_20;
  tmpvar_17 = ((hdr_18.x * (
    (hdr_18.w * (tmpvar_21.w - 1.0))
   + 1.0)) * tmpvar_21.xyz);
  lowp vec3 tmpvar_22;
  mediump vec4 c_23;
  highp vec3 tmpvar_24;
  tmpvar_24 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_25;
  mediump vec3 albedo_26;
  albedo_26 = tmpvar_6;
  mediump vec3 tmpvar_27;
  tmpvar_27 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_26, vec3(_Metallic));
  mediump float tmpvar_28;
  tmpvar_28 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_25 = (albedo_26 * tmpvar_28);
  tmpvar_22 = tmpvar_25;
  mediump vec3 diffColor_29;
  diffColor_29 = tmpvar_22;
  mediump float alpha_30;
  alpha_30 = tmpvar_7;
  tmpvar_22 = diffColor_29;
  mediump vec3 diffColor_31;
  diffColor_31 = tmpvar_22;
  mediump vec3 color_32;
  mediump float surfaceReduction_33;
  highp float specularTerm_34;
  highp float a2_35;
  mediump float roughness_36;
  mediump float perceptualRoughness_37;
  highp vec3 tmpvar_38;
  highp vec3 inVec_39;
  inVec_39 = (tmpvar_4 + worldViewDir_8);
  tmpvar_38 = (inVec_39 * inversesqrt(max (0.001, 
    dot (inVec_39, inVec_39)
  )));
  mediump float tmpvar_40;
  highp float tmpvar_41;
  tmpvar_41 = clamp (dot (tmpvar_24, tmpvar_4), 0.0, 1.0);
  tmpvar_40 = tmpvar_41;
  highp float tmpvar_42;
  tmpvar_42 = clamp (dot (tmpvar_24, tmpvar_38), 0.0, 1.0);
  mediump float tmpvar_43;
  highp float tmpvar_44;
  tmpvar_44 = clamp (dot (tmpvar_24, worldViewDir_8), 0.0, 1.0);
  tmpvar_43 = tmpvar_44;
  highp float tmpvar_45;
  highp float smoothness_46;
  smoothness_46 = _Glossiness;
  tmpvar_45 = (1.0 - smoothness_46);
  perceptualRoughness_37 = tmpvar_45;
  highp float tmpvar_47;
  highp float perceptualRoughness_48;
  perceptualRoughness_48 = perceptualRoughness_37;
  tmpvar_47 = (perceptualRoughness_48 * perceptualRoughness_48);
  roughness_36 = tmpvar_47;
  mediump float tmpvar_49;
  tmpvar_49 = (roughness_36 * roughness_36);
  a2_35 = tmpvar_49;
  specularTerm_34 = ((roughness_36 / (
    (max (0.32, clamp (dot (tmpvar_4, tmpvar_38), 0.0, 1.0)) * (1.5 + roughness_36))
   * 
    (((tmpvar_42 * tmpvar_42) * (a2_35 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_50;
  tmpvar_50 = clamp (specularTerm_34, 0.0, 100.0);
  specularTerm_34 = tmpvar_50;
  surfaceReduction_33 = (1.0 - ((roughness_36 * perceptualRoughness_37) * 0.28));
  mediump float x_51;
  x_51 = (1.0 - tmpvar_43);
  mediump vec3 tmpvar_52;
  tmpvar_52 = mix (tmpvar_27, vec3(clamp ((_Glossiness + 
    (1.0 - tmpvar_28)
  ), 0.0, 1.0)), vec3(((x_51 * x_51) * (x_51 * x_51))));
  highp vec3 tmpvar_53;
  tmpvar_53 = (((
    (diffColor_31 + (tmpvar_50 * tmpvar_27))
   * tmpvar_3) * tmpvar_40) + ((surfaceReduction_33 * tmpvar_17) * tmpvar_52));
  color_32 = tmpvar_53;
  mediump vec4 tmpvar_54;
  tmpvar_54.w = 1.0;
  tmpvar_54.xyz = color_32;
  c_23.xyz = tmpvar_54.xyz;
  c_23.w = alpha_30;
  c_5.xyz = c_23.xyz;
  c_5.w = 1.0;
  gl_FragData[0] = c_5;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 70 math, 2 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  highp vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_4;
  tmpvar_4[0] = unity_WorldToObject[0].xyz;
  tmpvar_4[1] = unity_WorldToObject[1].xyz;
  tmpvar_4[2] = unity_WorldToObject[2].xyz;
  tmpvar_1.xyz = (unity_ObjectToWorld * _glesVertex).xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_4));
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD6 = tmpvar_2;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
void main ()
{
  mediump vec3 tmpvar_1;
  highp vec4 tmpvar_2;
  mediump vec3 tmpvar_3;
  mediump vec3 tmpvar_4;
  lowp vec4 c_5;
  lowp vec3 tmpvar_6;
  lowp float tmpvar_7;
  highp vec3 worldViewDir_8;
  lowp vec3 lightDir_9;
  mediump vec3 tmpvar_10;
  tmpvar_10 = _WorldSpaceLightPos0.xyz;
  lightDir_9 = tmpvar_10;
  worldViewDir_8 = normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz));
  lowp vec4 tmpvar_11;
  tmpvar_11 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_6 = tmpvar_11.xyz;
  tmpvar_7 = tmpvar_11.w;
  tmpvar_3 = _LightColor0.xyz;
  tmpvar_4 = lightDir_9;
  tmpvar_1 = worldViewDir_8;
  tmpvar_2 = unity_SpecCube0_HDR;
  mediump vec3 Normal_12;
  Normal_12 = xlv_TEXCOORD1;
  mediump float tmpvar_13;
  highp float tmpvar_14;
  highp float smoothness_15;
  smoothness_15 = _Glossiness;
  tmpvar_14 = (1.0 - smoothness_15);
  tmpvar_13 = tmpvar_14;
  mediump vec3 I_16;
  I_16 = -(tmpvar_1);
  mediump vec3 tmpvar_17;
  mediump vec4 hdr_18;
  hdr_18 = tmpvar_2;
  mediump vec4 tmpvar_19;
  tmpvar_19.xyz = (I_16 - (2.0 * (
    dot (Normal_12, I_16)
   * Normal_12)));
  tmpvar_19.w = ((tmpvar_13 * (1.7 - 
    (0.7 * tmpvar_13)
  )) * 6.0);
  lowp vec4 tmpvar_20;
  tmpvar_20 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_19.xyz, tmpvar_19.w);
  mediump vec4 tmpvar_21;
  tmpvar_21 = tmpvar_20;
  tmpvar_17 = ((hdr_18.x * (
    (hdr_18.w * (tmpvar_21.w - 1.0))
   + 1.0)) * tmpvar_21.xyz);
  lowp vec3 tmpvar_22;
  mediump vec4 c_23;
  highp vec3 tmpvar_24;
  tmpvar_24 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_25;
  mediump vec3 albedo_26;
  albedo_26 = tmpvar_6;
  mediump vec3 tmpvar_27;
  tmpvar_27 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_26, vec3(_Metallic));
  mediump float tmpvar_28;
  tmpvar_28 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_25 = (albedo_26 * tmpvar_28);
  tmpvar_22 = tmpvar_25;
  mediump vec3 diffColor_29;
  diffColor_29 = tmpvar_22;
  mediump float alpha_30;
  alpha_30 = tmpvar_7;
  tmpvar_22 = diffColor_29;
  mediump vec3 diffColor_31;
  diffColor_31 = tmpvar_22;
  mediump vec3 color_32;
  mediump float surfaceReduction_33;
  highp float specularTerm_34;
  highp float a2_35;
  mediump float roughness_36;
  mediump float perceptualRoughness_37;
  highp vec3 tmpvar_38;
  highp vec3 inVec_39;
  inVec_39 = (tmpvar_4 + worldViewDir_8);
  tmpvar_38 = (inVec_39 * inversesqrt(max (0.001, 
    dot (inVec_39, inVec_39)
  )));
  mediump float tmpvar_40;
  highp float tmpvar_41;
  tmpvar_41 = clamp (dot (tmpvar_24, tmpvar_4), 0.0, 1.0);
  tmpvar_40 = tmpvar_41;
  highp float tmpvar_42;
  tmpvar_42 = clamp (dot (tmpvar_24, tmpvar_38), 0.0, 1.0);
  mediump float tmpvar_43;
  highp float tmpvar_44;
  tmpvar_44 = clamp (dot (tmpvar_24, worldViewDir_8), 0.0, 1.0);
  tmpvar_43 = tmpvar_44;
  highp float tmpvar_45;
  highp float smoothness_46;
  smoothness_46 = _Glossiness;
  tmpvar_45 = (1.0 - smoothness_46);
  perceptualRoughness_37 = tmpvar_45;
  highp float tmpvar_47;
  highp float perceptualRoughness_48;
  perceptualRoughness_48 = perceptualRoughness_37;
  tmpvar_47 = (perceptualRoughness_48 * perceptualRoughness_48);
  roughness_36 = tmpvar_47;
  mediump float tmpvar_49;
  tmpvar_49 = (roughness_36 * roughness_36);
  a2_35 = tmpvar_49;
  specularTerm_34 = ((roughness_36 / (
    (max (0.32, clamp (dot (tmpvar_4, tmpvar_38), 0.0, 1.0)) * (1.5 + roughness_36))
   * 
    (((tmpvar_42 * tmpvar_42) * (a2_35 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_50;
  tmpvar_50 = clamp (specularTerm_34, 0.0, 100.0);
  specularTerm_34 = tmpvar_50;
  surfaceReduction_33 = (1.0 - ((roughness_36 * perceptualRoughness_37) * 0.28));
  mediump float x_51;
  x_51 = (1.0 - tmpvar_43);
  mediump vec3 tmpvar_52;
  tmpvar_52 = mix (tmpvar_27, vec3(clamp ((_Glossiness + 
    (1.0 - tmpvar_28)
  ), 0.0, 1.0)), vec3(((x_51 * x_51) * (x_51 * x_51))));
  highp vec3 tmpvar_53;
  tmpvar_53 = (((
    (diffColor_31 + (tmpvar_50 * tmpvar_27))
   * tmpvar_3) * tmpvar_40) + ((surfaceReduction_33 * tmpvar_17) * tmpvar_52));
  color_32 = tmpvar_53;
  mediump vec4 tmpvar_54;
  tmpvar_54.w = 1.0;
  tmpvar_54.xyz = color_32;
  c_23.xyz = tmpvar_54.xyz;
  c_23.w = alpha_30;
  c_5.xyz = c_23.xyz;
  c_5.w = 1.0;
  gl_FragData[0] = c_5;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp samplerCube unity_SpecCube0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
mediump vec3 u_xlat16_1;
lowp vec4 u_xlat10_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_2;
lowp vec3 u_xlat10_2;
mediump vec3 u_xlat16_3;
vec3 u_xlat4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
mediump float u_xlat16_8;
float u_xlat10;
float u_xlat24;
mediump float u_xlat16_25;
mediump float u_xlat16_27;
mediump float u_xlat16_29;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat24 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat24 = inversesqrt(u_xlat24);
    u_xlat0.xyz = vec3(u_xlat24) * u_xlat0.xyz;
    u_xlat16_1.x = dot((-u_xlat0.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_1.x = u_xlat16_1.x + u_xlat16_1.x;
    u_xlat16_1.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_1.xxx) + (-u_xlat0.xyz);
    u_xlat2.z = (-_Glossiness) + 1.0;
    u_xlat16_25 = (-u_xlat2.z) * 0.699999988 + 1.70000005;
    u_xlat16_25 = u_xlat16_25 * u_xlat2.z;
    u_xlat16_25 = u_xlat16_25 * 6.0;
    u_xlat10_1 = textureLod(unity_SpecCube0, u_xlat16_1.xyz, u_xlat16_25);
    u_xlat16_3.x = u_xlat10_1.w + -1.0;
    u_xlat16_3.x = unity_SpecCube0_HDR.w * u_xlat16_3.x + 1.0;
    u_xlat16_3.x = u_xlat16_3.x * unity_SpecCube0_HDR.x;
    u_xlat16_3.xyz = u_xlat10_1.xyz * u_xlat16_3.xxx;
    u_xlat24 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat24 = inversesqrt(u_xlat24);
    u_xlat4.xyz = vec3(u_xlat24) * vs_TEXCOORD1.xyz;
    u_xlat24 = dot(u_xlat0.xyz, u_xlat4.xyz);
    u_xlat10 = u_xlat24;
#ifdef UNITY_ADRENO_ES3
    u_xlat10 = min(max(u_xlat10, 0.0), 1.0);
#else
    u_xlat10 = clamp(u_xlat10, 0.0, 1.0);
#endif
    u_xlat24 = u_xlat24 + u_xlat24;
    u_xlat0.xyz = u_xlat4.xyz * (-vec3(u_xlat24)) + u_xlat0.xyz;
    u_xlat24 = dot(u_xlat4.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat24 = min(max(u_xlat24, 0.0), 1.0);
#else
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
#endif
    u_xlat16_5.xyz = vec3(u_xlat24) * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat0.xyz, _WorldSpaceLightPos0.xyz);
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat2.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = texture(unity_NHxRoughness, u_xlat2.xz).w;
    u_xlat0.x = u_xlat0.x * 16.0;
    u_xlat16_27 = (-u_xlat10) + 1.0;
    u_xlat16_8 = u_xlat16_27 * u_xlat16_27;
    u_xlat16_8 = u_xlat16_27 * u_xlat16_8;
    u_xlat16_8 = u_xlat16_27 * u_xlat16_8;
    u_xlat16_27 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_29 = (-u_xlat16_27) + _Glossiness;
    u_xlat16_29 = u_xlat16_29 + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_29 = min(max(u_xlat16_29, 0.0), 1.0);
#else
    u_xlat16_29 = clamp(u_xlat16_29, 0.0, 1.0);
#endif
    u_xlat10_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_6.xyz = u_xlat10_2.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_2.xyz = u_xlat10_2.xyz * _Color.xyz;
    u_xlat16_6.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_6.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_7.xyz = vec3(u_xlat16_29) + (-u_xlat16_6.xyz);
    u_xlat16_7.xyz = vec3(u_xlat16_8) * u_xlat16_7.xyz + u_xlat16_6.xyz;
    u_xlat16_6.xyz = u_xlat0.xxx * u_xlat16_6.xyz;
    u_xlat16_6.xyz = u_xlat16_2.xyz * vec3(u_xlat16_27) + u_xlat16_6.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * u_xlat16_7.xyz;
    SV_Target0.xyz = u_xlat16_6.xyz * u_xlat16_5.xyz + u_xlat16_3.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform lowp samplerCube unity_SpecCube0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
lowp vec4 u_xlat10_2;
mediump vec3 u_xlat16_3;
vec3 u_xlat4;
mediump float u_xlat16_5;
mediump vec3 u_xlat16_6;
float u_xlat7;
mediump vec3 u_xlat16_12;
float u_xlat14;
float u_xlat21;
mediump float u_xlat16_21;
mediump float u_xlat16_22;
mediump float u_xlat16_23;
mediump float u_xlat16_24;
float u_xlat25;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat21 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat1.xyz = vec3(u_xlat21) * u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat21) + _WorldSpaceLightPos0.xyz;
    u_xlat16_2.x = dot((-u_xlat1.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_2.x = u_xlat16_2.x + u_xlat16_2.x;
    u_xlat16_2.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_2.xxx) + (-u_xlat1.xyz);
    u_xlat16_21 = (-_Glossiness) + 1.0;
    u_xlat16_23 = (-u_xlat16_21) * 0.699999988 + 1.70000005;
    u_xlat16_23 = u_xlat16_21 * u_xlat16_23;
    u_xlat16_23 = u_xlat16_23 * 6.0;
    u_xlat10_2 = textureLod(unity_SpecCube0, u_xlat16_2.xyz, u_xlat16_23);
    u_xlat16_3.x = u_xlat10_2.w + -1.0;
    u_xlat16_3.x = unity_SpecCube0_HDR.w * u_xlat16_3.x + 1.0;
    u_xlat16_3.x = u_xlat16_3.x * unity_SpecCube0_HDR.x;
    u_xlat16_3.xyz = u_xlat10_2.xyz * u_xlat16_3.xxx;
    u_xlat16_22 = u_xlat16_21 * u_xlat16_21;
    u_xlat16_24 = u_xlat16_21 * u_xlat16_22;
    u_xlat16_21 = u_xlat16_21 * u_xlat16_21 + 1.5;
    u_xlat16_24 = (-u_xlat16_24) * 0.280000001 + 1.0;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vec3(u_xlat16_24);
    u_xlat4.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat4.x = inversesqrt(u_xlat4.x);
    u_xlat4.xyz = u_xlat4.xxx * vs_TEXCOORD1.xyz;
    u_xlat1.x = dot(u_xlat4.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat1.x = min(max(u_xlat1.x, 0.0), 1.0);
#else
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
#endif
    u_xlat16_24 = (-u_xlat1.x) + 1.0;
    u_xlat16_24 = u_xlat16_24 * u_xlat16_24;
    u_xlat16_24 = u_xlat16_24 * u_xlat16_24;
    u_xlat16_5 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_12.x = (-u_xlat16_5) + _Glossiness;
    u_xlat16_12.x = u_xlat16_12.x + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_12.x = min(max(u_xlat16_12.x, 0.0), 1.0);
#else
    u_xlat16_12.x = clamp(u_xlat16_12.x, 0.0, 1.0);
#endif
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_6.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_6.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_6.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_12.xyz = u_xlat16_12.xxx + (-u_xlat16_6.xyz);
    u_xlat16_12.xyz = vec3(u_xlat16_24) * u_xlat16_12.xyz + u_xlat16_6.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * u_xlat16_12.xyz;
    u_xlat25 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat25 = max(u_xlat25, 0.00100000005);
    u_xlat25 = inversesqrt(u_xlat25);
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat25);
    u_xlat25 = dot(_WorldSpaceLightPos0.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat25 = min(max(u_xlat25, 0.0), 1.0);
#else
    u_xlat25 = clamp(u_xlat25, 0.0, 1.0);
#endif
    u_xlat0.x = dot(u_xlat4.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat7 = dot(u_xlat4.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat7 = min(max(u_xlat7, 0.0), 1.0);
#else
    u_xlat7 = clamp(u_xlat7, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat14 = max(u_xlat25, 0.319999993);
    u_xlat14 = u_xlat16_21 * u_xlat14;
    u_xlat16_21 = u_xlat16_22 * u_xlat16_22 + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_21 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat14;
    u_xlat0.x = u_xlat16_22 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat0.xzw = u_xlat16_6.xyz * u_xlat0.xxx;
    u_xlat0.xzw = u_xlat16_1.xyz * vec3(u_xlat16_5) + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat0.xzw * _LightColor0.xyz;
    u_xlat0.xyz = u_xlat0.xzw * vec3(u_xlat7) + u_xlat16_3.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform lowp samplerCube unity_SpecCube0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
lowp vec4 u_xlat10_2;
mediump vec3 u_xlat16_3;
vec3 u_xlat4;
mediump float u_xlat16_5;
mediump vec3 u_xlat16_6;
float u_xlat7;
mediump vec3 u_xlat16_12;
float u_xlat14;
float u_xlat21;
mediump float u_xlat16_21;
mediump float u_xlat16_22;
mediump float u_xlat16_23;
mediump float u_xlat16_24;
float u_xlat25;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat21 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat1.xyz = vec3(u_xlat21) * u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat21) + _WorldSpaceLightPos0.xyz;
    u_xlat16_2.x = dot((-u_xlat1.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_2.x = u_xlat16_2.x + u_xlat16_2.x;
    u_xlat16_2.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_2.xxx) + (-u_xlat1.xyz);
    u_xlat16_21 = (-_Glossiness) + 1.0;
    u_xlat16_23 = (-u_xlat16_21) * 0.699999988 + 1.70000005;
    u_xlat16_23 = u_xlat16_21 * u_xlat16_23;
    u_xlat16_23 = u_xlat16_23 * 6.0;
    u_xlat10_2 = textureLod(unity_SpecCube0, u_xlat16_2.xyz, u_xlat16_23);
    u_xlat16_3.x = u_xlat10_2.w + -1.0;
    u_xlat16_3.x = unity_SpecCube0_HDR.w * u_xlat16_3.x + 1.0;
    u_xlat16_3.x = u_xlat16_3.x * unity_SpecCube0_HDR.x;
    u_xlat16_3.xyz = u_xlat10_2.xyz * u_xlat16_3.xxx;
    u_xlat16_22 = u_xlat16_21 * u_xlat16_21;
    u_xlat16_24 = u_xlat16_21 * u_xlat16_22;
    u_xlat16_21 = u_xlat16_21 * u_xlat16_21 + 1.5;
    u_xlat16_24 = (-u_xlat16_24) * 0.280000001 + 1.0;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vec3(u_xlat16_24);
    u_xlat4.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat4.x = inversesqrt(u_xlat4.x);
    u_xlat4.xyz = u_xlat4.xxx * vs_TEXCOORD1.xyz;
    u_xlat1.x = dot(u_xlat4.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat1.x = min(max(u_xlat1.x, 0.0), 1.0);
#else
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
#endif
    u_xlat16_24 = (-u_xlat1.x) + 1.0;
    u_xlat16_24 = u_xlat16_24 * u_xlat16_24;
    u_xlat16_24 = u_xlat16_24 * u_xlat16_24;
    u_xlat16_5 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_12.x = (-u_xlat16_5) + _Glossiness;
    u_xlat16_12.x = u_xlat16_12.x + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_12.x = min(max(u_xlat16_12.x, 0.0), 1.0);
#else
    u_xlat16_12.x = clamp(u_xlat16_12.x, 0.0, 1.0);
#endif
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_6.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_6.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_6.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_12.xyz = u_xlat16_12.xxx + (-u_xlat16_6.xyz);
    u_xlat16_12.xyz = vec3(u_xlat16_24) * u_xlat16_12.xyz + u_xlat16_6.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * u_xlat16_12.xyz;
    u_xlat25 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat25 = max(u_xlat25, 0.00100000005);
    u_xlat25 = inversesqrt(u_xlat25);
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat25);
    u_xlat25 = dot(_WorldSpaceLightPos0.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat25 = min(max(u_xlat25, 0.0), 1.0);
#else
    u_xlat25 = clamp(u_xlat25, 0.0, 1.0);
#endif
    u_xlat0.x = dot(u_xlat4.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat7 = dot(u_xlat4.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat7 = min(max(u_xlat7, 0.0), 1.0);
#else
    u_xlat7 = clamp(u_xlat7, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat14 = max(u_xlat25, 0.319999993);
    u_xlat14 = u_xlat16_21 * u_xlat14;
    u_xlat16_21 = u_xlat16_22 * u_xlat16_22 + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_21 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat14;
    u_xlat0.x = u_xlat16_22 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat0.xzw = u_xlat16_6.xyz * u_xlat0.xxx;
    u_xlat0.xzw = u_xlat16_1.xyz * vec3(u_xlat16_5) + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat0.xzw * _LightColor0.xyz;
    u_xlat0.xyz = u_xlat0.xzw * vec3(u_xlat7) + u_xlat16_3.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

//////////////////////////////////////////////////////
Keywords set in this variant: DIRECTIONAL LIGHTPROBE_SH 
-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 62 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform mediump vec4 unity_SHBr;
uniform mediump vec4 unity_SHBg;
uniform mediump vec4 unity_SHBb;
uniform mediump vec4 unity_SHC;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  highp vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_4;
  tmpvar_4[0] = unity_WorldToObject[0].xyz;
  tmpvar_4[1] = unity_WorldToObject[1].xyz;
  tmpvar_4[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_5;
  tmpvar_5 = normalize((_glesNormal * tmpvar_4));
  tmpvar_1.xyz = (unity_ObjectToWorld * _glesVertex).xyz;
  mediump vec3 normal_6;
  normal_6 = tmpvar_5;
  mediump vec3 x1_7;
  mediump vec4 tmpvar_8;
  tmpvar_8 = (normal_6.xyzz * normal_6.yzzx);
  x1_7.x = dot (unity_SHBr, tmpvar_8);
  x1_7.y = dot (unity_SHBg, tmpvar_8);
  x1_7.z = dot (unity_SHBb, tmpvar_8);
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = tmpvar_5;
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD3 = (x1_7 + (unity_SHC.xyz * (
    (normal_6.x * normal_6.x)
   - 
    (normal_6.y * normal_6.y)
  )));
  xlv_TEXCOORD6 = tmpvar_2;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 unity_SHAr;
uniform mediump vec4 unity_SHAg;
uniform mediump vec4 unity_SHAb;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  highp vec4 tmpvar_2;
  mediump vec3 tmpvar_3;
  mediump vec3 tmpvar_4;
  lowp vec4 c_5;
  lowp vec3 tmpvar_6;
  lowp float tmpvar_7;
  highp vec3 worldViewDir_8;
  lowp vec3 lightDir_9;
  mediump vec3 tmpvar_10;
  tmpvar_10 = _WorldSpaceLightPos0.xyz;
  lightDir_9 = tmpvar_10;
  worldViewDir_8 = normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz));
  lowp vec4 tmpvar_11;
  tmpvar_11 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_6 = tmpvar_11.xyz;
  tmpvar_7 = tmpvar_11.w;
  tmpvar_3 = _LightColor0.xyz;
  tmpvar_4 = lightDir_9;
  tmpvar_1 = worldViewDir_8;
  tmpvar_2 = unity_SpecCube0_HDR;
  mediump vec3 Normal_12;
  Normal_12 = xlv_TEXCOORD1;
  mediump float tmpvar_13;
  highp float tmpvar_14;
  highp float smoothness_15;
  smoothness_15 = _Glossiness;
  tmpvar_14 = (1.0 - smoothness_15);
  tmpvar_13 = tmpvar_14;
  mediump vec3 I_16;
  I_16 = -(tmpvar_1);
  mediump vec3 normalWorld_17;
  normalWorld_17 = xlv_TEXCOORD1;
  mediump vec4 tmpvar_18;
  tmpvar_18.w = 1.0;
  tmpvar_18.xyz = normalWorld_17;
  mediump vec3 x_19;
  x_19.x = dot (unity_SHAr, tmpvar_18);
  x_19.y = dot (unity_SHAg, tmpvar_18);
  x_19.z = dot (unity_SHAb, tmpvar_18);
  mediump vec4 hdr_20;
  hdr_20 = tmpvar_2;
  mediump vec4 tmpvar_21;
  tmpvar_21.xyz = (I_16 - (2.0 * (
    dot (Normal_12, I_16)
   * Normal_12)));
  tmpvar_21.w = ((tmpvar_13 * (1.7 - 
    (0.7 * tmpvar_13)
  )) * 6.0);
  lowp vec4 tmpvar_22;
  tmpvar_22 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_21.xyz, tmpvar_21.w);
  mediump vec4 tmpvar_23;
  tmpvar_23 = tmpvar_22;
  lowp vec3 tmpvar_24;
  mediump vec4 c_25;
  highp vec3 tmpvar_26;
  tmpvar_26 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_27;
  mediump vec3 albedo_28;
  albedo_28 = tmpvar_6;
  mediump vec3 tmpvar_29;
  tmpvar_29 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_28, vec3(_Metallic));
  mediump float tmpvar_30;
  tmpvar_30 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_27 = (albedo_28 * tmpvar_30);
  tmpvar_24 = tmpvar_27;
  mediump vec3 diffColor_31;
  diffColor_31 = tmpvar_24;
  mediump float alpha_32;
  alpha_32 = tmpvar_7;
  tmpvar_24 = diffColor_31;
  mediump vec3 diffColor_33;
  diffColor_33 = tmpvar_24;
  mediump vec3 color_34;
  mediump vec2 rlPow4AndFresnelTerm_35;
  mediump float tmpvar_36;
  highp float tmpvar_37;
  tmpvar_37 = clamp (dot (tmpvar_26, tmpvar_4), 0.0, 1.0);
  tmpvar_36 = tmpvar_37;
  mediump float tmpvar_38;
  highp float tmpvar_39;
  tmpvar_39 = clamp (dot (tmpvar_26, worldViewDir_8), 0.0, 1.0);
  tmpvar_38 = tmpvar_39;
  highp vec2 tmpvar_40;
  tmpvar_40.x = dot ((worldViewDir_8 - (2.0 * 
    (dot (tmpvar_26, worldViewDir_8) * tmpvar_26)
  )), tmpvar_4);
  tmpvar_40.y = (1.0 - tmpvar_38);
  highp vec2 tmpvar_41;
  tmpvar_41 = ((tmpvar_40 * tmpvar_40) * (tmpvar_40 * tmpvar_40));
  rlPow4AndFresnelTerm_35 = tmpvar_41;
  mediump float tmpvar_42;
  tmpvar_42 = rlPow4AndFresnelTerm_35.x;
  mediump float specular_43;
  highp float smoothness_44;
  smoothness_44 = _Glossiness;
  highp vec2 tmpvar_45;
  tmpvar_45.x = tmpvar_42;
  tmpvar_45.y = (1.0 - smoothness_44);
  highp float tmpvar_46;
  tmpvar_46 = (texture2D (unity_NHxRoughness, tmpvar_45).w * 16.0);
  specular_43 = tmpvar_46;
  color_34 = ((diffColor_33 + (specular_43 * tmpvar_29)) * (tmpvar_3 * tmpvar_36));
  color_34 = (color_34 + ((
    max (((1.055 * pow (
      max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD3 + x_19))
    , vec3(0.4166667, 0.4166667, 0.4166667))) - 0.055), vec3(0.0, 0.0, 0.0))
   * diffColor_33) + (
    ((hdr_20.x * ((hdr_20.w * 
      (tmpvar_23.w - 1.0)
    ) + 1.0)) * tmpvar_23.xyz)
   * 
    mix (tmpvar_29, vec3(clamp ((_Glossiness + (1.0 - tmpvar_30)), 0.0, 1.0)), rlPow4AndFresnelTerm_35.yyy)
  )));
  mediump vec4 tmpvar_47;
  tmpvar_47.w = 1.0;
  tmpvar_47.xyz = color_34;
  c_25.xyz = tmpvar_47.xyz;
  c_25.w = alpha_32;
  c_5.xyz = c_25.xyz;
  c_5.w = 1.0;
  gl_FragData[0] = c_5;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 82 math, 2 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform mediump vec4 unity_SHBr;
uniform mediump vec4 unity_SHBg;
uniform mediump vec4 unity_SHBb;
uniform mediump vec4 unity_SHC;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  highp vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_4;
  tmpvar_4[0] = unity_WorldToObject[0].xyz;
  tmpvar_4[1] = unity_WorldToObject[1].xyz;
  tmpvar_4[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_5;
  tmpvar_5 = normalize((_glesNormal * tmpvar_4));
  tmpvar_1.xyz = (unity_ObjectToWorld * _glesVertex).xyz;
  mediump vec3 normal_6;
  normal_6 = tmpvar_5;
  mediump vec3 x1_7;
  mediump vec4 tmpvar_8;
  tmpvar_8 = (normal_6.xyzz * normal_6.yzzx);
  x1_7.x = dot (unity_SHBr, tmpvar_8);
  x1_7.y = dot (unity_SHBg, tmpvar_8);
  x1_7.z = dot (unity_SHBb, tmpvar_8);
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = tmpvar_5;
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD3 = (x1_7 + (unity_SHC.xyz * (
    (normal_6.x * normal_6.x)
   - 
    (normal_6.y * normal_6.y)
  )));
  xlv_TEXCOORD6 = tmpvar_2;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 unity_SHAr;
uniform mediump vec4 unity_SHAg;
uniform mediump vec4 unity_SHAb;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  highp vec4 tmpvar_2;
  mediump vec3 tmpvar_3;
  mediump vec3 tmpvar_4;
  lowp vec4 c_5;
  lowp vec3 tmpvar_6;
  lowp float tmpvar_7;
  highp vec3 worldViewDir_8;
  lowp vec3 lightDir_9;
  mediump vec3 tmpvar_10;
  tmpvar_10 = _WorldSpaceLightPos0.xyz;
  lightDir_9 = tmpvar_10;
  worldViewDir_8 = normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz));
  lowp vec4 tmpvar_11;
  tmpvar_11 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_6 = tmpvar_11.xyz;
  tmpvar_7 = tmpvar_11.w;
  tmpvar_3 = _LightColor0.xyz;
  tmpvar_4 = lightDir_9;
  tmpvar_1 = worldViewDir_8;
  tmpvar_2 = unity_SpecCube0_HDR;
  mediump vec3 Normal_12;
  Normal_12 = xlv_TEXCOORD1;
  mediump float tmpvar_13;
  highp float tmpvar_14;
  highp float smoothness_15;
  smoothness_15 = _Glossiness;
  tmpvar_14 = (1.0 - smoothness_15);
  tmpvar_13 = tmpvar_14;
  mediump vec3 I_16;
  I_16 = -(tmpvar_1);
  mediump vec3 normalWorld_17;
  normalWorld_17 = xlv_TEXCOORD1;
  mediump vec3 tmpvar_18;
  mediump vec3 tmpvar_19;
  mediump vec4 tmpvar_20;
  tmpvar_20.w = 1.0;
  tmpvar_20.xyz = normalWorld_17;
  mediump vec3 x_21;
  x_21.x = dot (unity_SHAr, tmpvar_20);
  x_21.y = dot (unity_SHAg, tmpvar_20);
  x_21.z = dot (unity_SHAb, tmpvar_20);
  tmpvar_19 = max (((1.055 * 
    pow (max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD3 + x_21)), vec3(0.4166667, 0.4166667, 0.4166667))
  ) - 0.055), vec3(0.0, 0.0, 0.0));
  mediump vec4 hdr_22;
  hdr_22 = tmpvar_2;
  mediump vec4 tmpvar_23;
  tmpvar_23.xyz = (I_16 - (2.0 * (
    dot (Normal_12, I_16)
   * Normal_12)));
  tmpvar_23.w = ((tmpvar_13 * (1.7 - 
    (0.7 * tmpvar_13)
  )) * 6.0);
  lowp vec4 tmpvar_24;
  tmpvar_24 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_23.xyz, tmpvar_23.w);
  mediump vec4 tmpvar_25;
  tmpvar_25 = tmpvar_24;
  tmpvar_18 = ((hdr_22.x * (
    (hdr_22.w * (tmpvar_25.w - 1.0))
   + 1.0)) * tmpvar_25.xyz);
  lowp vec3 tmpvar_26;
  mediump vec4 c_27;
  highp vec3 tmpvar_28;
  tmpvar_28 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_29;
  mediump vec3 albedo_30;
  albedo_30 = tmpvar_6;
  mediump vec3 tmpvar_31;
  tmpvar_31 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_30, vec3(_Metallic));
  mediump float tmpvar_32;
  tmpvar_32 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_29 = (albedo_30 * tmpvar_32);
  tmpvar_26 = tmpvar_29;
  mediump vec3 diffColor_33;
  diffColor_33 = tmpvar_26;
  mediump float alpha_34;
  alpha_34 = tmpvar_7;
  tmpvar_26 = diffColor_33;
  mediump vec3 diffColor_35;
  diffColor_35 = tmpvar_26;
  mediump vec3 color_36;
  mediump float surfaceReduction_37;
  highp float specularTerm_38;
  highp float a2_39;
  mediump float roughness_40;
  mediump float perceptualRoughness_41;
  highp vec3 tmpvar_42;
  highp vec3 inVec_43;
  inVec_43 = (tmpvar_4 + worldViewDir_8);
  tmpvar_42 = (inVec_43 * inversesqrt(max (0.001, 
    dot (inVec_43, inVec_43)
  )));
  mediump float tmpvar_44;
  highp float tmpvar_45;
  tmpvar_45 = clamp (dot (tmpvar_28, tmpvar_4), 0.0, 1.0);
  tmpvar_44 = tmpvar_45;
  highp float tmpvar_46;
  tmpvar_46 = clamp (dot (tmpvar_28, tmpvar_42), 0.0, 1.0);
  mediump float tmpvar_47;
  highp float tmpvar_48;
  tmpvar_48 = clamp (dot (tmpvar_28, worldViewDir_8), 0.0, 1.0);
  tmpvar_47 = tmpvar_48;
  highp float tmpvar_49;
  highp float smoothness_50;
  smoothness_50 = _Glossiness;
  tmpvar_49 = (1.0 - smoothness_50);
  perceptualRoughness_41 = tmpvar_49;
  highp float tmpvar_51;
  highp float perceptualRoughness_52;
  perceptualRoughness_52 = perceptualRoughness_41;
  tmpvar_51 = (perceptualRoughness_52 * perceptualRoughness_52);
  roughness_40 = tmpvar_51;
  mediump float tmpvar_53;
  tmpvar_53 = (roughness_40 * roughness_40);
  a2_39 = tmpvar_53;
  specularTerm_38 = ((roughness_40 / (
    (max (0.32, clamp (dot (tmpvar_4, tmpvar_42), 0.0, 1.0)) * (1.5 + roughness_40))
   * 
    (((tmpvar_46 * tmpvar_46) * (a2_39 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_54;
  tmpvar_54 = clamp (specularTerm_38, 0.0, 100.0);
  specularTerm_38 = tmpvar_54;
  surfaceReduction_37 = (1.0 - ((roughness_40 * perceptualRoughness_41) * 0.28));
  mediump float x_55;
  x_55 = (1.0 - tmpvar_47);
  mediump vec3 tmpvar_56;
  tmpvar_56 = mix (tmpvar_31, vec3(clamp ((_Glossiness + 
    (1.0 - tmpvar_32)
  ), 0.0, 1.0)), vec3(((x_55 * x_55) * (x_55 * x_55))));
  highp vec3 tmpvar_57;
  tmpvar_57 = (((
    ((diffColor_35 + (tmpvar_54 * tmpvar_31)) * tmpvar_3)
   * tmpvar_44) + (tmpvar_19 * diffColor_35)) + ((surfaceReduction_37 * tmpvar_18) * tmpvar_56));
  color_36 = tmpvar_57;
  mediump vec4 tmpvar_58;
  tmpvar_58.w = 1.0;
  tmpvar_58.xyz = color_36;
  c_27.xyz = tmpvar_58.xyz;
  c_27.w = alpha_34;
  c_5.xyz = c_27.xyz;
  c_5.w = 1.0;
  gl_FragData[0] = c_5;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 82 math, 2 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform mediump vec4 unity_SHBr;
uniform mediump vec4 unity_SHBg;
uniform mediump vec4 unity_SHBb;
uniform mediump vec4 unity_SHC;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  highp vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_4;
  tmpvar_4[0] = unity_WorldToObject[0].xyz;
  tmpvar_4[1] = unity_WorldToObject[1].xyz;
  tmpvar_4[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_5;
  tmpvar_5 = normalize((_glesNormal * tmpvar_4));
  tmpvar_1.xyz = (unity_ObjectToWorld * _glesVertex).xyz;
  mediump vec3 normal_6;
  normal_6 = tmpvar_5;
  mediump vec3 x1_7;
  mediump vec4 tmpvar_8;
  tmpvar_8 = (normal_6.xyzz * normal_6.yzzx);
  x1_7.x = dot (unity_SHBr, tmpvar_8);
  x1_7.y = dot (unity_SHBg, tmpvar_8);
  x1_7.z = dot (unity_SHBb, tmpvar_8);
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = tmpvar_5;
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD3 = (x1_7 + (unity_SHC.xyz * (
    (normal_6.x * normal_6.x)
   - 
    (normal_6.y * normal_6.y)
  )));
  xlv_TEXCOORD6 = tmpvar_2;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 unity_SHAr;
uniform mediump vec4 unity_SHAg;
uniform mediump vec4 unity_SHAb;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  highp vec4 tmpvar_2;
  mediump vec3 tmpvar_3;
  mediump vec3 tmpvar_4;
  lowp vec4 c_5;
  lowp vec3 tmpvar_6;
  lowp float tmpvar_7;
  highp vec3 worldViewDir_8;
  lowp vec3 lightDir_9;
  mediump vec3 tmpvar_10;
  tmpvar_10 = _WorldSpaceLightPos0.xyz;
  lightDir_9 = tmpvar_10;
  worldViewDir_8 = normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz));
  lowp vec4 tmpvar_11;
  tmpvar_11 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_6 = tmpvar_11.xyz;
  tmpvar_7 = tmpvar_11.w;
  tmpvar_3 = _LightColor0.xyz;
  tmpvar_4 = lightDir_9;
  tmpvar_1 = worldViewDir_8;
  tmpvar_2 = unity_SpecCube0_HDR;
  mediump vec3 Normal_12;
  Normal_12 = xlv_TEXCOORD1;
  mediump float tmpvar_13;
  highp float tmpvar_14;
  highp float smoothness_15;
  smoothness_15 = _Glossiness;
  tmpvar_14 = (1.0 - smoothness_15);
  tmpvar_13 = tmpvar_14;
  mediump vec3 I_16;
  I_16 = -(tmpvar_1);
  mediump vec3 normalWorld_17;
  normalWorld_17 = xlv_TEXCOORD1;
  mediump vec3 tmpvar_18;
  mediump vec3 tmpvar_19;
  mediump vec4 tmpvar_20;
  tmpvar_20.w = 1.0;
  tmpvar_20.xyz = normalWorld_17;
  mediump vec3 x_21;
  x_21.x = dot (unity_SHAr, tmpvar_20);
  x_21.y = dot (unity_SHAg, tmpvar_20);
  x_21.z = dot (unity_SHAb, tmpvar_20);
  tmpvar_19 = max (((1.055 * 
    pow (max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD3 + x_21)), vec3(0.4166667, 0.4166667, 0.4166667))
  ) - 0.055), vec3(0.0, 0.0, 0.0));
  mediump vec4 hdr_22;
  hdr_22 = tmpvar_2;
  mediump vec4 tmpvar_23;
  tmpvar_23.xyz = (I_16 - (2.0 * (
    dot (Normal_12, I_16)
   * Normal_12)));
  tmpvar_23.w = ((tmpvar_13 * (1.7 - 
    (0.7 * tmpvar_13)
  )) * 6.0);
  lowp vec4 tmpvar_24;
  tmpvar_24 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_23.xyz, tmpvar_23.w);
  mediump vec4 tmpvar_25;
  tmpvar_25 = tmpvar_24;
  tmpvar_18 = ((hdr_22.x * (
    (hdr_22.w * (tmpvar_25.w - 1.0))
   + 1.0)) * tmpvar_25.xyz);
  lowp vec3 tmpvar_26;
  mediump vec4 c_27;
  highp vec3 tmpvar_28;
  tmpvar_28 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_29;
  mediump vec3 albedo_30;
  albedo_30 = tmpvar_6;
  mediump vec3 tmpvar_31;
  tmpvar_31 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_30, vec3(_Metallic));
  mediump float tmpvar_32;
  tmpvar_32 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_29 = (albedo_30 * tmpvar_32);
  tmpvar_26 = tmpvar_29;
  mediump vec3 diffColor_33;
  diffColor_33 = tmpvar_26;
  mediump float alpha_34;
  alpha_34 = tmpvar_7;
  tmpvar_26 = diffColor_33;
  mediump vec3 diffColor_35;
  diffColor_35 = tmpvar_26;
  mediump vec3 color_36;
  mediump float surfaceReduction_37;
  highp float specularTerm_38;
  highp float a2_39;
  mediump float roughness_40;
  mediump float perceptualRoughness_41;
  highp vec3 tmpvar_42;
  highp vec3 inVec_43;
  inVec_43 = (tmpvar_4 + worldViewDir_8);
  tmpvar_42 = (inVec_43 * inversesqrt(max (0.001, 
    dot (inVec_43, inVec_43)
  )));
  mediump float tmpvar_44;
  highp float tmpvar_45;
  tmpvar_45 = clamp (dot (tmpvar_28, tmpvar_4), 0.0, 1.0);
  tmpvar_44 = tmpvar_45;
  highp float tmpvar_46;
  tmpvar_46 = clamp (dot (tmpvar_28, tmpvar_42), 0.0, 1.0);
  mediump float tmpvar_47;
  highp float tmpvar_48;
  tmpvar_48 = clamp (dot (tmpvar_28, worldViewDir_8), 0.0, 1.0);
  tmpvar_47 = tmpvar_48;
  highp float tmpvar_49;
  highp float smoothness_50;
  smoothness_50 = _Glossiness;
  tmpvar_49 = (1.0 - smoothness_50);
  perceptualRoughness_41 = tmpvar_49;
  highp float tmpvar_51;
  highp float perceptualRoughness_52;
  perceptualRoughness_52 = perceptualRoughness_41;
  tmpvar_51 = (perceptualRoughness_52 * perceptualRoughness_52);
  roughness_40 = tmpvar_51;
  mediump float tmpvar_53;
  tmpvar_53 = (roughness_40 * roughness_40);
  a2_39 = tmpvar_53;
  specularTerm_38 = ((roughness_40 / (
    (max (0.32, clamp (dot (tmpvar_4, tmpvar_42), 0.0, 1.0)) * (1.5 + roughness_40))
   * 
    (((tmpvar_46 * tmpvar_46) * (a2_39 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_54;
  tmpvar_54 = clamp (specularTerm_38, 0.0, 100.0);
  specularTerm_38 = tmpvar_54;
  surfaceReduction_37 = (1.0 - ((roughness_40 * perceptualRoughness_41) * 0.28));
  mediump float x_55;
  x_55 = (1.0 - tmpvar_47);
  mediump vec3 tmpvar_56;
  tmpvar_56 = mix (tmpvar_31, vec3(clamp ((_Glossiness + 
    (1.0 - tmpvar_32)
  ), 0.0, 1.0)), vec3(((x_55 * x_55) * (x_55 * x_55))));
  highp vec3 tmpvar_57;
  tmpvar_57 = (((
    ((diffColor_35 + (tmpvar_54 * tmpvar_31)) * tmpvar_3)
   * tmpvar_44) + (tmpvar_19 * diffColor_35)) + ((surfaceReduction_37 * tmpvar_18) * tmpvar_56));
  color_36 = tmpvar_57;
  mediump vec4 tmpvar_58;
  tmpvar_58.w = 1.0;
  tmpvar_58.xyz = color_36;
  c_27.xyz = tmpvar_58.xyz;
  c_27.w = alpha_34;
  c_5.xyz = c_27.xyz;
  c_5.w = 1.0;
  gl_FragData[0] = c_5;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out mediump vec3 vs_TEXCOORD3;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat12;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    vs_TEXCOORD1.xyz = u_xlat0.xyz;
    u_xlat16_2 = u_xlat0.y * u_xlat0.y;
    u_xlat16_2 = u_xlat0.x * u_xlat0.x + (-u_xlat16_2);
    u_xlat16_0 = u_xlat0.yzzx * u_xlat0.xyzz;
    u_xlat16_3.x = dot(unity_SHBr, u_xlat16_0);
    u_xlat16_3.y = dot(unity_SHBg, u_xlat16_0);
    u_xlat16_3.z = dot(unity_SHBb, u_xlat16_0);
    vs_TEXCOORD3.xyz = unity_SHC.xyz * vec3(u_xlat16_2) + u_xlat16_3.xyz;
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp samplerCube unity_SpecCube0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
in mediump vec3 vs_TEXCOORD3;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump vec3 u_xlat16_0;
mediump vec3 u_xlat16_1;
lowp vec4 u_xlat10_1;
vec3 u_xlat2;
mediump float u_xlat16_2;
vec3 u_xlat3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
mediump vec3 u_xlat16_8;
mediump vec3 u_xlat16_9;
mediump vec3 u_xlat16_12;
lowp vec3 u_xlat10_12;
float u_xlat30;
mediump float u_xlat16_31;
float u_xlat32;
mediump float u_xlat16_34;
mediump float u_xlat16_36;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD1.xyz;
    u_xlat0.w = 1.0;
    u_xlat16_1.x = dot(unity_SHAr, u_xlat0);
    u_xlat16_1.y = dot(unity_SHAg, u_xlat0);
    u_xlat16_1.z = dot(unity_SHAb, u_xlat0);
    u_xlat16_1.xyz = u_xlat16_1.xyz + vs_TEXCOORD3.xyz;
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_0.xyz = log2(u_xlat16_1.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_0.xyz = exp2(u_xlat16_0.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_0.xyz = max(u_xlat16_0.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat2.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat30 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat30 = inversesqrt(u_xlat30);
    u_xlat2.xyz = vec3(u_xlat30) * u_xlat2.xyz;
    u_xlat16_1.x = dot((-u_xlat2.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_1.x = u_xlat16_1.x + u_xlat16_1.x;
    u_xlat16_1.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_1.xxx) + (-u_xlat2.xyz);
    u_xlat3.z = (-_Glossiness) + 1.0;
    u_xlat16_31 = (-u_xlat3.z) * 0.699999988 + 1.70000005;
    u_xlat16_31 = u_xlat16_31 * u_xlat3.z;
    u_xlat16_31 = u_xlat16_31 * 6.0;
    u_xlat10_1 = textureLod(unity_SpecCube0, u_xlat16_1.xyz, u_xlat16_31);
    u_xlat16_4.x = u_xlat10_1.w + -1.0;
    u_xlat16_4.x = unity_SpecCube0_HDR.w * u_xlat16_4.x + 1.0;
    u_xlat16_4.x = u_xlat16_4.x * unity_SpecCube0_HDR.x;
    u_xlat16_4.xyz = u_xlat10_1.xyz * u_xlat16_4.xxx;
    u_xlat30 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat30 = inversesqrt(u_xlat30);
    u_xlat5.xyz = vec3(u_xlat30) * vs_TEXCOORD1.xyz;
    u_xlat30 = dot(u_xlat2.xyz, u_xlat5.xyz);
    u_xlat32 = u_xlat30;
#ifdef UNITY_ADRENO_ES3
    u_xlat32 = min(max(u_xlat32, 0.0), 1.0);
#else
    u_xlat32 = clamp(u_xlat32, 0.0, 1.0);
#endif
    u_xlat30 = u_xlat30 + u_xlat30;
    u_xlat2.xyz = u_xlat5.xyz * (-vec3(u_xlat30)) + u_xlat2.xyz;
    u_xlat30 = dot(u_xlat5.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat30 = min(max(u_xlat30, 0.0), 1.0);
#else
    u_xlat30 = clamp(u_xlat30, 0.0, 1.0);
#endif
    u_xlat16_6.xyz = vec3(u_xlat30) * _LightColor0.xyz;
    u_xlat30 = dot(u_xlat2.xyz, _WorldSpaceLightPos0.xyz);
    u_xlat30 = u_xlat30 * u_xlat30;
    u_xlat3.x = u_xlat30 * u_xlat30;
    u_xlat30 = texture(unity_NHxRoughness, u_xlat3.xz).w;
    u_xlat30 = u_xlat30 * 16.0;
    u_xlat16_34 = (-u_xlat32) + 1.0;
    u_xlat16_2 = u_xlat16_34 * u_xlat16_34;
    u_xlat16_2 = u_xlat16_34 * u_xlat16_2;
    u_xlat16_2 = u_xlat16_34 * u_xlat16_2;
    u_xlat16_34 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_36 = (-u_xlat16_34) + _Glossiness;
    u_xlat16_36 = u_xlat16_36 + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_36 = min(max(u_xlat16_36, 0.0), 1.0);
#else
    u_xlat16_36 = clamp(u_xlat16_36, 0.0, 1.0);
#endif
    u_xlat10_12.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_7.xyz = u_xlat10_12.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_12.xyz = u_xlat10_12.xyz * _Color.xyz;
    u_xlat16_8.xyz = vec3(u_xlat16_34) * u_xlat16_12.xyz;
    u_xlat16_7.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_7.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_9.xyz = vec3(u_xlat16_36) + (-u_xlat16_7.xyz);
    u_xlat16_9.xyz = vec3(u_xlat16_2) * u_xlat16_9.xyz + u_xlat16_7.xyz;
    u_xlat16_7.xyz = vec3(u_xlat30) * u_xlat16_7.xyz + u_xlat16_8.xyz;
    u_xlat16_4.xyz = u_xlat16_4.xyz * u_xlat16_9.xyz;
    u_xlat16_4.xyz = u_xlat16_0.xyz * u_xlat16_8.xyz + u_xlat16_4.xyz;
    SV_Target0.xyz = u_xlat16_7.xyz * u_xlat16_6.xyz + u_xlat16_4.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out mediump vec3 vs_TEXCOORD3;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat12;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    vs_TEXCOORD1.xyz = u_xlat0.xyz;
    u_xlat16_2 = u_xlat0.y * u_xlat0.y;
    u_xlat16_2 = u_xlat0.x * u_xlat0.x + (-u_xlat16_2);
    u_xlat16_0 = u_xlat0.yzzx * u_xlat0.xyzz;
    u_xlat16_3.x = dot(unity_SHBr, u_xlat16_0);
    u_xlat16_3.y = dot(unity_SHBg, u_xlat16_0);
    u_xlat16_3.z = dot(unity_SHBb, u_xlat16_0);
    vs_TEXCOORD3.xyz = unity_SHC.xyz * vec3(u_xlat16_2) + u_xlat16_3.xyz;
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform lowp samplerCube unity_SpecCube0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
in mediump vec3 vs_TEXCOORD3;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump vec3 u_xlat16_0;
lowp vec4 u_xlat10_0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
lowp vec3 u_xlat10_2;
vec3 u_xlat3;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
mediump float u_xlat16_9;
mediump vec3 u_xlat16_11;
mediump float u_xlat16_16;
float u_xlat21;
mediump float u_xlat16_22;
mediump float u_xlat16_23;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD1.xyz;
    u_xlat0.w = 1.0;
    u_xlat16_1.x = dot(unity_SHAr, u_xlat0);
    u_xlat16_1.y = dot(unity_SHAg, u_xlat0);
    u_xlat16_1.z = dot(unity_SHAb, u_xlat0);
    u_xlat16_1.xyz = u_xlat16_1.xyz + vs_TEXCOORD3.xyz;
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_0.xyz = log2(u_xlat16_1.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_0.xyz = exp2(u_xlat16_0.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_0.xyz = max(u_xlat16_0.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat10_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_2.xyz * _Color.xyz;
    u_xlat16_1.xyz = u_xlat10_2.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_1.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_22 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_4.xyz = vec3(u_xlat16_22) * u_xlat16_3.xyz;
    u_xlat16_22 = (-u_xlat16_22) + _Glossiness;
    u_xlat16_22 = u_xlat16_22 + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_22 = min(max(u_xlat16_22, 0.0), 1.0);
#else
    u_xlat16_22 = clamp(u_xlat16_22, 0.0, 1.0);
#endif
    u_xlat16_5.xyz = (-u_xlat16_1.xyz) + vec3(u_xlat16_22);
    u_xlat16_6.xyz = u_xlat16_0.xyz * u_xlat16_4.xyz;
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat21 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat2.xyz = u_xlat0.xyz * vec3(u_xlat21) + _WorldSpaceLightPos0.xyz;
    u_xlat0.xyz = vec3(u_xlat21) * u_xlat0.xyz;
    u_xlat21 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat21 = max(u_xlat21, 0.00100000005);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat2.xyz = vec3(u_xlat21) * u_xlat2.xyz;
    u_xlat21 = dot(_WorldSpaceLightPos0.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat21 = min(max(u_xlat21, 0.0), 1.0);
#else
    u_xlat21 = clamp(u_xlat21, 0.0, 1.0);
#endif
    u_xlat21 = max(u_xlat21, 0.319999993);
    u_xlat16_23 = (-_Glossiness) + 1.0;
    u_xlat16_3.x = u_xlat16_23 * u_xlat16_23 + 1.5;
    u_xlat21 = u_xlat21 * u_xlat16_3.x;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat2.x = dot(u_xlat3.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat2.x = min(max(u_xlat2.x, 0.0), 1.0);
#else
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
#endif
    u_xlat2.x = u_xlat2.x * u_xlat2.x;
    u_xlat16_9 = u_xlat16_23 * u_xlat16_23;
    u_xlat16_16 = u_xlat16_9 * u_xlat16_9 + -1.0;
    u_xlat2.x = u_xlat2.x * u_xlat16_16 + 1.00001001;
    u_xlat21 = u_xlat21 * u_xlat2.x;
    u_xlat21 = u_xlat16_9 / u_xlat21;
    u_xlat16_22 = u_xlat16_23 * u_xlat16_9;
    u_xlat16_22 = (-u_xlat16_22) * 0.280000001 + 1.0;
    u_xlat21 = u_xlat21 + -9.99999975e-05;
    u_xlat21 = max(u_xlat21, 0.0);
    u_xlat21 = min(u_xlat21, 100.0);
    u_xlat2.xyz = vec3(u_xlat21) * u_xlat16_1.xyz + u_xlat16_4.xyz;
    u_xlat2.xyz = u_xlat2.xyz * _LightColor0.xyz;
    u_xlat21 = dot(u_xlat3.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat21 = min(max(u_xlat21, 0.0), 1.0);
#else
    u_xlat21 = clamp(u_xlat21, 0.0, 1.0);
#endif
    u_xlat3.x = dot(u_xlat3.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat3.x = min(max(u_xlat3.x, 0.0), 1.0);
#else
    u_xlat3.x = clamp(u_xlat3.x, 0.0, 1.0);
#endif
    u_xlat16_4.x = (-u_xlat3.x) + 1.0;
    u_xlat16_4.x = u_xlat16_4.x * u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_4.x * u_xlat16_4.x;
    u_xlat16_1.xyz = u_xlat16_4.xxx * u_xlat16_5.xyz + u_xlat16_1.xyz;
    u_xlat2.xyz = u_xlat2.xyz * vec3(u_xlat21) + u_xlat16_6.xyz;
    u_xlat16_4.x = (-u_xlat16_23) * 0.699999988 + 1.70000005;
    u_xlat16_4.x = u_xlat16_23 * u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_4.x * 6.0;
    u_xlat16_11.x = dot((-u_xlat0.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_11.x = u_xlat16_11.x + u_xlat16_11.x;
    u_xlat16_11.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_11.xxx) + (-u_xlat0.xyz);
    u_xlat10_0 = textureLod(unity_SpecCube0, u_xlat16_11.xyz, u_xlat16_4.x);
    u_xlat16_4.x = u_xlat10_0.w + -1.0;
    u_xlat16_4.x = unity_SpecCube0_HDR.w * u_xlat16_4.x + 1.0;
    u_xlat16_4.x = u_xlat16_4.x * unity_SpecCube0_HDR.x;
    u_xlat16_4.xyz = u_xlat10_0.xyz * u_xlat16_4.xxx;
    u_xlat16_4.xyz = vec3(u_xlat16_22) * u_xlat16_4.xyz;
    u_xlat0.xyz = u_xlat16_4.xyz * u_xlat16_1.xyz + u_xlat2.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out mediump vec3 vs_TEXCOORD3;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat12;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    vs_TEXCOORD1.xyz = u_xlat0.xyz;
    u_xlat16_2 = u_xlat0.y * u_xlat0.y;
    u_xlat16_2 = u_xlat0.x * u_xlat0.x + (-u_xlat16_2);
    u_xlat16_0 = u_xlat0.yzzx * u_xlat0.xyzz;
    u_xlat16_3.x = dot(unity_SHBr, u_xlat16_0);
    u_xlat16_3.y = dot(unity_SHBg, u_xlat16_0);
    u_xlat16_3.z = dot(unity_SHBb, u_xlat16_0);
    vs_TEXCOORD3.xyz = unity_SHC.xyz * vec3(u_xlat16_2) + u_xlat16_3.xyz;
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform lowp samplerCube unity_SpecCube0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
in mediump vec3 vs_TEXCOORD3;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump vec3 u_xlat16_0;
lowp vec4 u_xlat10_0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
lowp vec3 u_xlat10_2;
vec3 u_xlat3;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
mediump float u_xlat16_9;
mediump vec3 u_xlat16_11;
mediump float u_xlat16_16;
float u_xlat21;
mediump float u_xlat16_22;
mediump float u_xlat16_23;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD1.xyz;
    u_xlat0.w = 1.0;
    u_xlat16_1.x = dot(unity_SHAr, u_xlat0);
    u_xlat16_1.y = dot(unity_SHAg, u_xlat0);
    u_xlat16_1.z = dot(unity_SHAb, u_xlat0);
    u_xlat16_1.xyz = u_xlat16_1.xyz + vs_TEXCOORD3.xyz;
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_0.xyz = log2(u_xlat16_1.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_0.xyz = exp2(u_xlat16_0.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_0.xyz = max(u_xlat16_0.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat10_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_2.xyz * _Color.xyz;
    u_xlat16_1.xyz = u_xlat10_2.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_1.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_22 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_4.xyz = vec3(u_xlat16_22) * u_xlat16_3.xyz;
    u_xlat16_22 = (-u_xlat16_22) + _Glossiness;
    u_xlat16_22 = u_xlat16_22 + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_22 = min(max(u_xlat16_22, 0.0), 1.0);
#else
    u_xlat16_22 = clamp(u_xlat16_22, 0.0, 1.0);
#endif
    u_xlat16_5.xyz = (-u_xlat16_1.xyz) + vec3(u_xlat16_22);
    u_xlat16_6.xyz = u_xlat16_0.xyz * u_xlat16_4.xyz;
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat21 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat2.xyz = u_xlat0.xyz * vec3(u_xlat21) + _WorldSpaceLightPos0.xyz;
    u_xlat0.xyz = vec3(u_xlat21) * u_xlat0.xyz;
    u_xlat21 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat21 = max(u_xlat21, 0.00100000005);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat2.xyz = vec3(u_xlat21) * u_xlat2.xyz;
    u_xlat21 = dot(_WorldSpaceLightPos0.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat21 = min(max(u_xlat21, 0.0), 1.0);
#else
    u_xlat21 = clamp(u_xlat21, 0.0, 1.0);
#endif
    u_xlat21 = max(u_xlat21, 0.319999993);
    u_xlat16_23 = (-_Glossiness) + 1.0;
    u_xlat16_3.x = u_xlat16_23 * u_xlat16_23 + 1.5;
    u_xlat21 = u_xlat21 * u_xlat16_3.x;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat2.x = dot(u_xlat3.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat2.x = min(max(u_xlat2.x, 0.0), 1.0);
#else
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
#endif
    u_xlat2.x = u_xlat2.x * u_xlat2.x;
    u_xlat16_9 = u_xlat16_23 * u_xlat16_23;
    u_xlat16_16 = u_xlat16_9 * u_xlat16_9 + -1.0;
    u_xlat2.x = u_xlat2.x * u_xlat16_16 + 1.00001001;
    u_xlat21 = u_xlat21 * u_xlat2.x;
    u_xlat21 = u_xlat16_9 / u_xlat21;
    u_xlat16_22 = u_xlat16_23 * u_xlat16_9;
    u_xlat16_22 = (-u_xlat16_22) * 0.280000001 + 1.0;
    u_xlat21 = u_xlat21 + -9.99999975e-05;
    u_xlat21 = max(u_xlat21, 0.0);
    u_xlat21 = min(u_xlat21, 100.0);
    u_xlat2.xyz = vec3(u_xlat21) * u_xlat16_1.xyz + u_xlat16_4.xyz;
    u_xlat2.xyz = u_xlat2.xyz * _LightColor0.xyz;
    u_xlat21 = dot(u_xlat3.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat21 = min(max(u_xlat21, 0.0), 1.0);
#else
    u_xlat21 = clamp(u_xlat21, 0.0, 1.0);
#endif
    u_xlat3.x = dot(u_xlat3.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat3.x = min(max(u_xlat3.x, 0.0), 1.0);
#else
    u_xlat3.x = clamp(u_xlat3.x, 0.0, 1.0);
#endif
    u_xlat16_4.x = (-u_xlat3.x) + 1.0;
    u_xlat16_4.x = u_xlat16_4.x * u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_4.x * u_xlat16_4.x;
    u_xlat16_1.xyz = u_xlat16_4.xxx * u_xlat16_5.xyz + u_xlat16_1.xyz;
    u_xlat2.xyz = u_xlat2.xyz * vec3(u_xlat21) + u_xlat16_6.xyz;
    u_xlat16_4.x = (-u_xlat16_23) * 0.699999988 + 1.70000005;
    u_xlat16_4.x = u_xlat16_23 * u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_4.x * 6.0;
    u_xlat16_11.x = dot((-u_xlat0.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_11.x = u_xlat16_11.x + u_xlat16_11.x;
    u_xlat16_11.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_11.xxx) + (-u_xlat0.xyz);
    u_xlat10_0 = textureLod(unity_SpecCube0, u_xlat16_11.xyz, u_xlat16_4.x);
    u_xlat16_4.x = u_xlat10_0.w + -1.0;
    u_xlat16_4.x = unity_SpecCube0_HDR.w * u_xlat16_4.x + 1.0;
    u_xlat16_4.x = u_xlat16_4.x * unity_SpecCube0_HDR.x;
    u_xlat16_4.xyz = u_xlat10_0.xyz * u_xlat16_4.xxx;
    u_xlat16_4.xyz = vec3(u_xlat16_22) * u_xlat16_4.xyz;
    u_xlat0.xyz = u_xlat16_4.xyz * u_xlat16_1.xyz + u_xlat2.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 63 math, 4 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD5;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  highp vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_4;
  tmpvar_4[0] = unity_WorldToObject[0].xyz;
  tmpvar_4[1] = unity_WorldToObject[1].xyz;
  tmpvar_4[2] = unity_WorldToObject[2].xyz;
  highp vec4 tmpvar_5;
  tmpvar_5 = (unity_ObjectToWorld * _glesVertex);
  tmpvar_1.xyz = tmpvar_5.xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_4));
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD5 = (unity_WorldToShadow[0] * tmpvar_5);
  xlv_TEXCOORD6 = tmpvar_2;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform highp sampler2D _ShadowMapTexture;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD5;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump float tmpvar_2;
  highp vec4 tmpvar_3;
  mediump vec3 tmpvar_4;
  mediump vec3 tmpvar_5;
  lowp vec4 c_6;
  lowp float atten_7;
  lowp vec3 lightDir_8;
  mediump vec3 tmpvar_9;
  tmpvar_9 = _WorldSpaceLightPos0.xyz;
  lightDir_8 = tmpvar_9;
  highp vec3 tmpvar_10;
  highp vec3 tmpvar_11;
  tmpvar_11 = (_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz);
  tmpvar_10 = normalize(tmpvar_11);
  lowp vec3 tmpvar_12;
  lowp float tmpvar_13;
  lowp vec4 tmpvar_14;
  tmpvar_14 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_12 = tmpvar_14.xyz;
  tmpvar_13 = tmpvar_14.w;
  mediump float realtimeShadowAttenuation_15;
  highp vec4 v_16;
  v_16.x = unity_MatrixV[0].z;
  v_16.y = unity_MatrixV[1].z;
  v_16.z = unity_MatrixV[2].z;
  v_16.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_17;
  tmpvar_17 = (xlv_TEXCOORD2.xyz - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_18;
  highp float tmpvar_19;
  tmpvar_19 = clamp (((
    mix (dot (tmpvar_11, v_16.xyz), sqrt(dot (tmpvar_17, tmpvar_17)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_18 = tmpvar_19;
  lowp float tmpvar_20;
  highp float lightShadowDataX_21;
  mediump float tmpvar_22;
  tmpvar_22 = _LightShadowData.x;
  lightShadowDataX_21 = tmpvar_22;
  highp float tmpvar_23;
  tmpvar_23 = max (float((texture2D (_ShadowMapTexture, xlv_TEXCOORD5.xy).x > xlv_TEXCOORD5.z)), lightShadowDataX_21);
  tmpvar_20 = tmpvar_23;
  realtimeShadowAttenuation_15 = tmpvar_20;
  mediump float tmpvar_24;
  tmpvar_24 = mix (realtimeShadowAttenuation_15, 1.0, tmpvar_18);
  atten_7 = tmpvar_24;
  tmpvar_4 = _LightColor0.xyz;
  tmpvar_5 = lightDir_8;
  tmpvar_1 = tmpvar_10;
  tmpvar_2 = atten_7;
  tmpvar_3 = unity_SpecCube0_HDR;
  mediump vec3 Normal_25;
  Normal_25 = xlv_TEXCOORD1;
  mediump float tmpvar_26;
  highp float tmpvar_27;
  highp float smoothness_28;
  smoothness_28 = _Glossiness;
  tmpvar_27 = (1.0 - smoothness_28);
  tmpvar_26 = tmpvar_27;
  mediump vec3 I_29;
  I_29 = -(tmpvar_1);
  mediump vec3 tmpvar_30;
  tmpvar_30 = (tmpvar_4 * tmpvar_2);
  mediump vec4 hdr_31;
  hdr_31 = tmpvar_3;
  mediump vec4 tmpvar_32;
  tmpvar_32.xyz = (I_29 - (2.0 * (
    dot (Normal_25, I_29)
   * Normal_25)));
  tmpvar_32.w = ((tmpvar_26 * (1.7 - 
    (0.7 * tmpvar_26)
  )) * 6.0);
  lowp vec4 tmpvar_33;
  tmpvar_33 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_32.xyz, tmpvar_32.w);
  mediump vec4 tmpvar_34;
  tmpvar_34 = tmpvar_33;
  tmpvar_4 = tmpvar_30;
  lowp vec3 tmpvar_35;
  mediump vec4 c_36;
  highp vec3 tmpvar_37;
  tmpvar_37 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_38;
  mediump vec3 albedo_39;
  albedo_39 = tmpvar_12;
  mediump vec3 tmpvar_40;
  tmpvar_40 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_39, vec3(_Metallic));
  mediump float tmpvar_41;
  tmpvar_41 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_38 = (albedo_39 * tmpvar_41);
  tmpvar_35 = tmpvar_38;
  mediump vec3 diffColor_42;
  diffColor_42 = tmpvar_35;
  mediump float alpha_43;
  alpha_43 = tmpvar_13;
  tmpvar_35 = diffColor_42;
  mediump vec3 diffColor_44;
  diffColor_44 = tmpvar_35;
  mediump vec3 color_45;
  mediump vec2 rlPow4AndFresnelTerm_46;
  mediump float tmpvar_47;
  highp float tmpvar_48;
  tmpvar_48 = clamp (dot (tmpvar_37, tmpvar_5), 0.0, 1.0);
  tmpvar_47 = tmpvar_48;
  mediump float tmpvar_49;
  highp float tmpvar_50;
  tmpvar_50 = clamp (dot (tmpvar_37, tmpvar_10), 0.0, 1.0);
  tmpvar_49 = tmpvar_50;
  highp vec2 tmpvar_51;
  tmpvar_51.x = dot ((tmpvar_10 - (2.0 * 
    (dot (tmpvar_37, tmpvar_10) * tmpvar_37)
  )), tmpvar_5);
  tmpvar_51.y = (1.0 - tmpvar_49);
  highp vec2 tmpvar_52;
  tmpvar_52 = ((tmpvar_51 * tmpvar_51) * (tmpvar_51 * tmpvar_51));
  rlPow4AndFresnelTerm_46 = tmpvar_52;
  mediump float tmpvar_53;
  tmpvar_53 = rlPow4AndFresnelTerm_46.x;
  mediump float specular_54;
  highp float smoothness_55;
  smoothness_55 = _Glossiness;
  highp vec2 tmpvar_56;
  tmpvar_56.x = tmpvar_53;
  tmpvar_56.y = (1.0 - smoothness_55);
  highp float tmpvar_57;
  tmpvar_57 = (texture2D (unity_NHxRoughness, tmpvar_56).w * 16.0);
  specular_54 = tmpvar_57;
  color_45 = ((diffColor_44 + (specular_54 * tmpvar_40)) * (tmpvar_30 * tmpvar_47));
  color_45 = (color_45 + ((
    (hdr_31.x * ((hdr_31.w * (tmpvar_34.w - 1.0)) + 1.0))
   * tmpvar_34.xyz) * mix (tmpvar_40, vec3(
    clamp ((_Glossiness + (1.0 - tmpvar_41)), 0.0, 1.0)
  ), rlPow4AndFresnelTerm_46.yyy)));
  mediump vec4 tmpvar_58;
  tmpvar_58.w = 1.0;
  tmpvar_58.xyz = color_45;
  c_36.xyz = tmpvar_58.xyz;
  c_36.w = alpha_43;
  c_6.xyz = c_36.xyz;
  c_6.w = 1.0;
  gl_FragData[0] = c_6;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 83 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD5;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  highp vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_4;
  tmpvar_4[0] = unity_WorldToObject[0].xyz;
  tmpvar_4[1] = unity_WorldToObject[1].xyz;
  tmpvar_4[2] = unity_WorldToObject[2].xyz;
  highp vec4 tmpvar_5;
  tmpvar_5 = (unity_ObjectToWorld * _glesVertex);
  tmpvar_1.xyz = tmpvar_5.xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_4));
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD5 = (unity_WorldToShadow[0] * tmpvar_5);
  xlv_TEXCOORD6 = tmpvar_2;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _ShadowMapTexture;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD5;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump float tmpvar_2;
  highp vec4 tmpvar_3;
  mediump vec3 tmpvar_4;
  mediump vec3 tmpvar_5;
  lowp vec4 c_6;
  lowp float atten_7;
  lowp vec3 lightDir_8;
  mediump vec3 tmpvar_9;
  tmpvar_9 = _WorldSpaceLightPos0.xyz;
  lightDir_8 = tmpvar_9;
  highp vec3 tmpvar_10;
  highp vec3 tmpvar_11;
  tmpvar_11 = (_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz);
  tmpvar_10 = normalize(tmpvar_11);
  lowp vec3 tmpvar_12;
  lowp float tmpvar_13;
  lowp vec4 tmpvar_14;
  tmpvar_14 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_12 = tmpvar_14.xyz;
  tmpvar_13 = tmpvar_14.w;
  mediump float realtimeShadowAttenuation_15;
  highp vec4 v_16;
  v_16.x = unity_MatrixV[0].z;
  v_16.y = unity_MatrixV[1].z;
  v_16.z = unity_MatrixV[2].z;
  v_16.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_17;
  tmpvar_17 = (xlv_TEXCOORD2.xyz - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_18;
  highp float tmpvar_19;
  tmpvar_19 = clamp (((
    mix (dot (tmpvar_11, v_16.xyz), sqrt(dot (tmpvar_17, tmpvar_17)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_18 = tmpvar_19;
  lowp float tmpvar_20;
  highp float lightShadowDataX_21;
  mediump float tmpvar_22;
  tmpvar_22 = _LightShadowData.x;
  lightShadowDataX_21 = tmpvar_22;
  highp float tmpvar_23;
  tmpvar_23 = max (float((texture2D (_ShadowMapTexture, xlv_TEXCOORD5.xy).x > xlv_TEXCOORD5.z)), lightShadowDataX_21);
  tmpvar_20 = tmpvar_23;
  realtimeShadowAttenuation_15 = tmpvar_20;
  mediump float tmpvar_24;
  tmpvar_24 = mix (realtimeShadowAttenuation_15, 1.0, tmpvar_18);
  atten_7 = tmpvar_24;
  tmpvar_4 = _LightColor0.xyz;
  tmpvar_5 = lightDir_8;
  tmpvar_1 = tmpvar_10;
  tmpvar_2 = atten_7;
  tmpvar_3 = unity_SpecCube0_HDR;
  mediump vec3 Normal_25;
  Normal_25 = xlv_TEXCOORD1;
  mediump float tmpvar_26;
  highp float tmpvar_27;
  highp float smoothness_28;
  smoothness_28 = _Glossiness;
  tmpvar_27 = (1.0 - smoothness_28);
  tmpvar_26 = tmpvar_27;
  mediump vec3 I_29;
  I_29 = -(tmpvar_1);
  mediump vec3 tmpvar_30;
  mediump vec3 tmpvar_31;
  tmpvar_31 = (tmpvar_4 * tmpvar_2);
  mediump vec4 hdr_32;
  hdr_32 = tmpvar_3;
  mediump vec4 tmpvar_33;
  tmpvar_33.xyz = (I_29 - (2.0 * (
    dot (Normal_25, I_29)
   * Normal_25)));
  tmpvar_33.w = ((tmpvar_26 * (1.7 - 
    (0.7 * tmpvar_26)
  )) * 6.0);
  lowp vec4 tmpvar_34;
  tmpvar_34 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_33.xyz, tmpvar_33.w);
  mediump vec4 tmpvar_35;
  tmpvar_35 = tmpvar_34;
  tmpvar_30 = ((hdr_32.x * (
    (hdr_32.w * (tmpvar_35.w - 1.0))
   + 1.0)) * tmpvar_35.xyz);
  tmpvar_4 = tmpvar_31;
  lowp vec3 tmpvar_36;
  mediump vec4 c_37;
  highp vec3 tmpvar_38;
  tmpvar_38 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_39;
  mediump vec3 albedo_40;
  albedo_40 = tmpvar_12;
  mediump vec3 tmpvar_41;
  tmpvar_41 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_40, vec3(_Metallic));
  mediump float tmpvar_42;
  tmpvar_42 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_39 = (albedo_40 * tmpvar_42);
  tmpvar_36 = tmpvar_39;
  mediump vec3 diffColor_43;
  diffColor_43 = tmpvar_36;
  mediump float alpha_44;
  alpha_44 = tmpvar_13;
  tmpvar_36 = diffColor_43;
  mediump vec3 diffColor_45;
  diffColor_45 = tmpvar_36;
  mediump vec3 color_46;
  mediump float surfaceReduction_47;
  highp float specularTerm_48;
  highp float a2_49;
  mediump float roughness_50;
  mediump float perceptualRoughness_51;
  highp vec3 tmpvar_52;
  highp vec3 inVec_53;
  inVec_53 = (tmpvar_5 + tmpvar_10);
  tmpvar_52 = (inVec_53 * inversesqrt(max (0.001, 
    dot (inVec_53, inVec_53)
  )));
  mediump float tmpvar_54;
  highp float tmpvar_55;
  tmpvar_55 = clamp (dot (tmpvar_38, tmpvar_5), 0.0, 1.0);
  tmpvar_54 = tmpvar_55;
  highp float tmpvar_56;
  tmpvar_56 = clamp (dot (tmpvar_38, tmpvar_52), 0.0, 1.0);
  mediump float tmpvar_57;
  highp float tmpvar_58;
  tmpvar_58 = clamp (dot (tmpvar_38, tmpvar_10), 0.0, 1.0);
  tmpvar_57 = tmpvar_58;
  highp float tmpvar_59;
  highp float smoothness_60;
  smoothness_60 = _Glossiness;
  tmpvar_59 = (1.0 - smoothness_60);
  perceptualRoughness_51 = tmpvar_59;
  highp float tmpvar_61;
  highp float perceptualRoughness_62;
  perceptualRoughness_62 = perceptualRoughness_51;
  tmpvar_61 = (perceptualRoughness_62 * perceptualRoughness_62);
  roughness_50 = tmpvar_61;
  mediump float tmpvar_63;
  tmpvar_63 = (roughness_50 * roughness_50);
  a2_49 = tmpvar_63;
  specularTerm_48 = ((roughness_50 / (
    (max (0.32, clamp (dot (tmpvar_5, tmpvar_52), 0.0, 1.0)) * (1.5 + roughness_50))
   * 
    (((tmpvar_56 * tmpvar_56) * (a2_49 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_64;
  tmpvar_64 = clamp (specularTerm_48, 0.0, 100.0);
  specularTerm_48 = tmpvar_64;
  surfaceReduction_47 = (1.0 - ((roughness_50 * perceptualRoughness_51) * 0.28));
  mediump float x_65;
  x_65 = (1.0 - tmpvar_57);
  mediump vec3 tmpvar_66;
  tmpvar_66 = mix (tmpvar_41, vec3(clamp ((_Glossiness + 
    (1.0 - tmpvar_42)
  ), 0.0, 1.0)), vec3(((x_65 * x_65) * (x_65 * x_65))));
  highp vec3 tmpvar_67;
  tmpvar_67 = (((
    (diffColor_45 + (tmpvar_64 * tmpvar_41))
   * tmpvar_31) * tmpvar_54) + ((surfaceReduction_47 * tmpvar_30) * tmpvar_66));
  color_46 = tmpvar_67;
  mediump vec4 tmpvar_68;
  tmpvar_68.w = 1.0;
  tmpvar_68.xyz = color_46;
  c_37.xyz = tmpvar_68.xyz;
  c_37.w = alpha_44;
  c_6.xyz = c_37.xyz;
  c_6.w = 1.0;
  gl_FragData[0] = c_6;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 83 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD5;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  highp vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_4;
  tmpvar_4[0] = unity_WorldToObject[0].xyz;
  tmpvar_4[1] = unity_WorldToObject[1].xyz;
  tmpvar_4[2] = unity_WorldToObject[2].xyz;
  highp vec4 tmpvar_5;
  tmpvar_5 = (unity_ObjectToWorld * _glesVertex);
  tmpvar_1.xyz = tmpvar_5.xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_4));
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD5 = (unity_WorldToShadow[0] * tmpvar_5);
  xlv_TEXCOORD6 = tmpvar_2;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _ShadowMapTexture;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD5;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump float tmpvar_2;
  highp vec4 tmpvar_3;
  mediump vec3 tmpvar_4;
  mediump vec3 tmpvar_5;
  lowp vec4 c_6;
  lowp float atten_7;
  lowp vec3 lightDir_8;
  mediump vec3 tmpvar_9;
  tmpvar_9 = _WorldSpaceLightPos0.xyz;
  lightDir_8 = tmpvar_9;
  highp vec3 tmpvar_10;
  highp vec3 tmpvar_11;
  tmpvar_11 = (_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz);
  tmpvar_10 = normalize(tmpvar_11);
  lowp vec3 tmpvar_12;
  lowp float tmpvar_13;
  lowp vec4 tmpvar_14;
  tmpvar_14 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_12 = tmpvar_14.xyz;
  tmpvar_13 = tmpvar_14.w;
  mediump float realtimeShadowAttenuation_15;
  highp vec4 v_16;
  v_16.x = unity_MatrixV[0].z;
  v_16.y = unity_MatrixV[1].z;
  v_16.z = unity_MatrixV[2].z;
  v_16.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_17;
  tmpvar_17 = (xlv_TEXCOORD2.xyz - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_18;
  highp float tmpvar_19;
  tmpvar_19 = clamp (((
    mix (dot (tmpvar_11, v_16.xyz), sqrt(dot (tmpvar_17, tmpvar_17)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_18 = tmpvar_19;
  lowp float tmpvar_20;
  highp float lightShadowDataX_21;
  mediump float tmpvar_22;
  tmpvar_22 = _LightShadowData.x;
  lightShadowDataX_21 = tmpvar_22;
  highp float tmpvar_23;
  tmpvar_23 = max (float((texture2D (_ShadowMapTexture, xlv_TEXCOORD5.xy).x > xlv_TEXCOORD5.z)), lightShadowDataX_21);
  tmpvar_20 = tmpvar_23;
  realtimeShadowAttenuation_15 = tmpvar_20;
  mediump float tmpvar_24;
  tmpvar_24 = mix (realtimeShadowAttenuation_15, 1.0, tmpvar_18);
  atten_7 = tmpvar_24;
  tmpvar_4 = _LightColor0.xyz;
  tmpvar_5 = lightDir_8;
  tmpvar_1 = tmpvar_10;
  tmpvar_2 = atten_7;
  tmpvar_3 = unity_SpecCube0_HDR;
  mediump vec3 Normal_25;
  Normal_25 = xlv_TEXCOORD1;
  mediump float tmpvar_26;
  highp float tmpvar_27;
  highp float smoothness_28;
  smoothness_28 = _Glossiness;
  tmpvar_27 = (1.0 - smoothness_28);
  tmpvar_26 = tmpvar_27;
  mediump vec3 I_29;
  I_29 = -(tmpvar_1);
  mediump vec3 tmpvar_30;
  mediump vec3 tmpvar_31;
  tmpvar_31 = (tmpvar_4 * tmpvar_2);
  mediump vec4 hdr_32;
  hdr_32 = tmpvar_3;
  mediump vec4 tmpvar_33;
  tmpvar_33.xyz = (I_29 - (2.0 * (
    dot (Normal_25, I_29)
   * Normal_25)));
  tmpvar_33.w = ((tmpvar_26 * (1.7 - 
    (0.7 * tmpvar_26)
  )) * 6.0);
  lowp vec4 tmpvar_34;
  tmpvar_34 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_33.xyz, tmpvar_33.w);
  mediump vec4 tmpvar_35;
  tmpvar_35 = tmpvar_34;
  tmpvar_30 = ((hdr_32.x * (
    (hdr_32.w * (tmpvar_35.w - 1.0))
   + 1.0)) * tmpvar_35.xyz);
  tmpvar_4 = tmpvar_31;
  lowp vec3 tmpvar_36;
  mediump vec4 c_37;
  highp vec3 tmpvar_38;
  tmpvar_38 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_39;
  mediump vec3 albedo_40;
  albedo_40 = tmpvar_12;
  mediump vec3 tmpvar_41;
  tmpvar_41 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_40, vec3(_Metallic));
  mediump float tmpvar_42;
  tmpvar_42 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_39 = (albedo_40 * tmpvar_42);
  tmpvar_36 = tmpvar_39;
  mediump vec3 diffColor_43;
  diffColor_43 = tmpvar_36;
  mediump float alpha_44;
  alpha_44 = tmpvar_13;
  tmpvar_36 = diffColor_43;
  mediump vec3 diffColor_45;
  diffColor_45 = tmpvar_36;
  mediump vec3 color_46;
  mediump float surfaceReduction_47;
  highp float specularTerm_48;
  highp float a2_49;
  mediump float roughness_50;
  mediump float perceptualRoughness_51;
  highp vec3 tmpvar_52;
  highp vec3 inVec_53;
  inVec_53 = (tmpvar_5 + tmpvar_10);
  tmpvar_52 = (inVec_53 * inversesqrt(max (0.001, 
    dot (inVec_53, inVec_53)
  )));
  mediump float tmpvar_54;
  highp float tmpvar_55;
  tmpvar_55 = clamp (dot (tmpvar_38, tmpvar_5), 0.0, 1.0);
  tmpvar_54 = tmpvar_55;
  highp float tmpvar_56;
  tmpvar_56 = clamp (dot (tmpvar_38, tmpvar_52), 0.0, 1.0);
  mediump float tmpvar_57;
  highp float tmpvar_58;
  tmpvar_58 = clamp (dot (tmpvar_38, tmpvar_10), 0.0, 1.0);
  tmpvar_57 = tmpvar_58;
  highp float tmpvar_59;
  highp float smoothness_60;
  smoothness_60 = _Glossiness;
  tmpvar_59 = (1.0 - smoothness_60);
  perceptualRoughness_51 = tmpvar_59;
  highp float tmpvar_61;
  highp float perceptualRoughness_62;
  perceptualRoughness_62 = perceptualRoughness_51;
  tmpvar_61 = (perceptualRoughness_62 * perceptualRoughness_62);
  roughness_50 = tmpvar_61;
  mediump float tmpvar_63;
  tmpvar_63 = (roughness_50 * roughness_50);
  a2_49 = tmpvar_63;
  specularTerm_48 = ((roughness_50 / (
    (max (0.32, clamp (dot (tmpvar_5, tmpvar_52), 0.0, 1.0)) * (1.5 + roughness_50))
   * 
    (((tmpvar_56 * tmpvar_56) * (a2_49 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_64;
  tmpvar_64 = clamp (specularTerm_48, 0.0, 100.0);
  specularTerm_48 = tmpvar_64;
  surfaceReduction_47 = (1.0 - ((roughness_50 * perceptualRoughness_51) * 0.28));
  mediump float x_65;
  x_65 = (1.0 - tmpvar_57);
  mediump vec3 tmpvar_66;
  tmpvar_66 = mix (tmpvar_41, vec3(clamp ((_Glossiness + 
    (1.0 - tmpvar_42)
  ), 0.0, 1.0)), vec3(((x_65 * x_65) * (x_65 * x_65))));
  highp vec3 tmpvar_67;
  tmpvar_67 = (((
    (diffColor_45 + (tmpvar_64 * tmpvar_41))
   * tmpvar_31) * tmpvar_54) + ((surfaceReduction_47 * tmpvar_30) * tmpvar_66));
  color_46 = tmpvar_67;
  mediump vec4 tmpvar_68;
  tmpvar_68.w = 1.0;
  tmpvar_68.xyz = color_46;
  c_37.xyz = tmpvar_68.xyz;
  c_37.w = alpha_44;
  c_6.xyz = c_37.xyz;
  c_6.w = 1.0;
  gl_FragData[0] = c_6;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD5;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD5 = vec4(0.0, 0.0, 0.0, 0.0);
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp samplerCube unity_SpecCube0;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
lowp float u_xlat10_0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_2;
lowp vec3 u_xlat10_2;
mediump vec3 u_xlat16_3;
lowp vec4 u_xlat10_3;
vec3 u_xlat4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
vec3 u_xlat8;
mediump float u_xlat16_8;
mediump float u_xlat16_9;
float u_xlat24;
mediump float u_xlat16_25;
float u_xlat26;
mediump float u_xlat16_29;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * vs_TEXCOORD2.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * vs_TEXCOORD2.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    vec3 txVec0 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat10_0 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_1.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_1.x = u_xlat10_0 * u_xlat16_1.x + _LightShadowData.x;
    u_xlat16_9 = (-u_xlat16_1.x) + 1.0;
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat2.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat8.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat2.x = dot(u_xlat8.xyz, u_xlat2.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat2.x);
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat0.x + u_xlat2.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_1.x = u_xlat0.x * u_xlat16_9 + u_xlat16_1.x;
    u_xlat16_1.xyz = u_xlat16_1.xxx * _LightColor0.xyz;
    u_xlat0.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat2.xyz = u_xlat0.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat2.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_1.xyz = u_xlat0.xxx * u_xlat16_1.xyz;
    u_xlat0.x = dot(u_xlat8.xyz, u_xlat8.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx * u_xlat8.xyz;
    u_xlat16_25 = dot((-u_xlat0.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_25 = u_xlat16_25 + u_xlat16_25;
    u_xlat16_3.xyz = vs_TEXCOORD1.xyz * (-vec3(u_xlat16_25)) + (-u_xlat0.xyz);
    u_xlat4.z = (-_Glossiness) + 1.0;
    u_xlat16_25 = (-u_xlat4.z) * 0.699999988 + 1.70000005;
    u_xlat16_25 = u_xlat16_25 * u_xlat4.z;
    u_xlat16_25 = u_xlat16_25 * 6.0;
    u_xlat10_3 = textureLod(unity_SpecCube0, u_xlat16_3.xyz, u_xlat16_25);
    u_xlat16_25 = u_xlat10_3.w + -1.0;
    u_xlat16_25 = unity_SpecCube0_HDR.w * u_xlat16_25 + 1.0;
    u_xlat16_25 = u_xlat16_25 * unity_SpecCube0_HDR.x;
    u_xlat16_5.xyz = u_xlat10_3.xyz * vec3(u_xlat16_25);
    u_xlat24 = dot(u_xlat0.xyz, u_xlat2.xyz);
    u_xlat26 = u_xlat24;
#ifdef UNITY_ADRENO_ES3
    u_xlat26 = min(max(u_xlat26, 0.0), 1.0);
#else
    u_xlat26 = clamp(u_xlat26, 0.0, 1.0);
#endif
    u_xlat24 = u_xlat24 + u_xlat24;
    u_xlat0.xyz = u_xlat2.xyz * (-vec3(u_xlat24)) + u_xlat0.xyz;
    u_xlat0.x = dot(u_xlat0.xyz, _WorldSpaceLightPos0.xyz);
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat4.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = texture(unity_NHxRoughness, u_xlat4.xz).w;
    u_xlat0.x = u_xlat0.x * 16.0;
    u_xlat16_25 = (-u_xlat26) + 1.0;
    u_xlat16_8 = u_xlat16_25 * u_xlat16_25;
    u_xlat16_8 = u_xlat16_25 * u_xlat16_8;
    u_xlat16_8 = u_xlat16_25 * u_xlat16_8;
    u_xlat16_25 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_29 = (-u_xlat16_25) + _Glossiness;
    u_xlat16_29 = u_xlat16_29 + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_29 = min(max(u_xlat16_29, 0.0), 1.0);
#else
    u_xlat16_29 = clamp(u_xlat16_29, 0.0, 1.0);
#endif
    u_xlat10_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_6.xyz = u_xlat10_2.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_2.xyz = u_xlat10_2.xyz * _Color.xyz;
    u_xlat16_6.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_6.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_7.xyz = vec3(u_xlat16_29) + (-u_xlat16_6.xyz);
    u_xlat16_7.xyz = vec3(u_xlat16_8) * u_xlat16_7.xyz + u_xlat16_6.xyz;
    u_xlat16_6.xyz = u_xlat0.xxx * u_xlat16_6.xyz;
    u_xlat16_6.xyz = u_xlat16_2.xyz * vec3(u_xlat16_25) + u_xlat16_6.xyz;
    u_xlat16_5.xyz = u_xlat16_5.xyz * u_xlat16_7.xyz;
    SV_Target0.xyz = u_xlat16_6.xyz * u_xlat16_1.xyz + u_xlat16_5.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD5;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD5 = vec4(0.0, 0.0, 0.0, 0.0);
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform lowp samplerCube unity_SpecCube0;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
lowp float u_xlat10_0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_2;
lowp vec3 u_xlat10_2;
vec3 u_xlat3;
mediump float u_xlat16_3;
lowp vec4 u_xlat10_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
vec3 u_xlat8;
mediump float u_xlat16_9;
mediump float u_xlat16_10;
mediump float u_xlat16_18;
float u_xlat24;
mediump float u_xlat16_25;
mediump float u_xlat16_26;
mediump float u_xlat16_28;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * vs_TEXCOORD2.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * vs_TEXCOORD2.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    vec3 txVec0 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat10_0 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_1.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_1.x = u_xlat10_0 * u_xlat16_1.x + _LightShadowData.x;
    u_xlat16_9 = (-u_xlat16_1.x) + 1.0;
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat2.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat8.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat2.x = dot(u_xlat8.xyz, u_xlat2.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat2.x);
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat0.x + u_xlat2.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_1.x = u_xlat0.x * u_xlat16_9 + u_xlat16_1.x;
    u_xlat16_1.xyz = u_xlat16_1.xxx * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat8.xyz, u_xlat8.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat2.xyz = u_xlat8.xyz * u_xlat0.xxx + _WorldSpaceLightPos0.xyz;
    u_xlat0.xyz = u_xlat0.xxx * u_xlat8.xyz;
    u_xlat24 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat24 = max(u_xlat24, 0.00100000005);
    u_xlat24 = inversesqrt(u_xlat24);
    u_xlat2.xyz = vec3(u_xlat24) * u_xlat2.xyz;
    u_xlat24 = dot(_WorldSpaceLightPos0.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat24 = min(max(u_xlat24, 0.0), 1.0);
#else
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
#endif
    u_xlat24 = max(u_xlat24, 0.319999993);
    u_xlat16_26 = (-_Glossiness) + 1.0;
    u_xlat16_3 = u_xlat16_26 * u_xlat16_26 + 1.5;
    u_xlat24 = u_xlat24 * u_xlat16_3;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat2.x = dot(u_xlat3.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat2.x = min(max(u_xlat2.x, 0.0), 1.0);
#else
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
#endif
    u_xlat2.x = u_xlat2.x * u_xlat2.x;
    u_xlat16_10 = u_xlat16_26 * u_xlat16_26;
    u_xlat16_18 = u_xlat16_10 * u_xlat16_10 + -1.0;
    u_xlat2.x = u_xlat2.x * u_xlat16_18 + 1.00001001;
    u_xlat24 = u_xlat24 * u_xlat2.x;
    u_xlat24 = u_xlat16_10 / u_xlat24;
    u_xlat16_25 = u_xlat16_26 * u_xlat16_10;
    u_xlat16_25 = (-u_xlat16_25) * 0.280000001 + 1.0;
    u_xlat24 = u_xlat24 + -9.99999975e-05;
    u_xlat24 = max(u_xlat24, 0.0);
    u_xlat24 = min(u_xlat24, 100.0);
    u_xlat10_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_2.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_2.xyz = u_xlat10_2.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat5.xyz = vec3(u_xlat24) * u_xlat16_4.xyz;
    u_xlat16_28 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat2.xyz = u_xlat16_2.xyz * vec3(u_xlat16_28) + u_xlat5.xyz;
    u_xlat16_28 = (-u_xlat16_28) + _Glossiness;
    u_xlat16_28 = u_xlat16_28 + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_28 = min(max(u_xlat16_28, 0.0), 1.0);
#else
    u_xlat16_28 = clamp(u_xlat16_28, 0.0, 1.0);
#endif
    u_xlat16_6.xyz = (-u_xlat16_4.xyz) + vec3(u_xlat16_28);
    u_xlat2.xyz = u_xlat16_1.xyz * u_xlat2.xyz;
    u_xlat16_1.x = (-u_xlat16_26) * 0.699999988 + 1.70000005;
    u_xlat16_1.x = u_xlat16_1.x * u_xlat16_26;
    u_xlat16_1.x = u_xlat16_1.x * 6.0;
    u_xlat16_9 = dot((-u_xlat0.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_9 = u_xlat16_9 + u_xlat16_9;
    u_xlat16_7.xyz = vs_TEXCOORD1.xyz * (-vec3(u_xlat16_9)) + (-u_xlat0.xyz);
    u_xlat0.x = dot(u_xlat3.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat8.x = dot(u_xlat3.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat8.x = min(max(u_xlat8.x, 0.0), 1.0);
#else
    u_xlat8.x = clamp(u_xlat8.x, 0.0, 1.0);
#endif
    u_xlat16_9 = (-u_xlat0.x) + 1.0;
    u_xlat16_9 = u_xlat16_9 * u_xlat16_9;
    u_xlat16_9 = u_xlat16_9 * u_xlat16_9;
    u_xlat16_4.xyz = vec3(u_xlat16_9) * u_xlat16_6.xyz + u_xlat16_4.xyz;
    u_xlat10_3 = textureLod(unity_SpecCube0, u_xlat16_7.xyz, u_xlat16_1.x);
    u_xlat16_1.x = u_xlat10_3.w + -1.0;
    u_xlat16_1.x = unity_SpecCube0_HDR.w * u_xlat16_1.x + 1.0;
    u_xlat16_1.x = u_xlat16_1.x * unity_SpecCube0_HDR.x;
    u_xlat16_1.xyz = u_xlat10_3.xyz * u_xlat16_1.xxx;
    u_xlat16_1.xyz = u_xlat16_1.xyz * vec3(u_xlat16_25);
    u_xlat16_1.xyz = u_xlat16_4.xyz * u_xlat16_1.xyz;
    u_xlat0.xyz = u_xlat2.xyz * u_xlat8.xxx + u_xlat16_1.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD5;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD5 = vec4(0.0, 0.0, 0.0, 0.0);
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform lowp samplerCube unity_SpecCube0;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
lowp float u_xlat10_0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_2;
lowp vec3 u_xlat10_2;
vec3 u_xlat3;
mediump float u_xlat16_3;
lowp vec4 u_xlat10_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
vec3 u_xlat8;
mediump float u_xlat16_9;
mediump float u_xlat16_10;
mediump float u_xlat16_18;
float u_xlat24;
mediump float u_xlat16_25;
mediump float u_xlat16_26;
mediump float u_xlat16_28;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * vs_TEXCOORD2.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * vs_TEXCOORD2.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    vec3 txVec0 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat10_0 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_1.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_1.x = u_xlat10_0 * u_xlat16_1.x + _LightShadowData.x;
    u_xlat16_9 = (-u_xlat16_1.x) + 1.0;
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat2.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat8.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat2.x = dot(u_xlat8.xyz, u_xlat2.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat2.x);
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat0.x + u_xlat2.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_1.x = u_xlat0.x * u_xlat16_9 + u_xlat16_1.x;
    u_xlat16_1.xyz = u_xlat16_1.xxx * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat8.xyz, u_xlat8.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat2.xyz = u_xlat8.xyz * u_xlat0.xxx + _WorldSpaceLightPos0.xyz;
    u_xlat0.xyz = u_xlat0.xxx * u_xlat8.xyz;
    u_xlat24 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat24 = max(u_xlat24, 0.00100000005);
    u_xlat24 = inversesqrt(u_xlat24);
    u_xlat2.xyz = vec3(u_xlat24) * u_xlat2.xyz;
    u_xlat24 = dot(_WorldSpaceLightPos0.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat24 = min(max(u_xlat24, 0.0), 1.0);
#else
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
#endif
    u_xlat24 = max(u_xlat24, 0.319999993);
    u_xlat16_26 = (-_Glossiness) + 1.0;
    u_xlat16_3 = u_xlat16_26 * u_xlat16_26 + 1.5;
    u_xlat24 = u_xlat24 * u_xlat16_3;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat2.x = dot(u_xlat3.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat2.x = min(max(u_xlat2.x, 0.0), 1.0);
#else
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
#endif
    u_xlat2.x = u_xlat2.x * u_xlat2.x;
    u_xlat16_10 = u_xlat16_26 * u_xlat16_26;
    u_xlat16_18 = u_xlat16_10 * u_xlat16_10 + -1.0;
    u_xlat2.x = u_xlat2.x * u_xlat16_18 + 1.00001001;
    u_xlat24 = u_xlat24 * u_xlat2.x;
    u_xlat24 = u_xlat16_10 / u_xlat24;
    u_xlat16_25 = u_xlat16_26 * u_xlat16_10;
    u_xlat16_25 = (-u_xlat16_25) * 0.280000001 + 1.0;
    u_xlat24 = u_xlat24 + -9.99999975e-05;
    u_xlat24 = max(u_xlat24, 0.0);
    u_xlat24 = min(u_xlat24, 100.0);
    u_xlat10_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_2.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_2.xyz = u_xlat10_2.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat5.xyz = vec3(u_xlat24) * u_xlat16_4.xyz;
    u_xlat16_28 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat2.xyz = u_xlat16_2.xyz * vec3(u_xlat16_28) + u_xlat5.xyz;
    u_xlat16_28 = (-u_xlat16_28) + _Glossiness;
    u_xlat16_28 = u_xlat16_28 + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_28 = min(max(u_xlat16_28, 0.0), 1.0);
#else
    u_xlat16_28 = clamp(u_xlat16_28, 0.0, 1.0);
#endif
    u_xlat16_6.xyz = (-u_xlat16_4.xyz) + vec3(u_xlat16_28);
    u_xlat2.xyz = u_xlat16_1.xyz * u_xlat2.xyz;
    u_xlat16_1.x = (-u_xlat16_26) * 0.699999988 + 1.70000005;
    u_xlat16_1.x = u_xlat16_1.x * u_xlat16_26;
    u_xlat16_1.x = u_xlat16_1.x * 6.0;
    u_xlat16_9 = dot((-u_xlat0.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_9 = u_xlat16_9 + u_xlat16_9;
    u_xlat16_7.xyz = vs_TEXCOORD1.xyz * (-vec3(u_xlat16_9)) + (-u_xlat0.xyz);
    u_xlat0.x = dot(u_xlat3.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat8.x = dot(u_xlat3.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat8.x = min(max(u_xlat8.x, 0.0), 1.0);
#else
    u_xlat8.x = clamp(u_xlat8.x, 0.0, 1.0);
#endif
    u_xlat16_9 = (-u_xlat0.x) + 1.0;
    u_xlat16_9 = u_xlat16_9 * u_xlat16_9;
    u_xlat16_9 = u_xlat16_9 * u_xlat16_9;
    u_xlat16_4.xyz = vec3(u_xlat16_9) * u_xlat16_6.xyz + u_xlat16_4.xyz;
    u_xlat10_3 = textureLod(unity_SpecCube0, u_xlat16_7.xyz, u_xlat16_1.x);
    u_xlat16_1.x = u_xlat10_3.w + -1.0;
    u_xlat16_1.x = unity_SpecCube0_HDR.w * u_xlat16_1.x + 1.0;
    u_xlat16_1.x = u_xlat16_1.x * unity_SpecCube0_HDR.x;
    u_xlat16_1.xyz = u_xlat10_3.xyz * u_xlat16_1.xxx;
    u_xlat16_1.xyz = u_xlat16_1.xyz * vec3(u_xlat16_25);
    u_xlat16_1.xyz = u_xlat16_4.xyz * u_xlat16_1.xyz;
    u_xlat0.xyz = u_xlat2.xyz * u_xlat8.xxx + u_xlat16_1.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

//////////////////////////////////////////////////////
Keywords set in this variant: DIRECTIONAL SHADOWS_SCREEN LIGHTPROBE_SH 
-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 75 math, 4 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform mediump vec4 unity_SHBr;
uniform mediump vec4 unity_SHBg;
uniform mediump vec4 unity_SHBb;
uniform mediump vec4 unity_SHC;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD5;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  highp vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_4;
  tmpvar_4[0] = unity_WorldToObject[0].xyz;
  tmpvar_4[1] = unity_WorldToObject[1].xyz;
  tmpvar_4[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_5;
  tmpvar_5 = normalize((_glesNormal * tmpvar_4));
  highp vec4 tmpvar_6;
  tmpvar_6 = (unity_ObjectToWorld * _glesVertex);
  tmpvar_1.xyz = tmpvar_6.xyz;
  mediump vec3 normal_7;
  normal_7 = tmpvar_5;
  mediump vec3 x1_8;
  mediump vec4 tmpvar_9;
  tmpvar_9 = (normal_7.xyzz * normal_7.yzzx);
  x1_8.x = dot (unity_SHBr, tmpvar_9);
  x1_8.y = dot (unity_SHBg, tmpvar_9);
  x1_8.z = dot (unity_SHBb, tmpvar_9);
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = tmpvar_5;
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD3 = (x1_8 + (unity_SHC.xyz * (
    (normal_7.x * normal_7.x)
   - 
    (normal_7.y * normal_7.y)
  )));
  xlv_TEXCOORD5 = (unity_WorldToShadow[0] * tmpvar_6);
  xlv_TEXCOORD6 = tmpvar_2;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 unity_SHAr;
uniform mediump vec4 unity_SHAg;
uniform mediump vec4 unity_SHAb;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform highp sampler2D _ShadowMapTexture;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD5;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump float tmpvar_2;
  highp vec4 tmpvar_3;
  mediump vec3 tmpvar_4;
  mediump vec3 tmpvar_5;
  lowp vec4 c_6;
  lowp float atten_7;
  lowp vec3 lightDir_8;
  mediump vec3 tmpvar_9;
  tmpvar_9 = _WorldSpaceLightPos0.xyz;
  lightDir_8 = tmpvar_9;
  highp vec3 tmpvar_10;
  highp vec3 tmpvar_11;
  tmpvar_11 = (_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz);
  tmpvar_10 = normalize(tmpvar_11);
  lowp vec3 tmpvar_12;
  lowp float tmpvar_13;
  lowp vec4 tmpvar_14;
  tmpvar_14 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_12 = tmpvar_14.xyz;
  tmpvar_13 = tmpvar_14.w;
  mediump float realtimeShadowAttenuation_15;
  highp vec4 v_16;
  v_16.x = unity_MatrixV[0].z;
  v_16.y = unity_MatrixV[1].z;
  v_16.z = unity_MatrixV[2].z;
  v_16.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_17;
  tmpvar_17 = (xlv_TEXCOORD2.xyz - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_18;
  highp float tmpvar_19;
  tmpvar_19 = clamp (((
    mix (dot (tmpvar_11, v_16.xyz), sqrt(dot (tmpvar_17, tmpvar_17)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_18 = tmpvar_19;
  lowp float tmpvar_20;
  highp float lightShadowDataX_21;
  mediump float tmpvar_22;
  tmpvar_22 = _LightShadowData.x;
  lightShadowDataX_21 = tmpvar_22;
  highp float tmpvar_23;
  tmpvar_23 = max (float((texture2D (_ShadowMapTexture, xlv_TEXCOORD5.xy).x > xlv_TEXCOORD5.z)), lightShadowDataX_21);
  tmpvar_20 = tmpvar_23;
  realtimeShadowAttenuation_15 = tmpvar_20;
  mediump float tmpvar_24;
  tmpvar_24 = mix (realtimeShadowAttenuation_15, 1.0, tmpvar_18);
  atten_7 = tmpvar_24;
  tmpvar_4 = _LightColor0.xyz;
  tmpvar_5 = lightDir_8;
  tmpvar_1 = tmpvar_10;
  tmpvar_2 = atten_7;
  tmpvar_3 = unity_SpecCube0_HDR;
  mediump vec3 Normal_25;
  Normal_25 = xlv_TEXCOORD1;
  mediump float tmpvar_26;
  highp float tmpvar_27;
  highp float smoothness_28;
  smoothness_28 = _Glossiness;
  tmpvar_27 = (1.0 - smoothness_28);
  tmpvar_26 = tmpvar_27;
  mediump vec3 I_29;
  I_29 = -(tmpvar_1);
  mediump vec3 normalWorld_30;
  normalWorld_30 = xlv_TEXCOORD1;
  mediump vec3 tmpvar_31;
  tmpvar_31 = (tmpvar_4 * tmpvar_2);
  mediump vec4 tmpvar_32;
  tmpvar_32.w = 1.0;
  tmpvar_32.xyz = normalWorld_30;
  mediump vec3 x_33;
  x_33.x = dot (unity_SHAr, tmpvar_32);
  x_33.y = dot (unity_SHAg, tmpvar_32);
  x_33.z = dot (unity_SHAb, tmpvar_32);
  mediump vec4 hdr_34;
  hdr_34 = tmpvar_3;
  mediump vec4 tmpvar_35;
  tmpvar_35.xyz = (I_29 - (2.0 * (
    dot (Normal_25, I_29)
   * Normal_25)));
  tmpvar_35.w = ((tmpvar_26 * (1.7 - 
    (0.7 * tmpvar_26)
  )) * 6.0);
  lowp vec4 tmpvar_36;
  tmpvar_36 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_35.xyz, tmpvar_35.w);
  mediump vec4 tmpvar_37;
  tmpvar_37 = tmpvar_36;
  tmpvar_4 = tmpvar_31;
  lowp vec3 tmpvar_38;
  mediump vec4 c_39;
  highp vec3 tmpvar_40;
  tmpvar_40 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_41;
  mediump vec3 albedo_42;
  albedo_42 = tmpvar_12;
  mediump vec3 tmpvar_43;
  tmpvar_43 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_42, vec3(_Metallic));
  mediump float tmpvar_44;
  tmpvar_44 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_41 = (albedo_42 * tmpvar_44);
  tmpvar_38 = tmpvar_41;
  mediump vec3 diffColor_45;
  diffColor_45 = tmpvar_38;
  mediump float alpha_46;
  alpha_46 = tmpvar_13;
  tmpvar_38 = diffColor_45;
  mediump vec3 diffColor_47;
  diffColor_47 = tmpvar_38;
  mediump vec3 color_48;
  mediump vec2 rlPow4AndFresnelTerm_49;
  mediump float tmpvar_50;
  highp float tmpvar_51;
  tmpvar_51 = clamp (dot (tmpvar_40, tmpvar_5), 0.0, 1.0);
  tmpvar_50 = tmpvar_51;
  mediump float tmpvar_52;
  highp float tmpvar_53;
  tmpvar_53 = clamp (dot (tmpvar_40, tmpvar_10), 0.0, 1.0);
  tmpvar_52 = tmpvar_53;
  highp vec2 tmpvar_54;
  tmpvar_54.x = dot ((tmpvar_10 - (2.0 * 
    (dot (tmpvar_40, tmpvar_10) * tmpvar_40)
  )), tmpvar_5);
  tmpvar_54.y = (1.0 - tmpvar_52);
  highp vec2 tmpvar_55;
  tmpvar_55 = ((tmpvar_54 * tmpvar_54) * (tmpvar_54 * tmpvar_54));
  rlPow4AndFresnelTerm_49 = tmpvar_55;
  mediump float tmpvar_56;
  tmpvar_56 = rlPow4AndFresnelTerm_49.x;
  mediump float specular_57;
  highp float smoothness_58;
  smoothness_58 = _Glossiness;
  highp vec2 tmpvar_59;
  tmpvar_59.x = tmpvar_56;
  tmpvar_59.y = (1.0 - smoothness_58);
  highp float tmpvar_60;
  tmpvar_60 = (texture2D (unity_NHxRoughness, tmpvar_59).w * 16.0);
  specular_57 = tmpvar_60;
  color_48 = ((diffColor_47 + (specular_57 * tmpvar_43)) * (tmpvar_31 * tmpvar_50));
  color_48 = (color_48 + ((
    max (((1.055 * pow (
      max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD3 + x_33))
    , vec3(0.4166667, 0.4166667, 0.4166667))) - 0.055), vec3(0.0, 0.0, 0.0))
   * diffColor_47) + (
    ((hdr_34.x * ((hdr_34.w * 
      (tmpvar_37.w - 1.0)
    ) + 1.0)) * tmpvar_37.xyz)
   * 
    mix (tmpvar_43, vec3(clamp ((_Glossiness + (1.0 - tmpvar_44)), 0.0, 1.0)), rlPow4AndFresnelTerm_49.yyy)
  )));
  mediump vec4 tmpvar_61;
  tmpvar_61.w = 1.0;
  tmpvar_61.xyz = color_48;
  c_39.xyz = tmpvar_61.xyz;
  c_39.w = alpha_46;
  c_6.xyz = c_39.xyz;
  c_6.w = 1.0;
  gl_FragData[0] = c_6;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 95 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform mediump vec4 unity_SHBr;
uniform mediump vec4 unity_SHBg;
uniform mediump vec4 unity_SHBb;
uniform mediump vec4 unity_SHC;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD5;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  highp vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_4;
  tmpvar_4[0] = unity_WorldToObject[0].xyz;
  tmpvar_4[1] = unity_WorldToObject[1].xyz;
  tmpvar_4[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_5;
  tmpvar_5 = normalize((_glesNormal * tmpvar_4));
  highp vec4 tmpvar_6;
  tmpvar_6 = (unity_ObjectToWorld * _glesVertex);
  tmpvar_1.xyz = tmpvar_6.xyz;
  mediump vec3 normal_7;
  normal_7 = tmpvar_5;
  mediump vec3 x1_8;
  mediump vec4 tmpvar_9;
  tmpvar_9 = (normal_7.xyzz * normal_7.yzzx);
  x1_8.x = dot (unity_SHBr, tmpvar_9);
  x1_8.y = dot (unity_SHBg, tmpvar_9);
  x1_8.z = dot (unity_SHBb, tmpvar_9);
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = tmpvar_5;
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD3 = (x1_8 + (unity_SHC.xyz * (
    (normal_7.x * normal_7.x)
   - 
    (normal_7.y * normal_7.y)
  )));
  xlv_TEXCOORD5 = (unity_WorldToShadow[0] * tmpvar_6);
  xlv_TEXCOORD6 = tmpvar_2;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 unity_SHAr;
uniform mediump vec4 unity_SHAg;
uniform mediump vec4 unity_SHAb;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _ShadowMapTexture;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD5;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump float tmpvar_2;
  highp vec4 tmpvar_3;
  mediump vec3 tmpvar_4;
  mediump vec3 tmpvar_5;
  lowp vec4 c_6;
  lowp float atten_7;
  lowp vec3 lightDir_8;
  mediump vec3 tmpvar_9;
  tmpvar_9 = _WorldSpaceLightPos0.xyz;
  lightDir_8 = tmpvar_9;
  highp vec3 tmpvar_10;
  highp vec3 tmpvar_11;
  tmpvar_11 = (_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz);
  tmpvar_10 = normalize(tmpvar_11);
  lowp vec3 tmpvar_12;
  lowp float tmpvar_13;
  lowp vec4 tmpvar_14;
  tmpvar_14 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_12 = tmpvar_14.xyz;
  tmpvar_13 = tmpvar_14.w;
  mediump float realtimeShadowAttenuation_15;
  highp vec4 v_16;
  v_16.x = unity_MatrixV[0].z;
  v_16.y = unity_MatrixV[1].z;
  v_16.z = unity_MatrixV[2].z;
  v_16.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_17;
  tmpvar_17 = (xlv_TEXCOORD2.xyz - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_18;
  highp float tmpvar_19;
  tmpvar_19 = clamp (((
    mix (dot (tmpvar_11, v_16.xyz), sqrt(dot (tmpvar_17, tmpvar_17)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_18 = tmpvar_19;
  lowp float tmpvar_20;
  highp float lightShadowDataX_21;
  mediump float tmpvar_22;
  tmpvar_22 = _LightShadowData.x;
  lightShadowDataX_21 = tmpvar_22;
  highp float tmpvar_23;
  tmpvar_23 = max (float((texture2D (_ShadowMapTexture, xlv_TEXCOORD5.xy).x > xlv_TEXCOORD5.z)), lightShadowDataX_21);
  tmpvar_20 = tmpvar_23;
  realtimeShadowAttenuation_15 = tmpvar_20;
  mediump float tmpvar_24;
  tmpvar_24 = mix (realtimeShadowAttenuation_15, 1.0, tmpvar_18);
  atten_7 = tmpvar_24;
  tmpvar_4 = _LightColor0.xyz;
  tmpvar_5 = lightDir_8;
  tmpvar_1 = tmpvar_10;
  tmpvar_2 = atten_7;
  tmpvar_3 = unity_SpecCube0_HDR;
  mediump vec3 Normal_25;
  Normal_25 = xlv_TEXCOORD1;
  mediump float tmpvar_26;
  highp float tmpvar_27;
  highp float smoothness_28;
  smoothness_28 = _Glossiness;
  tmpvar_27 = (1.0 - smoothness_28);
  tmpvar_26 = tmpvar_27;
  mediump vec3 I_29;
  I_29 = -(tmpvar_1);
  mediump vec3 normalWorld_30;
  normalWorld_30 = xlv_TEXCOORD1;
  mediump vec3 tmpvar_31;
  mediump vec3 tmpvar_32;
  mediump vec3 tmpvar_33;
  tmpvar_32 = (tmpvar_4 * tmpvar_2);
  mediump vec4 tmpvar_34;
  tmpvar_34.w = 1.0;
  tmpvar_34.xyz = normalWorld_30;
  mediump vec3 x_35;
  x_35.x = dot (unity_SHAr, tmpvar_34);
  x_35.y = dot (unity_SHAg, tmpvar_34);
  x_35.z = dot (unity_SHAb, tmpvar_34);
  tmpvar_33 = max (((1.055 * 
    pow (max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD3 + x_35)), vec3(0.4166667, 0.4166667, 0.4166667))
  ) - 0.055), vec3(0.0, 0.0, 0.0));
  mediump vec4 hdr_36;
  hdr_36 = tmpvar_3;
  mediump vec4 tmpvar_37;
  tmpvar_37.xyz = (I_29 - (2.0 * (
    dot (Normal_25, I_29)
   * Normal_25)));
  tmpvar_37.w = ((tmpvar_26 * (1.7 - 
    (0.7 * tmpvar_26)
  )) * 6.0);
  lowp vec4 tmpvar_38;
  tmpvar_38 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_37.xyz, tmpvar_37.w);
  mediump vec4 tmpvar_39;
  tmpvar_39 = tmpvar_38;
  tmpvar_31 = ((hdr_36.x * (
    (hdr_36.w * (tmpvar_39.w - 1.0))
   + 1.0)) * tmpvar_39.xyz);
  tmpvar_4 = tmpvar_32;
  lowp vec3 tmpvar_40;
  mediump vec4 c_41;
  highp vec3 tmpvar_42;
  tmpvar_42 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_43;
  mediump vec3 albedo_44;
  albedo_44 = tmpvar_12;
  mediump vec3 tmpvar_45;
  tmpvar_45 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_44, vec3(_Metallic));
  mediump float tmpvar_46;
  tmpvar_46 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_43 = (albedo_44 * tmpvar_46);
  tmpvar_40 = tmpvar_43;
  mediump vec3 diffColor_47;
  diffColor_47 = tmpvar_40;
  mediump float alpha_48;
  alpha_48 = tmpvar_13;
  tmpvar_40 = diffColor_47;
  mediump vec3 diffColor_49;
  diffColor_49 = tmpvar_40;
  mediump vec3 color_50;
  mediump float surfaceReduction_51;
  highp float specularTerm_52;
  highp float a2_53;
  mediump float roughness_54;
  mediump float perceptualRoughness_55;
  highp vec3 tmpvar_56;
  highp vec3 inVec_57;
  inVec_57 = (tmpvar_5 + tmpvar_10);
  tmpvar_56 = (inVec_57 * inversesqrt(max (0.001, 
    dot (inVec_57, inVec_57)
  )));
  mediump float tmpvar_58;
  highp float tmpvar_59;
  tmpvar_59 = clamp (dot (tmpvar_42, tmpvar_5), 0.0, 1.0);
  tmpvar_58 = tmpvar_59;
  highp float tmpvar_60;
  tmpvar_60 = clamp (dot (tmpvar_42, tmpvar_56), 0.0, 1.0);
  mediump float tmpvar_61;
  highp float tmpvar_62;
  tmpvar_62 = clamp (dot (tmpvar_42, tmpvar_10), 0.0, 1.0);
  tmpvar_61 = tmpvar_62;
  highp float tmpvar_63;
  highp float smoothness_64;
  smoothness_64 = _Glossiness;
  tmpvar_63 = (1.0 - smoothness_64);
  perceptualRoughness_55 = tmpvar_63;
  highp float tmpvar_65;
  highp float perceptualRoughness_66;
  perceptualRoughness_66 = perceptualRoughness_55;
  tmpvar_65 = (perceptualRoughness_66 * perceptualRoughness_66);
  roughness_54 = tmpvar_65;
  mediump float tmpvar_67;
  tmpvar_67 = (roughness_54 * roughness_54);
  a2_53 = tmpvar_67;
  specularTerm_52 = ((roughness_54 / (
    (max (0.32, clamp (dot (tmpvar_5, tmpvar_56), 0.0, 1.0)) * (1.5 + roughness_54))
   * 
    (((tmpvar_60 * tmpvar_60) * (a2_53 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_68;
  tmpvar_68 = clamp (specularTerm_52, 0.0, 100.0);
  specularTerm_52 = tmpvar_68;
  surfaceReduction_51 = (1.0 - ((roughness_54 * perceptualRoughness_55) * 0.28));
  mediump float x_69;
  x_69 = (1.0 - tmpvar_61);
  mediump vec3 tmpvar_70;
  tmpvar_70 = mix (tmpvar_45, vec3(clamp ((_Glossiness + 
    (1.0 - tmpvar_46)
  ), 0.0, 1.0)), vec3(((x_69 * x_69) * (x_69 * x_69))));
  highp vec3 tmpvar_71;
  tmpvar_71 = (((
    ((diffColor_49 + (tmpvar_68 * tmpvar_45)) * tmpvar_32)
   * tmpvar_58) + (tmpvar_33 * diffColor_49)) + ((surfaceReduction_51 * tmpvar_31) * tmpvar_70));
  color_50 = tmpvar_71;
  mediump vec4 tmpvar_72;
  tmpvar_72.w = 1.0;
  tmpvar_72.xyz = color_50;
  c_41.xyz = tmpvar_72.xyz;
  c_41.w = alpha_48;
  c_6.xyz = c_41.xyz;
  c_6.w = 1.0;
  gl_FragData[0] = c_6;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 95 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform mediump vec4 unity_SHBr;
uniform mediump vec4 unity_SHBg;
uniform mediump vec4 unity_SHBb;
uniform mediump vec4 unity_SHC;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD5;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  highp vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_4;
  tmpvar_4[0] = unity_WorldToObject[0].xyz;
  tmpvar_4[1] = unity_WorldToObject[1].xyz;
  tmpvar_4[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_5;
  tmpvar_5 = normalize((_glesNormal * tmpvar_4));
  highp vec4 tmpvar_6;
  tmpvar_6 = (unity_ObjectToWorld * _glesVertex);
  tmpvar_1.xyz = tmpvar_6.xyz;
  mediump vec3 normal_7;
  normal_7 = tmpvar_5;
  mediump vec3 x1_8;
  mediump vec4 tmpvar_9;
  tmpvar_9 = (normal_7.xyzz * normal_7.yzzx);
  x1_8.x = dot (unity_SHBr, tmpvar_9);
  x1_8.y = dot (unity_SHBg, tmpvar_9);
  x1_8.z = dot (unity_SHBb, tmpvar_9);
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = tmpvar_5;
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD3 = (x1_8 + (unity_SHC.xyz * (
    (normal_7.x * normal_7.x)
   - 
    (normal_7.y * normal_7.y)
  )));
  xlv_TEXCOORD5 = (unity_WorldToShadow[0] * tmpvar_6);
  xlv_TEXCOORD6 = tmpvar_2;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 unity_SHAr;
uniform mediump vec4 unity_SHAg;
uniform mediump vec4 unity_SHAb;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _ShadowMapTexture;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD5;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump float tmpvar_2;
  highp vec4 tmpvar_3;
  mediump vec3 tmpvar_4;
  mediump vec3 tmpvar_5;
  lowp vec4 c_6;
  lowp float atten_7;
  lowp vec3 lightDir_8;
  mediump vec3 tmpvar_9;
  tmpvar_9 = _WorldSpaceLightPos0.xyz;
  lightDir_8 = tmpvar_9;
  highp vec3 tmpvar_10;
  highp vec3 tmpvar_11;
  tmpvar_11 = (_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz);
  tmpvar_10 = normalize(tmpvar_11);
  lowp vec3 tmpvar_12;
  lowp float tmpvar_13;
  lowp vec4 tmpvar_14;
  tmpvar_14 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_12 = tmpvar_14.xyz;
  tmpvar_13 = tmpvar_14.w;
  mediump float realtimeShadowAttenuation_15;
  highp vec4 v_16;
  v_16.x = unity_MatrixV[0].z;
  v_16.y = unity_MatrixV[1].z;
  v_16.z = unity_MatrixV[2].z;
  v_16.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_17;
  tmpvar_17 = (xlv_TEXCOORD2.xyz - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_18;
  highp float tmpvar_19;
  tmpvar_19 = clamp (((
    mix (dot (tmpvar_11, v_16.xyz), sqrt(dot (tmpvar_17, tmpvar_17)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_18 = tmpvar_19;
  lowp float tmpvar_20;
  highp float lightShadowDataX_21;
  mediump float tmpvar_22;
  tmpvar_22 = _LightShadowData.x;
  lightShadowDataX_21 = tmpvar_22;
  highp float tmpvar_23;
  tmpvar_23 = max (float((texture2D (_ShadowMapTexture, xlv_TEXCOORD5.xy).x > xlv_TEXCOORD5.z)), lightShadowDataX_21);
  tmpvar_20 = tmpvar_23;
  realtimeShadowAttenuation_15 = tmpvar_20;
  mediump float tmpvar_24;
  tmpvar_24 = mix (realtimeShadowAttenuation_15, 1.0, tmpvar_18);
  atten_7 = tmpvar_24;
  tmpvar_4 = _LightColor0.xyz;
  tmpvar_5 = lightDir_8;
  tmpvar_1 = tmpvar_10;
  tmpvar_2 = atten_7;
  tmpvar_3 = unity_SpecCube0_HDR;
  mediump vec3 Normal_25;
  Normal_25 = xlv_TEXCOORD1;
  mediump float tmpvar_26;
  highp float tmpvar_27;
  highp float smoothness_28;
  smoothness_28 = _Glossiness;
  tmpvar_27 = (1.0 - smoothness_28);
  tmpvar_26 = tmpvar_27;
  mediump vec3 I_29;
  I_29 = -(tmpvar_1);
  mediump vec3 normalWorld_30;
  normalWorld_30 = xlv_TEXCOORD1;
  mediump vec3 tmpvar_31;
  mediump vec3 tmpvar_32;
  mediump vec3 tmpvar_33;
  tmpvar_32 = (tmpvar_4 * tmpvar_2);
  mediump vec4 tmpvar_34;
  tmpvar_34.w = 1.0;
  tmpvar_34.xyz = normalWorld_30;
  mediump vec3 x_35;
  x_35.x = dot (unity_SHAr, tmpvar_34);
  x_35.y = dot (unity_SHAg, tmpvar_34);
  x_35.z = dot (unity_SHAb, tmpvar_34);
  tmpvar_33 = max (((1.055 * 
    pow (max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD3 + x_35)), vec3(0.4166667, 0.4166667, 0.4166667))
  ) - 0.055), vec3(0.0, 0.0, 0.0));
  mediump vec4 hdr_36;
  hdr_36 = tmpvar_3;
  mediump vec4 tmpvar_37;
  tmpvar_37.xyz = (I_29 - (2.0 * (
    dot (Normal_25, I_29)
   * Normal_25)));
  tmpvar_37.w = ((tmpvar_26 * (1.7 - 
    (0.7 * tmpvar_26)
  )) * 6.0);
  lowp vec4 tmpvar_38;
  tmpvar_38 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_37.xyz, tmpvar_37.w);
  mediump vec4 tmpvar_39;
  tmpvar_39 = tmpvar_38;
  tmpvar_31 = ((hdr_36.x * (
    (hdr_36.w * (tmpvar_39.w - 1.0))
   + 1.0)) * tmpvar_39.xyz);
  tmpvar_4 = tmpvar_32;
  lowp vec3 tmpvar_40;
  mediump vec4 c_41;
  highp vec3 tmpvar_42;
  tmpvar_42 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_43;
  mediump vec3 albedo_44;
  albedo_44 = tmpvar_12;
  mediump vec3 tmpvar_45;
  tmpvar_45 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_44, vec3(_Metallic));
  mediump float tmpvar_46;
  tmpvar_46 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_43 = (albedo_44 * tmpvar_46);
  tmpvar_40 = tmpvar_43;
  mediump vec3 diffColor_47;
  diffColor_47 = tmpvar_40;
  mediump float alpha_48;
  alpha_48 = tmpvar_13;
  tmpvar_40 = diffColor_47;
  mediump vec3 diffColor_49;
  diffColor_49 = tmpvar_40;
  mediump vec3 color_50;
  mediump float surfaceReduction_51;
  highp float specularTerm_52;
  highp float a2_53;
  mediump float roughness_54;
  mediump float perceptualRoughness_55;
  highp vec3 tmpvar_56;
  highp vec3 inVec_57;
  inVec_57 = (tmpvar_5 + tmpvar_10);
  tmpvar_56 = (inVec_57 * inversesqrt(max (0.001, 
    dot (inVec_57, inVec_57)
  )));
  mediump float tmpvar_58;
  highp float tmpvar_59;
  tmpvar_59 = clamp (dot (tmpvar_42, tmpvar_5), 0.0, 1.0);
  tmpvar_58 = tmpvar_59;
  highp float tmpvar_60;
  tmpvar_60 = clamp (dot (tmpvar_42, tmpvar_56), 0.0, 1.0);
  mediump float tmpvar_61;
  highp float tmpvar_62;
  tmpvar_62 = clamp (dot (tmpvar_42, tmpvar_10), 0.0, 1.0);
  tmpvar_61 = tmpvar_62;
  highp float tmpvar_63;
  highp float smoothness_64;
  smoothness_64 = _Glossiness;
  tmpvar_63 = (1.0 - smoothness_64);
  perceptualRoughness_55 = tmpvar_63;
  highp float tmpvar_65;
  highp float perceptualRoughness_66;
  perceptualRoughness_66 = perceptualRoughness_55;
  tmpvar_65 = (perceptualRoughness_66 * perceptualRoughness_66);
  roughness_54 = tmpvar_65;
  mediump float tmpvar_67;
  tmpvar_67 = (roughness_54 * roughness_54);
  a2_53 = tmpvar_67;
  specularTerm_52 = ((roughness_54 / (
    (max (0.32, clamp (dot (tmpvar_5, tmpvar_56), 0.0, 1.0)) * (1.5 + roughness_54))
   * 
    (((tmpvar_60 * tmpvar_60) * (a2_53 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_68;
  tmpvar_68 = clamp (specularTerm_52, 0.0, 100.0);
  specularTerm_52 = tmpvar_68;
  surfaceReduction_51 = (1.0 - ((roughness_54 * perceptualRoughness_55) * 0.28));
  mediump float x_69;
  x_69 = (1.0 - tmpvar_61);
  mediump vec3 tmpvar_70;
  tmpvar_70 = mix (tmpvar_45, vec3(clamp ((_Glossiness + 
    (1.0 - tmpvar_46)
  ), 0.0, 1.0)), vec3(((x_69 * x_69) * (x_69 * x_69))));
  highp vec3 tmpvar_71;
  tmpvar_71 = (((
    ((diffColor_49 + (tmpvar_68 * tmpvar_45)) * tmpvar_32)
   * tmpvar_58) + (tmpvar_33 * diffColor_49)) + ((surfaceReduction_51 * tmpvar_31) * tmpvar_70));
  color_50 = tmpvar_71;
  mediump vec4 tmpvar_72;
  tmpvar_72.w = 1.0;
  tmpvar_72.xyz = color_50;
  c_41.xyz = tmpvar_72.xyz;
  c_41.w = alpha_48;
  c_6.xyz = c_41.xyz;
  c_6.w = 1.0;
  gl_FragData[0] = c_6;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out mediump vec3 vs_TEXCOORD3;
out highp vec4 vs_TEXCOORD5;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat12;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    vs_TEXCOORD1.xyz = u_xlat0.xyz;
    u_xlat16_2 = u_xlat0.y * u_xlat0.y;
    u_xlat16_2 = u_xlat0.x * u_xlat0.x + (-u_xlat16_2);
    u_xlat16_0 = u_xlat0.yzzx * u_xlat0.xyzz;
    u_xlat16_3.x = dot(unity_SHBr, u_xlat16_0);
    u_xlat16_3.y = dot(unity_SHBg, u_xlat16_0);
    u_xlat16_3.z = dot(unity_SHBb, u_xlat16_0);
    vs_TEXCOORD3.xyz = unity_SHC.xyz * vec3(u_xlat16_2) + u_xlat16_3.xyz;
    vs_TEXCOORD5 = vec4(0.0, 0.0, 0.0, 0.0);
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp samplerCube unity_SpecCube0;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
in mediump vec3 vs_TEXCOORD3;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump vec3 u_xlat16_0;
mediump vec3 u_xlat16_1;
lowp vec4 u_xlat10_1;
vec3 u_xlat2;
vec3 u_xlat3;
vec3 u_xlat4;
mediump vec3 u_xlat16_4;
lowp vec3 u_xlat10_4;
mediump vec3 u_xlat16_5;
vec3 u_xlat6;
mediump vec3 u_xlat16_7;
mediump vec3 u_xlat16_8;
mediump vec3 u_xlat16_9;
float u_xlat10;
lowp float u_xlat10_10;
mediump vec3 u_xlat16_11;
float u_xlat12;
mediump float u_xlat16_12;
mediump vec3 u_xlat16_17;
float u_xlat30;
float u_xlat32;
mediump float u_xlat16_35;
mediump float u_xlat16_37;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD1.xyz;
    u_xlat0.w = 1.0;
    u_xlat16_1.x = dot(unity_SHAr, u_xlat0);
    u_xlat16_1.y = dot(unity_SHAg, u_xlat0);
    u_xlat16_1.z = dot(unity_SHAb, u_xlat0);
    u_xlat16_1.xyz = u_xlat16_1.xyz + vs_TEXCOORD3.xyz;
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_0.xyz = log2(u_xlat16_1.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_0.xyz = exp2(u_xlat16_0.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_0.xyz = max(u_xlat16_0.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat2.z = (-_Glossiness) + 1.0;
    u_xlat16_1.x = (-u_xlat2.z) * 0.699999988 + 1.70000005;
    u_xlat16_1.x = u_xlat16_1.x * u_xlat2.z;
    u_xlat16_1.x = u_xlat16_1.x * 6.0;
    u_xlat3.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat30 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat30 = inversesqrt(u_xlat30);
    u_xlat4.xyz = vec3(u_xlat30) * u_xlat3.xyz;
    u_xlat16_11.x = dot((-u_xlat4.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_11.x = u_xlat16_11.x + u_xlat16_11.x;
    u_xlat16_11.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_11.xxx) + (-u_xlat4.xyz);
    u_xlat10_1 = textureLod(unity_SpecCube0, u_xlat16_11.xyz, u_xlat16_1.x);
    u_xlat16_5.x = u_xlat10_1.w + -1.0;
    u_xlat16_5.x = unity_SpecCube0_HDR.w * u_xlat16_5.x + 1.0;
    u_xlat16_5.x = u_xlat16_5.x * unity_SpecCube0_HDR.x;
    u_xlat16_5.xyz = u_xlat10_1.xyz * u_xlat16_5.xxx;
    u_xlat30 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat30 = inversesqrt(u_xlat30);
    u_xlat6.xyz = vec3(u_xlat30) * vs_TEXCOORD1.xyz;
    u_xlat30 = dot(u_xlat4.xyz, u_xlat6.xyz);
    u_xlat12 = u_xlat30;
#ifdef UNITY_ADRENO_ES3
    u_xlat12 = min(max(u_xlat12, 0.0), 1.0);
#else
    u_xlat12 = clamp(u_xlat12, 0.0, 1.0);
#endif
    u_xlat30 = u_xlat30 + u_xlat30;
    u_xlat4.xyz = u_xlat6.xyz * (-vec3(u_xlat30)) + u_xlat4.xyz;
    u_xlat30 = dot(u_xlat6.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat30 = min(max(u_xlat30, 0.0), 1.0);
#else
    u_xlat30 = clamp(u_xlat30, 0.0, 1.0);
#endif
    u_xlat32 = dot(u_xlat4.xyz, _WorldSpaceLightPos0.xyz);
    u_xlat32 = u_xlat32 * u_xlat32;
    u_xlat2.x = u_xlat32 * u_xlat32;
    u_xlat2.x = texture(unity_NHxRoughness, u_xlat2.xz).w;
    u_xlat2.x = u_xlat2.x * 16.0;
    u_xlat16_35 = (-u_xlat12) + 1.0;
    u_xlat16_12 = u_xlat16_35 * u_xlat16_35;
    u_xlat16_12 = u_xlat16_35 * u_xlat16_12;
    u_xlat16_12 = u_xlat16_35 * u_xlat16_12;
    u_xlat16_35 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_7.x = (-u_xlat16_35) + _Glossiness;
    u_xlat16_7.x = u_xlat16_7.x + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_7.x = min(max(u_xlat16_7.x, 0.0), 1.0);
#else
    u_xlat16_7.x = clamp(u_xlat16_7.x, 0.0, 1.0);
#endif
    u_xlat10_4.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_17.xyz = u_xlat10_4.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_4.xyz = u_xlat10_4.xyz * _Color.xyz;
    u_xlat16_8.xyz = vec3(u_xlat16_35) * u_xlat16_4.xyz;
    u_xlat16_17.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_17.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_9.xyz = (-u_xlat16_17.xyz) + u_xlat16_7.xxx;
    u_xlat16_9.xyz = vec3(u_xlat16_12) * u_xlat16_9.xyz + u_xlat16_17.xyz;
    u_xlat16_7.xyz = u_xlat2.xxx * u_xlat16_17.xyz + u_xlat16_8.xyz;
    u_xlat16_5.xyz = u_xlat16_5.xyz * u_xlat16_9.xyz;
    u_xlat16_5.xyz = u_xlat16_0.xyz * u_xlat16_8.xyz + u_xlat16_5.xyz;
    u_xlat0.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat0.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat0.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat0.x = dot(u_xlat3.xyz, u_xlat0.xyz);
    u_xlat2.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat10 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat10 = sqrt(u_xlat10);
    u_xlat10 = (-u_xlat0.x) + u_xlat10;
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat10 + u_xlat0.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat2.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat2.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * vs_TEXCOORD2.xxx + u_xlat2.xyz;
    u_xlat2.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * vs_TEXCOORD2.zzz + u_xlat2.xyz;
    u_xlat2.xyz = u_xlat2.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    vec3 txVec0 = vec3(u_xlat2.xy,u_xlat2.z);
    u_xlat10_10 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_35 = (-_LightShadowData.x) + 1.0;
    u_xlat16_35 = u_xlat10_10 * u_xlat16_35 + _LightShadowData.x;
    u_xlat16_37 = (-u_xlat16_35) + 1.0;
    u_xlat16_35 = u_xlat0.x * u_xlat16_37 + u_xlat16_35;
    u_xlat16_8.xyz = vec3(u_xlat16_35) * _LightColor0.xyz;
    u_xlat16_8.xyz = vec3(u_xlat30) * u_xlat16_8.xyz;
    SV_Target0.xyz = u_xlat16_7.xyz * u_xlat16_8.xyz + u_xlat16_5.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out mediump vec3 vs_TEXCOORD3;
out highp vec4 vs_TEXCOORD5;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat12;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    vs_TEXCOORD1.xyz = u_xlat0.xyz;
    u_xlat16_2 = u_xlat0.y * u_xlat0.y;
    u_xlat16_2 = u_xlat0.x * u_xlat0.x + (-u_xlat16_2);
    u_xlat16_0 = u_xlat0.yzzx * u_xlat0.xyzz;
    u_xlat16_3.x = dot(unity_SHBr, u_xlat16_0);
    u_xlat16_3.y = dot(unity_SHBg, u_xlat16_0);
    u_xlat16_3.z = dot(unity_SHBb, u_xlat16_0);
    vs_TEXCOORD3.xyz = unity_SHC.xyz * vec3(u_xlat16_2) + u_xlat16_3.xyz;
    vs_TEXCOORD5 = vec4(0.0, 0.0, 0.0, 0.0);
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform lowp samplerCube unity_SpecCube0;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
in mediump vec3 vs_TEXCOORD3;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump vec3 u_xlat16_0;
lowp vec4 u_xlat10_0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
lowp vec3 u_xlat10_2;
vec3 u_xlat3;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
vec3 u_xlat8;
mediump float u_xlat16_10;
mediump vec3 u_xlat16_12;
mediump float u_xlat16_18;
float u_xlat24;
mediump float u_xlat16_25;
mediump float u_xlat16_26;
mediump float u_xlat16_28;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD1.xyz;
    u_xlat0.w = 1.0;
    u_xlat16_1.x = dot(unity_SHAr, u_xlat0);
    u_xlat16_1.y = dot(unity_SHAg, u_xlat0);
    u_xlat16_1.z = dot(unity_SHAb, u_xlat0);
    u_xlat16_1.xyz = u_xlat16_1.xyz + vs_TEXCOORD3.xyz;
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_0.xyz = log2(u_xlat16_1.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_0.xyz = exp2(u_xlat16_0.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_0.xyz = max(u_xlat16_0.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat10_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_2.xyz * _Color.xyz;
    u_xlat16_1.xyz = u_xlat10_2.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_1.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_25 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_4.xyz = vec3(u_xlat16_25) * u_xlat16_3.xyz;
    u_xlat16_25 = (-u_xlat16_25) + _Glossiness;
    u_xlat16_25 = u_xlat16_25 + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_25 = min(max(u_xlat16_25, 0.0), 1.0);
#else
    u_xlat16_25 = clamp(u_xlat16_25, 0.0, 1.0);
#endif
    u_xlat16_5.xyz = (-u_xlat16_1.xyz) + vec3(u_xlat16_25);
    u_xlat16_6.xyz = u_xlat16_0.xyz * u_xlat16_4.xyz;
    u_xlat0.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * vs_TEXCOORD2.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * vs_TEXCOORD2.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    vec3 txVec0 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat10_0.x = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_25 = (-_LightShadowData.x) + 1.0;
    u_xlat16_25 = u_xlat10_0.x * u_xlat16_25 + _LightShadowData.x;
    u_xlat16_28 = (-u_xlat16_25) + 1.0;
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat2.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat8.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat2.x = dot(u_xlat8.xyz, u_xlat2.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat2.x);
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat0.x + u_xlat2.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_25 = u_xlat0.x * u_xlat16_28 + u_xlat16_25;
    u_xlat16_7.xyz = vec3(u_xlat16_25) * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat8.xyz, u_xlat8.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat2.xyz = u_xlat8.xyz * u_xlat0.xxx + _WorldSpaceLightPos0.xyz;
    u_xlat0.xyz = u_xlat0.xxx * u_xlat8.xyz;
    u_xlat24 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat24 = max(u_xlat24, 0.00100000005);
    u_xlat24 = inversesqrt(u_xlat24);
    u_xlat2.xyz = vec3(u_xlat24) * u_xlat2.xyz;
    u_xlat24 = dot(_WorldSpaceLightPos0.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat24 = min(max(u_xlat24, 0.0), 1.0);
#else
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
#endif
    u_xlat24 = max(u_xlat24, 0.319999993);
    u_xlat16_26 = (-_Glossiness) + 1.0;
    u_xlat16_3.x = u_xlat16_26 * u_xlat16_26 + 1.5;
    u_xlat24 = u_xlat24 * u_xlat16_3.x;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat2.x = dot(u_xlat3.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat2.x = min(max(u_xlat2.x, 0.0), 1.0);
#else
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
#endif
    u_xlat2.x = u_xlat2.x * u_xlat2.x;
    u_xlat16_10 = u_xlat16_26 * u_xlat16_26;
    u_xlat16_18 = u_xlat16_10 * u_xlat16_10 + -1.0;
    u_xlat2.x = u_xlat2.x * u_xlat16_18 + 1.00001001;
    u_xlat24 = u_xlat24 * u_xlat2.x;
    u_xlat24 = u_xlat16_10 / u_xlat24;
    u_xlat16_25 = u_xlat16_26 * u_xlat16_10;
    u_xlat16_25 = (-u_xlat16_25) * 0.280000001 + 1.0;
    u_xlat24 = u_xlat24 + -9.99999975e-05;
    u_xlat24 = max(u_xlat24, 0.0);
    u_xlat24 = min(u_xlat24, 100.0);
    u_xlat2.xyz = vec3(u_xlat24) * u_xlat16_1.xyz + u_xlat16_4.xyz;
    u_xlat2.xyz = u_xlat16_7.xyz * u_xlat2.xyz;
    u_xlat24 = dot(u_xlat3.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat24 = min(max(u_xlat24, 0.0), 1.0);
#else
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
#endif
    u_xlat3.x = dot(u_xlat3.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat3.x = min(max(u_xlat3.x, 0.0), 1.0);
#else
    u_xlat3.x = clamp(u_xlat3.x, 0.0, 1.0);
#endif
    u_xlat16_4.x = (-u_xlat3.x) + 1.0;
    u_xlat16_4.x = u_xlat16_4.x * u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_4.x * u_xlat16_4.x;
    u_xlat16_1.xyz = u_xlat16_4.xxx * u_xlat16_5.xyz + u_xlat16_1.xyz;
    u_xlat2.xyz = u_xlat2.xyz * vec3(u_xlat24) + u_xlat16_6.xyz;
    u_xlat16_4.x = (-u_xlat16_26) * 0.699999988 + 1.70000005;
    u_xlat16_4.x = u_xlat16_26 * u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_4.x * 6.0;
    u_xlat16_12.x = dot((-u_xlat0.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_12.x = u_xlat16_12.x + u_xlat16_12.x;
    u_xlat16_12.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_12.xxx) + (-u_xlat0.xyz);
    u_xlat10_0 = textureLod(unity_SpecCube0, u_xlat16_12.xyz, u_xlat16_4.x);
    u_xlat16_4.x = u_xlat10_0.w + -1.0;
    u_xlat16_4.x = unity_SpecCube0_HDR.w * u_xlat16_4.x + 1.0;
    u_xlat16_4.x = u_xlat16_4.x * unity_SpecCube0_HDR.x;
    u_xlat16_4.xyz = u_xlat10_0.xyz * u_xlat16_4.xxx;
    u_xlat16_4.xyz = vec3(u_xlat16_25) * u_xlat16_4.xyz;
    u_xlat0.xyz = u_xlat16_4.xyz * u_xlat16_1.xyz + u_xlat2.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out mediump vec3 vs_TEXCOORD3;
out highp vec4 vs_TEXCOORD5;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat12;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    vs_TEXCOORD1.xyz = u_xlat0.xyz;
    u_xlat16_2 = u_xlat0.y * u_xlat0.y;
    u_xlat16_2 = u_xlat0.x * u_xlat0.x + (-u_xlat16_2);
    u_xlat16_0 = u_xlat0.yzzx * u_xlat0.xyzz;
    u_xlat16_3.x = dot(unity_SHBr, u_xlat16_0);
    u_xlat16_3.y = dot(unity_SHBg, u_xlat16_0);
    u_xlat16_3.z = dot(unity_SHBb, u_xlat16_0);
    vs_TEXCOORD3.xyz = unity_SHC.xyz * vec3(u_xlat16_2) + u_xlat16_3.xyz;
    vs_TEXCOORD5 = vec4(0.0, 0.0, 0.0, 0.0);
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform lowp samplerCube unity_SpecCube0;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
in mediump vec3 vs_TEXCOORD3;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump vec3 u_xlat16_0;
lowp vec4 u_xlat10_0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
lowp vec3 u_xlat10_2;
vec3 u_xlat3;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
vec3 u_xlat8;
mediump float u_xlat16_10;
mediump vec3 u_xlat16_12;
mediump float u_xlat16_18;
float u_xlat24;
mediump float u_xlat16_25;
mediump float u_xlat16_26;
mediump float u_xlat16_28;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD1.xyz;
    u_xlat0.w = 1.0;
    u_xlat16_1.x = dot(unity_SHAr, u_xlat0);
    u_xlat16_1.y = dot(unity_SHAg, u_xlat0);
    u_xlat16_1.z = dot(unity_SHAb, u_xlat0);
    u_xlat16_1.xyz = u_xlat16_1.xyz + vs_TEXCOORD3.xyz;
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_0.xyz = log2(u_xlat16_1.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_0.xyz = exp2(u_xlat16_0.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_0.xyz = max(u_xlat16_0.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat10_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_2.xyz * _Color.xyz;
    u_xlat16_1.xyz = u_xlat10_2.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_1.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_25 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_4.xyz = vec3(u_xlat16_25) * u_xlat16_3.xyz;
    u_xlat16_25 = (-u_xlat16_25) + _Glossiness;
    u_xlat16_25 = u_xlat16_25 + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_25 = min(max(u_xlat16_25, 0.0), 1.0);
#else
    u_xlat16_25 = clamp(u_xlat16_25, 0.0, 1.0);
#endif
    u_xlat16_5.xyz = (-u_xlat16_1.xyz) + vec3(u_xlat16_25);
    u_xlat16_6.xyz = u_xlat16_0.xyz * u_xlat16_4.xyz;
    u_xlat0.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * vs_TEXCOORD2.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * vs_TEXCOORD2.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    vec3 txVec0 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat10_0.x = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_25 = (-_LightShadowData.x) + 1.0;
    u_xlat16_25 = u_xlat10_0.x * u_xlat16_25 + _LightShadowData.x;
    u_xlat16_28 = (-u_xlat16_25) + 1.0;
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat2.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat8.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat2.x = dot(u_xlat8.xyz, u_xlat2.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat2.x);
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat0.x + u_xlat2.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_25 = u_xlat0.x * u_xlat16_28 + u_xlat16_25;
    u_xlat16_7.xyz = vec3(u_xlat16_25) * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat8.xyz, u_xlat8.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat2.xyz = u_xlat8.xyz * u_xlat0.xxx + _WorldSpaceLightPos0.xyz;
    u_xlat0.xyz = u_xlat0.xxx * u_xlat8.xyz;
    u_xlat24 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat24 = max(u_xlat24, 0.00100000005);
    u_xlat24 = inversesqrt(u_xlat24);
    u_xlat2.xyz = vec3(u_xlat24) * u_xlat2.xyz;
    u_xlat24 = dot(_WorldSpaceLightPos0.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat24 = min(max(u_xlat24, 0.0), 1.0);
#else
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
#endif
    u_xlat24 = max(u_xlat24, 0.319999993);
    u_xlat16_26 = (-_Glossiness) + 1.0;
    u_xlat16_3.x = u_xlat16_26 * u_xlat16_26 + 1.5;
    u_xlat24 = u_xlat24 * u_xlat16_3.x;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat2.x = dot(u_xlat3.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat2.x = min(max(u_xlat2.x, 0.0), 1.0);
#else
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
#endif
    u_xlat2.x = u_xlat2.x * u_xlat2.x;
    u_xlat16_10 = u_xlat16_26 * u_xlat16_26;
    u_xlat16_18 = u_xlat16_10 * u_xlat16_10 + -1.0;
    u_xlat2.x = u_xlat2.x * u_xlat16_18 + 1.00001001;
    u_xlat24 = u_xlat24 * u_xlat2.x;
    u_xlat24 = u_xlat16_10 / u_xlat24;
    u_xlat16_25 = u_xlat16_26 * u_xlat16_10;
    u_xlat16_25 = (-u_xlat16_25) * 0.280000001 + 1.0;
    u_xlat24 = u_xlat24 + -9.99999975e-05;
    u_xlat24 = max(u_xlat24, 0.0);
    u_xlat24 = min(u_xlat24, 100.0);
    u_xlat2.xyz = vec3(u_xlat24) * u_xlat16_1.xyz + u_xlat16_4.xyz;
    u_xlat2.xyz = u_xlat16_7.xyz * u_xlat2.xyz;
    u_xlat24 = dot(u_xlat3.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat24 = min(max(u_xlat24, 0.0), 1.0);
#else
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
#endif
    u_xlat3.x = dot(u_xlat3.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat3.x = min(max(u_xlat3.x, 0.0), 1.0);
#else
    u_xlat3.x = clamp(u_xlat3.x, 0.0, 1.0);
#endif
    u_xlat16_4.x = (-u_xlat3.x) + 1.0;
    u_xlat16_4.x = u_xlat16_4.x * u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_4.x * u_xlat16_4.x;
    u_xlat16_1.xyz = u_xlat16_4.xxx * u_xlat16_5.xyz + u_xlat16_1.xyz;
    u_xlat2.xyz = u_xlat2.xyz * vec3(u_xlat24) + u_xlat16_6.xyz;
    u_xlat16_4.x = (-u_xlat16_26) * 0.699999988 + 1.70000005;
    u_xlat16_4.x = u_xlat16_26 * u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_4.x * 6.0;
    u_xlat16_12.x = dot((-u_xlat0.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_12.x = u_xlat16_12.x + u_xlat16_12.x;
    u_xlat16_12.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_12.xxx) + (-u_xlat0.xyz);
    u_xlat10_0 = textureLod(unity_SpecCube0, u_xlat16_12.xyz, u_xlat16_4.x);
    u_xlat16_4.x = u_xlat10_0.w + -1.0;
    u_xlat16_4.x = unity_SpecCube0_HDR.w * u_xlat16_4.x + 1.0;
    u_xlat16_4.x = u_xlat16_4.x * unity_SpecCube0_HDR.x;
    u_xlat16_4.xyz = u_xlat10_0.xyz * u_xlat16_4.xxx;
    u_xlat16_4.xyz = vec3(u_xlat16_25) * u_xlat16_4.xyz;
    u_xlat0.xyz = u_xlat16_4.xyz * u_xlat16_1.xyz + u_xlat2.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

//////////////////////////////////////////////////////
Keywords set in this variant: DIRECTIONAL VERTEXLIGHT_ON 
-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 50 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  highp vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_4;
  tmpvar_4[0] = unity_WorldToObject[0].xyz;
  tmpvar_4[1] = unity_WorldToObject[1].xyz;
  tmpvar_4[2] = unity_WorldToObject[2].xyz;
  tmpvar_1.xyz = (unity_ObjectToWorld * _glesVertex).xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_4));
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD6 = tmpvar_2;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
void main ()
{
  mediump vec3 tmpvar_1;
  highp vec4 tmpvar_2;
  mediump vec3 tmpvar_3;
  mediump vec3 tmpvar_4;
  lowp vec4 c_5;
  lowp vec3 tmpvar_6;
  lowp float tmpvar_7;
  highp vec3 worldViewDir_8;
  lowp vec3 lightDir_9;
  mediump vec3 tmpvar_10;
  tmpvar_10 = _WorldSpaceLightPos0.xyz;
  lightDir_9 = tmpvar_10;
  worldViewDir_8 = normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz));
  lowp vec4 tmpvar_11;
  tmpvar_11 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_6 = tmpvar_11.xyz;
  tmpvar_7 = tmpvar_11.w;
  tmpvar_3 = _LightColor0.xyz;
  tmpvar_4 = lightDir_9;
  tmpvar_1 = worldViewDir_8;
  tmpvar_2 = unity_SpecCube0_HDR;
  mediump vec3 Normal_12;
  Normal_12 = xlv_TEXCOORD1;
  mediump float tmpvar_13;
  highp float tmpvar_14;
  highp float smoothness_15;
  smoothness_15 = _Glossiness;
  tmpvar_14 = (1.0 - smoothness_15);
  tmpvar_13 = tmpvar_14;
  mediump vec3 I_16;
  I_16 = -(tmpvar_1);
  mediump vec4 hdr_17;
  hdr_17 = tmpvar_2;
  mediump vec4 tmpvar_18;
  tmpvar_18.xyz = (I_16 - (2.0 * (
    dot (Normal_12, I_16)
   * Normal_12)));
  tmpvar_18.w = ((tmpvar_13 * (1.7 - 
    (0.7 * tmpvar_13)
  )) * 6.0);
  lowp vec4 tmpvar_19;
  tmpvar_19 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_18.xyz, tmpvar_18.w);
  mediump vec4 tmpvar_20;
  tmpvar_20 = tmpvar_19;
  lowp vec3 tmpvar_21;
  mediump vec4 c_22;
  highp vec3 tmpvar_23;
  tmpvar_23 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_24;
  mediump vec3 albedo_25;
  albedo_25 = tmpvar_6;
  mediump vec3 tmpvar_26;
  tmpvar_26 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_25, vec3(_Metallic));
  mediump float tmpvar_27;
  tmpvar_27 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_24 = (albedo_25 * tmpvar_27);
  tmpvar_21 = tmpvar_24;
  mediump vec3 diffColor_28;
  diffColor_28 = tmpvar_21;
  mediump float alpha_29;
  alpha_29 = tmpvar_7;
  tmpvar_21 = diffColor_28;
  mediump vec3 diffColor_30;
  diffColor_30 = tmpvar_21;
  mediump vec3 color_31;
  mediump vec2 rlPow4AndFresnelTerm_32;
  mediump float tmpvar_33;
  highp float tmpvar_34;
  tmpvar_34 = clamp (dot (tmpvar_23, tmpvar_4), 0.0, 1.0);
  tmpvar_33 = tmpvar_34;
  mediump float tmpvar_35;
  highp float tmpvar_36;
  tmpvar_36 = clamp (dot (tmpvar_23, worldViewDir_8), 0.0, 1.0);
  tmpvar_35 = tmpvar_36;
  highp vec2 tmpvar_37;
  tmpvar_37.x = dot ((worldViewDir_8 - (2.0 * 
    (dot (tmpvar_23, worldViewDir_8) * tmpvar_23)
  )), tmpvar_4);
  tmpvar_37.y = (1.0 - tmpvar_35);
  highp vec2 tmpvar_38;
  tmpvar_38 = ((tmpvar_37 * tmpvar_37) * (tmpvar_37 * tmpvar_37));
  rlPow4AndFresnelTerm_32 = tmpvar_38;
  mediump float tmpvar_39;
  tmpvar_39 = rlPow4AndFresnelTerm_32.x;
  mediump float specular_40;
  highp float smoothness_41;
  smoothness_41 = _Glossiness;
  highp vec2 tmpvar_42;
  tmpvar_42.x = tmpvar_39;
  tmpvar_42.y = (1.0 - smoothness_41);
  highp float tmpvar_43;
  tmpvar_43 = (texture2D (unity_NHxRoughness, tmpvar_42).w * 16.0);
  specular_40 = tmpvar_43;
  color_31 = ((diffColor_30 + (specular_40 * tmpvar_26)) * (tmpvar_3 * tmpvar_33));
  color_31 = (color_31 + ((
    (hdr_17.x * ((hdr_17.w * (tmpvar_20.w - 1.0)) + 1.0))
   * tmpvar_20.xyz) * mix (tmpvar_26, vec3(
    clamp ((_Glossiness + (1.0 - tmpvar_27)), 0.0, 1.0)
  ), rlPow4AndFresnelTerm_32.yyy)));
  mediump vec4 tmpvar_44;
  tmpvar_44.w = 1.0;
  tmpvar_44.xyz = color_31;
  c_22.xyz = tmpvar_44.xyz;
  c_22.w = alpha_29;
  c_5.xyz = c_22.xyz;
  c_5.w = 1.0;
  gl_FragData[0] = c_5;
}


#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 70 math, 2 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  highp vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_4;
  tmpvar_4[0] = unity_WorldToObject[0].xyz;
  tmpvar_4[1] = unity_WorldToObject[1].xyz;
  tmpvar_4[2] = unity_WorldToObject[2].xyz;
  tmpvar_1.xyz = (unity_ObjectToWorld * _glesVertex).xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_4));
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD6 = tmpvar_2;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
void main ()
{
  mediump vec3 tmpvar_1;
  highp vec4 tmpvar_2;
  mediump vec3 tmpvar_3;
  mediump vec3 tmpvar_4;
  lowp vec4 c_5;
  lowp vec3 tmpvar_6;
  lowp float tmpvar_7;
  highp vec3 worldViewDir_8;
  lowp vec3 lightDir_9;
  mediump vec3 tmpvar_10;
  tmpvar_10 = _WorldSpaceLightPos0.xyz;
  lightDir_9 = tmpvar_10;
  worldViewDir_8 = normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz));
  lowp vec4 tmpvar_11;
  tmpvar_11 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_6 = tmpvar_11.xyz;
  tmpvar_7 = tmpvar_11.w;
  tmpvar_3 = _LightColor0.xyz;
  tmpvar_4 = lightDir_9;
  tmpvar_1 = worldViewDir_8;
  tmpvar_2 = unity_SpecCube0_HDR;
  mediump vec3 Normal_12;
  Normal_12 = xlv_TEXCOORD1;
  mediump float tmpvar_13;
  highp float tmpvar_14;
  highp float smoothness_15;
  smoothness_15 = _Glossiness;
  tmpvar_14 = (1.0 - smoothness_15);
  tmpvar_13 = tmpvar_14;
  mediump vec3 I_16;
  I_16 = -(tmpvar_1);
  mediump vec3 tmpvar_17;
  mediump vec4 hdr_18;
  hdr_18 = tmpvar_2;
  mediump vec4 tmpvar_19;
  tmpvar_19.xyz = (I_16 - (2.0 * (
    dot (Normal_12, I_16)
   * Normal_12)));
  tmpvar_19.w = ((tmpvar_13 * (1.7 - 
    (0.7 * tmpvar_13)
  )) * 6.0);
  lowp vec4 tmpvar_20;
  tmpvar_20 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_19.xyz, tmpvar_19.w);
  mediump vec4 tmpvar_21;
  tmpvar_21 = tmpvar_20;
  tmpvar_17 = ((hdr_18.x * (
    (hdr_18.w * (tmpvar_21.w - 1.0))
   + 1.0)) * tmpvar_21.xyz);
  lowp vec3 tmpvar_22;
  mediump vec4 c_23;
  highp vec3 tmpvar_24;
  tmpvar_24 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_25;
  mediump vec3 albedo_26;
  albedo_26 = tmpvar_6;
  mediump vec3 tmpvar_27;
  tmpvar_27 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_26, vec3(_Metallic));
  mediump float tmpvar_28;
  tmpvar_28 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_25 = (albedo_26 * tmpvar_28);
  tmpvar_22 = tmpvar_25;
  mediump vec3 diffColor_29;
  diffColor_29 = tmpvar_22;
  mediump float alpha_30;
  alpha_30 = tmpvar_7;
  tmpvar_22 = diffColor_29;
  mediump vec3 diffColor_31;
  diffColor_31 = tmpvar_22;
  mediump vec3 color_32;
  mediump float surfaceReduction_33;
  highp float specularTerm_34;
  highp float a2_35;
  mediump float roughness_36;
  mediump float perceptualRoughness_37;
  highp vec3 tmpvar_38;
  highp vec3 inVec_39;
  inVec_39 = (tmpvar_4 + worldViewDir_8);
  tmpvar_38 = (inVec_39 * inversesqrt(max (0.001, 
    dot (inVec_39, inVec_39)
  )));
  mediump float tmpvar_40;
  highp float tmpvar_41;
  tmpvar_41 = clamp (dot (tmpvar_24, tmpvar_4), 0.0, 1.0);
  tmpvar_40 = tmpvar_41;
  highp float tmpvar_42;
  tmpvar_42 = clamp (dot (tmpvar_24, tmpvar_38), 0.0, 1.0);
  mediump float tmpvar_43;
  highp float tmpvar_44;
  tmpvar_44 = clamp (dot (tmpvar_24, worldViewDir_8), 0.0, 1.0);
  tmpvar_43 = tmpvar_44;
  highp float tmpvar_45;
  highp float smoothness_46;
  smoothness_46 = _Glossiness;
  tmpvar_45 = (1.0 - smoothness_46);
  perceptualRoughness_37 = tmpvar_45;
  highp float tmpvar_47;
  highp float perceptualRoughness_48;
  perceptualRoughness_48 = perceptualRoughness_37;
  tmpvar_47 = (perceptualRoughness_48 * perceptualRoughness_48);
  roughness_36 = tmpvar_47;
  mediump float tmpvar_49;
  tmpvar_49 = (roughness_36 * roughness_36);
  a2_35 = tmpvar_49;
  specularTerm_34 = ((roughness_36 / (
    (max (0.32, clamp (dot (tmpvar_4, tmpvar_38), 0.0, 1.0)) * (1.5 + roughness_36))
   * 
    (((tmpvar_42 * tmpvar_42) * (a2_35 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_50;
  tmpvar_50 = clamp (specularTerm_34, 0.0, 100.0);
  specularTerm_34 = tmpvar_50;
  surfaceReduction_33 = (1.0 - ((roughness_36 * perceptualRoughness_37) * 0.28));
  mediump float x_51;
  x_51 = (1.0 - tmpvar_43);
  mediump vec3 tmpvar_52;
  tmpvar_52 = mix (tmpvar_27, vec3(clamp ((_Glossiness + 
    (1.0 - tmpvar_28)
  ), 0.0, 1.0)), vec3(((x_51 * x_51) * (x_51 * x_51))));
  highp vec3 tmpvar_53;
  tmpvar_53 = (((
    (diffColor_31 + (tmpvar_50 * tmpvar_27))
   * tmpvar_3) * tmpvar_40) + ((surfaceReduction_33 * tmpvar_17) * tmpvar_52));
  color_32 = tmpvar_53;
  mediump vec4 tmpvar_54;
  tmpvar_54.w = 1.0;
  tmpvar_54.xyz = color_32;
  c_23.xyz = tmpvar_54.xyz;
  c_23.w = alpha_30;
  c_5.xyz = c_23.xyz;
  c_5.w = 1.0;
  gl_FragData[0] = c_5;
}


#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 70 math, 2 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  highp vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_4;
  tmpvar_4[0] = unity_WorldToObject[0].xyz;
  tmpvar_4[1] = unity_WorldToObject[1].xyz;
  tmpvar_4[2] = unity_WorldToObject[2].xyz;
  tmpvar_1.xyz = (unity_ObjectToWorld * _glesVertex).xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_4));
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD6 = tmpvar_2;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
void main ()
{
  mediump vec3 tmpvar_1;
  highp vec4 tmpvar_2;
  mediump vec3 tmpvar_3;
  mediump vec3 tmpvar_4;
  lowp vec4 c_5;
  lowp vec3 tmpvar_6;
  lowp float tmpvar_7;
  highp vec3 worldViewDir_8;
  lowp vec3 lightDir_9;
  mediump vec3 tmpvar_10;
  tmpvar_10 = _WorldSpaceLightPos0.xyz;
  lightDir_9 = tmpvar_10;
  worldViewDir_8 = normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz));
  lowp vec4 tmpvar_11;
  tmpvar_11 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_6 = tmpvar_11.xyz;
  tmpvar_7 = tmpvar_11.w;
  tmpvar_3 = _LightColor0.xyz;
  tmpvar_4 = lightDir_9;
  tmpvar_1 = worldViewDir_8;
  tmpvar_2 = unity_SpecCube0_HDR;
  mediump vec3 Normal_12;
  Normal_12 = xlv_TEXCOORD1;
  mediump float tmpvar_13;
  highp float tmpvar_14;
  highp float smoothness_15;
  smoothness_15 = _Glossiness;
  tmpvar_14 = (1.0 - smoothness_15);
  tmpvar_13 = tmpvar_14;
  mediump vec3 I_16;
  I_16 = -(tmpvar_1);
  mediump vec3 tmpvar_17;
  mediump vec4 hdr_18;
  hdr_18 = tmpvar_2;
  mediump vec4 tmpvar_19;
  tmpvar_19.xyz = (I_16 - (2.0 * (
    dot (Normal_12, I_16)
   * Normal_12)));
  tmpvar_19.w = ((tmpvar_13 * (1.7 - 
    (0.7 * tmpvar_13)
  )) * 6.0);
  lowp vec4 tmpvar_20;
  tmpvar_20 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_19.xyz, tmpvar_19.w);
  mediump vec4 tmpvar_21;
  tmpvar_21 = tmpvar_20;
  tmpvar_17 = ((hdr_18.x * (
    (hdr_18.w * (tmpvar_21.w - 1.0))
   + 1.0)) * tmpvar_21.xyz);
  lowp vec3 tmpvar_22;
  mediump vec4 c_23;
  highp vec3 tmpvar_24;
  tmpvar_24 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_25;
  mediump vec3 albedo_26;
  albedo_26 = tmpvar_6;
  mediump vec3 tmpvar_27;
  tmpvar_27 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_26, vec3(_Metallic));
  mediump float tmpvar_28;
  tmpvar_28 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_25 = (albedo_26 * tmpvar_28);
  tmpvar_22 = tmpvar_25;
  mediump vec3 diffColor_29;
  diffColor_29 = tmpvar_22;
  mediump float alpha_30;
  alpha_30 = tmpvar_7;
  tmpvar_22 = diffColor_29;
  mediump vec3 diffColor_31;
  diffColor_31 = tmpvar_22;
  mediump vec3 color_32;
  mediump float surfaceReduction_33;
  highp float specularTerm_34;
  highp float a2_35;
  mediump float roughness_36;
  mediump float perceptualRoughness_37;
  highp vec3 tmpvar_38;
  highp vec3 inVec_39;
  inVec_39 = (tmpvar_4 + worldViewDir_8);
  tmpvar_38 = (inVec_39 * inversesqrt(max (0.001, 
    dot (inVec_39, inVec_39)
  )));
  mediump float tmpvar_40;
  highp float tmpvar_41;
  tmpvar_41 = clamp (dot (tmpvar_24, tmpvar_4), 0.0, 1.0);
  tmpvar_40 = tmpvar_41;
  highp float tmpvar_42;
  tmpvar_42 = clamp (dot (tmpvar_24, tmpvar_38), 0.0, 1.0);
  mediump float tmpvar_43;
  highp float tmpvar_44;
  tmpvar_44 = clamp (dot (tmpvar_24, worldViewDir_8), 0.0, 1.0);
  tmpvar_43 = tmpvar_44;
  highp float tmpvar_45;
  highp float smoothness_46;
  smoothness_46 = _Glossiness;
  tmpvar_45 = (1.0 - smoothness_46);
  perceptualRoughness_37 = tmpvar_45;
  highp float tmpvar_47;
  highp float perceptualRoughness_48;
  perceptualRoughness_48 = perceptualRoughness_37;
  tmpvar_47 = (perceptualRoughness_48 * perceptualRoughness_48);
  roughness_36 = tmpvar_47;
  mediump float tmpvar_49;
  tmpvar_49 = (roughness_36 * roughness_36);
  a2_35 = tmpvar_49;
  specularTerm_34 = ((roughness_36 / (
    (max (0.32, clamp (dot (tmpvar_4, tmpvar_38), 0.0, 1.0)) * (1.5 + roughness_36))
   * 
    (((tmpvar_42 * tmpvar_42) * (a2_35 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_50;
  tmpvar_50 = clamp (specularTerm_34, 0.0, 100.0);
  specularTerm_34 = tmpvar_50;
  surfaceReduction_33 = (1.0 - ((roughness_36 * perceptualRoughness_37) * 0.28));
  mediump float x_51;
  x_51 = (1.0 - tmpvar_43);
  mediump vec3 tmpvar_52;
  tmpvar_52 = mix (tmpvar_27, vec3(clamp ((_Glossiness + 
    (1.0 - tmpvar_28)
  ), 0.0, 1.0)), vec3(((x_51 * x_51) * (x_51 * x_51))));
  highp vec3 tmpvar_53;
  tmpvar_53 = (((
    (diffColor_31 + (tmpvar_50 * tmpvar_27))
   * tmpvar_3) * tmpvar_40) + ((surfaceReduction_33 * tmpvar_17) * tmpvar_52));
  color_32 = tmpvar_53;
  mediump vec4 tmpvar_54;
  tmpvar_54.w = 1.0;
  tmpvar_54.xyz = color_32;
  c_23.xyz = tmpvar_54.xyz;
  c_23.w = alpha_30;
  c_5.xyz = c_23.xyz;
  c_5.w = 1.0;
  gl_FragData[0] = c_5;
}


#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp samplerCube unity_SpecCube0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
mediump vec3 u_xlat16_1;
lowp vec4 u_xlat10_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_2;
lowp vec3 u_xlat10_2;
mediump vec3 u_xlat16_3;
vec3 u_xlat4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
mediump float u_xlat16_8;
float u_xlat10;
float u_xlat24;
mediump float u_xlat16_25;
mediump float u_xlat16_27;
mediump float u_xlat16_29;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat24 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat24 = inversesqrt(u_xlat24);
    u_xlat0.xyz = vec3(u_xlat24) * u_xlat0.xyz;
    u_xlat16_1.x = dot((-u_xlat0.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_1.x = u_xlat16_1.x + u_xlat16_1.x;
    u_xlat16_1.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_1.xxx) + (-u_xlat0.xyz);
    u_xlat2.z = (-_Glossiness) + 1.0;
    u_xlat16_25 = (-u_xlat2.z) * 0.699999988 + 1.70000005;
    u_xlat16_25 = u_xlat16_25 * u_xlat2.z;
    u_xlat16_25 = u_xlat16_25 * 6.0;
    u_xlat10_1 = textureLod(unity_SpecCube0, u_xlat16_1.xyz, u_xlat16_25);
    u_xlat16_3.x = u_xlat10_1.w + -1.0;
    u_xlat16_3.x = unity_SpecCube0_HDR.w * u_xlat16_3.x + 1.0;
    u_xlat16_3.x = u_xlat16_3.x * unity_SpecCube0_HDR.x;
    u_xlat16_3.xyz = u_xlat10_1.xyz * u_xlat16_3.xxx;
    u_xlat24 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat24 = inversesqrt(u_xlat24);
    u_xlat4.xyz = vec3(u_xlat24) * vs_TEXCOORD1.xyz;
    u_xlat24 = dot(u_xlat0.xyz, u_xlat4.xyz);
    u_xlat10 = u_xlat24;
#ifdef UNITY_ADRENO_ES3
    u_xlat10 = min(max(u_xlat10, 0.0), 1.0);
#else
    u_xlat10 = clamp(u_xlat10, 0.0, 1.0);
#endif
    u_xlat24 = u_xlat24 + u_xlat24;
    u_xlat0.xyz = u_xlat4.xyz * (-vec3(u_xlat24)) + u_xlat0.xyz;
    u_xlat24 = dot(u_xlat4.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat24 = min(max(u_xlat24, 0.0), 1.0);
#else
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
#endif
    u_xlat16_5.xyz = vec3(u_xlat24) * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat0.xyz, _WorldSpaceLightPos0.xyz);
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat2.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = texture(unity_NHxRoughness, u_xlat2.xz).w;
    u_xlat0.x = u_xlat0.x * 16.0;
    u_xlat16_27 = (-u_xlat10) + 1.0;
    u_xlat16_8 = u_xlat16_27 * u_xlat16_27;
    u_xlat16_8 = u_xlat16_27 * u_xlat16_8;
    u_xlat16_8 = u_xlat16_27 * u_xlat16_8;
    u_xlat16_27 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_29 = (-u_xlat16_27) + _Glossiness;
    u_xlat16_29 = u_xlat16_29 + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_29 = min(max(u_xlat16_29, 0.0), 1.0);
#else
    u_xlat16_29 = clamp(u_xlat16_29, 0.0, 1.0);
#endif
    u_xlat10_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_6.xyz = u_xlat10_2.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_2.xyz = u_xlat10_2.xyz * _Color.xyz;
    u_xlat16_6.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_6.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_7.xyz = vec3(u_xlat16_29) + (-u_xlat16_6.xyz);
    u_xlat16_7.xyz = vec3(u_xlat16_8) * u_xlat16_7.xyz + u_xlat16_6.xyz;
    u_xlat16_6.xyz = u_xlat0.xxx * u_xlat16_6.xyz;
    u_xlat16_6.xyz = u_xlat16_2.xyz * vec3(u_xlat16_27) + u_xlat16_6.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * u_xlat16_7.xyz;
    SV_Target0.xyz = u_xlat16_6.xyz * u_xlat16_5.xyz + u_xlat16_3.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform lowp samplerCube unity_SpecCube0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
lowp vec4 u_xlat10_2;
mediump vec3 u_xlat16_3;
vec3 u_xlat4;
mediump float u_xlat16_5;
mediump vec3 u_xlat16_6;
float u_xlat7;
mediump vec3 u_xlat16_12;
float u_xlat14;
float u_xlat21;
mediump float u_xlat16_21;
mediump float u_xlat16_22;
mediump float u_xlat16_23;
mediump float u_xlat16_24;
float u_xlat25;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat21 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat1.xyz = vec3(u_xlat21) * u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat21) + _WorldSpaceLightPos0.xyz;
    u_xlat16_2.x = dot((-u_xlat1.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_2.x = u_xlat16_2.x + u_xlat16_2.x;
    u_xlat16_2.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_2.xxx) + (-u_xlat1.xyz);
    u_xlat16_21 = (-_Glossiness) + 1.0;
    u_xlat16_23 = (-u_xlat16_21) * 0.699999988 + 1.70000005;
    u_xlat16_23 = u_xlat16_21 * u_xlat16_23;
    u_xlat16_23 = u_xlat16_23 * 6.0;
    u_xlat10_2 = textureLod(unity_SpecCube0, u_xlat16_2.xyz, u_xlat16_23);
    u_xlat16_3.x = u_xlat10_2.w + -1.0;
    u_xlat16_3.x = unity_SpecCube0_HDR.w * u_xlat16_3.x + 1.0;
    u_xlat16_3.x = u_xlat16_3.x * unity_SpecCube0_HDR.x;
    u_xlat16_3.xyz = u_xlat10_2.xyz * u_xlat16_3.xxx;
    u_xlat16_22 = u_xlat16_21 * u_xlat16_21;
    u_xlat16_24 = u_xlat16_21 * u_xlat16_22;
    u_xlat16_21 = u_xlat16_21 * u_xlat16_21 + 1.5;
    u_xlat16_24 = (-u_xlat16_24) * 0.280000001 + 1.0;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vec3(u_xlat16_24);
    u_xlat4.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat4.x = inversesqrt(u_xlat4.x);
    u_xlat4.xyz = u_xlat4.xxx * vs_TEXCOORD1.xyz;
    u_xlat1.x = dot(u_xlat4.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat1.x = min(max(u_xlat1.x, 0.0), 1.0);
#else
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
#endif
    u_xlat16_24 = (-u_xlat1.x) + 1.0;
    u_xlat16_24 = u_xlat16_24 * u_xlat16_24;
    u_xlat16_24 = u_xlat16_24 * u_xlat16_24;
    u_xlat16_5 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_12.x = (-u_xlat16_5) + _Glossiness;
    u_xlat16_12.x = u_xlat16_12.x + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_12.x = min(max(u_xlat16_12.x, 0.0), 1.0);
#else
    u_xlat16_12.x = clamp(u_xlat16_12.x, 0.0, 1.0);
#endif
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_6.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_6.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_6.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_12.xyz = u_xlat16_12.xxx + (-u_xlat16_6.xyz);
    u_xlat16_12.xyz = vec3(u_xlat16_24) * u_xlat16_12.xyz + u_xlat16_6.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * u_xlat16_12.xyz;
    u_xlat25 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat25 = max(u_xlat25, 0.00100000005);
    u_xlat25 = inversesqrt(u_xlat25);
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat25);
    u_xlat25 = dot(_WorldSpaceLightPos0.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat25 = min(max(u_xlat25, 0.0), 1.0);
#else
    u_xlat25 = clamp(u_xlat25, 0.0, 1.0);
#endif
    u_xlat0.x = dot(u_xlat4.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat7 = dot(u_xlat4.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat7 = min(max(u_xlat7, 0.0), 1.0);
#else
    u_xlat7 = clamp(u_xlat7, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat14 = max(u_xlat25, 0.319999993);
    u_xlat14 = u_xlat16_21 * u_xlat14;
    u_xlat16_21 = u_xlat16_22 * u_xlat16_22 + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_21 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat14;
    u_xlat0.x = u_xlat16_22 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat0.xzw = u_xlat16_6.xyz * u_xlat0.xxx;
    u_xlat0.xzw = u_xlat16_1.xyz * vec3(u_xlat16_5) + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat0.xzw * _LightColor0.xyz;
    u_xlat0.xyz = u_xlat0.xzw * vec3(u_xlat7) + u_xlat16_3.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform lowp samplerCube unity_SpecCube0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
lowp vec4 u_xlat10_2;
mediump vec3 u_xlat16_3;
vec3 u_xlat4;
mediump float u_xlat16_5;
mediump vec3 u_xlat16_6;
float u_xlat7;
mediump vec3 u_xlat16_12;
float u_xlat14;
float u_xlat21;
mediump float u_xlat16_21;
mediump float u_xlat16_22;
mediump float u_xlat16_23;
mediump float u_xlat16_24;
float u_xlat25;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat21 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat1.xyz = vec3(u_xlat21) * u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat21) + _WorldSpaceLightPos0.xyz;
    u_xlat16_2.x = dot((-u_xlat1.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_2.x = u_xlat16_2.x + u_xlat16_2.x;
    u_xlat16_2.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_2.xxx) + (-u_xlat1.xyz);
    u_xlat16_21 = (-_Glossiness) + 1.0;
    u_xlat16_23 = (-u_xlat16_21) * 0.699999988 + 1.70000005;
    u_xlat16_23 = u_xlat16_21 * u_xlat16_23;
    u_xlat16_23 = u_xlat16_23 * 6.0;
    u_xlat10_2 = textureLod(unity_SpecCube0, u_xlat16_2.xyz, u_xlat16_23);
    u_xlat16_3.x = u_xlat10_2.w + -1.0;
    u_xlat16_3.x = unity_SpecCube0_HDR.w * u_xlat16_3.x + 1.0;
    u_xlat16_3.x = u_xlat16_3.x * unity_SpecCube0_HDR.x;
    u_xlat16_3.xyz = u_xlat10_2.xyz * u_xlat16_3.xxx;
    u_xlat16_22 = u_xlat16_21 * u_xlat16_21;
    u_xlat16_24 = u_xlat16_21 * u_xlat16_22;
    u_xlat16_21 = u_xlat16_21 * u_xlat16_21 + 1.5;
    u_xlat16_24 = (-u_xlat16_24) * 0.280000001 + 1.0;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vec3(u_xlat16_24);
    u_xlat4.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat4.x = inversesqrt(u_xlat4.x);
    u_xlat4.xyz = u_xlat4.xxx * vs_TEXCOORD1.xyz;
    u_xlat1.x = dot(u_xlat4.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat1.x = min(max(u_xlat1.x, 0.0), 1.0);
#else
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
#endif
    u_xlat16_24 = (-u_xlat1.x) + 1.0;
    u_xlat16_24 = u_xlat16_24 * u_xlat16_24;
    u_xlat16_24 = u_xlat16_24 * u_xlat16_24;
    u_xlat16_5 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_12.x = (-u_xlat16_5) + _Glossiness;
    u_xlat16_12.x = u_xlat16_12.x + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_12.x = min(max(u_xlat16_12.x, 0.0), 1.0);
#else
    u_xlat16_12.x = clamp(u_xlat16_12.x, 0.0, 1.0);
#endif
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_6.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_6.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_6.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_12.xyz = u_xlat16_12.xxx + (-u_xlat16_6.xyz);
    u_xlat16_12.xyz = vec3(u_xlat16_24) * u_xlat16_12.xyz + u_xlat16_6.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * u_xlat16_12.xyz;
    u_xlat25 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat25 = max(u_xlat25, 0.00100000005);
    u_xlat25 = inversesqrt(u_xlat25);
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat25);
    u_xlat25 = dot(_WorldSpaceLightPos0.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat25 = min(max(u_xlat25, 0.0), 1.0);
#else
    u_xlat25 = clamp(u_xlat25, 0.0, 1.0);
#endif
    u_xlat0.x = dot(u_xlat4.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat7 = dot(u_xlat4.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat7 = min(max(u_xlat7, 0.0), 1.0);
#else
    u_xlat7 = clamp(u_xlat7, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat14 = max(u_xlat25, 0.319999993);
    u_xlat14 = u_xlat16_21 * u_xlat14;
    u_xlat16_21 = u_xlat16_22 * u_xlat16_22 + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_21 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat14;
    u_xlat0.x = u_xlat16_22 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat0.xzw = u_xlat16_6.xyz * u_xlat0.xxx;
    u_xlat0.xzw = u_xlat16_1.xyz * vec3(u_xlat16_5) + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat0.xzw * _LightColor0.xyz;
    u_xlat0.xyz = u_xlat0.xzw * vec3(u_xlat7) + u_xlat16_3.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

//////////////////////////////////////////////////////
Keywords set in this variant: DIRECTIONAL LIGHTPROBE_SH VERTEXLIGHT_ON 
-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 62 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
uniform mediump vec4 unity_4LightAtten0;
uniform mediump vec4 unity_LightColor[8];
uniform mediump vec4 unity_SHBr;
uniform mediump vec4 unity_SHBg;
uniform mediump vec4 unity_SHBb;
uniform mediump vec4 unity_SHC;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  mediump vec3 tmpvar_2;
  highp vec4 tmpvar_3;
  highp vec4 tmpvar_4;
  tmpvar_4.w = 1.0;
  tmpvar_4.xyz = _glesVertex.xyz;
  highp vec3 tmpvar_5;
  tmpvar_5 = (unity_ObjectToWorld * _glesVertex).xyz;
  highp mat3 tmpvar_6;
  tmpvar_6[0] = unity_WorldToObject[0].xyz;
  tmpvar_6[1] = unity_WorldToObject[1].xyz;
  tmpvar_6[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_glesNormal * tmpvar_6));
  tmpvar_1.xyz = tmpvar_5;
  highp vec3 lightColor0_8;
  lightColor0_8 = unity_LightColor[0].xyz;
  highp vec3 lightColor1_9;
  lightColor1_9 = unity_LightColor[1].xyz;
  highp vec3 lightColor2_10;
  lightColor2_10 = unity_LightColor[2].xyz;
  highp vec3 lightColor3_11;
  lightColor3_11 = unity_LightColor[3].xyz;
  highp vec4 lightAttenSq_12;
  lightAttenSq_12 = unity_4LightAtten0;
  highp vec3 col_13;
  highp vec4 ndotl_14;
  highp vec4 lengthSq_15;
  highp vec4 tmpvar_16;
  tmpvar_16 = (unity_4LightPosX0 - tmpvar_5.x);
  highp vec4 tmpvar_17;
  tmpvar_17 = (unity_4LightPosY0 - tmpvar_5.y);
  highp vec4 tmpvar_18;
  tmpvar_18 = (unity_4LightPosZ0 - tmpvar_5.z);
  lengthSq_15 = (tmpvar_16 * tmpvar_16);
  lengthSq_15 = (lengthSq_15 + (tmpvar_17 * tmpvar_17));
  lengthSq_15 = (lengthSq_15 + (tmpvar_18 * tmpvar_18));
  highp vec4 tmpvar_19;
  tmpvar_19 = max (lengthSq_15, vec4(1e-06, 1e-06, 1e-06, 1e-06));
  lengthSq_15 = tmpvar_19;
  ndotl_14 = (tmpvar_16 * tmpvar_7.x);
  ndotl_14 = (ndotl_14 + (tmpvar_17 * tmpvar_7.y));
  ndotl_14 = (ndotl_14 + (tmpvar_18 * tmpvar_7.z));
  highp vec4 tmpvar_20;
  tmpvar_20 = max (vec4(0.0, 0.0, 0.0, 0.0), (ndotl_14 * inversesqrt(tmpvar_19)));
  ndotl_14 = tmpvar_20;
  highp vec4 tmpvar_21;
  tmpvar_21 = (tmpvar_20 * (1.0/((1.0 + 
    (tmpvar_19 * lightAttenSq_12)
  ))));
  col_13 = (lightColor0_8 * tmpvar_21.x);
  col_13 = (col_13 + (lightColor1_9 * tmpvar_21.y));
  col_13 = (col_13 + (lightColor2_10 * tmpvar_21.z));
  col_13 = (col_13 + (lightColor3_11 * tmpvar_21.w));
  tmpvar_2 = col_13;
  mediump vec3 normal_22;
  normal_22 = tmpvar_7;
  mediump vec3 ambient_23;
  mediump vec3 x1_24;
  mediump vec4 tmpvar_25;
  tmpvar_25 = (normal_22.xyzz * normal_22.yzzx);
  x1_24.x = dot (unity_SHBr, tmpvar_25);
  x1_24.y = dot (unity_SHBg, tmpvar_25);
  x1_24.z = dot (unity_SHBb, tmpvar_25);
  ambient_23 = ((tmpvar_2 * (
    (tmpvar_2 * ((tmpvar_2 * 0.305306) + 0.6821711))
   + 0.01252288)) + (x1_24 + (unity_SHC.xyz * 
    ((normal_22.x * normal_22.x) - (normal_22.y * normal_22.y))
  )));
  tmpvar_2 = ambient_23;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_4));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = tmpvar_7;
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD3 = ambient_23;
  xlv_TEXCOORD6 = tmpvar_3;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 unity_SHAr;
uniform mediump vec4 unity_SHAg;
uniform mediump vec4 unity_SHAb;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  highp vec4 tmpvar_2;
  mediump vec3 tmpvar_3;
  mediump vec3 tmpvar_4;
  lowp vec4 c_5;
  lowp vec3 tmpvar_6;
  lowp float tmpvar_7;
  highp vec3 worldViewDir_8;
  lowp vec3 lightDir_9;
  mediump vec3 tmpvar_10;
  tmpvar_10 = _WorldSpaceLightPos0.xyz;
  lightDir_9 = tmpvar_10;
  worldViewDir_8 = normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz));
  lowp vec4 tmpvar_11;
  tmpvar_11 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_6 = tmpvar_11.xyz;
  tmpvar_7 = tmpvar_11.w;
  tmpvar_3 = _LightColor0.xyz;
  tmpvar_4 = lightDir_9;
  tmpvar_1 = worldViewDir_8;
  tmpvar_2 = unity_SpecCube0_HDR;
  mediump vec3 Normal_12;
  Normal_12 = xlv_TEXCOORD1;
  mediump float tmpvar_13;
  highp float tmpvar_14;
  highp float smoothness_15;
  smoothness_15 = _Glossiness;
  tmpvar_14 = (1.0 - smoothness_15);
  tmpvar_13 = tmpvar_14;
  mediump vec3 I_16;
  I_16 = -(tmpvar_1);
  mediump vec3 normalWorld_17;
  normalWorld_17 = xlv_TEXCOORD1;
  mediump vec4 tmpvar_18;
  tmpvar_18.w = 1.0;
  tmpvar_18.xyz = normalWorld_17;
  mediump vec3 x_19;
  x_19.x = dot (unity_SHAr, tmpvar_18);
  x_19.y = dot (unity_SHAg, tmpvar_18);
  x_19.z = dot (unity_SHAb, tmpvar_18);
  mediump vec4 hdr_20;
  hdr_20 = tmpvar_2;
  mediump vec4 tmpvar_21;
  tmpvar_21.xyz = (I_16 - (2.0 * (
    dot (Normal_12, I_16)
   * Normal_12)));
  tmpvar_21.w = ((tmpvar_13 * (1.7 - 
    (0.7 * tmpvar_13)
  )) * 6.0);
  lowp vec4 tmpvar_22;
  tmpvar_22 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_21.xyz, tmpvar_21.w);
  mediump vec4 tmpvar_23;
  tmpvar_23 = tmpvar_22;
  lowp vec3 tmpvar_24;
  mediump vec4 c_25;
  highp vec3 tmpvar_26;
  tmpvar_26 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_27;
  mediump vec3 albedo_28;
  albedo_28 = tmpvar_6;
  mediump vec3 tmpvar_29;
  tmpvar_29 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_28, vec3(_Metallic));
  mediump float tmpvar_30;
  tmpvar_30 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_27 = (albedo_28 * tmpvar_30);
  tmpvar_24 = tmpvar_27;
  mediump vec3 diffColor_31;
  diffColor_31 = tmpvar_24;
  mediump float alpha_32;
  alpha_32 = tmpvar_7;
  tmpvar_24 = diffColor_31;
  mediump vec3 diffColor_33;
  diffColor_33 = tmpvar_24;
  mediump vec3 color_34;
  mediump vec2 rlPow4AndFresnelTerm_35;
  mediump float tmpvar_36;
  highp float tmpvar_37;
  tmpvar_37 = clamp (dot (tmpvar_26, tmpvar_4), 0.0, 1.0);
  tmpvar_36 = tmpvar_37;
  mediump float tmpvar_38;
  highp float tmpvar_39;
  tmpvar_39 = clamp (dot (tmpvar_26, worldViewDir_8), 0.0, 1.0);
  tmpvar_38 = tmpvar_39;
  highp vec2 tmpvar_40;
  tmpvar_40.x = dot ((worldViewDir_8 - (2.0 * 
    (dot (tmpvar_26, worldViewDir_8) * tmpvar_26)
  )), tmpvar_4);
  tmpvar_40.y = (1.0 - tmpvar_38);
  highp vec2 tmpvar_41;
  tmpvar_41 = ((tmpvar_40 * tmpvar_40) * (tmpvar_40 * tmpvar_40));
  rlPow4AndFresnelTerm_35 = tmpvar_41;
  mediump float tmpvar_42;
  tmpvar_42 = rlPow4AndFresnelTerm_35.x;
  mediump float specular_43;
  highp float smoothness_44;
  smoothness_44 = _Glossiness;
  highp vec2 tmpvar_45;
  tmpvar_45.x = tmpvar_42;
  tmpvar_45.y = (1.0 - smoothness_44);
  highp float tmpvar_46;
  tmpvar_46 = (texture2D (unity_NHxRoughness, tmpvar_45).w * 16.0);
  specular_43 = tmpvar_46;
  color_34 = ((diffColor_33 + (specular_43 * tmpvar_29)) * (tmpvar_3 * tmpvar_36));
  color_34 = (color_34 + ((
    max (((1.055 * pow (
      max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD3 + x_19))
    , vec3(0.4166667, 0.4166667, 0.4166667))) - 0.055), vec3(0.0, 0.0, 0.0))
   * diffColor_33) + (
    ((hdr_20.x * ((hdr_20.w * 
      (tmpvar_23.w - 1.0)
    ) + 1.0)) * tmpvar_23.xyz)
   * 
    mix (tmpvar_29, vec3(clamp ((_Glossiness + (1.0 - tmpvar_30)), 0.0, 1.0)), rlPow4AndFresnelTerm_35.yyy)
  )));
  mediump vec4 tmpvar_47;
  tmpvar_47.w = 1.0;
  tmpvar_47.xyz = color_34;
  c_25.xyz = tmpvar_47.xyz;
  c_25.w = alpha_32;
  c_5.xyz = c_25.xyz;
  c_5.w = 1.0;
  gl_FragData[0] = c_5;
}


#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 82 math, 2 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
uniform mediump vec4 unity_4LightAtten0;
uniform mediump vec4 unity_LightColor[8];
uniform mediump vec4 unity_SHBr;
uniform mediump vec4 unity_SHBg;
uniform mediump vec4 unity_SHBb;
uniform mediump vec4 unity_SHC;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  mediump vec3 tmpvar_2;
  highp vec4 tmpvar_3;
  highp vec4 tmpvar_4;
  tmpvar_4.w = 1.0;
  tmpvar_4.xyz = _glesVertex.xyz;
  highp vec3 tmpvar_5;
  tmpvar_5 = (unity_ObjectToWorld * _glesVertex).xyz;
  highp mat3 tmpvar_6;
  tmpvar_6[0] = unity_WorldToObject[0].xyz;
  tmpvar_6[1] = unity_WorldToObject[1].xyz;
  tmpvar_6[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_glesNormal * tmpvar_6));
  tmpvar_1.xyz = tmpvar_5;
  highp vec3 lightColor0_8;
  lightColor0_8 = unity_LightColor[0].xyz;
  highp vec3 lightColor1_9;
  lightColor1_9 = unity_LightColor[1].xyz;
  highp vec3 lightColor2_10;
  lightColor2_10 = unity_LightColor[2].xyz;
  highp vec3 lightColor3_11;
  lightColor3_11 = unity_LightColor[3].xyz;
  highp vec4 lightAttenSq_12;
  lightAttenSq_12 = unity_4LightAtten0;
  highp vec3 col_13;
  highp vec4 ndotl_14;
  highp vec4 lengthSq_15;
  highp vec4 tmpvar_16;
  tmpvar_16 = (unity_4LightPosX0 - tmpvar_5.x);
  highp vec4 tmpvar_17;
  tmpvar_17 = (unity_4LightPosY0 - tmpvar_5.y);
  highp vec4 tmpvar_18;
  tmpvar_18 = (unity_4LightPosZ0 - tmpvar_5.z);
  lengthSq_15 = (tmpvar_16 * tmpvar_16);
  lengthSq_15 = (lengthSq_15 + (tmpvar_17 * tmpvar_17));
  lengthSq_15 = (lengthSq_15 + (tmpvar_18 * tmpvar_18));
  highp vec4 tmpvar_19;
  tmpvar_19 = max (lengthSq_15, vec4(1e-06, 1e-06, 1e-06, 1e-06));
  lengthSq_15 = tmpvar_19;
  ndotl_14 = (tmpvar_16 * tmpvar_7.x);
  ndotl_14 = (ndotl_14 + (tmpvar_17 * tmpvar_7.y));
  ndotl_14 = (ndotl_14 + (tmpvar_18 * tmpvar_7.z));
  highp vec4 tmpvar_20;
  tmpvar_20 = max (vec4(0.0, 0.0, 0.0, 0.0), (ndotl_14 * inversesqrt(tmpvar_19)));
  ndotl_14 = tmpvar_20;
  highp vec4 tmpvar_21;
  tmpvar_21 = (tmpvar_20 * (1.0/((1.0 + 
    (tmpvar_19 * lightAttenSq_12)
  ))));
  col_13 = (lightColor0_8 * tmpvar_21.x);
  col_13 = (col_13 + (lightColor1_9 * tmpvar_21.y));
  col_13 = (col_13 + (lightColor2_10 * tmpvar_21.z));
  col_13 = (col_13 + (lightColor3_11 * tmpvar_21.w));
  tmpvar_2 = col_13;
  mediump vec3 normal_22;
  normal_22 = tmpvar_7;
  mediump vec3 ambient_23;
  mediump vec3 x1_24;
  mediump vec4 tmpvar_25;
  tmpvar_25 = (normal_22.xyzz * normal_22.yzzx);
  x1_24.x = dot (unity_SHBr, tmpvar_25);
  x1_24.y = dot (unity_SHBg, tmpvar_25);
  x1_24.z = dot (unity_SHBb, tmpvar_25);
  ambient_23 = ((tmpvar_2 * (
    (tmpvar_2 * ((tmpvar_2 * 0.305306) + 0.6821711))
   + 0.01252288)) + (x1_24 + (unity_SHC.xyz * 
    ((normal_22.x * normal_22.x) - (normal_22.y * normal_22.y))
  )));
  tmpvar_2 = ambient_23;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_4));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = tmpvar_7;
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD3 = ambient_23;
  xlv_TEXCOORD6 = tmpvar_3;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 unity_SHAr;
uniform mediump vec4 unity_SHAg;
uniform mediump vec4 unity_SHAb;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  highp vec4 tmpvar_2;
  mediump vec3 tmpvar_3;
  mediump vec3 tmpvar_4;
  lowp vec4 c_5;
  lowp vec3 tmpvar_6;
  lowp float tmpvar_7;
  highp vec3 worldViewDir_8;
  lowp vec3 lightDir_9;
  mediump vec3 tmpvar_10;
  tmpvar_10 = _WorldSpaceLightPos0.xyz;
  lightDir_9 = tmpvar_10;
  worldViewDir_8 = normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz));
  lowp vec4 tmpvar_11;
  tmpvar_11 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_6 = tmpvar_11.xyz;
  tmpvar_7 = tmpvar_11.w;
  tmpvar_3 = _LightColor0.xyz;
  tmpvar_4 = lightDir_9;
  tmpvar_1 = worldViewDir_8;
  tmpvar_2 = unity_SpecCube0_HDR;
  mediump vec3 Normal_12;
  Normal_12 = xlv_TEXCOORD1;
  mediump float tmpvar_13;
  highp float tmpvar_14;
  highp float smoothness_15;
  smoothness_15 = _Glossiness;
  tmpvar_14 = (1.0 - smoothness_15);
  tmpvar_13 = tmpvar_14;
  mediump vec3 I_16;
  I_16 = -(tmpvar_1);
  mediump vec3 normalWorld_17;
  normalWorld_17 = xlv_TEXCOORD1;
  mediump vec3 tmpvar_18;
  mediump vec3 tmpvar_19;
  mediump vec4 tmpvar_20;
  tmpvar_20.w = 1.0;
  tmpvar_20.xyz = normalWorld_17;
  mediump vec3 x_21;
  x_21.x = dot (unity_SHAr, tmpvar_20);
  x_21.y = dot (unity_SHAg, tmpvar_20);
  x_21.z = dot (unity_SHAb, tmpvar_20);
  tmpvar_19 = max (((1.055 * 
    pow (max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD3 + x_21)), vec3(0.4166667, 0.4166667, 0.4166667))
  ) - 0.055), vec3(0.0, 0.0, 0.0));
  mediump vec4 hdr_22;
  hdr_22 = tmpvar_2;
  mediump vec4 tmpvar_23;
  tmpvar_23.xyz = (I_16 - (2.0 * (
    dot (Normal_12, I_16)
   * Normal_12)));
  tmpvar_23.w = ((tmpvar_13 * (1.7 - 
    (0.7 * tmpvar_13)
  )) * 6.0);
  lowp vec4 tmpvar_24;
  tmpvar_24 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_23.xyz, tmpvar_23.w);
  mediump vec4 tmpvar_25;
  tmpvar_25 = tmpvar_24;
  tmpvar_18 = ((hdr_22.x * (
    (hdr_22.w * (tmpvar_25.w - 1.0))
   + 1.0)) * tmpvar_25.xyz);
  lowp vec3 tmpvar_26;
  mediump vec4 c_27;
  highp vec3 tmpvar_28;
  tmpvar_28 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_29;
  mediump vec3 albedo_30;
  albedo_30 = tmpvar_6;
  mediump vec3 tmpvar_31;
  tmpvar_31 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_30, vec3(_Metallic));
  mediump float tmpvar_32;
  tmpvar_32 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_29 = (albedo_30 * tmpvar_32);
  tmpvar_26 = tmpvar_29;
  mediump vec3 diffColor_33;
  diffColor_33 = tmpvar_26;
  mediump float alpha_34;
  alpha_34 = tmpvar_7;
  tmpvar_26 = diffColor_33;
  mediump vec3 diffColor_35;
  diffColor_35 = tmpvar_26;
  mediump vec3 color_36;
  mediump float surfaceReduction_37;
  highp float specularTerm_38;
  highp float a2_39;
  mediump float roughness_40;
  mediump float perceptualRoughness_41;
  highp vec3 tmpvar_42;
  highp vec3 inVec_43;
  inVec_43 = (tmpvar_4 + worldViewDir_8);
  tmpvar_42 = (inVec_43 * inversesqrt(max (0.001, 
    dot (inVec_43, inVec_43)
  )));
  mediump float tmpvar_44;
  highp float tmpvar_45;
  tmpvar_45 = clamp (dot (tmpvar_28, tmpvar_4), 0.0, 1.0);
  tmpvar_44 = tmpvar_45;
  highp float tmpvar_46;
  tmpvar_46 = clamp (dot (tmpvar_28, tmpvar_42), 0.0, 1.0);
  mediump float tmpvar_47;
  highp float tmpvar_48;
  tmpvar_48 = clamp (dot (tmpvar_28, worldViewDir_8), 0.0, 1.0);
  tmpvar_47 = tmpvar_48;
  highp float tmpvar_49;
  highp float smoothness_50;
  smoothness_50 = _Glossiness;
  tmpvar_49 = (1.0 - smoothness_50);
  perceptualRoughness_41 = tmpvar_49;
  highp float tmpvar_51;
  highp float perceptualRoughness_52;
  perceptualRoughness_52 = perceptualRoughness_41;
  tmpvar_51 = (perceptualRoughness_52 * perceptualRoughness_52);
  roughness_40 = tmpvar_51;
  mediump float tmpvar_53;
  tmpvar_53 = (roughness_40 * roughness_40);
  a2_39 = tmpvar_53;
  specularTerm_38 = ((roughness_40 / (
    (max (0.32, clamp (dot (tmpvar_4, tmpvar_42), 0.0, 1.0)) * (1.5 + roughness_40))
   * 
    (((tmpvar_46 * tmpvar_46) * (a2_39 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_54;
  tmpvar_54 = clamp (specularTerm_38, 0.0, 100.0);
  specularTerm_38 = tmpvar_54;
  surfaceReduction_37 = (1.0 - ((roughness_40 * perceptualRoughness_41) * 0.28));
  mediump float x_55;
  x_55 = (1.0 - tmpvar_47);
  mediump vec3 tmpvar_56;
  tmpvar_56 = mix (tmpvar_31, vec3(clamp ((_Glossiness + 
    (1.0 - tmpvar_32)
  ), 0.0, 1.0)), vec3(((x_55 * x_55) * (x_55 * x_55))));
  highp vec3 tmpvar_57;
  tmpvar_57 = (((
    ((diffColor_35 + (tmpvar_54 * tmpvar_31)) * tmpvar_3)
   * tmpvar_44) + (tmpvar_19 * diffColor_35)) + ((surfaceReduction_37 * tmpvar_18) * tmpvar_56));
  color_36 = tmpvar_57;
  mediump vec4 tmpvar_58;
  tmpvar_58.w = 1.0;
  tmpvar_58.xyz = color_36;
  c_27.xyz = tmpvar_58.xyz;
  c_27.w = alpha_34;
  c_5.xyz = c_27.xyz;
  c_5.w = 1.0;
  gl_FragData[0] = c_5;
}


#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 82 math, 2 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
uniform mediump vec4 unity_4LightAtten0;
uniform mediump vec4 unity_LightColor[8];
uniform mediump vec4 unity_SHBr;
uniform mediump vec4 unity_SHBg;
uniform mediump vec4 unity_SHBb;
uniform mediump vec4 unity_SHC;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  mediump vec3 tmpvar_2;
  highp vec4 tmpvar_3;
  highp vec4 tmpvar_4;
  tmpvar_4.w = 1.0;
  tmpvar_4.xyz = _glesVertex.xyz;
  highp vec3 tmpvar_5;
  tmpvar_5 = (unity_ObjectToWorld * _glesVertex).xyz;
  highp mat3 tmpvar_6;
  tmpvar_6[0] = unity_WorldToObject[0].xyz;
  tmpvar_6[1] = unity_WorldToObject[1].xyz;
  tmpvar_6[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_glesNormal * tmpvar_6));
  tmpvar_1.xyz = tmpvar_5;
  highp vec3 lightColor0_8;
  lightColor0_8 = unity_LightColor[0].xyz;
  highp vec3 lightColor1_9;
  lightColor1_9 = unity_LightColor[1].xyz;
  highp vec3 lightColor2_10;
  lightColor2_10 = unity_LightColor[2].xyz;
  highp vec3 lightColor3_11;
  lightColor3_11 = unity_LightColor[3].xyz;
  highp vec4 lightAttenSq_12;
  lightAttenSq_12 = unity_4LightAtten0;
  highp vec3 col_13;
  highp vec4 ndotl_14;
  highp vec4 lengthSq_15;
  highp vec4 tmpvar_16;
  tmpvar_16 = (unity_4LightPosX0 - tmpvar_5.x);
  highp vec4 tmpvar_17;
  tmpvar_17 = (unity_4LightPosY0 - tmpvar_5.y);
  highp vec4 tmpvar_18;
  tmpvar_18 = (unity_4LightPosZ0 - tmpvar_5.z);
  lengthSq_15 = (tmpvar_16 * tmpvar_16);
  lengthSq_15 = (lengthSq_15 + (tmpvar_17 * tmpvar_17));
  lengthSq_15 = (lengthSq_15 + (tmpvar_18 * tmpvar_18));
  highp vec4 tmpvar_19;
  tmpvar_19 = max (lengthSq_15, vec4(1e-06, 1e-06, 1e-06, 1e-06));
  lengthSq_15 = tmpvar_19;
  ndotl_14 = (tmpvar_16 * tmpvar_7.x);
  ndotl_14 = (ndotl_14 + (tmpvar_17 * tmpvar_7.y));
  ndotl_14 = (ndotl_14 + (tmpvar_18 * tmpvar_7.z));
  highp vec4 tmpvar_20;
  tmpvar_20 = max (vec4(0.0, 0.0, 0.0, 0.0), (ndotl_14 * inversesqrt(tmpvar_19)));
  ndotl_14 = tmpvar_20;
  highp vec4 tmpvar_21;
  tmpvar_21 = (tmpvar_20 * (1.0/((1.0 + 
    (tmpvar_19 * lightAttenSq_12)
  ))));
  col_13 = (lightColor0_8 * tmpvar_21.x);
  col_13 = (col_13 + (lightColor1_9 * tmpvar_21.y));
  col_13 = (col_13 + (lightColor2_10 * tmpvar_21.z));
  col_13 = (col_13 + (lightColor3_11 * tmpvar_21.w));
  tmpvar_2 = col_13;
  mediump vec3 normal_22;
  normal_22 = tmpvar_7;
  mediump vec3 ambient_23;
  mediump vec3 x1_24;
  mediump vec4 tmpvar_25;
  tmpvar_25 = (normal_22.xyzz * normal_22.yzzx);
  x1_24.x = dot (unity_SHBr, tmpvar_25);
  x1_24.y = dot (unity_SHBg, tmpvar_25);
  x1_24.z = dot (unity_SHBb, tmpvar_25);
  ambient_23 = ((tmpvar_2 * (
    (tmpvar_2 * ((tmpvar_2 * 0.305306) + 0.6821711))
   + 0.01252288)) + (x1_24 + (unity_SHC.xyz * 
    ((normal_22.x * normal_22.x) - (normal_22.y * normal_22.y))
  )));
  tmpvar_2 = ambient_23;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_4));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = tmpvar_7;
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD3 = ambient_23;
  xlv_TEXCOORD6 = tmpvar_3;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 unity_SHAr;
uniform mediump vec4 unity_SHAg;
uniform mediump vec4 unity_SHAb;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  highp vec4 tmpvar_2;
  mediump vec3 tmpvar_3;
  mediump vec3 tmpvar_4;
  lowp vec4 c_5;
  lowp vec3 tmpvar_6;
  lowp float tmpvar_7;
  highp vec3 worldViewDir_8;
  lowp vec3 lightDir_9;
  mediump vec3 tmpvar_10;
  tmpvar_10 = _WorldSpaceLightPos0.xyz;
  lightDir_9 = tmpvar_10;
  worldViewDir_8 = normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz));
  lowp vec4 tmpvar_11;
  tmpvar_11 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_6 = tmpvar_11.xyz;
  tmpvar_7 = tmpvar_11.w;
  tmpvar_3 = _LightColor0.xyz;
  tmpvar_4 = lightDir_9;
  tmpvar_1 = worldViewDir_8;
  tmpvar_2 = unity_SpecCube0_HDR;
  mediump vec3 Normal_12;
  Normal_12 = xlv_TEXCOORD1;
  mediump float tmpvar_13;
  highp float tmpvar_14;
  highp float smoothness_15;
  smoothness_15 = _Glossiness;
  tmpvar_14 = (1.0 - smoothness_15);
  tmpvar_13 = tmpvar_14;
  mediump vec3 I_16;
  I_16 = -(tmpvar_1);
  mediump vec3 normalWorld_17;
  normalWorld_17 = xlv_TEXCOORD1;
  mediump vec3 tmpvar_18;
  mediump vec3 tmpvar_19;
  mediump vec4 tmpvar_20;
  tmpvar_20.w = 1.0;
  tmpvar_20.xyz = normalWorld_17;
  mediump vec3 x_21;
  x_21.x = dot (unity_SHAr, tmpvar_20);
  x_21.y = dot (unity_SHAg, tmpvar_20);
  x_21.z = dot (unity_SHAb, tmpvar_20);
  tmpvar_19 = max (((1.055 * 
    pow (max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD3 + x_21)), vec3(0.4166667, 0.4166667, 0.4166667))
  ) - 0.055), vec3(0.0, 0.0, 0.0));
  mediump vec4 hdr_22;
  hdr_22 = tmpvar_2;
  mediump vec4 tmpvar_23;
  tmpvar_23.xyz = (I_16 - (2.0 * (
    dot (Normal_12, I_16)
   * Normal_12)));
  tmpvar_23.w = ((tmpvar_13 * (1.7 - 
    (0.7 * tmpvar_13)
  )) * 6.0);
  lowp vec4 tmpvar_24;
  tmpvar_24 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_23.xyz, tmpvar_23.w);
  mediump vec4 tmpvar_25;
  tmpvar_25 = tmpvar_24;
  tmpvar_18 = ((hdr_22.x * (
    (hdr_22.w * (tmpvar_25.w - 1.0))
   + 1.0)) * tmpvar_25.xyz);
  lowp vec3 tmpvar_26;
  mediump vec4 c_27;
  highp vec3 tmpvar_28;
  tmpvar_28 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_29;
  mediump vec3 albedo_30;
  albedo_30 = tmpvar_6;
  mediump vec3 tmpvar_31;
  tmpvar_31 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_30, vec3(_Metallic));
  mediump float tmpvar_32;
  tmpvar_32 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_29 = (albedo_30 * tmpvar_32);
  tmpvar_26 = tmpvar_29;
  mediump vec3 diffColor_33;
  diffColor_33 = tmpvar_26;
  mediump float alpha_34;
  alpha_34 = tmpvar_7;
  tmpvar_26 = diffColor_33;
  mediump vec3 diffColor_35;
  diffColor_35 = tmpvar_26;
  mediump vec3 color_36;
  mediump float surfaceReduction_37;
  highp float specularTerm_38;
  highp float a2_39;
  mediump float roughness_40;
  mediump float perceptualRoughness_41;
  highp vec3 tmpvar_42;
  highp vec3 inVec_43;
  inVec_43 = (tmpvar_4 + worldViewDir_8);
  tmpvar_42 = (inVec_43 * inversesqrt(max (0.001, 
    dot (inVec_43, inVec_43)
  )));
  mediump float tmpvar_44;
  highp float tmpvar_45;
  tmpvar_45 = clamp (dot (tmpvar_28, tmpvar_4), 0.0, 1.0);
  tmpvar_44 = tmpvar_45;
  highp float tmpvar_46;
  tmpvar_46 = clamp (dot (tmpvar_28, tmpvar_42), 0.0, 1.0);
  mediump float tmpvar_47;
  highp float tmpvar_48;
  tmpvar_48 = clamp (dot (tmpvar_28, worldViewDir_8), 0.0, 1.0);
  tmpvar_47 = tmpvar_48;
  highp float tmpvar_49;
  highp float smoothness_50;
  smoothness_50 = _Glossiness;
  tmpvar_49 = (1.0 - smoothness_50);
  perceptualRoughness_41 = tmpvar_49;
  highp float tmpvar_51;
  highp float perceptualRoughness_52;
  perceptualRoughness_52 = perceptualRoughness_41;
  tmpvar_51 = (perceptualRoughness_52 * perceptualRoughness_52);
  roughness_40 = tmpvar_51;
  mediump float tmpvar_53;
  tmpvar_53 = (roughness_40 * roughness_40);
  a2_39 = tmpvar_53;
  specularTerm_38 = ((roughness_40 / (
    (max (0.32, clamp (dot (tmpvar_4, tmpvar_42), 0.0, 1.0)) * (1.5 + roughness_40))
   * 
    (((tmpvar_46 * tmpvar_46) * (a2_39 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_54;
  tmpvar_54 = clamp (specularTerm_38, 0.0, 100.0);
  specularTerm_38 = tmpvar_54;
  surfaceReduction_37 = (1.0 - ((roughness_40 * perceptualRoughness_41) * 0.28));
  mediump float x_55;
  x_55 = (1.0 - tmpvar_47);
  mediump vec3 tmpvar_56;
  tmpvar_56 = mix (tmpvar_31, vec3(clamp ((_Glossiness + 
    (1.0 - tmpvar_32)
  ), 0.0, 1.0)), vec3(((x_55 * x_55) * (x_55 * x_55))));
  highp vec3 tmpvar_57;
  tmpvar_57 = (((
    ((diffColor_35 + (tmpvar_54 * tmpvar_31)) * tmpvar_3)
   * tmpvar_44) + (tmpvar_19 * diffColor_35)) + ((surfaceReduction_37 * tmpvar_18) * tmpvar_56));
  color_36 = tmpvar_57;
  mediump vec4 tmpvar_58;
  tmpvar_58.w = 1.0;
  tmpvar_58.xyz = color_36;
  c_27.xyz = tmpvar_58.xyz;
  c_27.w = alpha_34;
  c_5.xyz = c_27.xyz;
  c_5.w = 1.0;
  gl_FragData[0] = c_5;
}


#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 unity_4LightPosX0;
uniform 	vec4 unity_4LightPosY0;
uniform 	vec4 unity_4LightPosZ0;
uniform 	mediump vec4 unity_4LightAtten0;
uniform 	mediump vec4 unity_LightColor[8];
uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out mediump vec3 vs_TEXCOORD3;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
mediump vec4 u_xlat16_1;
vec4 u_xlat2;
vec4 u_xlat3;
vec4 u_xlat4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
float u_xlat21;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat1.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat21 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat1.xyz = vec3(u_xlat21) * u_xlat1.xyz;
    vs_TEXCOORD1.xyz = u_xlat1.xyz;
    vs_TEXCOORD2.xyz = u_xlat0.xyz;
    u_xlat2 = (-u_xlat0.xxxx) + unity_4LightPosX0;
    u_xlat3 = (-u_xlat0.yyyy) + unity_4LightPosY0;
    u_xlat0 = (-u_xlat0.zzzz) + unity_4LightPosZ0;
    u_xlat4 = u_xlat1.yyyy * u_xlat3;
    u_xlat3 = u_xlat3 * u_xlat3;
    u_xlat3 = u_xlat2 * u_xlat2 + u_xlat3;
    u_xlat2 = u_xlat2 * u_xlat1.xxxx + u_xlat4;
    u_xlat2 = u_xlat0 * u_xlat1.zzzz + u_xlat2;
    u_xlat0 = u_xlat0 * u_xlat0 + u_xlat3;
    u_xlat0 = max(u_xlat0, vec4(9.99999997e-07, 9.99999997e-07, 9.99999997e-07, 9.99999997e-07));
    u_xlat3 = inversesqrt(u_xlat0);
    u_xlat0 = u_xlat0 * unity_4LightAtten0 + vec4(1.0, 1.0, 1.0, 1.0);
    u_xlat0 = vec4(1.0, 1.0, 1.0, 1.0) / u_xlat0;
    u_xlat2 = u_xlat2 * u_xlat3;
    u_xlat2 = max(u_xlat2, vec4(0.0, 0.0, 0.0, 0.0));
    u_xlat0 = u_xlat0 * u_xlat2;
    u_xlat2.xyz = u_xlat0.yyy * unity_LightColor[1].xyz;
    u_xlat2.xyz = unity_LightColor[0].xyz * u_xlat0.xxx + u_xlat2.xyz;
    u_xlat0.xyz = unity_LightColor[2].xyz * u_xlat0.zzz + u_xlat2.xyz;
    u_xlat0.xyz = unity_LightColor[3].xyz * u_xlat0.www + u_xlat0.xyz;
    u_xlat2.xyz = u_xlat0.xyz * vec3(0.305306017, 0.305306017, 0.305306017) + vec3(0.682171106, 0.682171106, 0.682171106);
    u_xlat2.xyz = u_xlat0.xyz * u_xlat2.xyz + vec3(0.0125228781, 0.0125228781, 0.0125228781);
    u_xlat16_5.x = u_xlat1.y * u_xlat1.y;
    u_xlat16_5.x = u_xlat1.x * u_xlat1.x + (-u_xlat16_5.x);
    u_xlat16_1 = u_xlat1.yzzx * u_xlat1.xyzz;
    u_xlat16_6.x = dot(unity_SHBr, u_xlat16_1);
    u_xlat16_6.y = dot(unity_SHBg, u_xlat16_1);
    u_xlat16_6.z = dot(unity_SHBb, u_xlat16_1);
    u_xlat16_5.xyz = unity_SHC.xyz * u_xlat16_5.xxx + u_xlat16_6.xyz;
    vs_TEXCOORD3.xyz = u_xlat0.xyz * u_xlat2.xyz + u_xlat16_5.xyz;
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp samplerCube unity_SpecCube0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
in mediump vec3 vs_TEXCOORD3;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump vec3 u_xlat16_0;
mediump vec3 u_xlat16_1;
lowp vec4 u_xlat10_1;
vec3 u_xlat2;
mediump float u_xlat16_2;
vec3 u_xlat3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
mediump vec3 u_xlat16_8;
mediump vec3 u_xlat16_9;
mediump vec3 u_xlat16_12;
lowp vec3 u_xlat10_12;
float u_xlat30;
mediump float u_xlat16_31;
float u_xlat32;
mediump float u_xlat16_34;
mediump float u_xlat16_36;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD1.xyz;
    u_xlat0.w = 1.0;
    u_xlat16_1.x = dot(unity_SHAr, u_xlat0);
    u_xlat16_1.y = dot(unity_SHAg, u_xlat0);
    u_xlat16_1.z = dot(unity_SHAb, u_xlat0);
    u_xlat16_1.xyz = u_xlat16_1.xyz + vs_TEXCOORD3.xyz;
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_0.xyz = log2(u_xlat16_1.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_0.xyz = exp2(u_xlat16_0.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_0.xyz = max(u_xlat16_0.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat2.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat30 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat30 = inversesqrt(u_xlat30);
    u_xlat2.xyz = vec3(u_xlat30) * u_xlat2.xyz;
    u_xlat16_1.x = dot((-u_xlat2.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_1.x = u_xlat16_1.x + u_xlat16_1.x;
    u_xlat16_1.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_1.xxx) + (-u_xlat2.xyz);
    u_xlat3.z = (-_Glossiness) + 1.0;
    u_xlat16_31 = (-u_xlat3.z) * 0.699999988 + 1.70000005;
    u_xlat16_31 = u_xlat16_31 * u_xlat3.z;
    u_xlat16_31 = u_xlat16_31 * 6.0;
    u_xlat10_1 = textureLod(unity_SpecCube0, u_xlat16_1.xyz, u_xlat16_31);
    u_xlat16_4.x = u_xlat10_1.w + -1.0;
    u_xlat16_4.x = unity_SpecCube0_HDR.w * u_xlat16_4.x + 1.0;
    u_xlat16_4.x = u_xlat16_4.x * unity_SpecCube0_HDR.x;
    u_xlat16_4.xyz = u_xlat10_1.xyz * u_xlat16_4.xxx;
    u_xlat30 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat30 = inversesqrt(u_xlat30);
    u_xlat5.xyz = vec3(u_xlat30) * vs_TEXCOORD1.xyz;
    u_xlat30 = dot(u_xlat2.xyz, u_xlat5.xyz);
    u_xlat32 = u_xlat30;
#ifdef UNITY_ADRENO_ES3
    u_xlat32 = min(max(u_xlat32, 0.0), 1.0);
#else
    u_xlat32 = clamp(u_xlat32, 0.0, 1.0);
#endif
    u_xlat30 = u_xlat30 + u_xlat30;
    u_xlat2.xyz = u_xlat5.xyz * (-vec3(u_xlat30)) + u_xlat2.xyz;
    u_xlat30 = dot(u_xlat5.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat30 = min(max(u_xlat30, 0.0), 1.0);
#else
    u_xlat30 = clamp(u_xlat30, 0.0, 1.0);
#endif
    u_xlat16_6.xyz = vec3(u_xlat30) * _LightColor0.xyz;
    u_xlat30 = dot(u_xlat2.xyz, _WorldSpaceLightPos0.xyz);
    u_xlat30 = u_xlat30 * u_xlat30;
    u_xlat3.x = u_xlat30 * u_xlat30;
    u_xlat30 = texture(unity_NHxRoughness, u_xlat3.xz).w;
    u_xlat30 = u_xlat30 * 16.0;
    u_xlat16_34 = (-u_xlat32) + 1.0;
    u_xlat16_2 = u_xlat16_34 * u_xlat16_34;
    u_xlat16_2 = u_xlat16_34 * u_xlat16_2;
    u_xlat16_2 = u_xlat16_34 * u_xlat16_2;
    u_xlat16_34 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_36 = (-u_xlat16_34) + _Glossiness;
    u_xlat16_36 = u_xlat16_36 + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_36 = min(max(u_xlat16_36, 0.0), 1.0);
#else
    u_xlat16_36 = clamp(u_xlat16_36, 0.0, 1.0);
#endif
    u_xlat10_12.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_7.xyz = u_xlat10_12.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_12.xyz = u_xlat10_12.xyz * _Color.xyz;
    u_xlat16_8.xyz = vec3(u_xlat16_34) * u_xlat16_12.xyz;
    u_xlat16_7.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_7.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_9.xyz = vec3(u_xlat16_36) + (-u_xlat16_7.xyz);
    u_xlat16_9.xyz = vec3(u_xlat16_2) * u_xlat16_9.xyz + u_xlat16_7.xyz;
    u_xlat16_7.xyz = vec3(u_xlat30) * u_xlat16_7.xyz + u_xlat16_8.xyz;
    u_xlat16_4.xyz = u_xlat16_4.xyz * u_xlat16_9.xyz;
    u_xlat16_4.xyz = u_xlat16_0.xyz * u_xlat16_8.xyz + u_xlat16_4.xyz;
    SV_Target0.xyz = u_xlat16_7.xyz * u_xlat16_6.xyz + u_xlat16_4.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 unity_4LightPosX0;
uniform 	vec4 unity_4LightPosY0;
uniform 	vec4 unity_4LightPosZ0;
uniform 	mediump vec4 unity_4LightAtten0;
uniform 	mediump vec4 unity_LightColor[8];
uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out mediump vec3 vs_TEXCOORD3;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
mediump vec4 u_xlat16_1;
vec4 u_xlat2;
vec4 u_xlat3;
vec4 u_xlat4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
float u_xlat21;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat1.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat21 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat1.xyz = vec3(u_xlat21) * u_xlat1.xyz;
    vs_TEXCOORD1.xyz = u_xlat1.xyz;
    vs_TEXCOORD2.xyz = u_xlat0.xyz;
    u_xlat2 = (-u_xlat0.xxxx) + unity_4LightPosX0;
    u_xlat3 = (-u_xlat0.yyyy) + unity_4LightPosY0;
    u_xlat0 = (-u_xlat0.zzzz) + unity_4LightPosZ0;
    u_xlat4 = u_xlat1.yyyy * u_xlat3;
    u_xlat3 = u_xlat3 * u_xlat3;
    u_xlat3 = u_xlat2 * u_xlat2 + u_xlat3;
    u_xlat2 = u_xlat2 * u_xlat1.xxxx + u_xlat4;
    u_xlat2 = u_xlat0 * u_xlat1.zzzz + u_xlat2;
    u_xlat0 = u_xlat0 * u_xlat0 + u_xlat3;
    u_xlat0 = max(u_xlat0, vec4(9.99999997e-07, 9.99999997e-07, 9.99999997e-07, 9.99999997e-07));
    u_xlat3 = inversesqrt(u_xlat0);
    u_xlat0 = u_xlat0 * unity_4LightAtten0 + vec4(1.0, 1.0, 1.0, 1.0);
    u_xlat0 = vec4(1.0, 1.0, 1.0, 1.0) / u_xlat0;
    u_xlat2 = u_xlat2 * u_xlat3;
    u_xlat2 = max(u_xlat2, vec4(0.0, 0.0, 0.0, 0.0));
    u_xlat0 = u_xlat0 * u_xlat2;
    u_xlat2.xyz = u_xlat0.yyy * unity_LightColor[1].xyz;
    u_xlat2.xyz = unity_LightColor[0].xyz * u_xlat0.xxx + u_xlat2.xyz;
    u_xlat0.xyz = unity_LightColor[2].xyz * u_xlat0.zzz + u_xlat2.xyz;
    u_xlat0.xyz = unity_LightColor[3].xyz * u_xlat0.www + u_xlat0.xyz;
    u_xlat2.xyz = u_xlat0.xyz * vec3(0.305306017, 0.305306017, 0.305306017) + vec3(0.682171106, 0.682171106, 0.682171106);
    u_xlat2.xyz = u_xlat0.xyz * u_xlat2.xyz + vec3(0.0125228781, 0.0125228781, 0.0125228781);
    u_xlat16_5.x = u_xlat1.y * u_xlat1.y;
    u_xlat16_5.x = u_xlat1.x * u_xlat1.x + (-u_xlat16_5.x);
    u_xlat16_1 = u_xlat1.yzzx * u_xlat1.xyzz;
    u_xlat16_6.x = dot(unity_SHBr, u_xlat16_1);
    u_xlat16_6.y = dot(unity_SHBg, u_xlat16_1);
    u_xlat16_6.z = dot(unity_SHBb, u_xlat16_1);
    u_xlat16_5.xyz = unity_SHC.xyz * u_xlat16_5.xxx + u_xlat16_6.xyz;
    vs_TEXCOORD3.xyz = u_xlat0.xyz * u_xlat2.xyz + u_xlat16_5.xyz;
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform lowp samplerCube unity_SpecCube0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
in mediump vec3 vs_TEXCOORD3;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump vec3 u_xlat16_0;
lowp vec4 u_xlat10_0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
lowp vec3 u_xlat10_2;
vec3 u_xlat3;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
mediump float u_xlat16_9;
mediump vec3 u_xlat16_11;
mediump float u_xlat16_16;
float u_xlat21;
mediump float u_xlat16_22;
mediump float u_xlat16_23;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD1.xyz;
    u_xlat0.w = 1.0;
    u_xlat16_1.x = dot(unity_SHAr, u_xlat0);
    u_xlat16_1.y = dot(unity_SHAg, u_xlat0);
    u_xlat16_1.z = dot(unity_SHAb, u_xlat0);
    u_xlat16_1.xyz = u_xlat16_1.xyz + vs_TEXCOORD3.xyz;
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_0.xyz = log2(u_xlat16_1.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_0.xyz = exp2(u_xlat16_0.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_0.xyz = max(u_xlat16_0.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat10_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_2.xyz * _Color.xyz;
    u_xlat16_1.xyz = u_xlat10_2.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_1.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_22 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_4.xyz = vec3(u_xlat16_22) * u_xlat16_3.xyz;
    u_xlat16_22 = (-u_xlat16_22) + _Glossiness;
    u_xlat16_22 = u_xlat16_22 + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_22 = min(max(u_xlat16_22, 0.0), 1.0);
#else
    u_xlat16_22 = clamp(u_xlat16_22, 0.0, 1.0);
#endif
    u_xlat16_5.xyz = (-u_xlat16_1.xyz) + vec3(u_xlat16_22);
    u_xlat16_6.xyz = u_xlat16_0.xyz * u_xlat16_4.xyz;
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat21 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat2.xyz = u_xlat0.xyz * vec3(u_xlat21) + _WorldSpaceLightPos0.xyz;
    u_xlat0.xyz = vec3(u_xlat21) * u_xlat0.xyz;
    u_xlat21 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat21 = max(u_xlat21, 0.00100000005);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat2.xyz = vec3(u_xlat21) * u_xlat2.xyz;
    u_xlat21 = dot(_WorldSpaceLightPos0.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat21 = min(max(u_xlat21, 0.0), 1.0);
#else
    u_xlat21 = clamp(u_xlat21, 0.0, 1.0);
#endif
    u_xlat21 = max(u_xlat21, 0.319999993);
    u_xlat16_23 = (-_Glossiness) + 1.0;
    u_xlat16_3.x = u_xlat16_23 * u_xlat16_23 + 1.5;
    u_xlat21 = u_xlat21 * u_xlat16_3.x;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat2.x = dot(u_xlat3.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat2.x = min(max(u_xlat2.x, 0.0), 1.0);
#else
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
#endif
    u_xlat2.x = u_xlat2.x * u_xlat2.x;
    u_xlat16_9 = u_xlat16_23 * u_xlat16_23;
    u_xlat16_16 = u_xlat16_9 * u_xlat16_9 + -1.0;
    u_xlat2.x = u_xlat2.x * u_xlat16_16 + 1.00001001;
    u_xlat21 = u_xlat21 * u_xlat2.x;
    u_xlat21 = u_xlat16_9 / u_xlat21;
    u_xlat16_22 = u_xlat16_23 * u_xlat16_9;
    u_xlat16_22 = (-u_xlat16_22) * 0.280000001 + 1.0;
    u_xlat21 = u_xlat21 + -9.99999975e-05;
    u_xlat21 = max(u_xlat21, 0.0);
    u_xlat21 = min(u_xlat21, 100.0);
    u_xlat2.xyz = vec3(u_xlat21) * u_xlat16_1.xyz + u_xlat16_4.xyz;
    u_xlat2.xyz = u_xlat2.xyz * _LightColor0.xyz;
    u_xlat21 = dot(u_xlat3.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat21 = min(max(u_xlat21, 0.0), 1.0);
#else
    u_xlat21 = clamp(u_xlat21, 0.0, 1.0);
#endif
    u_xlat3.x = dot(u_xlat3.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat3.x = min(max(u_xlat3.x, 0.0), 1.0);
#else
    u_xlat3.x = clamp(u_xlat3.x, 0.0, 1.0);
#endif
    u_xlat16_4.x = (-u_xlat3.x) + 1.0;
    u_xlat16_4.x = u_xlat16_4.x * u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_4.x * u_xlat16_4.x;
    u_xlat16_1.xyz = u_xlat16_4.xxx * u_xlat16_5.xyz + u_xlat16_1.xyz;
    u_xlat2.xyz = u_xlat2.xyz * vec3(u_xlat21) + u_xlat16_6.xyz;
    u_xlat16_4.x = (-u_xlat16_23) * 0.699999988 + 1.70000005;
    u_xlat16_4.x = u_xlat16_23 * u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_4.x * 6.0;
    u_xlat16_11.x = dot((-u_xlat0.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_11.x = u_xlat16_11.x + u_xlat16_11.x;
    u_xlat16_11.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_11.xxx) + (-u_xlat0.xyz);
    u_xlat10_0 = textureLod(unity_SpecCube0, u_xlat16_11.xyz, u_xlat16_4.x);
    u_xlat16_4.x = u_xlat10_0.w + -1.0;
    u_xlat16_4.x = unity_SpecCube0_HDR.w * u_xlat16_4.x + 1.0;
    u_xlat16_4.x = u_xlat16_4.x * unity_SpecCube0_HDR.x;
    u_xlat16_4.xyz = u_xlat10_0.xyz * u_xlat16_4.xxx;
    u_xlat16_4.xyz = vec3(u_xlat16_22) * u_xlat16_4.xyz;
    u_xlat0.xyz = u_xlat16_4.xyz * u_xlat16_1.xyz + u_xlat2.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 unity_4LightPosX0;
uniform 	vec4 unity_4LightPosY0;
uniform 	vec4 unity_4LightPosZ0;
uniform 	mediump vec4 unity_4LightAtten0;
uniform 	mediump vec4 unity_LightColor[8];
uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out mediump vec3 vs_TEXCOORD3;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
mediump vec4 u_xlat16_1;
vec4 u_xlat2;
vec4 u_xlat3;
vec4 u_xlat4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
float u_xlat21;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat1.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat21 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat1.xyz = vec3(u_xlat21) * u_xlat1.xyz;
    vs_TEXCOORD1.xyz = u_xlat1.xyz;
    vs_TEXCOORD2.xyz = u_xlat0.xyz;
    u_xlat2 = (-u_xlat0.xxxx) + unity_4LightPosX0;
    u_xlat3 = (-u_xlat0.yyyy) + unity_4LightPosY0;
    u_xlat0 = (-u_xlat0.zzzz) + unity_4LightPosZ0;
    u_xlat4 = u_xlat1.yyyy * u_xlat3;
    u_xlat3 = u_xlat3 * u_xlat3;
    u_xlat3 = u_xlat2 * u_xlat2 + u_xlat3;
    u_xlat2 = u_xlat2 * u_xlat1.xxxx + u_xlat4;
    u_xlat2 = u_xlat0 * u_xlat1.zzzz + u_xlat2;
    u_xlat0 = u_xlat0 * u_xlat0 + u_xlat3;
    u_xlat0 = max(u_xlat0, vec4(9.99999997e-07, 9.99999997e-07, 9.99999997e-07, 9.99999997e-07));
    u_xlat3 = inversesqrt(u_xlat0);
    u_xlat0 = u_xlat0 * unity_4LightAtten0 + vec4(1.0, 1.0, 1.0, 1.0);
    u_xlat0 = vec4(1.0, 1.0, 1.0, 1.0) / u_xlat0;
    u_xlat2 = u_xlat2 * u_xlat3;
    u_xlat2 = max(u_xlat2, vec4(0.0, 0.0, 0.0, 0.0));
    u_xlat0 = u_xlat0 * u_xlat2;
    u_xlat2.xyz = u_xlat0.yyy * unity_LightColor[1].xyz;
    u_xlat2.xyz = unity_LightColor[0].xyz * u_xlat0.xxx + u_xlat2.xyz;
    u_xlat0.xyz = unity_LightColor[2].xyz * u_xlat0.zzz + u_xlat2.xyz;
    u_xlat0.xyz = unity_LightColor[3].xyz * u_xlat0.www + u_xlat0.xyz;
    u_xlat2.xyz = u_xlat0.xyz * vec3(0.305306017, 0.305306017, 0.305306017) + vec3(0.682171106, 0.682171106, 0.682171106);
    u_xlat2.xyz = u_xlat0.xyz * u_xlat2.xyz + vec3(0.0125228781, 0.0125228781, 0.0125228781);
    u_xlat16_5.x = u_xlat1.y * u_xlat1.y;
    u_xlat16_5.x = u_xlat1.x * u_xlat1.x + (-u_xlat16_5.x);
    u_xlat16_1 = u_xlat1.yzzx * u_xlat1.xyzz;
    u_xlat16_6.x = dot(unity_SHBr, u_xlat16_1);
    u_xlat16_6.y = dot(unity_SHBg, u_xlat16_1);
    u_xlat16_6.z = dot(unity_SHBb, u_xlat16_1);
    u_xlat16_5.xyz = unity_SHC.xyz * u_xlat16_5.xxx + u_xlat16_6.xyz;
    vs_TEXCOORD3.xyz = u_xlat0.xyz * u_xlat2.xyz + u_xlat16_5.xyz;
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform lowp samplerCube unity_SpecCube0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
in mediump vec3 vs_TEXCOORD3;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump vec3 u_xlat16_0;
lowp vec4 u_xlat10_0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
lowp vec3 u_xlat10_2;
vec3 u_xlat3;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
mediump float u_xlat16_9;
mediump vec3 u_xlat16_11;
mediump float u_xlat16_16;
float u_xlat21;
mediump float u_xlat16_22;
mediump float u_xlat16_23;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD1.xyz;
    u_xlat0.w = 1.0;
    u_xlat16_1.x = dot(unity_SHAr, u_xlat0);
    u_xlat16_1.y = dot(unity_SHAg, u_xlat0);
    u_xlat16_1.z = dot(unity_SHAb, u_xlat0);
    u_xlat16_1.xyz = u_xlat16_1.xyz + vs_TEXCOORD3.xyz;
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_0.xyz = log2(u_xlat16_1.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_0.xyz = exp2(u_xlat16_0.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_0.xyz = max(u_xlat16_0.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat10_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_2.xyz * _Color.xyz;
    u_xlat16_1.xyz = u_xlat10_2.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_1.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_22 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_4.xyz = vec3(u_xlat16_22) * u_xlat16_3.xyz;
    u_xlat16_22 = (-u_xlat16_22) + _Glossiness;
    u_xlat16_22 = u_xlat16_22 + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_22 = min(max(u_xlat16_22, 0.0), 1.0);
#else
    u_xlat16_22 = clamp(u_xlat16_22, 0.0, 1.0);
#endif
    u_xlat16_5.xyz = (-u_xlat16_1.xyz) + vec3(u_xlat16_22);
    u_xlat16_6.xyz = u_xlat16_0.xyz * u_xlat16_4.xyz;
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat21 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat2.xyz = u_xlat0.xyz * vec3(u_xlat21) + _WorldSpaceLightPos0.xyz;
    u_xlat0.xyz = vec3(u_xlat21) * u_xlat0.xyz;
    u_xlat21 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat21 = max(u_xlat21, 0.00100000005);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat2.xyz = vec3(u_xlat21) * u_xlat2.xyz;
    u_xlat21 = dot(_WorldSpaceLightPos0.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat21 = min(max(u_xlat21, 0.0), 1.0);
#else
    u_xlat21 = clamp(u_xlat21, 0.0, 1.0);
#endif
    u_xlat21 = max(u_xlat21, 0.319999993);
    u_xlat16_23 = (-_Glossiness) + 1.0;
    u_xlat16_3.x = u_xlat16_23 * u_xlat16_23 + 1.5;
    u_xlat21 = u_xlat21 * u_xlat16_3.x;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat2.x = dot(u_xlat3.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat2.x = min(max(u_xlat2.x, 0.0), 1.0);
#else
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
#endif
    u_xlat2.x = u_xlat2.x * u_xlat2.x;
    u_xlat16_9 = u_xlat16_23 * u_xlat16_23;
    u_xlat16_16 = u_xlat16_9 * u_xlat16_9 + -1.0;
    u_xlat2.x = u_xlat2.x * u_xlat16_16 + 1.00001001;
    u_xlat21 = u_xlat21 * u_xlat2.x;
    u_xlat21 = u_xlat16_9 / u_xlat21;
    u_xlat16_22 = u_xlat16_23 * u_xlat16_9;
    u_xlat16_22 = (-u_xlat16_22) * 0.280000001 + 1.0;
    u_xlat21 = u_xlat21 + -9.99999975e-05;
    u_xlat21 = max(u_xlat21, 0.0);
    u_xlat21 = min(u_xlat21, 100.0);
    u_xlat2.xyz = vec3(u_xlat21) * u_xlat16_1.xyz + u_xlat16_4.xyz;
    u_xlat2.xyz = u_xlat2.xyz * _LightColor0.xyz;
    u_xlat21 = dot(u_xlat3.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat21 = min(max(u_xlat21, 0.0), 1.0);
#else
    u_xlat21 = clamp(u_xlat21, 0.0, 1.0);
#endif
    u_xlat3.x = dot(u_xlat3.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat3.x = min(max(u_xlat3.x, 0.0), 1.0);
#else
    u_xlat3.x = clamp(u_xlat3.x, 0.0, 1.0);
#endif
    u_xlat16_4.x = (-u_xlat3.x) + 1.0;
    u_xlat16_4.x = u_xlat16_4.x * u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_4.x * u_xlat16_4.x;
    u_xlat16_1.xyz = u_xlat16_4.xxx * u_xlat16_5.xyz + u_xlat16_1.xyz;
    u_xlat2.xyz = u_xlat2.xyz * vec3(u_xlat21) + u_xlat16_6.xyz;
    u_xlat16_4.x = (-u_xlat16_23) * 0.699999988 + 1.70000005;
    u_xlat16_4.x = u_xlat16_23 * u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_4.x * 6.0;
    u_xlat16_11.x = dot((-u_xlat0.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_11.x = u_xlat16_11.x + u_xlat16_11.x;
    u_xlat16_11.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_11.xxx) + (-u_xlat0.xyz);
    u_xlat10_0 = textureLod(unity_SpecCube0, u_xlat16_11.xyz, u_xlat16_4.x);
    u_xlat16_4.x = u_xlat10_0.w + -1.0;
    u_xlat16_4.x = unity_SpecCube0_HDR.w * u_xlat16_4.x + 1.0;
    u_xlat16_4.x = u_xlat16_4.x * unity_SpecCube0_HDR.x;
    u_xlat16_4.xyz = u_xlat10_0.xyz * u_xlat16_4.xxx;
    u_xlat16_4.xyz = vec3(u_xlat16_22) * u_xlat16_4.xyz;
    u_xlat0.xyz = u_xlat16_4.xyz * u_xlat16_1.xyz + u_xlat2.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 63 math, 4 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD5;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  highp vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_4;
  tmpvar_4[0] = unity_WorldToObject[0].xyz;
  tmpvar_4[1] = unity_WorldToObject[1].xyz;
  tmpvar_4[2] = unity_WorldToObject[2].xyz;
  highp vec4 tmpvar_5;
  tmpvar_5 = (unity_ObjectToWorld * _glesVertex);
  tmpvar_1.xyz = tmpvar_5.xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_4));
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD5 = (unity_WorldToShadow[0] * tmpvar_5);
  xlv_TEXCOORD6 = tmpvar_2;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform highp sampler2D _ShadowMapTexture;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD5;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump float tmpvar_2;
  highp vec4 tmpvar_3;
  mediump vec3 tmpvar_4;
  mediump vec3 tmpvar_5;
  lowp vec4 c_6;
  lowp float atten_7;
  lowp vec3 lightDir_8;
  mediump vec3 tmpvar_9;
  tmpvar_9 = _WorldSpaceLightPos0.xyz;
  lightDir_8 = tmpvar_9;
  highp vec3 tmpvar_10;
  highp vec3 tmpvar_11;
  tmpvar_11 = (_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz);
  tmpvar_10 = normalize(tmpvar_11);
  lowp vec3 tmpvar_12;
  lowp float tmpvar_13;
  lowp vec4 tmpvar_14;
  tmpvar_14 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_12 = tmpvar_14.xyz;
  tmpvar_13 = tmpvar_14.w;
  mediump float realtimeShadowAttenuation_15;
  highp vec4 v_16;
  v_16.x = unity_MatrixV[0].z;
  v_16.y = unity_MatrixV[1].z;
  v_16.z = unity_MatrixV[2].z;
  v_16.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_17;
  tmpvar_17 = (xlv_TEXCOORD2.xyz - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_18;
  highp float tmpvar_19;
  tmpvar_19 = clamp (((
    mix (dot (tmpvar_11, v_16.xyz), sqrt(dot (tmpvar_17, tmpvar_17)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_18 = tmpvar_19;
  lowp float tmpvar_20;
  highp float lightShadowDataX_21;
  mediump float tmpvar_22;
  tmpvar_22 = _LightShadowData.x;
  lightShadowDataX_21 = tmpvar_22;
  highp float tmpvar_23;
  tmpvar_23 = max (float((texture2D (_ShadowMapTexture, xlv_TEXCOORD5.xy).x > xlv_TEXCOORD5.z)), lightShadowDataX_21);
  tmpvar_20 = tmpvar_23;
  realtimeShadowAttenuation_15 = tmpvar_20;
  mediump float tmpvar_24;
  tmpvar_24 = mix (realtimeShadowAttenuation_15, 1.0, tmpvar_18);
  atten_7 = tmpvar_24;
  tmpvar_4 = _LightColor0.xyz;
  tmpvar_5 = lightDir_8;
  tmpvar_1 = tmpvar_10;
  tmpvar_2 = atten_7;
  tmpvar_3 = unity_SpecCube0_HDR;
  mediump vec3 Normal_25;
  Normal_25 = xlv_TEXCOORD1;
  mediump float tmpvar_26;
  highp float tmpvar_27;
  highp float smoothness_28;
  smoothness_28 = _Glossiness;
  tmpvar_27 = (1.0 - smoothness_28);
  tmpvar_26 = tmpvar_27;
  mediump vec3 I_29;
  I_29 = -(tmpvar_1);
  mediump vec3 tmpvar_30;
  tmpvar_30 = (tmpvar_4 * tmpvar_2);
  mediump vec4 hdr_31;
  hdr_31 = tmpvar_3;
  mediump vec4 tmpvar_32;
  tmpvar_32.xyz = (I_29 - (2.0 * (
    dot (Normal_25, I_29)
   * Normal_25)));
  tmpvar_32.w = ((tmpvar_26 * (1.7 - 
    (0.7 * tmpvar_26)
  )) * 6.0);
  lowp vec4 tmpvar_33;
  tmpvar_33 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_32.xyz, tmpvar_32.w);
  mediump vec4 tmpvar_34;
  tmpvar_34 = tmpvar_33;
  tmpvar_4 = tmpvar_30;
  lowp vec3 tmpvar_35;
  mediump vec4 c_36;
  highp vec3 tmpvar_37;
  tmpvar_37 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_38;
  mediump vec3 albedo_39;
  albedo_39 = tmpvar_12;
  mediump vec3 tmpvar_40;
  tmpvar_40 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_39, vec3(_Metallic));
  mediump float tmpvar_41;
  tmpvar_41 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_38 = (albedo_39 * tmpvar_41);
  tmpvar_35 = tmpvar_38;
  mediump vec3 diffColor_42;
  diffColor_42 = tmpvar_35;
  mediump float alpha_43;
  alpha_43 = tmpvar_13;
  tmpvar_35 = diffColor_42;
  mediump vec3 diffColor_44;
  diffColor_44 = tmpvar_35;
  mediump vec3 color_45;
  mediump vec2 rlPow4AndFresnelTerm_46;
  mediump float tmpvar_47;
  highp float tmpvar_48;
  tmpvar_48 = clamp (dot (tmpvar_37, tmpvar_5), 0.0, 1.0);
  tmpvar_47 = tmpvar_48;
  mediump float tmpvar_49;
  highp float tmpvar_50;
  tmpvar_50 = clamp (dot (tmpvar_37, tmpvar_10), 0.0, 1.0);
  tmpvar_49 = tmpvar_50;
  highp vec2 tmpvar_51;
  tmpvar_51.x = dot ((tmpvar_10 - (2.0 * 
    (dot (tmpvar_37, tmpvar_10) * tmpvar_37)
  )), tmpvar_5);
  tmpvar_51.y = (1.0 - tmpvar_49);
  highp vec2 tmpvar_52;
  tmpvar_52 = ((tmpvar_51 * tmpvar_51) * (tmpvar_51 * tmpvar_51));
  rlPow4AndFresnelTerm_46 = tmpvar_52;
  mediump float tmpvar_53;
  tmpvar_53 = rlPow4AndFresnelTerm_46.x;
  mediump float specular_54;
  highp float smoothness_55;
  smoothness_55 = _Glossiness;
  highp vec2 tmpvar_56;
  tmpvar_56.x = tmpvar_53;
  tmpvar_56.y = (1.0 - smoothness_55);
  highp float tmpvar_57;
  tmpvar_57 = (texture2D (unity_NHxRoughness, tmpvar_56).w * 16.0);
  specular_54 = tmpvar_57;
  color_45 = ((diffColor_44 + (specular_54 * tmpvar_40)) * (tmpvar_30 * tmpvar_47));
  color_45 = (color_45 + ((
    (hdr_31.x * ((hdr_31.w * (tmpvar_34.w - 1.0)) + 1.0))
   * tmpvar_34.xyz) * mix (tmpvar_40, vec3(
    clamp ((_Glossiness + (1.0 - tmpvar_41)), 0.0, 1.0)
  ), rlPow4AndFresnelTerm_46.yyy)));
  mediump vec4 tmpvar_58;
  tmpvar_58.w = 1.0;
  tmpvar_58.xyz = color_45;
  c_36.xyz = tmpvar_58.xyz;
  c_36.w = alpha_43;
  c_6.xyz = c_36.xyz;
  c_6.w = 1.0;
  gl_FragData[0] = c_6;
}


#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 83 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD5;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  highp vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_4;
  tmpvar_4[0] = unity_WorldToObject[0].xyz;
  tmpvar_4[1] = unity_WorldToObject[1].xyz;
  tmpvar_4[2] = unity_WorldToObject[2].xyz;
  highp vec4 tmpvar_5;
  tmpvar_5 = (unity_ObjectToWorld * _glesVertex);
  tmpvar_1.xyz = tmpvar_5.xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_4));
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD5 = (unity_WorldToShadow[0] * tmpvar_5);
  xlv_TEXCOORD6 = tmpvar_2;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _ShadowMapTexture;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD5;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump float tmpvar_2;
  highp vec4 tmpvar_3;
  mediump vec3 tmpvar_4;
  mediump vec3 tmpvar_5;
  lowp vec4 c_6;
  lowp float atten_7;
  lowp vec3 lightDir_8;
  mediump vec3 tmpvar_9;
  tmpvar_9 = _WorldSpaceLightPos0.xyz;
  lightDir_8 = tmpvar_9;
  highp vec3 tmpvar_10;
  highp vec3 tmpvar_11;
  tmpvar_11 = (_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz);
  tmpvar_10 = normalize(tmpvar_11);
  lowp vec3 tmpvar_12;
  lowp float tmpvar_13;
  lowp vec4 tmpvar_14;
  tmpvar_14 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_12 = tmpvar_14.xyz;
  tmpvar_13 = tmpvar_14.w;
  mediump float realtimeShadowAttenuation_15;
  highp vec4 v_16;
  v_16.x = unity_MatrixV[0].z;
  v_16.y = unity_MatrixV[1].z;
  v_16.z = unity_MatrixV[2].z;
  v_16.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_17;
  tmpvar_17 = (xlv_TEXCOORD2.xyz - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_18;
  highp float tmpvar_19;
  tmpvar_19 = clamp (((
    mix (dot (tmpvar_11, v_16.xyz), sqrt(dot (tmpvar_17, tmpvar_17)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_18 = tmpvar_19;
  lowp float tmpvar_20;
  highp float lightShadowDataX_21;
  mediump float tmpvar_22;
  tmpvar_22 = _LightShadowData.x;
  lightShadowDataX_21 = tmpvar_22;
  highp float tmpvar_23;
  tmpvar_23 = max (float((texture2D (_ShadowMapTexture, xlv_TEXCOORD5.xy).x > xlv_TEXCOORD5.z)), lightShadowDataX_21);
  tmpvar_20 = tmpvar_23;
  realtimeShadowAttenuation_15 = tmpvar_20;
  mediump float tmpvar_24;
  tmpvar_24 = mix (realtimeShadowAttenuation_15, 1.0, tmpvar_18);
  atten_7 = tmpvar_24;
  tmpvar_4 = _LightColor0.xyz;
  tmpvar_5 = lightDir_8;
  tmpvar_1 = tmpvar_10;
  tmpvar_2 = atten_7;
  tmpvar_3 = unity_SpecCube0_HDR;
  mediump vec3 Normal_25;
  Normal_25 = xlv_TEXCOORD1;
  mediump float tmpvar_26;
  highp float tmpvar_27;
  highp float smoothness_28;
  smoothness_28 = _Glossiness;
  tmpvar_27 = (1.0 - smoothness_28);
  tmpvar_26 = tmpvar_27;
  mediump vec3 I_29;
  I_29 = -(tmpvar_1);
  mediump vec3 tmpvar_30;
  mediump vec3 tmpvar_31;
  tmpvar_31 = (tmpvar_4 * tmpvar_2);
  mediump vec4 hdr_32;
  hdr_32 = tmpvar_3;
  mediump vec4 tmpvar_33;
  tmpvar_33.xyz = (I_29 - (2.0 * (
    dot (Normal_25, I_29)
   * Normal_25)));
  tmpvar_33.w = ((tmpvar_26 * (1.7 - 
    (0.7 * tmpvar_26)
  )) * 6.0);
  lowp vec4 tmpvar_34;
  tmpvar_34 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_33.xyz, tmpvar_33.w);
  mediump vec4 tmpvar_35;
  tmpvar_35 = tmpvar_34;
  tmpvar_30 = ((hdr_32.x * (
    (hdr_32.w * (tmpvar_35.w - 1.0))
   + 1.0)) * tmpvar_35.xyz);
  tmpvar_4 = tmpvar_31;
  lowp vec3 tmpvar_36;
  mediump vec4 c_37;
  highp vec3 tmpvar_38;
  tmpvar_38 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_39;
  mediump vec3 albedo_40;
  albedo_40 = tmpvar_12;
  mediump vec3 tmpvar_41;
  tmpvar_41 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_40, vec3(_Metallic));
  mediump float tmpvar_42;
  tmpvar_42 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_39 = (albedo_40 * tmpvar_42);
  tmpvar_36 = tmpvar_39;
  mediump vec3 diffColor_43;
  diffColor_43 = tmpvar_36;
  mediump float alpha_44;
  alpha_44 = tmpvar_13;
  tmpvar_36 = diffColor_43;
  mediump vec3 diffColor_45;
  diffColor_45 = tmpvar_36;
  mediump vec3 color_46;
  mediump float surfaceReduction_47;
  highp float specularTerm_48;
  highp float a2_49;
  mediump float roughness_50;
  mediump float perceptualRoughness_51;
  highp vec3 tmpvar_52;
  highp vec3 inVec_53;
  inVec_53 = (tmpvar_5 + tmpvar_10);
  tmpvar_52 = (inVec_53 * inversesqrt(max (0.001, 
    dot (inVec_53, inVec_53)
  )));
  mediump float tmpvar_54;
  highp float tmpvar_55;
  tmpvar_55 = clamp (dot (tmpvar_38, tmpvar_5), 0.0, 1.0);
  tmpvar_54 = tmpvar_55;
  highp float tmpvar_56;
  tmpvar_56 = clamp (dot (tmpvar_38, tmpvar_52), 0.0, 1.0);
  mediump float tmpvar_57;
  highp float tmpvar_58;
  tmpvar_58 = clamp (dot (tmpvar_38, tmpvar_10), 0.0, 1.0);
  tmpvar_57 = tmpvar_58;
  highp float tmpvar_59;
  highp float smoothness_60;
  smoothness_60 = _Glossiness;
  tmpvar_59 = (1.0 - smoothness_60);
  perceptualRoughness_51 = tmpvar_59;
  highp float tmpvar_61;
  highp float perceptualRoughness_62;
  perceptualRoughness_62 = perceptualRoughness_51;
  tmpvar_61 = (perceptualRoughness_62 * perceptualRoughness_62);
  roughness_50 = tmpvar_61;
  mediump float tmpvar_63;
  tmpvar_63 = (roughness_50 * roughness_50);
  a2_49 = tmpvar_63;
  specularTerm_48 = ((roughness_50 / (
    (max (0.32, clamp (dot (tmpvar_5, tmpvar_52), 0.0, 1.0)) * (1.5 + roughness_50))
   * 
    (((tmpvar_56 * tmpvar_56) * (a2_49 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_64;
  tmpvar_64 = clamp (specularTerm_48, 0.0, 100.0);
  specularTerm_48 = tmpvar_64;
  surfaceReduction_47 = (1.0 - ((roughness_50 * perceptualRoughness_51) * 0.28));
  mediump float x_65;
  x_65 = (1.0 - tmpvar_57);
  mediump vec3 tmpvar_66;
  tmpvar_66 = mix (tmpvar_41, vec3(clamp ((_Glossiness + 
    (1.0 - tmpvar_42)
  ), 0.0, 1.0)), vec3(((x_65 * x_65) * (x_65 * x_65))));
  highp vec3 tmpvar_67;
  tmpvar_67 = (((
    (diffColor_45 + (tmpvar_64 * tmpvar_41))
   * tmpvar_31) * tmpvar_54) + ((surfaceReduction_47 * tmpvar_30) * tmpvar_66));
  color_46 = tmpvar_67;
  mediump vec4 tmpvar_68;
  tmpvar_68.w = 1.0;
  tmpvar_68.xyz = color_46;
  c_37.xyz = tmpvar_68.xyz;
  c_37.w = alpha_44;
  c_6.xyz = c_37.xyz;
  c_6.w = 1.0;
  gl_FragData[0] = c_6;
}


#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 83 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD5;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  highp vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_4;
  tmpvar_4[0] = unity_WorldToObject[0].xyz;
  tmpvar_4[1] = unity_WorldToObject[1].xyz;
  tmpvar_4[2] = unity_WorldToObject[2].xyz;
  highp vec4 tmpvar_5;
  tmpvar_5 = (unity_ObjectToWorld * _glesVertex);
  tmpvar_1.xyz = tmpvar_5.xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_3));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_4));
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD5 = (unity_WorldToShadow[0] * tmpvar_5);
  xlv_TEXCOORD6 = tmpvar_2;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _ShadowMapTexture;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD5;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump float tmpvar_2;
  highp vec4 tmpvar_3;
  mediump vec3 tmpvar_4;
  mediump vec3 tmpvar_5;
  lowp vec4 c_6;
  lowp float atten_7;
  lowp vec3 lightDir_8;
  mediump vec3 tmpvar_9;
  tmpvar_9 = _WorldSpaceLightPos0.xyz;
  lightDir_8 = tmpvar_9;
  highp vec3 tmpvar_10;
  highp vec3 tmpvar_11;
  tmpvar_11 = (_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz);
  tmpvar_10 = normalize(tmpvar_11);
  lowp vec3 tmpvar_12;
  lowp float tmpvar_13;
  lowp vec4 tmpvar_14;
  tmpvar_14 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_12 = tmpvar_14.xyz;
  tmpvar_13 = tmpvar_14.w;
  mediump float realtimeShadowAttenuation_15;
  highp vec4 v_16;
  v_16.x = unity_MatrixV[0].z;
  v_16.y = unity_MatrixV[1].z;
  v_16.z = unity_MatrixV[2].z;
  v_16.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_17;
  tmpvar_17 = (xlv_TEXCOORD2.xyz - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_18;
  highp float tmpvar_19;
  tmpvar_19 = clamp (((
    mix (dot (tmpvar_11, v_16.xyz), sqrt(dot (tmpvar_17, tmpvar_17)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_18 = tmpvar_19;
  lowp float tmpvar_20;
  highp float lightShadowDataX_21;
  mediump float tmpvar_22;
  tmpvar_22 = _LightShadowData.x;
  lightShadowDataX_21 = tmpvar_22;
  highp float tmpvar_23;
  tmpvar_23 = max (float((texture2D (_ShadowMapTexture, xlv_TEXCOORD5.xy).x > xlv_TEXCOORD5.z)), lightShadowDataX_21);
  tmpvar_20 = tmpvar_23;
  realtimeShadowAttenuation_15 = tmpvar_20;
  mediump float tmpvar_24;
  tmpvar_24 = mix (realtimeShadowAttenuation_15, 1.0, tmpvar_18);
  atten_7 = tmpvar_24;
  tmpvar_4 = _LightColor0.xyz;
  tmpvar_5 = lightDir_8;
  tmpvar_1 = tmpvar_10;
  tmpvar_2 = atten_7;
  tmpvar_3 = unity_SpecCube0_HDR;
  mediump vec3 Normal_25;
  Normal_25 = xlv_TEXCOORD1;
  mediump float tmpvar_26;
  highp float tmpvar_27;
  highp float smoothness_28;
  smoothness_28 = _Glossiness;
  tmpvar_27 = (1.0 - smoothness_28);
  tmpvar_26 = tmpvar_27;
  mediump vec3 I_29;
  I_29 = -(tmpvar_1);
  mediump vec3 tmpvar_30;
  mediump vec3 tmpvar_31;
  tmpvar_31 = (tmpvar_4 * tmpvar_2);
  mediump vec4 hdr_32;
  hdr_32 = tmpvar_3;
  mediump vec4 tmpvar_33;
  tmpvar_33.xyz = (I_29 - (2.0 * (
    dot (Normal_25, I_29)
   * Normal_25)));
  tmpvar_33.w = ((tmpvar_26 * (1.7 - 
    (0.7 * tmpvar_26)
  )) * 6.0);
  lowp vec4 tmpvar_34;
  tmpvar_34 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_33.xyz, tmpvar_33.w);
  mediump vec4 tmpvar_35;
  tmpvar_35 = tmpvar_34;
  tmpvar_30 = ((hdr_32.x * (
    (hdr_32.w * (tmpvar_35.w - 1.0))
   + 1.0)) * tmpvar_35.xyz);
  tmpvar_4 = tmpvar_31;
  lowp vec3 tmpvar_36;
  mediump vec4 c_37;
  highp vec3 tmpvar_38;
  tmpvar_38 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_39;
  mediump vec3 albedo_40;
  albedo_40 = tmpvar_12;
  mediump vec3 tmpvar_41;
  tmpvar_41 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_40, vec3(_Metallic));
  mediump float tmpvar_42;
  tmpvar_42 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_39 = (albedo_40 * tmpvar_42);
  tmpvar_36 = tmpvar_39;
  mediump vec3 diffColor_43;
  diffColor_43 = tmpvar_36;
  mediump float alpha_44;
  alpha_44 = tmpvar_13;
  tmpvar_36 = diffColor_43;
  mediump vec3 diffColor_45;
  diffColor_45 = tmpvar_36;
  mediump vec3 color_46;
  mediump float surfaceReduction_47;
  highp float specularTerm_48;
  highp float a2_49;
  mediump float roughness_50;
  mediump float perceptualRoughness_51;
  highp vec3 tmpvar_52;
  highp vec3 inVec_53;
  inVec_53 = (tmpvar_5 + tmpvar_10);
  tmpvar_52 = (inVec_53 * inversesqrt(max (0.001, 
    dot (inVec_53, inVec_53)
  )));
  mediump float tmpvar_54;
  highp float tmpvar_55;
  tmpvar_55 = clamp (dot (tmpvar_38, tmpvar_5), 0.0, 1.0);
  tmpvar_54 = tmpvar_55;
  highp float tmpvar_56;
  tmpvar_56 = clamp (dot (tmpvar_38, tmpvar_52), 0.0, 1.0);
  mediump float tmpvar_57;
  highp float tmpvar_58;
  tmpvar_58 = clamp (dot (tmpvar_38, tmpvar_10), 0.0, 1.0);
  tmpvar_57 = tmpvar_58;
  highp float tmpvar_59;
  highp float smoothness_60;
  smoothness_60 = _Glossiness;
  tmpvar_59 = (1.0 - smoothness_60);
  perceptualRoughness_51 = tmpvar_59;
  highp float tmpvar_61;
  highp float perceptualRoughness_62;
  perceptualRoughness_62 = perceptualRoughness_51;
  tmpvar_61 = (perceptualRoughness_62 * perceptualRoughness_62);
  roughness_50 = tmpvar_61;
  mediump float tmpvar_63;
  tmpvar_63 = (roughness_50 * roughness_50);
  a2_49 = tmpvar_63;
  specularTerm_48 = ((roughness_50 / (
    (max (0.32, clamp (dot (tmpvar_5, tmpvar_52), 0.0, 1.0)) * (1.5 + roughness_50))
   * 
    (((tmpvar_56 * tmpvar_56) * (a2_49 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_64;
  tmpvar_64 = clamp (specularTerm_48, 0.0, 100.0);
  specularTerm_48 = tmpvar_64;
  surfaceReduction_47 = (1.0 - ((roughness_50 * perceptualRoughness_51) * 0.28));
  mediump float x_65;
  x_65 = (1.0 - tmpvar_57);
  mediump vec3 tmpvar_66;
  tmpvar_66 = mix (tmpvar_41, vec3(clamp ((_Glossiness + 
    (1.0 - tmpvar_42)
  ), 0.0, 1.0)), vec3(((x_65 * x_65) * (x_65 * x_65))));
  highp vec3 tmpvar_67;
  tmpvar_67 = (((
    (diffColor_45 + (tmpvar_64 * tmpvar_41))
   * tmpvar_31) * tmpvar_54) + ((surfaceReduction_47 * tmpvar_30) * tmpvar_66));
  color_46 = tmpvar_67;
  mediump vec4 tmpvar_68;
  tmpvar_68.w = 1.0;
  tmpvar_68.xyz = color_46;
  c_37.xyz = tmpvar_68.xyz;
  c_37.w = alpha_44;
  c_6.xyz = c_37.xyz;
  c_6.w = 1.0;
  gl_FragData[0] = c_6;
}


#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD5;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD5 = vec4(0.0, 0.0, 0.0, 0.0);
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp samplerCube unity_SpecCube0;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
lowp float u_xlat10_0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_2;
lowp vec3 u_xlat10_2;
mediump vec3 u_xlat16_3;
lowp vec4 u_xlat10_3;
vec3 u_xlat4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
vec3 u_xlat8;
mediump float u_xlat16_8;
mediump float u_xlat16_9;
float u_xlat24;
mediump float u_xlat16_25;
float u_xlat26;
mediump float u_xlat16_29;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * vs_TEXCOORD2.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * vs_TEXCOORD2.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    vec3 txVec0 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat10_0 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_1.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_1.x = u_xlat10_0 * u_xlat16_1.x + _LightShadowData.x;
    u_xlat16_9 = (-u_xlat16_1.x) + 1.0;
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat2.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat8.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat2.x = dot(u_xlat8.xyz, u_xlat2.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat2.x);
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat0.x + u_xlat2.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_1.x = u_xlat0.x * u_xlat16_9 + u_xlat16_1.x;
    u_xlat16_1.xyz = u_xlat16_1.xxx * _LightColor0.xyz;
    u_xlat0.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat2.xyz = u_xlat0.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat2.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_1.xyz = u_xlat0.xxx * u_xlat16_1.xyz;
    u_xlat0.x = dot(u_xlat8.xyz, u_xlat8.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx * u_xlat8.xyz;
    u_xlat16_25 = dot((-u_xlat0.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_25 = u_xlat16_25 + u_xlat16_25;
    u_xlat16_3.xyz = vs_TEXCOORD1.xyz * (-vec3(u_xlat16_25)) + (-u_xlat0.xyz);
    u_xlat4.z = (-_Glossiness) + 1.0;
    u_xlat16_25 = (-u_xlat4.z) * 0.699999988 + 1.70000005;
    u_xlat16_25 = u_xlat16_25 * u_xlat4.z;
    u_xlat16_25 = u_xlat16_25 * 6.0;
    u_xlat10_3 = textureLod(unity_SpecCube0, u_xlat16_3.xyz, u_xlat16_25);
    u_xlat16_25 = u_xlat10_3.w + -1.0;
    u_xlat16_25 = unity_SpecCube0_HDR.w * u_xlat16_25 + 1.0;
    u_xlat16_25 = u_xlat16_25 * unity_SpecCube0_HDR.x;
    u_xlat16_5.xyz = u_xlat10_3.xyz * vec3(u_xlat16_25);
    u_xlat24 = dot(u_xlat0.xyz, u_xlat2.xyz);
    u_xlat26 = u_xlat24;
#ifdef UNITY_ADRENO_ES3
    u_xlat26 = min(max(u_xlat26, 0.0), 1.0);
#else
    u_xlat26 = clamp(u_xlat26, 0.0, 1.0);
#endif
    u_xlat24 = u_xlat24 + u_xlat24;
    u_xlat0.xyz = u_xlat2.xyz * (-vec3(u_xlat24)) + u_xlat0.xyz;
    u_xlat0.x = dot(u_xlat0.xyz, _WorldSpaceLightPos0.xyz);
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat4.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = texture(unity_NHxRoughness, u_xlat4.xz).w;
    u_xlat0.x = u_xlat0.x * 16.0;
    u_xlat16_25 = (-u_xlat26) + 1.0;
    u_xlat16_8 = u_xlat16_25 * u_xlat16_25;
    u_xlat16_8 = u_xlat16_25 * u_xlat16_8;
    u_xlat16_8 = u_xlat16_25 * u_xlat16_8;
    u_xlat16_25 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_29 = (-u_xlat16_25) + _Glossiness;
    u_xlat16_29 = u_xlat16_29 + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_29 = min(max(u_xlat16_29, 0.0), 1.0);
#else
    u_xlat16_29 = clamp(u_xlat16_29, 0.0, 1.0);
#endif
    u_xlat10_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_6.xyz = u_xlat10_2.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_2.xyz = u_xlat10_2.xyz * _Color.xyz;
    u_xlat16_6.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_6.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_7.xyz = vec3(u_xlat16_29) + (-u_xlat16_6.xyz);
    u_xlat16_7.xyz = vec3(u_xlat16_8) * u_xlat16_7.xyz + u_xlat16_6.xyz;
    u_xlat16_6.xyz = u_xlat0.xxx * u_xlat16_6.xyz;
    u_xlat16_6.xyz = u_xlat16_2.xyz * vec3(u_xlat16_25) + u_xlat16_6.xyz;
    u_xlat16_5.xyz = u_xlat16_5.xyz * u_xlat16_7.xyz;
    SV_Target0.xyz = u_xlat16_6.xyz * u_xlat16_1.xyz + u_xlat16_5.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD5;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD5 = vec4(0.0, 0.0, 0.0, 0.0);
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform lowp samplerCube unity_SpecCube0;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
lowp float u_xlat10_0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_2;
lowp vec3 u_xlat10_2;
vec3 u_xlat3;
mediump float u_xlat16_3;
lowp vec4 u_xlat10_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
vec3 u_xlat8;
mediump float u_xlat16_9;
mediump float u_xlat16_10;
mediump float u_xlat16_18;
float u_xlat24;
mediump float u_xlat16_25;
mediump float u_xlat16_26;
mediump float u_xlat16_28;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * vs_TEXCOORD2.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * vs_TEXCOORD2.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    vec3 txVec0 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat10_0 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_1.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_1.x = u_xlat10_0 * u_xlat16_1.x + _LightShadowData.x;
    u_xlat16_9 = (-u_xlat16_1.x) + 1.0;
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat2.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat8.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat2.x = dot(u_xlat8.xyz, u_xlat2.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat2.x);
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat0.x + u_xlat2.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_1.x = u_xlat0.x * u_xlat16_9 + u_xlat16_1.x;
    u_xlat16_1.xyz = u_xlat16_1.xxx * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat8.xyz, u_xlat8.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat2.xyz = u_xlat8.xyz * u_xlat0.xxx + _WorldSpaceLightPos0.xyz;
    u_xlat0.xyz = u_xlat0.xxx * u_xlat8.xyz;
    u_xlat24 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat24 = max(u_xlat24, 0.00100000005);
    u_xlat24 = inversesqrt(u_xlat24);
    u_xlat2.xyz = vec3(u_xlat24) * u_xlat2.xyz;
    u_xlat24 = dot(_WorldSpaceLightPos0.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat24 = min(max(u_xlat24, 0.0), 1.0);
#else
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
#endif
    u_xlat24 = max(u_xlat24, 0.319999993);
    u_xlat16_26 = (-_Glossiness) + 1.0;
    u_xlat16_3 = u_xlat16_26 * u_xlat16_26 + 1.5;
    u_xlat24 = u_xlat24 * u_xlat16_3;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat2.x = dot(u_xlat3.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat2.x = min(max(u_xlat2.x, 0.0), 1.0);
#else
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
#endif
    u_xlat2.x = u_xlat2.x * u_xlat2.x;
    u_xlat16_10 = u_xlat16_26 * u_xlat16_26;
    u_xlat16_18 = u_xlat16_10 * u_xlat16_10 + -1.0;
    u_xlat2.x = u_xlat2.x * u_xlat16_18 + 1.00001001;
    u_xlat24 = u_xlat24 * u_xlat2.x;
    u_xlat24 = u_xlat16_10 / u_xlat24;
    u_xlat16_25 = u_xlat16_26 * u_xlat16_10;
    u_xlat16_25 = (-u_xlat16_25) * 0.280000001 + 1.0;
    u_xlat24 = u_xlat24 + -9.99999975e-05;
    u_xlat24 = max(u_xlat24, 0.0);
    u_xlat24 = min(u_xlat24, 100.0);
    u_xlat10_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_2.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_2.xyz = u_xlat10_2.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat5.xyz = vec3(u_xlat24) * u_xlat16_4.xyz;
    u_xlat16_28 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat2.xyz = u_xlat16_2.xyz * vec3(u_xlat16_28) + u_xlat5.xyz;
    u_xlat16_28 = (-u_xlat16_28) + _Glossiness;
    u_xlat16_28 = u_xlat16_28 + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_28 = min(max(u_xlat16_28, 0.0), 1.0);
#else
    u_xlat16_28 = clamp(u_xlat16_28, 0.0, 1.0);
#endif
    u_xlat16_6.xyz = (-u_xlat16_4.xyz) + vec3(u_xlat16_28);
    u_xlat2.xyz = u_xlat16_1.xyz * u_xlat2.xyz;
    u_xlat16_1.x = (-u_xlat16_26) * 0.699999988 + 1.70000005;
    u_xlat16_1.x = u_xlat16_1.x * u_xlat16_26;
    u_xlat16_1.x = u_xlat16_1.x * 6.0;
    u_xlat16_9 = dot((-u_xlat0.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_9 = u_xlat16_9 + u_xlat16_9;
    u_xlat16_7.xyz = vs_TEXCOORD1.xyz * (-vec3(u_xlat16_9)) + (-u_xlat0.xyz);
    u_xlat0.x = dot(u_xlat3.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat8.x = dot(u_xlat3.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat8.x = min(max(u_xlat8.x, 0.0), 1.0);
#else
    u_xlat8.x = clamp(u_xlat8.x, 0.0, 1.0);
#endif
    u_xlat16_9 = (-u_xlat0.x) + 1.0;
    u_xlat16_9 = u_xlat16_9 * u_xlat16_9;
    u_xlat16_9 = u_xlat16_9 * u_xlat16_9;
    u_xlat16_4.xyz = vec3(u_xlat16_9) * u_xlat16_6.xyz + u_xlat16_4.xyz;
    u_xlat10_3 = textureLod(unity_SpecCube0, u_xlat16_7.xyz, u_xlat16_1.x);
    u_xlat16_1.x = u_xlat10_3.w + -1.0;
    u_xlat16_1.x = unity_SpecCube0_HDR.w * u_xlat16_1.x + 1.0;
    u_xlat16_1.x = u_xlat16_1.x * unity_SpecCube0_HDR.x;
    u_xlat16_1.xyz = u_xlat10_3.xyz * u_xlat16_1.xxx;
    u_xlat16_1.xyz = u_xlat16_1.xyz * vec3(u_xlat16_25);
    u_xlat16_1.xyz = u_xlat16_4.xyz * u_xlat16_1.xyz;
    u_xlat0.xyz = u_xlat2.xyz * u_xlat8.xxx + u_xlat16_1.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD5;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD5 = vec4(0.0, 0.0, 0.0, 0.0);
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform lowp samplerCube unity_SpecCube0;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
lowp float u_xlat10_0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_2;
lowp vec3 u_xlat10_2;
vec3 u_xlat3;
mediump float u_xlat16_3;
lowp vec4 u_xlat10_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
vec3 u_xlat8;
mediump float u_xlat16_9;
mediump float u_xlat16_10;
mediump float u_xlat16_18;
float u_xlat24;
mediump float u_xlat16_25;
mediump float u_xlat16_26;
mediump float u_xlat16_28;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * vs_TEXCOORD2.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * vs_TEXCOORD2.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    vec3 txVec0 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat10_0 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_1.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_1.x = u_xlat10_0 * u_xlat16_1.x + _LightShadowData.x;
    u_xlat16_9 = (-u_xlat16_1.x) + 1.0;
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat2.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat8.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat2.x = dot(u_xlat8.xyz, u_xlat2.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat2.x);
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat0.x + u_xlat2.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_1.x = u_xlat0.x * u_xlat16_9 + u_xlat16_1.x;
    u_xlat16_1.xyz = u_xlat16_1.xxx * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat8.xyz, u_xlat8.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat2.xyz = u_xlat8.xyz * u_xlat0.xxx + _WorldSpaceLightPos0.xyz;
    u_xlat0.xyz = u_xlat0.xxx * u_xlat8.xyz;
    u_xlat24 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat24 = max(u_xlat24, 0.00100000005);
    u_xlat24 = inversesqrt(u_xlat24);
    u_xlat2.xyz = vec3(u_xlat24) * u_xlat2.xyz;
    u_xlat24 = dot(_WorldSpaceLightPos0.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat24 = min(max(u_xlat24, 0.0), 1.0);
#else
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
#endif
    u_xlat24 = max(u_xlat24, 0.319999993);
    u_xlat16_26 = (-_Glossiness) + 1.0;
    u_xlat16_3 = u_xlat16_26 * u_xlat16_26 + 1.5;
    u_xlat24 = u_xlat24 * u_xlat16_3;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat2.x = dot(u_xlat3.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat2.x = min(max(u_xlat2.x, 0.0), 1.0);
#else
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
#endif
    u_xlat2.x = u_xlat2.x * u_xlat2.x;
    u_xlat16_10 = u_xlat16_26 * u_xlat16_26;
    u_xlat16_18 = u_xlat16_10 * u_xlat16_10 + -1.0;
    u_xlat2.x = u_xlat2.x * u_xlat16_18 + 1.00001001;
    u_xlat24 = u_xlat24 * u_xlat2.x;
    u_xlat24 = u_xlat16_10 / u_xlat24;
    u_xlat16_25 = u_xlat16_26 * u_xlat16_10;
    u_xlat16_25 = (-u_xlat16_25) * 0.280000001 + 1.0;
    u_xlat24 = u_xlat24 + -9.99999975e-05;
    u_xlat24 = max(u_xlat24, 0.0);
    u_xlat24 = min(u_xlat24, 100.0);
    u_xlat10_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_2.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_2.xyz = u_xlat10_2.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat5.xyz = vec3(u_xlat24) * u_xlat16_4.xyz;
    u_xlat16_28 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat2.xyz = u_xlat16_2.xyz * vec3(u_xlat16_28) + u_xlat5.xyz;
    u_xlat16_28 = (-u_xlat16_28) + _Glossiness;
    u_xlat16_28 = u_xlat16_28 + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_28 = min(max(u_xlat16_28, 0.0), 1.0);
#else
    u_xlat16_28 = clamp(u_xlat16_28, 0.0, 1.0);
#endif
    u_xlat16_6.xyz = (-u_xlat16_4.xyz) + vec3(u_xlat16_28);
    u_xlat2.xyz = u_xlat16_1.xyz * u_xlat2.xyz;
    u_xlat16_1.x = (-u_xlat16_26) * 0.699999988 + 1.70000005;
    u_xlat16_1.x = u_xlat16_1.x * u_xlat16_26;
    u_xlat16_1.x = u_xlat16_1.x * 6.0;
    u_xlat16_9 = dot((-u_xlat0.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_9 = u_xlat16_9 + u_xlat16_9;
    u_xlat16_7.xyz = vs_TEXCOORD1.xyz * (-vec3(u_xlat16_9)) + (-u_xlat0.xyz);
    u_xlat0.x = dot(u_xlat3.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat8.x = dot(u_xlat3.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat8.x = min(max(u_xlat8.x, 0.0), 1.0);
#else
    u_xlat8.x = clamp(u_xlat8.x, 0.0, 1.0);
#endif
    u_xlat16_9 = (-u_xlat0.x) + 1.0;
    u_xlat16_9 = u_xlat16_9 * u_xlat16_9;
    u_xlat16_9 = u_xlat16_9 * u_xlat16_9;
    u_xlat16_4.xyz = vec3(u_xlat16_9) * u_xlat16_6.xyz + u_xlat16_4.xyz;
    u_xlat10_3 = textureLod(unity_SpecCube0, u_xlat16_7.xyz, u_xlat16_1.x);
    u_xlat16_1.x = u_xlat10_3.w + -1.0;
    u_xlat16_1.x = unity_SpecCube0_HDR.w * u_xlat16_1.x + 1.0;
    u_xlat16_1.x = u_xlat16_1.x * unity_SpecCube0_HDR.x;
    u_xlat16_1.xyz = u_xlat10_3.xyz * u_xlat16_1.xxx;
    u_xlat16_1.xyz = u_xlat16_1.xyz * vec3(u_xlat16_25);
    u_xlat16_1.xyz = u_xlat16_4.xyz * u_xlat16_1.xyz;
    u_xlat0.xyz = u_xlat2.xyz * u_xlat8.xxx + u_xlat16_1.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

//////////////////////////////////////////////////////
Keywords set in this variant: DIRECTIONAL SHADOWS_SCREEN LIGHTPROBE_SH VERTEXLIGHT_ON 
-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 75 math, 4 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
uniform mediump vec4 unity_4LightAtten0;
uniform mediump vec4 unity_LightColor[8];
uniform mediump vec4 unity_SHBr;
uniform mediump vec4 unity_SHBg;
uniform mediump vec4 unity_SHBb;
uniform mediump vec4 unity_SHC;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD5;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  mediump vec3 tmpvar_2;
  highp vec4 tmpvar_3;
  highp vec4 tmpvar_4;
  tmpvar_4.w = 1.0;
  tmpvar_4.xyz = _glesVertex.xyz;
  highp vec4 tmpvar_5;
  tmpvar_5 = (unity_ObjectToWorld * _glesVertex);
  highp mat3 tmpvar_6;
  tmpvar_6[0] = unity_WorldToObject[0].xyz;
  tmpvar_6[1] = unity_WorldToObject[1].xyz;
  tmpvar_6[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_glesNormal * tmpvar_6));
  tmpvar_1.xyz = tmpvar_5.xyz;
  highp vec3 lightColor0_8;
  lightColor0_8 = unity_LightColor[0].xyz;
  highp vec3 lightColor1_9;
  lightColor1_9 = unity_LightColor[1].xyz;
  highp vec3 lightColor2_10;
  lightColor2_10 = unity_LightColor[2].xyz;
  highp vec3 lightColor3_11;
  lightColor3_11 = unity_LightColor[3].xyz;
  highp vec4 lightAttenSq_12;
  lightAttenSq_12 = unity_4LightAtten0;
  highp vec3 col_13;
  highp vec4 ndotl_14;
  highp vec4 lengthSq_15;
  highp vec4 tmpvar_16;
  tmpvar_16 = (unity_4LightPosX0 - tmpvar_5.x);
  highp vec4 tmpvar_17;
  tmpvar_17 = (unity_4LightPosY0 - tmpvar_5.y);
  highp vec4 tmpvar_18;
  tmpvar_18 = (unity_4LightPosZ0 - tmpvar_5.z);
  lengthSq_15 = (tmpvar_16 * tmpvar_16);
  lengthSq_15 = (lengthSq_15 + (tmpvar_17 * tmpvar_17));
  lengthSq_15 = (lengthSq_15 + (tmpvar_18 * tmpvar_18));
  highp vec4 tmpvar_19;
  tmpvar_19 = max (lengthSq_15, vec4(1e-06, 1e-06, 1e-06, 1e-06));
  lengthSq_15 = tmpvar_19;
  ndotl_14 = (tmpvar_16 * tmpvar_7.x);
  ndotl_14 = (ndotl_14 + (tmpvar_17 * tmpvar_7.y));
  ndotl_14 = (ndotl_14 + (tmpvar_18 * tmpvar_7.z));
  highp vec4 tmpvar_20;
  tmpvar_20 = max (vec4(0.0, 0.0, 0.0, 0.0), (ndotl_14 * inversesqrt(tmpvar_19)));
  ndotl_14 = tmpvar_20;
  highp vec4 tmpvar_21;
  tmpvar_21 = (tmpvar_20 * (1.0/((1.0 + 
    (tmpvar_19 * lightAttenSq_12)
  ))));
  col_13 = (lightColor0_8 * tmpvar_21.x);
  col_13 = (col_13 + (lightColor1_9 * tmpvar_21.y));
  col_13 = (col_13 + (lightColor2_10 * tmpvar_21.z));
  col_13 = (col_13 + (lightColor3_11 * tmpvar_21.w));
  tmpvar_2 = col_13;
  mediump vec3 normal_22;
  normal_22 = tmpvar_7;
  mediump vec3 ambient_23;
  mediump vec3 x1_24;
  mediump vec4 tmpvar_25;
  tmpvar_25 = (normal_22.xyzz * normal_22.yzzx);
  x1_24.x = dot (unity_SHBr, tmpvar_25);
  x1_24.y = dot (unity_SHBg, tmpvar_25);
  x1_24.z = dot (unity_SHBb, tmpvar_25);
  ambient_23 = ((tmpvar_2 * (
    (tmpvar_2 * ((tmpvar_2 * 0.305306) + 0.6821711))
   + 0.01252288)) + (x1_24 + (unity_SHC.xyz * 
    ((normal_22.x * normal_22.x) - (normal_22.y * normal_22.y))
  )));
  tmpvar_2 = ambient_23;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_4));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = tmpvar_7;
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD3 = ambient_23;
  xlv_TEXCOORD5 = (unity_WorldToShadow[0] * tmpvar_5);
  xlv_TEXCOORD6 = tmpvar_3;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 unity_SHAr;
uniform mediump vec4 unity_SHAg;
uniform mediump vec4 unity_SHAb;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform highp sampler2D _ShadowMapTexture;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD5;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump float tmpvar_2;
  highp vec4 tmpvar_3;
  mediump vec3 tmpvar_4;
  mediump vec3 tmpvar_5;
  lowp vec4 c_6;
  lowp float atten_7;
  lowp vec3 lightDir_8;
  mediump vec3 tmpvar_9;
  tmpvar_9 = _WorldSpaceLightPos0.xyz;
  lightDir_8 = tmpvar_9;
  highp vec3 tmpvar_10;
  highp vec3 tmpvar_11;
  tmpvar_11 = (_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz);
  tmpvar_10 = normalize(tmpvar_11);
  lowp vec3 tmpvar_12;
  lowp float tmpvar_13;
  lowp vec4 tmpvar_14;
  tmpvar_14 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_12 = tmpvar_14.xyz;
  tmpvar_13 = tmpvar_14.w;
  mediump float realtimeShadowAttenuation_15;
  highp vec4 v_16;
  v_16.x = unity_MatrixV[0].z;
  v_16.y = unity_MatrixV[1].z;
  v_16.z = unity_MatrixV[2].z;
  v_16.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_17;
  tmpvar_17 = (xlv_TEXCOORD2.xyz - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_18;
  highp float tmpvar_19;
  tmpvar_19 = clamp (((
    mix (dot (tmpvar_11, v_16.xyz), sqrt(dot (tmpvar_17, tmpvar_17)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_18 = tmpvar_19;
  lowp float tmpvar_20;
  highp float lightShadowDataX_21;
  mediump float tmpvar_22;
  tmpvar_22 = _LightShadowData.x;
  lightShadowDataX_21 = tmpvar_22;
  highp float tmpvar_23;
  tmpvar_23 = max (float((texture2D (_ShadowMapTexture, xlv_TEXCOORD5.xy).x > xlv_TEXCOORD5.z)), lightShadowDataX_21);
  tmpvar_20 = tmpvar_23;
  realtimeShadowAttenuation_15 = tmpvar_20;
  mediump float tmpvar_24;
  tmpvar_24 = mix (realtimeShadowAttenuation_15, 1.0, tmpvar_18);
  atten_7 = tmpvar_24;
  tmpvar_4 = _LightColor0.xyz;
  tmpvar_5 = lightDir_8;
  tmpvar_1 = tmpvar_10;
  tmpvar_2 = atten_7;
  tmpvar_3 = unity_SpecCube0_HDR;
  mediump vec3 Normal_25;
  Normal_25 = xlv_TEXCOORD1;
  mediump float tmpvar_26;
  highp float tmpvar_27;
  highp float smoothness_28;
  smoothness_28 = _Glossiness;
  tmpvar_27 = (1.0 - smoothness_28);
  tmpvar_26 = tmpvar_27;
  mediump vec3 I_29;
  I_29 = -(tmpvar_1);
  mediump vec3 normalWorld_30;
  normalWorld_30 = xlv_TEXCOORD1;
  mediump vec3 tmpvar_31;
  tmpvar_31 = (tmpvar_4 * tmpvar_2);
  mediump vec4 tmpvar_32;
  tmpvar_32.w = 1.0;
  tmpvar_32.xyz = normalWorld_30;
  mediump vec3 x_33;
  x_33.x = dot (unity_SHAr, tmpvar_32);
  x_33.y = dot (unity_SHAg, tmpvar_32);
  x_33.z = dot (unity_SHAb, tmpvar_32);
  mediump vec4 hdr_34;
  hdr_34 = tmpvar_3;
  mediump vec4 tmpvar_35;
  tmpvar_35.xyz = (I_29 - (2.0 * (
    dot (Normal_25, I_29)
   * Normal_25)));
  tmpvar_35.w = ((tmpvar_26 * (1.7 - 
    (0.7 * tmpvar_26)
  )) * 6.0);
  lowp vec4 tmpvar_36;
  tmpvar_36 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_35.xyz, tmpvar_35.w);
  mediump vec4 tmpvar_37;
  tmpvar_37 = tmpvar_36;
  tmpvar_4 = tmpvar_31;
  lowp vec3 tmpvar_38;
  mediump vec4 c_39;
  highp vec3 tmpvar_40;
  tmpvar_40 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_41;
  mediump vec3 albedo_42;
  albedo_42 = tmpvar_12;
  mediump vec3 tmpvar_43;
  tmpvar_43 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_42, vec3(_Metallic));
  mediump float tmpvar_44;
  tmpvar_44 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_41 = (albedo_42 * tmpvar_44);
  tmpvar_38 = tmpvar_41;
  mediump vec3 diffColor_45;
  diffColor_45 = tmpvar_38;
  mediump float alpha_46;
  alpha_46 = tmpvar_13;
  tmpvar_38 = diffColor_45;
  mediump vec3 diffColor_47;
  diffColor_47 = tmpvar_38;
  mediump vec3 color_48;
  mediump vec2 rlPow4AndFresnelTerm_49;
  mediump float tmpvar_50;
  highp float tmpvar_51;
  tmpvar_51 = clamp (dot (tmpvar_40, tmpvar_5), 0.0, 1.0);
  tmpvar_50 = tmpvar_51;
  mediump float tmpvar_52;
  highp float tmpvar_53;
  tmpvar_53 = clamp (dot (tmpvar_40, tmpvar_10), 0.0, 1.0);
  tmpvar_52 = tmpvar_53;
  highp vec2 tmpvar_54;
  tmpvar_54.x = dot ((tmpvar_10 - (2.0 * 
    (dot (tmpvar_40, tmpvar_10) * tmpvar_40)
  )), tmpvar_5);
  tmpvar_54.y = (1.0 - tmpvar_52);
  highp vec2 tmpvar_55;
  tmpvar_55 = ((tmpvar_54 * tmpvar_54) * (tmpvar_54 * tmpvar_54));
  rlPow4AndFresnelTerm_49 = tmpvar_55;
  mediump float tmpvar_56;
  tmpvar_56 = rlPow4AndFresnelTerm_49.x;
  mediump float specular_57;
  highp float smoothness_58;
  smoothness_58 = _Glossiness;
  highp vec2 tmpvar_59;
  tmpvar_59.x = tmpvar_56;
  tmpvar_59.y = (1.0 - smoothness_58);
  highp float tmpvar_60;
  tmpvar_60 = (texture2D (unity_NHxRoughness, tmpvar_59).w * 16.0);
  specular_57 = tmpvar_60;
  color_48 = ((diffColor_47 + (specular_57 * tmpvar_43)) * (tmpvar_31 * tmpvar_50));
  color_48 = (color_48 + ((
    max (((1.055 * pow (
      max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD3 + x_33))
    , vec3(0.4166667, 0.4166667, 0.4166667))) - 0.055), vec3(0.0, 0.0, 0.0))
   * diffColor_47) + (
    ((hdr_34.x * ((hdr_34.w * 
      (tmpvar_37.w - 1.0)
    ) + 1.0)) * tmpvar_37.xyz)
   * 
    mix (tmpvar_43, vec3(clamp ((_Glossiness + (1.0 - tmpvar_44)), 0.0, 1.0)), rlPow4AndFresnelTerm_49.yyy)
  )));
  mediump vec4 tmpvar_61;
  tmpvar_61.w = 1.0;
  tmpvar_61.xyz = color_48;
  c_39.xyz = tmpvar_61.xyz;
  c_39.w = alpha_46;
  c_6.xyz = c_39.xyz;
  c_6.w = 1.0;
  gl_FragData[0] = c_6;
}


#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 95 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
uniform mediump vec4 unity_4LightAtten0;
uniform mediump vec4 unity_LightColor[8];
uniform mediump vec4 unity_SHBr;
uniform mediump vec4 unity_SHBg;
uniform mediump vec4 unity_SHBb;
uniform mediump vec4 unity_SHC;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD5;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  mediump vec3 tmpvar_2;
  highp vec4 tmpvar_3;
  highp vec4 tmpvar_4;
  tmpvar_4.w = 1.0;
  tmpvar_4.xyz = _glesVertex.xyz;
  highp vec4 tmpvar_5;
  tmpvar_5 = (unity_ObjectToWorld * _glesVertex);
  highp mat3 tmpvar_6;
  tmpvar_6[0] = unity_WorldToObject[0].xyz;
  tmpvar_6[1] = unity_WorldToObject[1].xyz;
  tmpvar_6[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_glesNormal * tmpvar_6));
  tmpvar_1.xyz = tmpvar_5.xyz;
  highp vec3 lightColor0_8;
  lightColor0_8 = unity_LightColor[0].xyz;
  highp vec3 lightColor1_9;
  lightColor1_9 = unity_LightColor[1].xyz;
  highp vec3 lightColor2_10;
  lightColor2_10 = unity_LightColor[2].xyz;
  highp vec3 lightColor3_11;
  lightColor3_11 = unity_LightColor[3].xyz;
  highp vec4 lightAttenSq_12;
  lightAttenSq_12 = unity_4LightAtten0;
  highp vec3 col_13;
  highp vec4 ndotl_14;
  highp vec4 lengthSq_15;
  highp vec4 tmpvar_16;
  tmpvar_16 = (unity_4LightPosX0 - tmpvar_5.x);
  highp vec4 tmpvar_17;
  tmpvar_17 = (unity_4LightPosY0 - tmpvar_5.y);
  highp vec4 tmpvar_18;
  tmpvar_18 = (unity_4LightPosZ0 - tmpvar_5.z);
  lengthSq_15 = (tmpvar_16 * tmpvar_16);
  lengthSq_15 = (lengthSq_15 + (tmpvar_17 * tmpvar_17));
  lengthSq_15 = (lengthSq_15 + (tmpvar_18 * tmpvar_18));
  highp vec4 tmpvar_19;
  tmpvar_19 = max (lengthSq_15, vec4(1e-06, 1e-06, 1e-06, 1e-06));
  lengthSq_15 = tmpvar_19;
  ndotl_14 = (tmpvar_16 * tmpvar_7.x);
  ndotl_14 = (ndotl_14 + (tmpvar_17 * tmpvar_7.y));
  ndotl_14 = (ndotl_14 + (tmpvar_18 * tmpvar_7.z));
  highp vec4 tmpvar_20;
  tmpvar_20 = max (vec4(0.0, 0.0, 0.0, 0.0), (ndotl_14 * inversesqrt(tmpvar_19)));
  ndotl_14 = tmpvar_20;
  highp vec4 tmpvar_21;
  tmpvar_21 = (tmpvar_20 * (1.0/((1.0 + 
    (tmpvar_19 * lightAttenSq_12)
  ))));
  col_13 = (lightColor0_8 * tmpvar_21.x);
  col_13 = (col_13 + (lightColor1_9 * tmpvar_21.y));
  col_13 = (col_13 + (lightColor2_10 * tmpvar_21.z));
  col_13 = (col_13 + (lightColor3_11 * tmpvar_21.w));
  tmpvar_2 = col_13;
  mediump vec3 normal_22;
  normal_22 = tmpvar_7;
  mediump vec3 ambient_23;
  mediump vec3 x1_24;
  mediump vec4 tmpvar_25;
  tmpvar_25 = (normal_22.xyzz * normal_22.yzzx);
  x1_24.x = dot (unity_SHBr, tmpvar_25);
  x1_24.y = dot (unity_SHBg, tmpvar_25);
  x1_24.z = dot (unity_SHBb, tmpvar_25);
  ambient_23 = ((tmpvar_2 * (
    (tmpvar_2 * ((tmpvar_2 * 0.305306) + 0.6821711))
   + 0.01252288)) + (x1_24 + (unity_SHC.xyz * 
    ((normal_22.x * normal_22.x) - (normal_22.y * normal_22.y))
  )));
  tmpvar_2 = ambient_23;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_4));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = tmpvar_7;
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD3 = ambient_23;
  xlv_TEXCOORD5 = (unity_WorldToShadow[0] * tmpvar_5);
  xlv_TEXCOORD6 = tmpvar_3;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 unity_SHAr;
uniform mediump vec4 unity_SHAg;
uniform mediump vec4 unity_SHAb;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _ShadowMapTexture;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD5;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump float tmpvar_2;
  highp vec4 tmpvar_3;
  mediump vec3 tmpvar_4;
  mediump vec3 tmpvar_5;
  lowp vec4 c_6;
  lowp float atten_7;
  lowp vec3 lightDir_8;
  mediump vec3 tmpvar_9;
  tmpvar_9 = _WorldSpaceLightPos0.xyz;
  lightDir_8 = tmpvar_9;
  highp vec3 tmpvar_10;
  highp vec3 tmpvar_11;
  tmpvar_11 = (_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz);
  tmpvar_10 = normalize(tmpvar_11);
  lowp vec3 tmpvar_12;
  lowp float tmpvar_13;
  lowp vec4 tmpvar_14;
  tmpvar_14 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_12 = tmpvar_14.xyz;
  tmpvar_13 = tmpvar_14.w;
  mediump float realtimeShadowAttenuation_15;
  highp vec4 v_16;
  v_16.x = unity_MatrixV[0].z;
  v_16.y = unity_MatrixV[1].z;
  v_16.z = unity_MatrixV[2].z;
  v_16.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_17;
  tmpvar_17 = (xlv_TEXCOORD2.xyz - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_18;
  highp float tmpvar_19;
  tmpvar_19 = clamp (((
    mix (dot (tmpvar_11, v_16.xyz), sqrt(dot (tmpvar_17, tmpvar_17)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_18 = tmpvar_19;
  lowp float tmpvar_20;
  highp float lightShadowDataX_21;
  mediump float tmpvar_22;
  tmpvar_22 = _LightShadowData.x;
  lightShadowDataX_21 = tmpvar_22;
  highp float tmpvar_23;
  tmpvar_23 = max (float((texture2D (_ShadowMapTexture, xlv_TEXCOORD5.xy).x > xlv_TEXCOORD5.z)), lightShadowDataX_21);
  tmpvar_20 = tmpvar_23;
  realtimeShadowAttenuation_15 = tmpvar_20;
  mediump float tmpvar_24;
  tmpvar_24 = mix (realtimeShadowAttenuation_15, 1.0, tmpvar_18);
  atten_7 = tmpvar_24;
  tmpvar_4 = _LightColor0.xyz;
  tmpvar_5 = lightDir_8;
  tmpvar_1 = tmpvar_10;
  tmpvar_2 = atten_7;
  tmpvar_3 = unity_SpecCube0_HDR;
  mediump vec3 Normal_25;
  Normal_25 = xlv_TEXCOORD1;
  mediump float tmpvar_26;
  highp float tmpvar_27;
  highp float smoothness_28;
  smoothness_28 = _Glossiness;
  tmpvar_27 = (1.0 - smoothness_28);
  tmpvar_26 = tmpvar_27;
  mediump vec3 I_29;
  I_29 = -(tmpvar_1);
  mediump vec3 normalWorld_30;
  normalWorld_30 = xlv_TEXCOORD1;
  mediump vec3 tmpvar_31;
  mediump vec3 tmpvar_32;
  mediump vec3 tmpvar_33;
  tmpvar_32 = (tmpvar_4 * tmpvar_2);
  mediump vec4 tmpvar_34;
  tmpvar_34.w = 1.0;
  tmpvar_34.xyz = normalWorld_30;
  mediump vec3 x_35;
  x_35.x = dot (unity_SHAr, tmpvar_34);
  x_35.y = dot (unity_SHAg, tmpvar_34);
  x_35.z = dot (unity_SHAb, tmpvar_34);
  tmpvar_33 = max (((1.055 * 
    pow (max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD3 + x_35)), vec3(0.4166667, 0.4166667, 0.4166667))
  ) - 0.055), vec3(0.0, 0.0, 0.0));
  mediump vec4 hdr_36;
  hdr_36 = tmpvar_3;
  mediump vec4 tmpvar_37;
  tmpvar_37.xyz = (I_29 - (2.0 * (
    dot (Normal_25, I_29)
   * Normal_25)));
  tmpvar_37.w = ((tmpvar_26 * (1.7 - 
    (0.7 * tmpvar_26)
  )) * 6.0);
  lowp vec4 tmpvar_38;
  tmpvar_38 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_37.xyz, tmpvar_37.w);
  mediump vec4 tmpvar_39;
  tmpvar_39 = tmpvar_38;
  tmpvar_31 = ((hdr_36.x * (
    (hdr_36.w * (tmpvar_39.w - 1.0))
   + 1.0)) * tmpvar_39.xyz);
  tmpvar_4 = tmpvar_32;
  lowp vec3 tmpvar_40;
  mediump vec4 c_41;
  highp vec3 tmpvar_42;
  tmpvar_42 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_43;
  mediump vec3 albedo_44;
  albedo_44 = tmpvar_12;
  mediump vec3 tmpvar_45;
  tmpvar_45 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_44, vec3(_Metallic));
  mediump float tmpvar_46;
  tmpvar_46 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_43 = (albedo_44 * tmpvar_46);
  tmpvar_40 = tmpvar_43;
  mediump vec3 diffColor_47;
  diffColor_47 = tmpvar_40;
  mediump float alpha_48;
  alpha_48 = tmpvar_13;
  tmpvar_40 = diffColor_47;
  mediump vec3 diffColor_49;
  diffColor_49 = tmpvar_40;
  mediump vec3 color_50;
  mediump float surfaceReduction_51;
  highp float specularTerm_52;
  highp float a2_53;
  mediump float roughness_54;
  mediump float perceptualRoughness_55;
  highp vec3 tmpvar_56;
  highp vec3 inVec_57;
  inVec_57 = (tmpvar_5 + tmpvar_10);
  tmpvar_56 = (inVec_57 * inversesqrt(max (0.001, 
    dot (inVec_57, inVec_57)
  )));
  mediump float tmpvar_58;
  highp float tmpvar_59;
  tmpvar_59 = clamp (dot (tmpvar_42, tmpvar_5), 0.0, 1.0);
  tmpvar_58 = tmpvar_59;
  highp float tmpvar_60;
  tmpvar_60 = clamp (dot (tmpvar_42, tmpvar_56), 0.0, 1.0);
  mediump float tmpvar_61;
  highp float tmpvar_62;
  tmpvar_62 = clamp (dot (tmpvar_42, tmpvar_10), 0.0, 1.0);
  tmpvar_61 = tmpvar_62;
  highp float tmpvar_63;
  highp float smoothness_64;
  smoothness_64 = _Glossiness;
  tmpvar_63 = (1.0 - smoothness_64);
  perceptualRoughness_55 = tmpvar_63;
  highp float tmpvar_65;
  highp float perceptualRoughness_66;
  perceptualRoughness_66 = perceptualRoughness_55;
  tmpvar_65 = (perceptualRoughness_66 * perceptualRoughness_66);
  roughness_54 = tmpvar_65;
  mediump float tmpvar_67;
  tmpvar_67 = (roughness_54 * roughness_54);
  a2_53 = tmpvar_67;
  specularTerm_52 = ((roughness_54 / (
    (max (0.32, clamp (dot (tmpvar_5, tmpvar_56), 0.0, 1.0)) * (1.5 + roughness_54))
   * 
    (((tmpvar_60 * tmpvar_60) * (a2_53 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_68;
  tmpvar_68 = clamp (specularTerm_52, 0.0, 100.0);
  specularTerm_52 = tmpvar_68;
  surfaceReduction_51 = (1.0 - ((roughness_54 * perceptualRoughness_55) * 0.28));
  mediump float x_69;
  x_69 = (1.0 - tmpvar_61);
  mediump vec3 tmpvar_70;
  tmpvar_70 = mix (tmpvar_45, vec3(clamp ((_Glossiness + 
    (1.0 - tmpvar_46)
  ), 0.0, 1.0)), vec3(((x_69 * x_69) * (x_69 * x_69))));
  highp vec3 tmpvar_71;
  tmpvar_71 = (((
    ((diffColor_49 + (tmpvar_68 * tmpvar_45)) * tmpvar_32)
   * tmpvar_58) + (tmpvar_33 * diffColor_49)) + ((surfaceReduction_51 * tmpvar_31) * tmpvar_70));
  color_50 = tmpvar_71;
  mediump vec4 tmpvar_72;
  tmpvar_72.w = 1.0;
  tmpvar_72.xyz = color_50;
  c_41.xyz = tmpvar_72.xyz;
  c_41.w = alpha_48;
  c_6.xyz = c_41.xyz;
  c_6.w = 1.0;
  gl_FragData[0] = c_6;
}


#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 95 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp vec4 unity_4LightPosX0;
uniform highp vec4 unity_4LightPosY0;
uniform highp vec4 unity_4LightPosZ0;
uniform mediump vec4 unity_4LightAtten0;
uniform mediump vec4 unity_LightColor[8];
uniform mediump vec4 unity_SHBr;
uniform mediump vec4 unity_SHBg;
uniform mediump vec4 unity_SHBb;
uniform mediump vec4 unity_SHC;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD5;
varying highp vec4 xlv_TEXCOORD6;
void main ()
{
  highp vec4 tmpvar_1;
  mediump vec3 tmpvar_2;
  highp vec4 tmpvar_3;
  highp vec4 tmpvar_4;
  tmpvar_4.w = 1.0;
  tmpvar_4.xyz = _glesVertex.xyz;
  highp vec4 tmpvar_5;
  tmpvar_5 = (unity_ObjectToWorld * _glesVertex);
  highp mat3 tmpvar_6;
  tmpvar_6[0] = unity_WorldToObject[0].xyz;
  tmpvar_6[1] = unity_WorldToObject[1].xyz;
  tmpvar_6[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_glesNormal * tmpvar_6));
  tmpvar_1.xyz = tmpvar_5.xyz;
  highp vec3 lightColor0_8;
  lightColor0_8 = unity_LightColor[0].xyz;
  highp vec3 lightColor1_9;
  lightColor1_9 = unity_LightColor[1].xyz;
  highp vec3 lightColor2_10;
  lightColor2_10 = unity_LightColor[2].xyz;
  highp vec3 lightColor3_11;
  lightColor3_11 = unity_LightColor[3].xyz;
  highp vec4 lightAttenSq_12;
  lightAttenSq_12 = unity_4LightAtten0;
  highp vec3 col_13;
  highp vec4 ndotl_14;
  highp vec4 lengthSq_15;
  highp vec4 tmpvar_16;
  tmpvar_16 = (unity_4LightPosX0 - tmpvar_5.x);
  highp vec4 tmpvar_17;
  tmpvar_17 = (unity_4LightPosY0 - tmpvar_5.y);
  highp vec4 tmpvar_18;
  tmpvar_18 = (unity_4LightPosZ0 - tmpvar_5.z);
  lengthSq_15 = (tmpvar_16 * tmpvar_16);
  lengthSq_15 = (lengthSq_15 + (tmpvar_17 * tmpvar_17));
  lengthSq_15 = (lengthSq_15 + (tmpvar_18 * tmpvar_18));
  highp vec4 tmpvar_19;
  tmpvar_19 = max (lengthSq_15, vec4(1e-06, 1e-06, 1e-06, 1e-06));
  lengthSq_15 = tmpvar_19;
  ndotl_14 = (tmpvar_16 * tmpvar_7.x);
  ndotl_14 = (ndotl_14 + (tmpvar_17 * tmpvar_7.y));
  ndotl_14 = (ndotl_14 + (tmpvar_18 * tmpvar_7.z));
  highp vec4 tmpvar_20;
  tmpvar_20 = max (vec4(0.0, 0.0, 0.0, 0.0), (ndotl_14 * inversesqrt(tmpvar_19)));
  ndotl_14 = tmpvar_20;
  highp vec4 tmpvar_21;
  tmpvar_21 = (tmpvar_20 * (1.0/((1.0 + 
    (tmpvar_19 * lightAttenSq_12)
  ))));
  col_13 = (lightColor0_8 * tmpvar_21.x);
  col_13 = (col_13 + (lightColor1_9 * tmpvar_21.y));
  col_13 = (col_13 + (lightColor2_10 * tmpvar_21.z));
  col_13 = (col_13 + (lightColor3_11 * tmpvar_21.w));
  tmpvar_2 = col_13;
  mediump vec3 normal_22;
  normal_22 = tmpvar_7;
  mediump vec3 ambient_23;
  mediump vec3 x1_24;
  mediump vec4 tmpvar_25;
  tmpvar_25 = (normal_22.xyzz * normal_22.yzzx);
  x1_24.x = dot (unity_SHBr, tmpvar_25);
  x1_24.y = dot (unity_SHBg, tmpvar_25);
  x1_24.z = dot (unity_SHBb, tmpvar_25);
  ambient_23 = ((tmpvar_2 * (
    (tmpvar_2 * ((tmpvar_2 * 0.305306) + 0.6821711))
   + 0.01252288)) + (x1_24 + (unity_SHC.xyz * 
    ((normal_22.x * normal_22.x) - (normal_22.y * normal_22.y))
  )));
  tmpvar_2 = ambient_23;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_4));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = tmpvar_7;
  xlv_TEXCOORD2 = tmpvar_1;
  xlv_TEXCOORD3 = ambient_23;
  xlv_TEXCOORD5 = (unity_WorldToShadow[0] * tmpvar_5);
  xlv_TEXCOORD6 = tmpvar_3;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 unity_SHAr;
uniform mediump vec4 unity_SHAg;
uniform mediump vec4 unity_SHAb;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp samplerCube unity_SpecCube0;
uniform mediump vec4 unity_SpecCube0_HDR;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _ShadowMapTexture;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec4 xlv_TEXCOORD2;
varying mediump vec3 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD5;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump float tmpvar_2;
  highp vec4 tmpvar_3;
  mediump vec3 tmpvar_4;
  mediump vec3 tmpvar_5;
  lowp vec4 c_6;
  lowp float atten_7;
  lowp vec3 lightDir_8;
  mediump vec3 tmpvar_9;
  tmpvar_9 = _WorldSpaceLightPos0.xyz;
  lightDir_8 = tmpvar_9;
  highp vec3 tmpvar_10;
  highp vec3 tmpvar_11;
  tmpvar_11 = (_WorldSpaceCameraPos - xlv_TEXCOORD2.xyz);
  tmpvar_10 = normalize(tmpvar_11);
  lowp vec3 tmpvar_12;
  lowp float tmpvar_13;
  lowp vec4 tmpvar_14;
  tmpvar_14 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_12 = tmpvar_14.xyz;
  tmpvar_13 = tmpvar_14.w;
  mediump float realtimeShadowAttenuation_15;
  highp vec4 v_16;
  v_16.x = unity_MatrixV[0].z;
  v_16.y = unity_MatrixV[1].z;
  v_16.z = unity_MatrixV[2].z;
  v_16.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_17;
  tmpvar_17 = (xlv_TEXCOORD2.xyz - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_18;
  highp float tmpvar_19;
  tmpvar_19 = clamp (((
    mix (dot (tmpvar_11, v_16.xyz), sqrt(dot (tmpvar_17, tmpvar_17)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_18 = tmpvar_19;
  lowp float tmpvar_20;
  highp float lightShadowDataX_21;
  mediump float tmpvar_22;
  tmpvar_22 = _LightShadowData.x;
  lightShadowDataX_21 = tmpvar_22;
  highp float tmpvar_23;
  tmpvar_23 = max (float((texture2D (_ShadowMapTexture, xlv_TEXCOORD5.xy).x > xlv_TEXCOORD5.z)), lightShadowDataX_21);
  tmpvar_20 = tmpvar_23;
  realtimeShadowAttenuation_15 = tmpvar_20;
  mediump float tmpvar_24;
  tmpvar_24 = mix (realtimeShadowAttenuation_15, 1.0, tmpvar_18);
  atten_7 = tmpvar_24;
  tmpvar_4 = _LightColor0.xyz;
  tmpvar_5 = lightDir_8;
  tmpvar_1 = tmpvar_10;
  tmpvar_2 = atten_7;
  tmpvar_3 = unity_SpecCube0_HDR;
  mediump vec3 Normal_25;
  Normal_25 = xlv_TEXCOORD1;
  mediump float tmpvar_26;
  highp float tmpvar_27;
  highp float smoothness_28;
  smoothness_28 = _Glossiness;
  tmpvar_27 = (1.0 - smoothness_28);
  tmpvar_26 = tmpvar_27;
  mediump vec3 I_29;
  I_29 = -(tmpvar_1);
  mediump vec3 normalWorld_30;
  normalWorld_30 = xlv_TEXCOORD1;
  mediump vec3 tmpvar_31;
  mediump vec3 tmpvar_32;
  mediump vec3 tmpvar_33;
  tmpvar_32 = (tmpvar_4 * tmpvar_2);
  mediump vec4 tmpvar_34;
  tmpvar_34.w = 1.0;
  tmpvar_34.xyz = normalWorld_30;
  mediump vec3 x_35;
  x_35.x = dot (unity_SHAr, tmpvar_34);
  x_35.y = dot (unity_SHAg, tmpvar_34);
  x_35.z = dot (unity_SHAb, tmpvar_34);
  tmpvar_33 = max (((1.055 * 
    pow (max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD3 + x_35)), vec3(0.4166667, 0.4166667, 0.4166667))
  ) - 0.055), vec3(0.0, 0.0, 0.0));
  mediump vec4 hdr_36;
  hdr_36 = tmpvar_3;
  mediump vec4 tmpvar_37;
  tmpvar_37.xyz = (I_29 - (2.0 * (
    dot (Normal_25, I_29)
   * Normal_25)));
  tmpvar_37.w = ((tmpvar_26 * (1.7 - 
    (0.7 * tmpvar_26)
  )) * 6.0);
  lowp vec4 tmpvar_38;
  tmpvar_38 = impl_low_textureCubeLodEXT (unity_SpecCube0, tmpvar_37.xyz, tmpvar_37.w);
  mediump vec4 tmpvar_39;
  tmpvar_39 = tmpvar_38;
  tmpvar_31 = ((hdr_36.x * (
    (hdr_36.w * (tmpvar_39.w - 1.0))
   + 1.0)) * tmpvar_39.xyz);
  tmpvar_4 = tmpvar_32;
  lowp vec3 tmpvar_40;
  mediump vec4 c_41;
  highp vec3 tmpvar_42;
  tmpvar_42 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_43;
  mediump vec3 albedo_44;
  albedo_44 = tmpvar_12;
  mediump vec3 tmpvar_45;
  tmpvar_45 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_44, vec3(_Metallic));
  mediump float tmpvar_46;
  tmpvar_46 = (0.7790837 - (_Metallic * 0.7790837));
  tmpvar_43 = (albedo_44 * tmpvar_46);
  tmpvar_40 = tmpvar_43;
  mediump vec3 diffColor_47;
  diffColor_47 = tmpvar_40;
  mediump float alpha_48;
  alpha_48 = tmpvar_13;
  tmpvar_40 = diffColor_47;
  mediump vec3 diffColor_49;
  diffColor_49 = tmpvar_40;
  mediump vec3 color_50;
  mediump float surfaceReduction_51;
  highp float specularTerm_52;
  highp float a2_53;
  mediump float roughness_54;
  mediump float perceptualRoughness_55;
  highp vec3 tmpvar_56;
  highp vec3 inVec_57;
  inVec_57 = (tmpvar_5 + tmpvar_10);
  tmpvar_56 = (inVec_57 * inversesqrt(max (0.001, 
    dot (inVec_57, inVec_57)
  )));
  mediump float tmpvar_58;
  highp float tmpvar_59;
  tmpvar_59 = clamp (dot (tmpvar_42, tmpvar_5), 0.0, 1.0);
  tmpvar_58 = tmpvar_59;
  highp float tmpvar_60;
  tmpvar_60 = clamp (dot (tmpvar_42, tmpvar_56), 0.0, 1.0);
  mediump float tmpvar_61;
  highp float tmpvar_62;
  tmpvar_62 = clamp (dot (tmpvar_42, tmpvar_10), 0.0, 1.0);
  tmpvar_61 = tmpvar_62;
  highp float tmpvar_63;
  highp float smoothness_64;
  smoothness_64 = _Glossiness;
  tmpvar_63 = (1.0 - smoothness_64);
  perceptualRoughness_55 = tmpvar_63;
  highp float tmpvar_65;
  highp float perceptualRoughness_66;
  perceptualRoughness_66 = perceptualRoughness_55;
  tmpvar_65 = (perceptualRoughness_66 * perceptualRoughness_66);
  roughness_54 = tmpvar_65;
  mediump float tmpvar_67;
  tmpvar_67 = (roughness_54 * roughness_54);
  a2_53 = tmpvar_67;
  specularTerm_52 = ((roughness_54 / (
    (max (0.32, clamp (dot (tmpvar_5, tmpvar_56), 0.0, 1.0)) * (1.5 + roughness_54))
   * 
    (((tmpvar_60 * tmpvar_60) * (a2_53 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_68;
  tmpvar_68 = clamp (specularTerm_52, 0.0, 100.0);
  specularTerm_52 = tmpvar_68;
  surfaceReduction_51 = (1.0 - ((roughness_54 * perceptualRoughness_55) * 0.28));
  mediump float x_69;
  x_69 = (1.0 - tmpvar_61);
  mediump vec3 tmpvar_70;
  tmpvar_70 = mix (tmpvar_45, vec3(clamp ((_Glossiness + 
    (1.0 - tmpvar_46)
  ), 0.0, 1.0)), vec3(((x_69 * x_69) * (x_69 * x_69))));
  highp vec3 tmpvar_71;
  tmpvar_71 = (((
    ((diffColor_49 + (tmpvar_68 * tmpvar_45)) * tmpvar_32)
   * tmpvar_58) + (tmpvar_33 * diffColor_49)) + ((surfaceReduction_51 * tmpvar_31) * tmpvar_70));
  color_50 = tmpvar_71;
  mediump vec4 tmpvar_72;
  tmpvar_72.w = 1.0;
  tmpvar_72.xyz = color_50;
  c_41.xyz = tmpvar_72.xyz;
  c_41.w = alpha_48;
  c_6.xyz = c_41.xyz;
  c_6.w = 1.0;
  gl_FragData[0] = c_6;
}


#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 unity_4LightPosX0;
uniform 	vec4 unity_4LightPosY0;
uniform 	vec4 unity_4LightPosZ0;
uniform 	mediump vec4 unity_4LightAtten0;
uniform 	mediump vec4 unity_LightColor[8];
uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out mediump vec3 vs_TEXCOORD3;
out highp vec4 vs_TEXCOORD5;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
mediump vec4 u_xlat16_1;
vec4 u_xlat2;
vec4 u_xlat3;
vec4 u_xlat4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
float u_xlat21;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat1.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat21 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat1.xyz = vec3(u_xlat21) * u_xlat1.xyz;
    vs_TEXCOORD1.xyz = u_xlat1.xyz;
    vs_TEXCOORD2.xyz = u_xlat0.xyz;
    u_xlat2 = (-u_xlat0.xxxx) + unity_4LightPosX0;
    u_xlat3 = (-u_xlat0.yyyy) + unity_4LightPosY0;
    u_xlat0 = (-u_xlat0.zzzz) + unity_4LightPosZ0;
    u_xlat4 = u_xlat1.yyyy * u_xlat3;
    u_xlat3 = u_xlat3 * u_xlat3;
    u_xlat3 = u_xlat2 * u_xlat2 + u_xlat3;
    u_xlat2 = u_xlat2 * u_xlat1.xxxx + u_xlat4;
    u_xlat2 = u_xlat0 * u_xlat1.zzzz + u_xlat2;
    u_xlat0 = u_xlat0 * u_xlat0 + u_xlat3;
    u_xlat0 = max(u_xlat0, vec4(9.99999997e-07, 9.99999997e-07, 9.99999997e-07, 9.99999997e-07));
    u_xlat3 = inversesqrt(u_xlat0);
    u_xlat0 = u_xlat0 * unity_4LightAtten0 + vec4(1.0, 1.0, 1.0, 1.0);
    u_xlat0 = vec4(1.0, 1.0, 1.0, 1.0) / u_xlat0;
    u_xlat2 = u_xlat2 * u_xlat3;
    u_xlat2 = max(u_xlat2, vec4(0.0, 0.0, 0.0, 0.0));
    u_xlat0 = u_xlat0 * u_xlat2;
    u_xlat2.xyz = u_xlat0.yyy * unity_LightColor[1].xyz;
    u_xlat2.xyz = unity_LightColor[0].xyz * u_xlat0.xxx + u_xlat2.xyz;
    u_xlat0.xyz = unity_LightColor[2].xyz * u_xlat0.zzz + u_xlat2.xyz;
    u_xlat0.xyz = unity_LightColor[3].xyz * u_xlat0.www + u_xlat0.xyz;
    u_xlat2.xyz = u_xlat0.xyz * vec3(0.305306017, 0.305306017, 0.305306017) + vec3(0.682171106, 0.682171106, 0.682171106);
    u_xlat2.xyz = u_xlat0.xyz * u_xlat2.xyz + vec3(0.0125228781, 0.0125228781, 0.0125228781);
    u_xlat16_5.x = u_xlat1.y * u_xlat1.y;
    u_xlat16_5.x = u_xlat1.x * u_xlat1.x + (-u_xlat16_5.x);
    u_xlat16_1 = u_xlat1.yzzx * u_xlat1.xyzz;
    u_xlat16_6.x = dot(unity_SHBr, u_xlat16_1);
    u_xlat16_6.y = dot(unity_SHBg, u_xlat16_1);
    u_xlat16_6.z = dot(unity_SHBb, u_xlat16_1);
    u_xlat16_5.xyz = unity_SHC.xyz * u_xlat16_5.xxx + u_xlat16_6.xyz;
    vs_TEXCOORD3.xyz = u_xlat0.xyz * u_xlat2.xyz + u_xlat16_5.xyz;
    vs_TEXCOORD5 = vec4(0.0, 0.0, 0.0, 0.0);
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp samplerCube unity_SpecCube0;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
in mediump vec3 vs_TEXCOORD3;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump vec3 u_xlat16_0;
mediump vec3 u_xlat16_1;
lowp vec4 u_xlat10_1;
vec3 u_xlat2;
vec3 u_xlat3;
vec3 u_xlat4;
mediump vec3 u_xlat16_4;
lowp vec3 u_xlat10_4;
mediump vec3 u_xlat16_5;
vec3 u_xlat6;
mediump vec3 u_xlat16_7;
mediump vec3 u_xlat16_8;
mediump vec3 u_xlat16_9;
float u_xlat10;
lowp float u_xlat10_10;
mediump vec3 u_xlat16_11;
float u_xlat12;
mediump float u_xlat16_12;
mediump vec3 u_xlat16_17;
float u_xlat30;
float u_xlat32;
mediump float u_xlat16_35;
mediump float u_xlat16_37;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD1.xyz;
    u_xlat0.w = 1.0;
    u_xlat16_1.x = dot(unity_SHAr, u_xlat0);
    u_xlat16_1.y = dot(unity_SHAg, u_xlat0);
    u_xlat16_1.z = dot(unity_SHAb, u_xlat0);
    u_xlat16_1.xyz = u_xlat16_1.xyz + vs_TEXCOORD3.xyz;
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_0.xyz = log2(u_xlat16_1.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_0.xyz = exp2(u_xlat16_0.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_0.xyz = max(u_xlat16_0.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat2.z = (-_Glossiness) + 1.0;
    u_xlat16_1.x = (-u_xlat2.z) * 0.699999988 + 1.70000005;
    u_xlat16_1.x = u_xlat16_1.x * u_xlat2.z;
    u_xlat16_1.x = u_xlat16_1.x * 6.0;
    u_xlat3.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat30 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat30 = inversesqrt(u_xlat30);
    u_xlat4.xyz = vec3(u_xlat30) * u_xlat3.xyz;
    u_xlat16_11.x = dot((-u_xlat4.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_11.x = u_xlat16_11.x + u_xlat16_11.x;
    u_xlat16_11.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_11.xxx) + (-u_xlat4.xyz);
    u_xlat10_1 = textureLod(unity_SpecCube0, u_xlat16_11.xyz, u_xlat16_1.x);
    u_xlat16_5.x = u_xlat10_1.w + -1.0;
    u_xlat16_5.x = unity_SpecCube0_HDR.w * u_xlat16_5.x + 1.0;
    u_xlat16_5.x = u_xlat16_5.x * unity_SpecCube0_HDR.x;
    u_xlat16_5.xyz = u_xlat10_1.xyz * u_xlat16_5.xxx;
    u_xlat30 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat30 = inversesqrt(u_xlat30);
    u_xlat6.xyz = vec3(u_xlat30) * vs_TEXCOORD1.xyz;
    u_xlat30 = dot(u_xlat4.xyz, u_xlat6.xyz);
    u_xlat12 = u_xlat30;
#ifdef UNITY_ADRENO_ES3
    u_xlat12 = min(max(u_xlat12, 0.0), 1.0);
#else
    u_xlat12 = clamp(u_xlat12, 0.0, 1.0);
#endif
    u_xlat30 = u_xlat30 + u_xlat30;
    u_xlat4.xyz = u_xlat6.xyz * (-vec3(u_xlat30)) + u_xlat4.xyz;
    u_xlat30 = dot(u_xlat6.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat30 = min(max(u_xlat30, 0.0), 1.0);
#else
    u_xlat30 = clamp(u_xlat30, 0.0, 1.0);
#endif
    u_xlat32 = dot(u_xlat4.xyz, _WorldSpaceLightPos0.xyz);
    u_xlat32 = u_xlat32 * u_xlat32;
    u_xlat2.x = u_xlat32 * u_xlat32;
    u_xlat2.x = texture(unity_NHxRoughness, u_xlat2.xz).w;
    u_xlat2.x = u_xlat2.x * 16.0;
    u_xlat16_35 = (-u_xlat12) + 1.0;
    u_xlat16_12 = u_xlat16_35 * u_xlat16_35;
    u_xlat16_12 = u_xlat16_35 * u_xlat16_12;
    u_xlat16_12 = u_xlat16_35 * u_xlat16_12;
    u_xlat16_35 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_7.x = (-u_xlat16_35) + _Glossiness;
    u_xlat16_7.x = u_xlat16_7.x + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_7.x = min(max(u_xlat16_7.x, 0.0), 1.0);
#else
    u_xlat16_7.x = clamp(u_xlat16_7.x, 0.0, 1.0);
#endif
    u_xlat10_4.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_17.xyz = u_xlat10_4.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_4.xyz = u_xlat10_4.xyz * _Color.xyz;
    u_xlat16_8.xyz = vec3(u_xlat16_35) * u_xlat16_4.xyz;
    u_xlat16_17.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_17.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_9.xyz = (-u_xlat16_17.xyz) + u_xlat16_7.xxx;
    u_xlat16_9.xyz = vec3(u_xlat16_12) * u_xlat16_9.xyz + u_xlat16_17.xyz;
    u_xlat16_7.xyz = u_xlat2.xxx * u_xlat16_17.xyz + u_xlat16_8.xyz;
    u_xlat16_5.xyz = u_xlat16_5.xyz * u_xlat16_9.xyz;
    u_xlat16_5.xyz = u_xlat16_0.xyz * u_xlat16_8.xyz + u_xlat16_5.xyz;
    u_xlat0.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat0.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat0.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat0.x = dot(u_xlat3.xyz, u_xlat0.xyz);
    u_xlat2.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat10 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat10 = sqrt(u_xlat10);
    u_xlat10 = (-u_xlat0.x) + u_xlat10;
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat10 + u_xlat0.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat2.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat2.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * vs_TEXCOORD2.xxx + u_xlat2.xyz;
    u_xlat2.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * vs_TEXCOORD2.zzz + u_xlat2.xyz;
    u_xlat2.xyz = u_xlat2.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    vec3 txVec0 = vec3(u_xlat2.xy,u_xlat2.z);
    u_xlat10_10 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_35 = (-_LightShadowData.x) + 1.0;
    u_xlat16_35 = u_xlat10_10 * u_xlat16_35 + _LightShadowData.x;
    u_xlat16_37 = (-u_xlat16_35) + 1.0;
    u_xlat16_35 = u_xlat0.x * u_xlat16_37 + u_xlat16_35;
    u_xlat16_8.xyz = vec3(u_xlat16_35) * _LightColor0.xyz;
    u_xlat16_8.xyz = vec3(u_xlat30) * u_xlat16_8.xyz;
    SV_Target0.xyz = u_xlat16_7.xyz * u_xlat16_8.xyz + u_xlat16_5.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 unity_4LightPosX0;
uniform 	vec4 unity_4LightPosY0;
uniform 	vec4 unity_4LightPosZ0;
uniform 	mediump vec4 unity_4LightAtten0;
uniform 	mediump vec4 unity_LightColor[8];
uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out mediump vec3 vs_TEXCOORD3;
out highp vec4 vs_TEXCOORD5;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
mediump vec4 u_xlat16_1;
vec4 u_xlat2;
vec4 u_xlat3;
vec4 u_xlat4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
float u_xlat21;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat1.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat21 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat1.xyz = vec3(u_xlat21) * u_xlat1.xyz;
    vs_TEXCOORD1.xyz = u_xlat1.xyz;
    vs_TEXCOORD2.xyz = u_xlat0.xyz;
    u_xlat2 = (-u_xlat0.xxxx) + unity_4LightPosX0;
    u_xlat3 = (-u_xlat0.yyyy) + unity_4LightPosY0;
    u_xlat0 = (-u_xlat0.zzzz) + unity_4LightPosZ0;
    u_xlat4 = u_xlat1.yyyy * u_xlat3;
    u_xlat3 = u_xlat3 * u_xlat3;
    u_xlat3 = u_xlat2 * u_xlat2 + u_xlat3;
    u_xlat2 = u_xlat2 * u_xlat1.xxxx + u_xlat4;
    u_xlat2 = u_xlat0 * u_xlat1.zzzz + u_xlat2;
    u_xlat0 = u_xlat0 * u_xlat0 + u_xlat3;
    u_xlat0 = max(u_xlat0, vec4(9.99999997e-07, 9.99999997e-07, 9.99999997e-07, 9.99999997e-07));
    u_xlat3 = inversesqrt(u_xlat0);
    u_xlat0 = u_xlat0 * unity_4LightAtten0 + vec4(1.0, 1.0, 1.0, 1.0);
    u_xlat0 = vec4(1.0, 1.0, 1.0, 1.0) / u_xlat0;
    u_xlat2 = u_xlat2 * u_xlat3;
    u_xlat2 = max(u_xlat2, vec4(0.0, 0.0, 0.0, 0.0));
    u_xlat0 = u_xlat0 * u_xlat2;
    u_xlat2.xyz = u_xlat0.yyy * unity_LightColor[1].xyz;
    u_xlat2.xyz = unity_LightColor[0].xyz * u_xlat0.xxx + u_xlat2.xyz;
    u_xlat0.xyz = unity_LightColor[2].xyz * u_xlat0.zzz + u_xlat2.xyz;
    u_xlat0.xyz = unity_LightColor[3].xyz * u_xlat0.www + u_xlat0.xyz;
    u_xlat2.xyz = u_xlat0.xyz * vec3(0.305306017, 0.305306017, 0.305306017) + vec3(0.682171106, 0.682171106, 0.682171106);
    u_xlat2.xyz = u_xlat0.xyz * u_xlat2.xyz + vec3(0.0125228781, 0.0125228781, 0.0125228781);
    u_xlat16_5.x = u_xlat1.y * u_xlat1.y;
    u_xlat16_5.x = u_xlat1.x * u_xlat1.x + (-u_xlat16_5.x);
    u_xlat16_1 = u_xlat1.yzzx * u_xlat1.xyzz;
    u_xlat16_6.x = dot(unity_SHBr, u_xlat16_1);
    u_xlat16_6.y = dot(unity_SHBg, u_xlat16_1);
    u_xlat16_6.z = dot(unity_SHBb, u_xlat16_1);
    u_xlat16_5.xyz = unity_SHC.xyz * u_xlat16_5.xxx + u_xlat16_6.xyz;
    vs_TEXCOORD3.xyz = u_xlat0.xyz * u_xlat2.xyz + u_xlat16_5.xyz;
    vs_TEXCOORD5 = vec4(0.0, 0.0, 0.0, 0.0);
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform lowp samplerCube unity_SpecCube0;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
in mediump vec3 vs_TEXCOORD3;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump vec3 u_xlat16_0;
lowp vec4 u_xlat10_0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
lowp vec3 u_xlat10_2;
vec3 u_xlat3;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
vec3 u_xlat8;
mediump float u_xlat16_10;
mediump vec3 u_xlat16_12;
mediump float u_xlat16_18;
float u_xlat24;
mediump float u_xlat16_25;
mediump float u_xlat16_26;
mediump float u_xlat16_28;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD1.xyz;
    u_xlat0.w = 1.0;
    u_xlat16_1.x = dot(unity_SHAr, u_xlat0);
    u_xlat16_1.y = dot(unity_SHAg, u_xlat0);
    u_xlat16_1.z = dot(unity_SHAb, u_xlat0);
    u_xlat16_1.xyz = u_xlat16_1.xyz + vs_TEXCOORD3.xyz;
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_0.xyz = log2(u_xlat16_1.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_0.xyz = exp2(u_xlat16_0.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_0.xyz = max(u_xlat16_0.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat10_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_2.xyz * _Color.xyz;
    u_xlat16_1.xyz = u_xlat10_2.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_1.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_25 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_4.xyz = vec3(u_xlat16_25) * u_xlat16_3.xyz;
    u_xlat16_25 = (-u_xlat16_25) + _Glossiness;
    u_xlat16_25 = u_xlat16_25 + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_25 = min(max(u_xlat16_25, 0.0), 1.0);
#else
    u_xlat16_25 = clamp(u_xlat16_25, 0.0, 1.0);
#endif
    u_xlat16_5.xyz = (-u_xlat16_1.xyz) + vec3(u_xlat16_25);
    u_xlat16_6.xyz = u_xlat16_0.xyz * u_xlat16_4.xyz;
    u_xlat0.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * vs_TEXCOORD2.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * vs_TEXCOORD2.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    vec3 txVec0 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat10_0.x = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_25 = (-_LightShadowData.x) + 1.0;
    u_xlat16_25 = u_xlat10_0.x * u_xlat16_25 + _LightShadowData.x;
    u_xlat16_28 = (-u_xlat16_25) + 1.0;
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat2.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat8.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat2.x = dot(u_xlat8.xyz, u_xlat2.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat2.x);
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat0.x + u_xlat2.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_25 = u_xlat0.x * u_xlat16_28 + u_xlat16_25;
    u_xlat16_7.xyz = vec3(u_xlat16_25) * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat8.xyz, u_xlat8.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat2.xyz = u_xlat8.xyz * u_xlat0.xxx + _WorldSpaceLightPos0.xyz;
    u_xlat0.xyz = u_xlat0.xxx * u_xlat8.xyz;
    u_xlat24 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat24 = max(u_xlat24, 0.00100000005);
    u_xlat24 = inversesqrt(u_xlat24);
    u_xlat2.xyz = vec3(u_xlat24) * u_xlat2.xyz;
    u_xlat24 = dot(_WorldSpaceLightPos0.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat24 = min(max(u_xlat24, 0.0), 1.0);
#else
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
#endif
    u_xlat24 = max(u_xlat24, 0.319999993);
    u_xlat16_26 = (-_Glossiness) + 1.0;
    u_xlat16_3.x = u_xlat16_26 * u_xlat16_26 + 1.5;
    u_xlat24 = u_xlat24 * u_xlat16_3.x;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat2.x = dot(u_xlat3.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat2.x = min(max(u_xlat2.x, 0.0), 1.0);
#else
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
#endif
    u_xlat2.x = u_xlat2.x * u_xlat2.x;
    u_xlat16_10 = u_xlat16_26 * u_xlat16_26;
    u_xlat16_18 = u_xlat16_10 * u_xlat16_10 + -1.0;
    u_xlat2.x = u_xlat2.x * u_xlat16_18 + 1.00001001;
    u_xlat24 = u_xlat24 * u_xlat2.x;
    u_xlat24 = u_xlat16_10 / u_xlat24;
    u_xlat16_25 = u_xlat16_26 * u_xlat16_10;
    u_xlat16_25 = (-u_xlat16_25) * 0.280000001 + 1.0;
    u_xlat24 = u_xlat24 + -9.99999975e-05;
    u_xlat24 = max(u_xlat24, 0.0);
    u_xlat24 = min(u_xlat24, 100.0);
    u_xlat2.xyz = vec3(u_xlat24) * u_xlat16_1.xyz + u_xlat16_4.xyz;
    u_xlat2.xyz = u_xlat16_7.xyz * u_xlat2.xyz;
    u_xlat24 = dot(u_xlat3.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat24 = min(max(u_xlat24, 0.0), 1.0);
#else
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
#endif
    u_xlat3.x = dot(u_xlat3.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat3.x = min(max(u_xlat3.x, 0.0), 1.0);
#else
    u_xlat3.x = clamp(u_xlat3.x, 0.0, 1.0);
#endif
    u_xlat16_4.x = (-u_xlat3.x) + 1.0;
    u_xlat16_4.x = u_xlat16_4.x * u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_4.x * u_xlat16_4.x;
    u_xlat16_1.xyz = u_xlat16_4.xxx * u_xlat16_5.xyz + u_xlat16_1.xyz;
    u_xlat2.xyz = u_xlat2.xyz * vec3(u_xlat24) + u_xlat16_6.xyz;
    u_xlat16_4.x = (-u_xlat16_26) * 0.699999988 + 1.70000005;
    u_xlat16_4.x = u_xlat16_26 * u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_4.x * 6.0;
    u_xlat16_12.x = dot((-u_xlat0.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_12.x = u_xlat16_12.x + u_xlat16_12.x;
    u_xlat16_12.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_12.xxx) + (-u_xlat0.xyz);
    u_xlat10_0 = textureLod(unity_SpecCube0, u_xlat16_12.xyz, u_xlat16_4.x);
    u_xlat16_4.x = u_xlat10_0.w + -1.0;
    u_xlat16_4.x = unity_SpecCube0_HDR.w * u_xlat16_4.x + 1.0;
    u_xlat16_4.x = u_xlat16_4.x * unity_SpecCube0_HDR.x;
    u_xlat16_4.xyz = u_xlat10_0.xyz * u_xlat16_4.xxx;
    u_xlat16_4.xyz = vec3(u_xlat16_25) * u_xlat16_4.xyz;
    u_xlat0.xyz = u_xlat16_4.xyz * u_xlat16_1.xyz + u_xlat2.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 unity_4LightPosX0;
uniform 	vec4 unity_4LightPosY0;
uniform 	vec4 unity_4LightPosZ0;
uniform 	mediump vec4 unity_4LightAtten0;
uniform 	mediump vec4 unity_LightColor[8];
uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out mediump vec3 vs_TEXCOORD3;
out highp vec4 vs_TEXCOORD5;
out highp vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
mediump vec4 u_xlat16_1;
vec4 u_xlat2;
vec4 u_xlat3;
vec4 u_xlat4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
float u_xlat21;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat1.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat21 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat1.xyz = vec3(u_xlat21) * u_xlat1.xyz;
    vs_TEXCOORD1.xyz = u_xlat1.xyz;
    vs_TEXCOORD2.xyz = u_xlat0.xyz;
    u_xlat2 = (-u_xlat0.xxxx) + unity_4LightPosX0;
    u_xlat3 = (-u_xlat0.yyyy) + unity_4LightPosY0;
    u_xlat0 = (-u_xlat0.zzzz) + unity_4LightPosZ0;
    u_xlat4 = u_xlat1.yyyy * u_xlat3;
    u_xlat3 = u_xlat3 * u_xlat3;
    u_xlat3 = u_xlat2 * u_xlat2 + u_xlat3;
    u_xlat2 = u_xlat2 * u_xlat1.xxxx + u_xlat4;
    u_xlat2 = u_xlat0 * u_xlat1.zzzz + u_xlat2;
    u_xlat0 = u_xlat0 * u_xlat0 + u_xlat3;
    u_xlat0 = max(u_xlat0, vec4(9.99999997e-07, 9.99999997e-07, 9.99999997e-07, 9.99999997e-07));
    u_xlat3 = inversesqrt(u_xlat0);
    u_xlat0 = u_xlat0 * unity_4LightAtten0 + vec4(1.0, 1.0, 1.0, 1.0);
    u_xlat0 = vec4(1.0, 1.0, 1.0, 1.0) / u_xlat0;
    u_xlat2 = u_xlat2 * u_xlat3;
    u_xlat2 = max(u_xlat2, vec4(0.0, 0.0, 0.0, 0.0));
    u_xlat0 = u_xlat0 * u_xlat2;
    u_xlat2.xyz = u_xlat0.yyy * unity_LightColor[1].xyz;
    u_xlat2.xyz = unity_LightColor[0].xyz * u_xlat0.xxx + u_xlat2.xyz;
    u_xlat0.xyz = unity_LightColor[2].xyz * u_xlat0.zzz + u_xlat2.xyz;
    u_xlat0.xyz = unity_LightColor[3].xyz * u_xlat0.www + u_xlat0.xyz;
    u_xlat2.xyz = u_xlat0.xyz * vec3(0.305306017, 0.305306017, 0.305306017) + vec3(0.682171106, 0.682171106, 0.682171106);
    u_xlat2.xyz = u_xlat0.xyz * u_xlat2.xyz + vec3(0.0125228781, 0.0125228781, 0.0125228781);
    u_xlat16_5.x = u_xlat1.y * u_xlat1.y;
    u_xlat16_5.x = u_xlat1.x * u_xlat1.x + (-u_xlat16_5.x);
    u_xlat16_1 = u_xlat1.yzzx * u_xlat1.xyzz;
    u_xlat16_6.x = dot(unity_SHBr, u_xlat16_1);
    u_xlat16_6.y = dot(unity_SHBg, u_xlat16_1);
    u_xlat16_6.z = dot(unity_SHBb, u_xlat16_1);
    u_xlat16_5.xyz = unity_SHC.xyz * u_xlat16_5.xxx + u_xlat16_6.xyz;
    vs_TEXCOORD3.xyz = u_xlat0.xyz * u_xlat2.xyz + u_xlat16_5.xyz;
    vs_TEXCOORD5 = vec4(0.0, 0.0, 0.0, 0.0);
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform lowp samplerCube unity_SpecCube0;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
in mediump vec3 vs_TEXCOORD3;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump vec3 u_xlat16_0;
lowp vec4 u_xlat10_0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
lowp vec3 u_xlat10_2;
vec3 u_xlat3;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
vec3 u_xlat8;
mediump float u_xlat16_10;
mediump vec3 u_xlat16_12;
mediump float u_xlat16_18;
float u_xlat24;
mediump float u_xlat16_25;
mediump float u_xlat16_26;
mediump float u_xlat16_28;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD1.xyz;
    u_xlat0.w = 1.0;
    u_xlat16_1.x = dot(unity_SHAr, u_xlat0);
    u_xlat16_1.y = dot(unity_SHAg, u_xlat0);
    u_xlat16_1.z = dot(unity_SHAb, u_xlat0);
    u_xlat16_1.xyz = u_xlat16_1.xyz + vs_TEXCOORD3.xyz;
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_0.xyz = log2(u_xlat16_1.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_0.xyz = exp2(u_xlat16_0.xyz);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_0.xyz = max(u_xlat16_0.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat10_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_2.xyz * _Color.xyz;
    u_xlat16_1.xyz = u_xlat10_2.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_1.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_25 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_4.xyz = vec3(u_xlat16_25) * u_xlat16_3.xyz;
    u_xlat16_25 = (-u_xlat16_25) + _Glossiness;
    u_xlat16_25 = u_xlat16_25 + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_25 = min(max(u_xlat16_25, 0.0), 1.0);
#else
    u_xlat16_25 = clamp(u_xlat16_25, 0.0, 1.0);
#endif
    u_xlat16_5.xyz = (-u_xlat16_1.xyz) + vec3(u_xlat16_25);
    u_xlat16_6.xyz = u_xlat16_0.xyz * u_xlat16_4.xyz;
    u_xlat0.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * vs_TEXCOORD2.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * vs_TEXCOORD2.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    vec3 txVec0 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat10_0.x = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_25 = (-_LightShadowData.x) + 1.0;
    u_xlat16_25 = u_xlat10_0.x * u_xlat16_25 + _LightShadowData.x;
    u_xlat16_28 = (-u_xlat16_25) + 1.0;
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat2.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat8.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat2.x = dot(u_xlat8.xyz, u_xlat2.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat2.x);
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat0.x + u_xlat2.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_25 = u_xlat0.x * u_xlat16_28 + u_xlat16_25;
    u_xlat16_7.xyz = vec3(u_xlat16_25) * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat8.xyz, u_xlat8.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat2.xyz = u_xlat8.xyz * u_xlat0.xxx + _WorldSpaceLightPos0.xyz;
    u_xlat0.xyz = u_xlat0.xxx * u_xlat8.xyz;
    u_xlat24 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat24 = max(u_xlat24, 0.00100000005);
    u_xlat24 = inversesqrt(u_xlat24);
    u_xlat2.xyz = vec3(u_xlat24) * u_xlat2.xyz;
    u_xlat24 = dot(_WorldSpaceLightPos0.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat24 = min(max(u_xlat24, 0.0), 1.0);
#else
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
#endif
    u_xlat24 = max(u_xlat24, 0.319999993);
    u_xlat16_26 = (-_Glossiness) + 1.0;
    u_xlat16_3.x = u_xlat16_26 * u_xlat16_26 + 1.5;
    u_xlat24 = u_xlat24 * u_xlat16_3.x;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat2.x = dot(u_xlat3.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat2.x = min(max(u_xlat2.x, 0.0), 1.0);
#else
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
#endif
    u_xlat2.x = u_xlat2.x * u_xlat2.x;
    u_xlat16_10 = u_xlat16_26 * u_xlat16_26;
    u_xlat16_18 = u_xlat16_10 * u_xlat16_10 + -1.0;
    u_xlat2.x = u_xlat2.x * u_xlat16_18 + 1.00001001;
    u_xlat24 = u_xlat24 * u_xlat2.x;
    u_xlat24 = u_xlat16_10 / u_xlat24;
    u_xlat16_25 = u_xlat16_26 * u_xlat16_10;
    u_xlat16_25 = (-u_xlat16_25) * 0.280000001 + 1.0;
    u_xlat24 = u_xlat24 + -9.99999975e-05;
    u_xlat24 = max(u_xlat24, 0.0);
    u_xlat24 = min(u_xlat24, 100.0);
    u_xlat2.xyz = vec3(u_xlat24) * u_xlat16_1.xyz + u_xlat16_4.xyz;
    u_xlat2.xyz = u_xlat16_7.xyz * u_xlat2.xyz;
    u_xlat24 = dot(u_xlat3.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat24 = min(max(u_xlat24, 0.0), 1.0);
#else
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
#endif
    u_xlat3.x = dot(u_xlat3.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat3.x = min(max(u_xlat3.x, 0.0), 1.0);
#else
    u_xlat3.x = clamp(u_xlat3.x, 0.0, 1.0);
#endif
    u_xlat16_4.x = (-u_xlat3.x) + 1.0;
    u_xlat16_4.x = u_xlat16_4.x * u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_4.x * u_xlat16_4.x;
    u_xlat16_1.xyz = u_xlat16_4.xxx * u_xlat16_5.xyz + u_xlat16_1.xyz;
    u_xlat2.xyz = u_xlat2.xyz * vec3(u_xlat24) + u_xlat16_6.xyz;
    u_xlat16_4.x = (-u_xlat16_26) * 0.699999988 + 1.70000005;
    u_xlat16_4.x = u_xlat16_26 * u_xlat16_4.x;
    u_xlat16_4.x = u_xlat16_4.x * 6.0;
    u_xlat16_12.x = dot((-u_xlat0.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_12.x = u_xlat16_12.x + u_xlat16_12.x;
    u_xlat16_12.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_12.xxx) + (-u_xlat0.xyz);
    u_xlat10_0 = textureLod(unity_SpecCube0, u_xlat16_12.xyz, u_xlat16_4.x);
    u_xlat16_4.x = u_xlat10_0.w + -1.0;
    u_xlat16_4.x = unity_SpecCube0_HDR.w * u_xlat16_4.x + 1.0;
    u_xlat16_4.x = u_xlat16_4.x * unity_SpecCube0_HDR.x;
    u_xlat16_4.xyz = u_xlat10_0.xyz * u_xlat16_4.xxx;
    u_xlat16_4.xyz = vec3(u_xlat16_25) * u_xlat16_4.xyz;
    u_xlat0.xyz = u_xlat16_4.xyz * u_xlat16_1.xyz + u_xlat2.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Fragment shader for "gles":
// No shader variant for this keyword set. The closest match will be used instead.

 }


 // Stats for Vertex shader:
 //         gles: 53 avg math (29..78), 3 avg texture (1..8), 1 avg branch (0..4)
 Pass {
  Name "FORWARD"
  Tags { "LIGHTMODE"="FORWARDADD" "SHADOWSUPPORT"="true" "RenderType"="Opaque" }
  ZWrite Off
  Blend One One
  //////////////////////////////////
  //                              //
  //      Compiled programs       //
  //                              //
  //////////////////////////////////
//////////////////////////////////////////////////////
Keywords set in this variant: POINT 
-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 35 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xyz;
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform highp sampler2D _LightTexture0;
uniform highp mat4 unity_WorldToLight;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  highp vec3 lightCoord_5;
  lowp vec3 tmpvar_6;
  lowp float tmpvar_7;
  highp vec3 worldViewDir_8;
  lowp vec3 lightDir_9;
  highp vec3 tmpvar_10;
  tmpvar_10 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_9 = tmpvar_10;
  worldViewDir_8 = normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2));
  lowp vec4 tmpvar_11;
  tmpvar_11 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_6 = tmpvar_11.xyz;
  tmpvar_7 = tmpvar_11.w;
  highp vec4 tmpvar_12;
  tmpvar_12.w = 1.0;
  tmpvar_12.xyz = xlv_TEXCOORD2;
  lightCoord_5 = (unity_WorldToLight * tmpvar_12).xyz;
  highp float tmpvar_13;
  tmpvar_13 = texture2D (_LightTexture0, vec2(dot (lightCoord_5, lightCoord_5))).w;
  atten_4 = tmpvar_13;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_9;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_14;
  mediump vec4 c_15;
  highp vec3 tmpvar_16;
  tmpvar_16 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_17;
  mediump vec3 albedo_18;
  albedo_18 = tmpvar_6;
  tmpvar_17 = (albedo_18 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_14 = tmpvar_17;
  mediump vec3 diffColor_19;
  diffColor_19 = tmpvar_14;
  mediump float alpha_20;
  alpha_20 = tmpvar_7;
  tmpvar_14 = diffColor_19;
  mediump vec3 diffColor_21;
  diffColor_21 = tmpvar_14;
  mediump vec2 rlPow4AndFresnelTerm_22;
  mediump float tmpvar_23;
  highp float tmpvar_24;
  tmpvar_24 = clamp (dot (tmpvar_16, tmpvar_2), 0.0, 1.0);
  tmpvar_23 = tmpvar_24;
  mediump float tmpvar_25;
  highp float tmpvar_26;
  tmpvar_26 = clamp (dot (tmpvar_16, worldViewDir_8), 0.0, 1.0);
  tmpvar_25 = tmpvar_26;
  highp vec2 tmpvar_27;
  tmpvar_27.x = dot ((worldViewDir_8 - (2.0 * 
    (dot (tmpvar_16, worldViewDir_8) * tmpvar_16)
  )), tmpvar_2);
  tmpvar_27.y = (1.0 - tmpvar_25);
  highp vec2 tmpvar_28;
  tmpvar_28 = ((tmpvar_27 * tmpvar_27) * (tmpvar_27 * tmpvar_27));
  rlPow4AndFresnelTerm_22 = tmpvar_28;
  mediump float tmpvar_29;
  tmpvar_29 = rlPow4AndFresnelTerm_22.x;
  mediump float specular_30;
  highp float smoothness_31;
  smoothness_31 = _Glossiness;
  highp vec2 tmpvar_32;
  tmpvar_32.x = tmpvar_29;
  tmpvar_32.y = (1.0 - smoothness_31);
  highp float tmpvar_33;
  tmpvar_33 = (texture2D (unity_NHxRoughness, tmpvar_32).w * 16.0);
  specular_30 = tmpvar_33;
  mediump vec4 tmpvar_34;
  tmpvar_34.w = 1.0;
  tmpvar_34.xyz = ((diffColor_21 + (specular_30 * 
    mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_18, vec3(_Metallic))
  )) * (tmpvar_1 * tmpvar_23));
  c_15.xyz = tmpvar_34.xyz;
  c_15.w = alpha_20;
  c_3.xyz = c_15.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 45 math, 2 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xyz;
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _LightTexture0;
uniform highp mat4 unity_WorldToLight;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  highp vec3 lightCoord_5;
  lowp vec3 tmpvar_6;
  lowp float tmpvar_7;
  lowp vec3 lightDir_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_8 = tmpvar_9;
  lowp vec4 tmpvar_10;
  tmpvar_10 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_6 = tmpvar_10.xyz;
  tmpvar_7 = tmpvar_10.w;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = xlv_TEXCOORD2;
  lightCoord_5 = (unity_WorldToLight * tmpvar_11).xyz;
  highp float tmpvar_12;
  tmpvar_12 = texture2D (_LightTexture0, vec2(dot (lightCoord_5, lightCoord_5))).w;
  atten_4 = tmpvar_12;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_8;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_13;
  mediump vec4 c_14;
  highp vec3 tmpvar_15;
  tmpvar_15 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_16;
  mediump vec3 albedo_17;
  albedo_17 = tmpvar_6;
  mediump vec3 tmpvar_18;
  tmpvar_18 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_17, vec3(_Metallic));
  tmpvar_16 = (albedo_17 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_13 = tmpvar_16;
  mediump vec3 diffColor_19;
  diffColor_19 = tmpvar_13;
  mediump float alpha_20;
  alpha_20 = tmpvar_7;
  tmpvar_13 = diffColor_19;
  mediump vec3 diffColor_21;
  diffColor_21 = tmpvar_13;
  mediump vec3 color_22;
  highp float specularTerm_23;
  highp float a2_24;
  mediump float roughness_25;
  mediump float perceptualRoughness_26;
  highp vec3 tmpvar_27;
  highp vec3 inVec_28;
  inVec_28 = (tmpvar_2 + normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2)));
  tmpvar_27 = (inVec_28 * inversesqrt(max (0.001, 
    dot (inVec_28, inVec_28)
  )));
  mediump float tmpvar_29;
  highp float tmpvar_30;
  tmpvar_30 = clamp (dot (tmpvar_15, tmpvar_2), 0.0, 1.0);
  tmpvar_29 = tmpvar_30;
  highp float tmpvar_31;
  tmpvar_31 = clamp (dot (tmpvar_15, tmpvar_27), 0.0, 1.0);
  highp float tmpvar_32;
  highp float smoothness_33;
  smoothness_33 = _Glossiness;
  tmpvar_32 = (1.0 - smoothness_33);
  perceptualRoughness_26 = tmpvar_32;
  highp float tmpvar_34;
  highp float perceptualRoughness_35;
  perceptualRoughness_35 = perceptualRoughness_26;
  tmpvar_34 = (perceptualRoughness_35 * perceptualRoughness_35);
  roughness_25 = tmpvar_34;
  mediump float tmpvar_36;
  tmpvar_36 = (roughness_25 * roughness_25);
  a2_24 = tmpvar_36;
  specularTerm_23 = ((roughness_25 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_27), 0.0, 1.0)) * (1.5 + roughness_25))
   * 
    (((tmpvar_31 * tmpvar_31) * (a2_24 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_37;
  tmpvar_37 = clamp (specularTerm_23, 0.0, 100.0);
  specularTerm_23 = tmpvar_37;
  highp vec3 tmpvar_38;
  tmpvar_38 = (((diffColor_21 + 
    (tmpvar_37 * tmpvar_18)
  ) * tmpvar_1) * tmpvar_29);
  color_22 = tmpvar_38;
  mediump vec4 tmpvar_39;
  tmpvar_39.w = 1.0;
  tmpvar_39.xyz = color_22;
  c_14.xyz = tmpvar_39.xyz;
  c_14.w = alpha_20;
  c_3.xyz = c_14.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 45 math, 2 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xyz;
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _LightTexture0;
uniform highp mat4 unity_WorldToLight;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  highp vec3 lightCoord_5;
  lowp vec3 tmpvar_6;
  lowp float tmpvar_7;
  lowp vec3 lightDir_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_8 = tmpvar_9;
  lowp vec4 tmpvar_10;
  tmpvar_10 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_6 = tmpvar_10.xyz;
  tmpvar_7 = tmpvar_10.w;
  highp vec4 tmpvar_11;
  tmpvar_11.w = 1.0;
  tmpvar_11.xyz = xlv_TEXCOORD2;
  lightCoord_5 = (unity_WorldToLight * tmpvar_11).xyz;
  highp float tmpvar_12;
  tmpvar_12 = texture2D (_LightTexture0, vec2(dot (lightCoord_5, lightCoord_5))).w;
  atten_4 = tmpvar_12;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_8;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_13;
  mediump vec4 c_14;
  highp vec3 tmpvar_15;
  tmpvar_15 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_16;
  mediump vec3 albedo_17;
  albedo_17 = tmpvar_6;
  mediump vec3 tmpvar_18;
  tmpvar_18 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_17, vec3(_Metallic));
  tmpvar_16 = (albedo_17 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_13 = tmpvar_16;
  mediump vec3 diffColor_19;
  diffColor_19 = tmpvar_13;
  mediump float alpha_20;
  alpha_20 = tmpvar_7;
  tmpvar_13 = diffColor_19;
  mediump vec3 diffColor_21;
  diffColor_21 = tmpvar_13;
  mediump vec3 color_22;
  highp float specularTerm_23;
  highp float a2_24;
  mediump float roughness_25;
  mediump float perceptualRoughness_26;
  highp vec3 tmpvar_27;
  highp vec3 inVec_28;
  inVec_28 = (tmpvar_2 + normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2)));
  tmpvar_27 = (inVec_28 * inversesqrt(max (0.001, 
    dot (inVec_28, inVec_28)
  )));
  mediump float tmpvar_29;
  highp float tmpvar_30;
  tmpvar_30 = clamp (dot (tmpvar_15, tmpvar_2), 0.0, 1.0);
  tmpvar_29 = tmpvar_30;
  highp float tmpvar_31;
  tmpvar_31 = clamp (dot (tmpvar_15, tmpvar_27), 0.0, 1.0);
  highp float tmpvar_32;
  highp float smoothness_33;
  smoothness_33 = _Glossiness;
  tmpvar_32 = (1.0 - smoothness_33);
  perceptualRoughness_26 = tmpvar_32;
  highp float tmpvar_34;
  highp float perceptualRoughness_35;
  perceptualRoughness_35 = perceptualRoughness_26;
  tmpvar_34 = (perceptualRoughness_35 * perceptualRoughness_35);
  roughness_25 = tmpvar_34;
  mediump float tmpvar_36;
  tmpvar_36 = (roughness_25 * roughness_25);
  a2_24 = tmpvar_36;
  specularTerm_23 = ((roughness_25 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_27), 0.0, 1.0)) * (1.5 + roughness_25))
   * 
    (((tmpvar_31 * tmpvar_31) * (a2_24 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_37;
  tmpvar_37 = clamp (specularTerm_23, 0.0, 100.0);
  specularTerm_23 = tmpvar_37;
  highp vec3 tmpvar_38;
  tmpvar_38 = (((diffColor_21 + 
    (tmpvar_37 * tmpvar_18)
  ) * tmpvar_1) * tmpvar_29);
  color_22 = tmpvar_38;
  mediump vec4 tmpvar_39;
  tmpvar_39.w = 1.0;
  tmpvar_39.xyz = color_22;
  c_14.xyz = tmpvar_39.xyz;
  c_14.w = alpha_20;
  c_3.xyz = c_14.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D unity_NHxRoughness;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
float u_xlat5;
float u_xlat15;
mediump float u_xlat16_18;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat15 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat1.xyz = vec3(u_xlat15) * vs_TEXCOORD1.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat1.xyz);
    u_xlat15 = u_xlat15 + u_xlat15;
    u_xlat0.xyz = u_xlat1.xyz * (-vec3(u_xlat15)) + u_xlat0.xyz;
    u_xlat2.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat15 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat2.xyz = vec3(u_xlat15) * u_xlat2.xyz;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat2.xyz);
    u_xlat5 = dot(u_xlat1.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5 = min(max(u_xlat5, 0.0), 1.0);
#else
    u_xlat5 = clamp(u_xlat5, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat1.x = u_xlat0.x * u_xlat0.x;
    u_xlat1.y = (-_Glossiness) + 1.0;
    u_xlat0.x = texture(unity_NHxRoughness, u_xlat1.xy).w;
    u_xlat0.x = u_xlat0.x * 16.0;
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_3.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_3.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_3.xyz = u_xlat0.xxx * u_xlat16_3.xyz;
    u_xlat16_18 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_3.xyz = u_xlat16_1.xyz * vec3(u_xlat16_18) + u_xlat16_3.xyz;
    u_xlat0.xzw = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat0.xzw = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD2.xxx + u_xlat0.xzw;
    u_xlat0.xzw = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD2.zzz + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat0.xzw + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat0.x = dot(u_xlat0.xzw, u_xlat0.xzw);
    u_xlat0.x = texture(_LightTexture0, u_xlat0.xx).w;
    u_xlat16_4.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat16_4.xyz = vec3(u_xlat5) * u_xlat16_4.xyz;
    SV_Target0.xyz = u_xlat16_3.xyz * u_xlat16_4.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
vec3 u_xlat2;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat4;
mediump float u_xlat16_8;
float u_xlat12;
float u_xlat13;
mediump float u_xlat16_13;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat13 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat13 = inversesqrt(u_xlat13);
    u_xlat1.xyz = vec3(u_xlat13) * u_xlat1.xyz;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat12) + u_xlat1.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = max(u_xlat12, 0.00100000005);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    u_xlat12 = dot(u_xlat1.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat12 = min(max(u_xlat12, 0.0), 1.0);
#else
    u_xlat12 = clamp(u_xlat12, 0.0, 1.0);
#endif
    u_xlat12 = max(u_xlat12, 0.319999993);
    u_xlat16_13 = (-_Glossiness) + 1.0;
    u_xlat16_2 = u_xlat16_13 * u_xlat16_13 + 1.5;
    u_xlat16_13 = u_xlat16_13 * u_xlat16_13;
    u_xlat12 = u_xlat12 * u_xlat16_2;
    u_xlat2.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat2.x = inversesqrt(u_xlat2.x);
    u_xlat2.xyz = u_xlat2.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat2.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat4 = dot(u_xlat2.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat4 = min(max(u_xlat4, 0.0), 1.0);
#else
    u_xlat4 = clamp(u_xlat4, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat16_8 = u_xlat16_13 * u_xlat16_13 + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_8 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat12;
    u_xlat0.x = u_xlat16_13 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_3.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_3.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_3.xyz;
    u_xlat16_3.x = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * u_xlat16_3.xxx + u_xlat0.xzw;
    u_xlat1.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD2.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD2.zzz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat1.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat1.x = texture(_LightTexture0, u_xlat1.xx).w;
    u_xlat16_3.xyz = u_xlat1.xxx * _LightColor0.xyz;
    u_xlat0.xzw = u_xlat0.xzw * u_xlat16_3.xyz;
    u_xlat0.xyz = vec3(u_xlat4) * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
vec3 u_xlat2;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat4;
mediump float u_xlat16_8;
float u_xlat12;
float u_xlat13;
mediump float u_xlat16_13;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat13 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat13 = inversesqrt(u_xlat13);
    u_xlat1.xyz = vec3(u_xlat13) * u_xlat1.xyz;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat12) + u_xlat1.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = max(u_xlat12, 0.00100000005);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    u_xlat12 = dot(u_xlat1.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat12 = min(max(u_xlat12, 0.0), 1.0);
#else
    u_xlat12 = clamp(u_xlat12, 0.0, 1.0);
#endif
    u_xlat12 = max(u_xlat12, 0.319999993);
    u_xlat16_13 = (-_Glossiness) + 1.0;
    u_xlat16_2 = u_xlat16_13 * u_xlat16_13 + 1.5;
    u_xlat16_13 = u_xlat16_13 * u_xlat16_13;
    u_xlat12 = u_xlat12 * u_xlat16_2;
    u_xlat2.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat2.x = inversesqrt(u_xlat2.x);
    u_xlat2.xyz = u_xlat2.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat2.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat4 = dot(u_xlat2.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat4 = min(max(u_xlat4, 0.0), 1.0);
#else
    u_xlat4 = clamp(u_xlat4, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat16_8 = u_xlat16_13 * u_xlat16_13 + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_8 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat12;
    u_xlat0.x = u_xlat16_13 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_3.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_3.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_3.xyz;
    u_xlat16_3.x = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * u_xlat16_3.xxx + u_xlat0.xzw;
    u_xlat1.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD2.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD2.zzz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat1.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat1.x = texture(_LightTexture0, u_xlat1.xx).w;
    u_xlat16_3.xyz = u_xlat1.xxx * _LightColor0.xyz;
    u_xlat0.xzw = u_xlat0.xzw * u_xlat16_3.xyz;
    u_xlat0.xyz = vec3(u_xlat4) * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 29 math, 2 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  xlv_TEXCOORD2 = (unity_ObjectToWorld * _glesVertex).xyz;
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp vec3 tmpvar_4;
  lowp float tmpvar_5;
  highp vec3 worldViewDir_6;
  lowp vec3 lightDir_7;
  mediump vec3 tmpvar_8;
  tmpvar_8 = _WorldSpaceLightPos0.xyz;
  lightDir_7 = tmpvar_8;
  worldViewDir_6 = normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2));
  lowp vec4 tmpvar_9;
  tmpvar_9 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_4 = tmpvar_9.xyz;
  tmpvar_5 = tmpvar_9.w;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_7;
  lowp vec3 tmpvar_10;
  mediump vec4 c_11;
  highp vec3 tmpvar_12;
  tmpvar_12 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_13;
  mediump vec3 albedo_14;
  albedo_14 = tmpvar_4;
  tmpvar_13 = (albedo_14 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_10 = tmpvar_13;
  mediump vec3 diffColor_15;
  diffColor_15 = tmpvar_10;
  mediump float alpha_16;
  alpha_16 = tmpvar_5;
  tmpvar_10 = diffColor_15;
  mediump vec3 diffColor_17;
  diffColor_17 = tmpvar_10;
  mediump vec2 rlPow4AndFresnelTerm_18;
  mediump float tmpvar_19;
  highp float tmpvar_20;
  tmpvar_20 = clamp (dot (tmpvar_12, tmpvar_2), 0.0, 1.0);
  tmpvar_19 = tmpvar_20;
  mediump float tmpvar_21;
  highp float tmpvar_22;
  tmpvar_22 = clamp (dot (tmpvar_12, worldViewDir_6), 0.0, 1.0);
  tmpvar_21 = tmpvar_22;
  highp vec2 tmpvar_23;
  tmpvar_23.x = dot ((worldViewDir_6 - (2.0 * 
    (dot (tmpvar_12, worldViewDir_6) * tmpvar_12)
  )), tmpvar_2);
  tmpvar_23.y = (1.0 - tmpvar_21);
  highp vec2 tmpvar_24;
  tmpvar_24 = ((tmpvar_23 * tmpvar_23) * (tmpvar_23 * tmpvar_23));
  rlPow4AndFresnelTerm_18 = tmpvar_24;
  mediump float tmpvar_25;
  tmpvar_25 = rlPow4AndFresnelTerm_18.x;
  mediump float specular_26;
  highp float smoothness_27;
  smoothness_27 = _Glossiness;
  highp vec2 tmpvar_28;
  tmpvar_28.x = tmpvar_25;
  tmpvar_28.y = (1.0 - smoothness_27);
  highp float tmpvar_29;
  tmpvar_29 = (texture2D (unity_NHxRoughness, tmpvar_28).w * 16.0);
  specular_26 = tmpvar_29;
  mediump vec4 tmpvar_30;
  tmpvar_30.w = 1.0;
  tmpvar_30.xyz = ((diffColor_17 + (specular_26 * 
    mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_14, vec3(_Metallic))
  )) * (tmpvar_1 * tmpvar_19));
  c_11.xyz = tmpvar_30.xyz;
  c_11.w = alpha_16;
  c_3.xyz = c_11.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 39 math, 1 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  xlv_TEXCOORD2 = (unity_ObjectToWorld * _glesVertex).xyz;
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform lowp vec4 _LightColor0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp vec3 tmpvar_4;
  lowp float tmpvar_5;
  lowp vec3 lightDir_6;
  mediump vec3 tmpvar_7;
  tmpvar_7 = _WorldSpaceLightPos0.xyz;
  lightDir_6 = tmpvar_7;
  lowp vec4 tmpvar_8;
  tmpvar_8 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_4 = tmpvar_8.xyz;
  tmpvar_5 = tmpvar_8.w;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  lowp vec3 tmpvar_9;
  mediump vec4 c_10;
  highp vec3 tmpvar_11;
  tmpvar_11 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_12;
  mediump vec3 albedo_13;
  albedo_13 = tmpvar_4;
  mediump vec3 tmpvar_14;
  tmpvar_14 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_13, vec3(_Metallic));
  tmpvar_12 = (albedo_13 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_9 = tmpvar_12;
  mediump vec3 diffColor_15;
  diffColor_15 = tmpvar_9;
  mediump float alpha_16;
  alpha_16 = tmpvar_5;
  tmpvar_9 = diffColor_15;
  mediump vec3 diffColor_17;
  diffColor_17 = tmpvar_9;
  mediump vec3 color_18;
  highp float specularTerm_19;
  highp float a2_20;
  mediump float roughness_21;
  mediump float perceptualRoughness_22;
  highp vec3 tmpvar_23;
  highp vec3 inVec_24;
  inVec_24 = (tmpvar_2 + normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2)));
  tmpvar_23 = (inVec_24 * inversesqrt(max (0.001, 
    dot (inVec_24, inVec_24)
  )));
  mediump float tmpvar_25;
  highp float tmpvar_26;
  tmpvar_26 = clamp (dot (tmpvar_11, tmpvar_2), 0.0, 1.0);
  tmpvar_25 = tmpvar_26;
  highp float tmpvar_27;
  tmpvar_27 = clamp (dot (tmpvar_11, tmpvar_23), 0.0, 1.0);
  highp float tmpvar_28;
  highp float smoothness_29;
  smoothness_29 = _Glossiness;
  tmpvar_28 = (1.0 - smoothness_29);
  perceptualRoughness_22 = tmpvar_28;
  highp float tmpvar_30;
  highp float perceptualRoughness_31;
  perceptualRoughness_31 = perceptualRoughness_22;
  tmpvar_30 = (perceptualRoughness_31 * perceptualRoughness_31);
  roughness_21 = tmpvar_30;
  mediump float tmpvar_32;
  tmpvar_32 = (roughness_21 * roughness_21);
  a2_20 = tmpvar_32;
  specularTerm_19 = ((roughness_21 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_23), 0.0, 1.0)) * (1.5 + roughness_21))
   * 
    (((tmpvar_27 * tmpvar_27) * (a2_20 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_33;
  tmpvar_33 = clamp (specularTerm_19, 0.0, 100.0);
  specularTerm_19 = tmpvar_33;
  highp vec3 tmpvar_34;
  tmpvar_34 = (((diffColor_17 + 
    (tmpvar_33 * tmpvar_14)
  ) * tmpvar_1) * tmpvar_25);
  color_18 = tmpvar_34;
  mediump vec4 tmpvar_35;
  tmpvar_35.w = 1.0;
  tmpvar_35.xyz = color_18;
  c_10.xyz = tmpvar_35.xyz;
  c_10.w = alpha_16;
  c_3.xyz = c_10.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 39 math, 1 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  xlv_TEXCOORD2 = (unity_ObjectToWorld * _glesVertex).xyz;
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform lowp vec4 _LightColor0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp vec3 tmpvar_4;
  lowp float tmpvar_5;
  lowp vec3 lightDir_6;
  mediump vec3 tmpvar_7;
  tmpvar_7 = _WorldSpaceLightPos0.xyz;
  lightDir_6 = tmpvar_7;
  lowp vec4 tmpvar_8;
  tmpvar_8 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_4 = tmpvar_8.xyz;
  tmpvar_5 = tmpvar_8.w;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  lowp vec3 tmpvar_9;
  mediump vec4 c_10;
  highp vec3 tmpvar_11;
  tmpvar_11 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_12;
  mediump vec3 albedo_13;
  albedo_13 = tmpvar_4;
  mediump vec3 tmpvar_14;
  tmpvar_14 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_13, vec3(_Metallic));
  tmpvar_12 = (albedo_13 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_9 = tmpvar_12;
  mediump vec3 diffColor_15;
  diffColor_15 = tmpvar_9;
  mediump float alpha_16;
  alpha_16 = tmpvar_5;
  tmpvar_9 = diffColor_15;
  mediump vec3 diffColor_17;
  diffColor_17 = tmpvar_9;
  mediump vec3 color_18;
  highp float specularTerm_19;
  highp float a2_20;
  mediump float roughness_21;
  mediump float perceptualRoughness_22;
  highp vec3 tmpvar_23;
  highp vec3 inVec_24;
  inVec_24 = (tmpvar_2 + normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2)));
  tmpvar_23 = (inVec_24 * inversesqrt(max (0.001, 
    dot (inVec_24, inVec_24)
  )));
  mediump float tmpvar_25;
  highp float tmpvar_26;
  tmpvar_26 = clamp (dot (tmpvar_11, tmpvar_2), 0.0, 1.0);
  tmpvar_25 = tmpvar_26;
  highp float tmpvar_27;
  tmpvar_27 = clamp (dot (tmpvar_11, tmpvar_23), 0.0, 1.0);
  highp float tmpvar_28;
  highp float smoothness_29;
  smoothness_29 = _Glossiness;
  tmpvar_28 = (1.0 - smoothness_29);
  perceptualRoughness_22 = tmpvar_28;
  highp float tmpvar_30;
  highp float perceptualRoughness_31;
  perceptualRoughness_31 = perceptualRoughness_22;
  tmpvar_30 = (perceptualRoughness_31 * perceptualRoughness_31);
  roughness_21 = tmpvar_30;
  mediump float tmpvar_32;
  tmpvar_32 = (roughness_21 * roughness_21);
  a2_20 = tmpvar_32;
  specularTerm_19 = ((roughness_21 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_23), 0.0, 1.0)) * (1.5 + roughness_21))
   * 
    (((tmpvar_27 * tmpvar_27) * (a2_20 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_33;
  tmpvar_33 = clamp (specularTerm_19, 0.0, 100.0);
  specularTerm_19 = tmpvar_33;
  highp vec3 tmpvar_34;
  tmpvar_34 = (((diffColor_17 + 
    (tmpvar_33 * tmpvar_14)
  ) * tmpvar_1) * tmpvar_25);
  color_18 = tmpvar_34;
  mediump vec4 tmpvar_35;
  tmpvar_35.w = 1.0;
  tmpvar_35.xyz = color_18;
  c_10.xyz = tmpvar_35.xyz;
  c_10.w = alpha_16;
  c_3.xyz = c_10.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D unity_NHxRoughness;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec3 u_xlat1;
mediump vec3 u_xlat16_2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
lowp vec3 u_xlat10_4;
float u_xlat12;
mediump float u_xlat16_14;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    u_xlat12 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = vec3(u_xlat12) * vs_TEXCOORD1.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat1.xyz);
    u_xlat12 = u_xlat12 + u_xlat12;
    u_xlat0.xyz = u_xlat1.xyz * (-vec3(u_xlat12)) + u_xlat0.xyz;
    u_xlat12 = dot(u_xlat1.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat12 = min(max(u_xlat12, 0.0), 1.0);
#else
    u_xlat12 = clamp(u_xlat12, 0.0, 1.0);
#endif
    u_xlat16_2.xyz = vec3(u_xlat12) * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat0.xyz, _WorldSpaceLightPos0.xyz);
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.y = (-_Glossiness) + 1.0;
    u_xlat0.x = texture(unity_NHxRoughness, u_xlat0.xy).w;
    u_xlat0.x = u_xlat0.x * 16.0;
    u_xlat10_4.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_4.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_4.xyz = u_xlat10_4.xyz * _Color.xyz;
    u_xlat16_3.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_3.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_3.xyz = u_xlat0.xxx * u_xlat16_3.xyz;
    u_xlat16_14 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_3.xyz = u_xlat16_4.xyz * vec3(u_xlat16_14) + u_xlat16_3.xyz;
    SV_Target0.xyz = u_xlat16_2.xyz * u_xlat16_3.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
float u_xlat3;
vec3 u_xlat4;
mediump float u_xlat16_4;
mediump float u_xlat16_6;
float u_xlat9;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat9 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat9 = inversesqrt(u_xlat9);
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat9) + _WorldSpaceLightPos0.xyz;
    u_xlat9 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat9 = max(u_xlat9, 0.00100000005);
    u_xlat9 = inversesqrt(u_xlat9);
    u_xlat0.xyz = vec3(u_xlat9) * u_xlat0.xyz;
    u_xlat9 = dot(_WorldSpaceLightPos0.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat9 = min(max(u_xlat9, 0.0), 1.0);
#else
    u_xlat9 = clamp(u_xlat9, 0.0, 1.0);
#endif
    u_xlat9 = max(u_xlat9, 0.319999993);
    u_xlat16_1.x = (-_Glossiness) + 1.0;
    u_xlat16_4 = u_xlat16_1.x * u_xlat16_1.x + 1.5;
    u_xlat16_1.x = u_xlat16_1.x * u_xlat16_1.x;
    u_xlat9 = u_xlat9 * u_xlat16_4;
    u_xlat4.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat4.x = inversesqrt(u_xlat4.x);
    u_xlat4.xyz = u_xlat4.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat4.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat3 = dot(u_xlat4.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat3 = min(max(u_xlat3, 0.0), 1.0);
#else
    u_xlat3 = clamp(u_xlat3, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat16_6 = u_xlat16_1.x * u_xlat16_1.x + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_6 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat9;
    u_xlat0.x = u_xlat16_1.x / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_2.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_2.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_2.xyz;
    u_xlat16_2.x = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * u_xlat16_2.xxx + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat0.xzw * _LightColor0.xyz;
    u_xlat0.xyz = vec3(u_xlat3) * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
float u_xlat3;
vec3 u_xlat4;
mediump float u_xlat16_4;
mediump float u_xlat16_6;
float u_xlat9;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat9 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat9 = inversesqrt(u_xlat9);
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat9) + _WorldSpaceLightPos0.xyz;
    u_xlat9 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat9 = max(u_xlat9, 0.00100000005);
    u_xlat9 = inversesqrt(u_xlat9);
    u_xlat0.xyz = vec3(u_xlat9) * u_xlat0.xyz;
    u_xlat9 = dot(_WorldSpaceLightPos0.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat9 = min(max(u_xlat9, 0.0), 1.0);
#else
    u_xlat9 = clamp(u_xlat9, 0.0, 1.0);
#endif
    u_xlat9 = max(u_xlat9, 0.319999993);
    u_xlat16_1.x = (-_Glossiness) + 1.0;
    u_xlat16_4 = u_xlat16_1.x * u_xlat16_1.x + 1.5;
    u_xlat16_1.x = u_xlat16_1.x * u_xlat16_1.x;
    u_xlat9 = u_xlat9 * u_xlat16_4;
    u_xlat4.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat4.x = inversesqrt(u_xlat4.x);
    u_xlat4.xyz = u_xlat4.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat4.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat3 = dot(u_xlat4.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat3 = min(max(u_xlat3, 0.0), 1.0);
#else
    u_xlat3 = clamp(u_xlat3, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat16_6 = u_xlat16_1.x * u_xlat16_1.x + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_6 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat9;
    u_xlat0.x = u_xlat16_1.x / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_2.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_2.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_2.xyz;
    u_xlat16_2.x = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * u_xlat16_2.xxx + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat0.xzw * _LightColor0.xyz;
    u_xlat0.xyz = vec3(u_xlat3) * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 39 math, 4 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp vec3 tmpvar_5;
  lowp float tmpvar_6;
  highp vec3 worldViewDir_7;
  lowp vec3 lightDir_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_8 = tmpvar_9;
  worldViewDir_7 = normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2));
  lowp vec4 tmpvar_10;
  tmpvar_10 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_5 = tmpvar_10.xyz;
  tmpvar_6 = tmpvar_10.w;
  lowp float tmpvar_11;
  highp vec4 tmpvar_12;
  tmpvar_12 = texture2D (_LightTexture0, ((xlv_TEXCOORD3.xy / xlv_TEXCOORD3.w) + 0.5));
  tmpvar_11 = tmpvar_12.w;
  lowp float tmpvar_13;
  highp vec4 tmpvar_14;
  tmpvar_14 = texture2D (_LightTextureB0, vec2(dot (xlv_TEXCOORD3.xyz, xlv_TEXCOORD3.xyz)));
  tmpvar_13 = tmpvar_14.w;
  highp float tmpvar_15;
  tmpvar_15 = ((float(
    (xlv_TEXCOORD3.z > 0.0)
  ) * tmpvar_11) * tmpvar_13);
  atten_4 = tmpvar_15;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_8;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_16;
  mediump vec4 c_17;
  highp vec3 tmpvar_18;
  tmpvar_18 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_19;
  mediump vec3 albedo_20;
  albedo_20 = tmpvar_5;
  tmpvar_19 = (albedo_20 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_16 = tmpvar_19;
  mediump vec3 diffColor_21;
  diffColor_21 = tmpvar_16;
  mediump float alpha_22;
  alpha_22 = tmpvar_6;
  tmpvar_16 = diffColor_21;
  mediump vec3 diffColor_23;
  diffColor_23 = tmpvar_16;
  mediump vec2 rlPow4AndFresnelTerm_24;
  mediump float tmpvar_25;
  highp float tmpvar_26;
  tmpvar_26 = clamp (dot (tmpvar_18, tmpvar_2), 0.0, 1.0);
  tmpvar_25 = tmpvar_26;
  mediump float tmpvar_27;
  highp float tmpvar_28;
  tmpvar_28 = clamp (dot (tmpvar_18, worldViewDir_7), 0.0, 1.0);
  tmpvar_27 = tmpvar_28;
  highp vec2 tmpvar_29;
  tmpvar_29.x = dot ((worldViewDir_7 - (2.0 * 
    (dot (tmpvar_18, worldViewDir_7) * tmpvar_18)
  )), tmpvar_2);
  tmpvar_29.y = (1.0 - tmpvar_27);
  highp vec2 tmpvar_30;
  tmpvar_30 = ((tmpvar_29 * tmpvar_29) * (tmpvar_29 * tmpvar_29));
  rlPow4AndFresnelTerm_24 = tmpvar_30;
  mediump float tmpvar_31;
  tmpvar_31 = rlPow4AndFresnelTerm_24.x;
  mediump float specular_32;
  highp float smoothness_33;
  smoothness_33 = _Glossiness;
  highp vec2 tmpvar_34;
  tmpvar_34.x = tmpvar_31;
  tmpvar_34.y = (1.0 - smoothness_33);
  highp float tmpvar_35;
  tmpvar_35 = (texture2D (unity_NHxRoughness, tmpvar_34).w * 16.0);
  specular_32 = tmpvar_35;
  mediump vec4 tmpvar_36;
  tmpvar_36.w = 1.0;
  tmpvar_36.xyz = ((diffColor_23 + (specular_32 * 
    mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_20, vec3(_Metallic))
  )) * (tmpvar_1 * tmpvar_25));
  c_17.xyz = tmpvar_36.xyz;
  c_17.w = alpha_22;
  c_3.xyz = c_17.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 49 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp vec3 tmpvar_5;
  lowp float tmpvar_6;
  lowp vec3 lightDir_7;
  highp vec3 tmpvar_8;
  tmpvar_8 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_7 = tmpvar_8;
  lowp vec4 tmpvar_9;
  tmpvar_9 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_5 = tmpvar_9.xyz;
  tmpvar_6 = tmpvar_9.w;
  lowp float tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11 = texture2D (_LightTexture0, ((xlv_TEXCOORD3.xy / xlv_TEXCOORD3.w) + 0.5));
  tmpvar_10 = tmpvar_11.w;
  lowp float tmpvar_12;
  highp vec4 tmpvar_13;
  tmpvar_13 = texture2D (_LightTextureB0, vec2(dot (xlv_TEXCOORD3.xyz, xlv_TEXCOORD3.xyz)));
  tmpvar_12 = tmpvar_13.w;
  highp float tmpvar_14;
  tmpvar_14 = ((float(
    (xlv_TEXCOORD3.z > 0.0)
  ) * tmpvar_10) * tmpvar_12);
  atten_4 = tmpvar_14;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_7;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_15;
  mediump vec4 c_16;
  highp vec3 tmpvar_17;
  tmpvar_17 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_18;
  mediump vec3 albedo_19;
  albedo_19 = tmpvar_5;
  mediump vec3 tmpvar_20;
  tmpvar_20 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_19, vec3(_Metallic));
  tmpvar_18 = (albedo_19 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_15 = tmpvar_18;
  mediump vec3 diffColor_21;
  diffColor_21 = tmpvar_15;
  mediump float alpha_22;
  alpha_22 = tmpvar_6;
  tmpvar_15 = diffColor_21;
  mediump vec3 diffColor_23;
  diffColor_23 = tmpvar_15;
  mediump vec3 color_24;
  highp float specularTerm_25;
  highp float a2_26;
  mediump float roughness_27;
  mediump float perceptualRoughness_28;
  highp vec3 tmpvar_29;
  highp vec3 inVec_30;
  inVec_30 = (tmpvar_2 + normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2)));
  tmpvar_29 = (inVec_30 * inversesqrt(max (0.001, 
    dot (inVec_30, inVec_30)
  )));
  mediump float tmpvar_31;
  highp float tmpvar_32;
  tmpvar_32 = clamp (dot (tmpvar_17, tmpvar_2), 0.0, 1.0);
  tmpvar_31 = tmpvar_32;
  highp float tmpvar_33;
  tmpvar_33 = clamp (dot (tmpvar_17, tmpvar_29), 0.0, 1.0);
  highp float tmpvar_34;
  highp float smoothness_35;
  smoothness_35 = _Glossiness;
  tmpvar_34 = (1.0 - smoothness_35);
  perceptualRoughness_28 = tmpvar_34;
  highp float tmpvar_36;
  highp float perceptualRoughness_37;
  perceptualRoughness_37 = perceptualRoughness_28;
  tmpvar_36 = (perceptualRoughness_37 * perceptualRoughness_37);
  roughness_27 = tmpvar_36;
  mediump float tmpvar_38;
  tmpvar_38 = (roughness_27 * roughness_27);
  a2_26 = tmpvar_38;
  specularTerm_25 = ((roughness_27 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_29), 0.0, 1.0)) * (1.5 + roughness_27))
   * 
    (((tmpvar_33 * tmpvar_33) * (a2_26 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_39;
  tmpvar_39 = clamp (specularTerm_25, 0.0, 100.0);
  specularTerm_25 = tmpvar_39;
  highp vec3 tmpvar_40;
  tmpvar_40 = (((diffColor_23 + 
    (tmpvar_39 * tmpvar_20)
  ) * tmpvar_1) * tmpvar_31);
  color_24 = tmpvar_40;
  mediump vec4 tmpvar_41;
  tmpvar_41.w = 1.0;
  tmpvar_41.xyz = color_24;
  c_16.xyz = tmpvar_41.xyz;
  c_16.w = alpha_22;
  c_3.xyz = c_16.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 49 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp vec3 tmpvar_5;
  lowp float tmpvar_6;
  lowp vec3 lightDir_7;
  highp vec3 tmpvar_8;
  tmpvar_8 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_7 = tmpvar_8;
  lowp vec4 tmpvar_9;
  tmpvar_9 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_5 = tmpvar_9.xyz;
  tmpvar_6 = tmpvar_9.w;
  lowp float tmpvar_10;
  highp vec4 tmpvar_11;
  tmpvar_11 = texture2D (_LightTexture0, ((xlv_TEXCOORD3.xy / xlv_TEXCOORD3.w) + 0.5));
  tmpvar_10 = tmpvar_11.w;
  lowp float tmpvar_12;
  highp vec4 tmpvar_13;
  tmpvar_13 = texture2D (_LightTextureB0, vec2(dot (xlv_TEXCOORD3.xyz, xlv_TEXCOORD3.xyz)));
  tmpvar_12 = tmpvar_13.w;
  highp float tmpvar_14;
  tmpvar_14 = ((float(
    (xlv_TEXCOORD3.z > 0.0)
  ) * tmpvar_10) * tmpvar_12);
  atten_4 = tmpvar_14;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_7;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_15;
  mediump vec4 c_16;
  highp vec3 tmpvar_17;
  tmpvar_17 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_18;
  mediump vec3 albedo_19;
  albedo_19 = tmpvar_5;
  mediump vec3 tmpvar_20;
  tmpvar_20 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_19, vec3(_Metallic));
  tmpvar_18 = (albedo_19 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_15 = tmpvar_18;
  mediump vec3 diffColor_21;
  diffColor_21 = tmpvar_15;
  mediump float alpha_22;
  alpha_22 = tmpvar_6;
  tmpvar_15 = diffColor_21;
  mediump vec3 diffColor_23;
  diffColor_23 = tmpvar_15;
  mediump vec3 color_24;
  highp float specularTerm_25;
  highp float a2_26;
  mediump float roughness_27;
  mediump float perceptualRoughness_28;
  highp vec3 tmpvar_29;
  highp vec3 inVec_30;
  inVec_30 = (tmpvar_2 + normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2)));
  tmpvar_29 = (inVec_30 * inversesqrt(max (0.001, 
    dot (inVec_30, inVec_30)
  )));
  mediump float tmpvar_31;
  highp float tmpvar_32;
  tmpvar_32 = clamp (dot (tmpvar_17, tmpvar_2), 0.0, 1.0);
  tmpvar_31 = tmpvar_32;
  highp float tmpvar_33;
  tmpvar_33 = clamp (dot (tmpvar_17, tmpvar_29), 0.0, 1.0);
  highp float tmpvar_34;
  highp float smoothness_35;
  smoothness_35 = _Glossiness;
  tmpvar_34 = (1.0 - smoothness_35);
  perceptualRoughness_28 = tmpvar_34;
  highp float tmpvar_36;
  highp float perceptualRoughness_37;
  perceptualRoughness_37 = perceptualRoughness_28;
  tmpvar_36 = (perceptualRoughness_37 * perceptualRoughness_37);
  roughness_27 = tmpvar_36;
  mediump float tmpvar_38;
  tmpvar_38 = (roughness_27 * roughness_27);
  a2_26 = tmpvar_38;
  specularTerm_25 = ((roughness_27 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_29), 0.0, 1.0)) * (1.5 + roughness_27))
   * 
    (((tmpvar_33 * tmpvar_33) * (a2_26 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_39;
  tmpvar_39 = clamp (specularTerm_25, 0.0, 100.0);
  specularTerm_25 = tmpvar_39;
  highp vec3 tmpvar_40;
  tmpvar_40 = (((diffColor_23 + 
    (tmpvar_39 * tmpvar_20)
  ) * tmpvar_1) * tmpvar_31);
  color_24 = tmpvar_40;
  mediump vec4 tmpvar_41;
  tmpvar_41.w = 1.0;
  tmpvar_41.xyz = color_24;
  c_16.xyz = tmpvar_41.xyz;
  c_16.w = alpha_22;
  c_3.xyz = c_16.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform highp sampler2D unity_NHxRoughness;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec4 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
float u_xlat5;
bool u_xlatb10;
float u_xlat15;
mediump float u_xlat16_18;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat15 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat1.xyz = vec3(u_xlat15) * vs_TEXCOORD1.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat1.xyz);
    u_xlat15 = u_xlat15 + u_xlat15;
    u_xlat0.xyz = u_xlat1.xyz * (-vec3(u_xlat15)) + u_xlat0.xyz;
    u_xlat2.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat15 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat2.xyz = vec3(u_xlat15) * u_xlat2.xyz;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat2.xyz);
    u_xlat5 = dot(u_xlat1.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5 = min(max(u_xlat5, 0.0), 1.0);
#else
    u_xlat5 = clamp(u_xlat5, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat1.x = u_xlat0.x * u_xlat0.x;
    u_xlat1.y = (-_Glossiness) + 1.0;
    u_xlat0.x = texture(unity_NHxRoughness, u_xlat1.xy).w;
    u_xlat0.x = u_xlat0.x * 16.0;
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_3.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_3.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_3.xyz = u_xlat0.xxx * u_xlat16_3.xyz;
    u_xlat16_18 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_3.xyz = u_xlat16_1.xyz * vec3(u_xlat16_18) + u_xlat16_3.xyz;
    u_xlat1 = vs_TEXCOORD2.yyyy * hlslcc_mtx4x4unity_WorldToLight[1];
    u_xlat1 = hlslcc_mtx4x4unity_WorldToLight[0] * vs_TEXCOORD2.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_WorldToLight[2] * vs_TEXCOORD2.zzzz + u_xlat1;
    u_xlat1 = u_xlat1 + hlslcc_mtx4x4unity_WorldToLight[3];
    u_xlat0.xz = u_xlat1.xy / u_xlat1.ww;
    u_xlat0.xz = u_xlat0.xz + vec2(0.5, 0.5);
    u_xlat0.x = texture(_LightTexture0, u_xlat0.xz).w;
#ifdef UNITY_ADRENO_ES3
    u_xlatb10 = !!(0.0<u_xlat1.z);
#else
    u_xlatb10 = 0.0<u_xlat1.z;
#endif
    u_xlat15 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat15 = texture(_LightTextureB0, vec2(u_xlat15)).w;
    u_xlat16_18 = (u_xlatb10) ? 1.0 : 0.0;
    u_xlat16_18 = u_xlat0.x * u_xlat16_18;
    u_xlat16_18 = u_xlat15 * u_xlat16_18;
    u_xlat16_4.xyz = vec3(u_xlat16_18) * _LightColor0.xyz;
    u_xlat16_4.xyz = vec3(u_xlat5) * u_xlat16_4.xyz;
    SV_Target0.xyz = u_xlat16_3.xyz * u_xlat16_4.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D _LightTextureB0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
bool u_xlatb1;
mediump vec3 u_xlat16_2;
vec3 u_xlat3;
mediump float u_xlat16_3;
mediump vec3 u_xlat16_4;
float u_xlat5;
mediump float u_xlat16_10;
float u_xlat15;
float u_xlat16;
mediump float u_xlat16_16;
mediump float u_xlat16_17;
void main()
{
    u_xlat0 = vs_TEXCOORD2.yyyy * hlslcc_mtx4x4unity_WorldToLight[1];
    u_xlat0 = hlslcc_mtx4x4unity_WorldToLight[0] * vs_TEXCOORD2.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_WorldToLight[2] * vs_TEXCOORD2.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_WorldToLight[3];
    u_xlat1.xy = u_xlat0.xy / u_xlat0.ww;
    u_xlat1.xy = u_xlat1.xy + vec2(0.5, 0.5);
    u_xlat15 = texture(_LightTexture0, u_xlat1.xy).w;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0<u_xlat0.z);
#else
    u_xlatb1 = 0.0<u_xlat0.z;
#endif
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = texture(_LightTextureB0, u_xlat0.xx).w;
    u_xlat16_2.x = (u_xlatb1) ? 1.0 : 0.0;
    u_xlat16_2.x = u_xlat15 * u_xlat16_2.x;
    u_xlat16_2.x = u_xlat0.x * u_xlat16_2.x;
    u_xlat16_2.xyz = u_xlat16_2.xxx * _LightColor0.xyz;
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat1.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat16 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat16 = inversesqrt(u_xlat16);
    u_xlat1.xyz = vec3(u_xlat16) * u_xlat1.xyz;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat15) + u_xlat1.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = max(u_xlat15, 0.00100000005);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat15 = dot(u_xlat1.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat15 = min(max(u_xlat15, 0.0), 1.0);
#else
    u_xlat15 = clamp(u_xlat15, 0.0, 1.0);
#endif
    u_xlat15 = max(u_xlat15, 0.319999993);
    u_xlat16_16 = (-_Glossiness) + 1.0;
    u_xlat16_3 = u_xlat16_16 * u_xlat16_16 + 1.5;
    u_xlat16_16 = u_xlat16_16 * u_xlat16_16;
    u_xlat15 = u_xlat15 * u_xlat16_3;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat3.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat5 = dot(u_xlat3.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5 = min(max(u_xlat5, 0.0), 1.0);
#else
    u_xlat5 = clamp(u_xlat5, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat16_10 = u_xlat16_16 * u_xlat16_16 + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_10 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat15;
    u_xlat0.x = u_xlat16_16 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_17 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * vec3(u_xlat16_17) + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat16_2.xyz * u_xlat0.xzw;
    u_xlat0.xyz = vec3(u_xlat5) * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D _LightTextureB0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
bool u_xlatb1;
mediump vec3 u_xlat16_2;
vec3 u_xlat3;
mediump float u_xlat16_3;
mediump vec3 u_xlat16_4;
float u_xlat5;
mediump float u_xlat16_10;
float u_xlat15;
float u_xlat16;
mediump float u_xlat16_16;
mediump float u_xlat16_17;
void main()
{
    u_xlat0 = vs_TEXCOORD2.yyyy * hlslcc_mtx4x4unity_WorldToLight[1];
    u_xlat0 = hlslcc_mtx4x4unity_WorldToLight[0] * vs_TEXCOORD2.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_WorldToLight[2] * vs_TEXCOORD2.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_WorldToLight[3];
    u_xlat1.xy = u_xlat0.xy / u_xlat0.ww;
    u_xlat1.xy = u_xlat1.xy + vec2(0.5, 0.5);
    u_xlat15 = texture(_LightTexture0, u_xlat1.xy).w;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0<u_xlat0.z);
#else
    u_xlatb1 = 0.0<u_xlat0.z;
#endif
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = texture(_LightTextureB0, u_xlat0.xx).w;
    u_xlat16_2.x = (u_xlatb1) ? 1.0 : 0.0;
    u_xlat16_2.x = u_xlat15 * u_xlat16_2.x;
    u_xlat16_2.x = u_xlat0.x * u_xlat16_2.x;
    u_xlat16_2.xyz = u_xlat16_2.xxx * _LightColor0.xyz;
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat1.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat16 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat16 = inversesqrt(u_xlat16);
    u_xlat1.xyz = vec3(u_xlat16) * u_xlat1.xyz;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat15) + u_xlat1.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = max(u_xlat15, 0.00100000005);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat15 = dot(u_xlat1.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat15 = min(max(u_xlat15, 0.0), 1.0);
#else
    u_xlat15 = clamp(u_xlat15, 0.0, 1.0);
#endif
    u_xlat15 = max(u_xlat15, 0.319999993);
    u_xlat16_16 = (-_Glossiness) + 1.0;
    u_xlat16_3 = u_xlat16_16 * u_xlat16_16 + 1.5;
    u_xlat16_16 = u_xlat16_16 * u_xlat16_16;
    u_xlat15 = u_xlat15 * u_xlat16_3;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat3.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat5 = dot(u_xlat3.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5 = min(max(u_xlat5, 0.0), 1.0);
#else
    u_xlat5 = clamp(u_xlat5, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat16_10 = u_xlat16_16 * u_xlat16_16 + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_10 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat15;
    u_xlat0.x = u_xlat16_16 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_17 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * vec3(u_xlat16_17) + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat16_2.xyz * u_xlat0.xzw;
    u_xlat0.xyz = vec3(u_xlat5) * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

//////////////////////////////////////////////////////
Keywords set in this variant: POINT_COOKIE 
-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 34 math, 4 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xyz;
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform highp samplerCube _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp vec3 tmpvar_5;
  lowp float tmpvar_6;
  highp vec3 worldViewDir_7;
  lowp vec3 lightDir_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_8 = tmpvar_9;
  worldViewDir_7 = normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2));
  lowp vec4 tmpvar_10;
  tmpvar_10 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_5 = tmpvar_10.xyz;
  tmpvar_6 = tmpvar_10.w;
  highp float tmpvar_11;
  tmpvar_11 = (texture2D (_LightTextureB0, vec2(dot (xlv_TEXCOORD3, xlv_TEXCOORD3))).w * textureCube (_LightTexture0, xlv_TEXCOORD3).w);
  atten_4 = tmpvar_11;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_8;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_12;
  mediump vec4 c_13;
  highp vec3 tmpvar_14;
  tmpvar_14 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_15;
  mediump vec3 albedo_16;
  albedo_16 = tmpvar_5;
  tmpvar_15 = (albedo_16 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_12 = tmpvar_15;
  mediump vec3 diffColor_17;
  diffColor_17 = tmpvar_12;
  mediump float alpha_18;
  alpha_18 = tmpvar_6;
  tmpvar_12 = diffColor_17;
  mediump vec3 diffColor_19;
  diffColor_19 = tmpvar_12;
  mediump vec2 rlPow4AndFresnelTerm_20;
  mediump float tmpvar_21;
  highp float tmpvar_22;
  tmpvar_22 = clamp (dot (tmpvar_14, tmpvar_2), 0.0, 1.0);
  tmpvar_21 = tmpvar_22;
  mediump float tmpvar_23;
  highp float tmpvar_24;
  tmpvar_24 = clamp (dot (tmpvar_14, worldViewDir_7), 0.0, 1.0);
  tmpvar_23 = tmpvar_24;
  highp vec2 tmpvar_25;
  tmpvar_25.x = dot ((worldViewDir_7 - (2.0 * 
    (dot (tmpvar_14, worldViewDir_7) * tmpvar_14)
  )), tmpvar_2);
  tmpvar_25.y = (1.0 - tmpvar_23);
  highp vec2 tmpvar_26;
  tmpvar_26 = ((tmpvar_25 * tmpvar_25) * (tmpvar_25 * tmpvar_25));
  rlPow4AndFresnelTerm_20 = tmpvar_26;
  mediump float tmpvar_27;
  tmpvar_27 = rlPow4AndFresnelTerm_20.x;
  mediump float specular_28;
  highp float smoothness_29;
  smoothness_29 = _Glossiness;
  highp vec2 tmpvar_30;
  tmpvar_30.x = tmpvar_27;
  tmpvar_30.y = (1.0 - smoothness_29);
  highp float tmpvar_31;
  tmpvar_31 = (texture2D (unity_NHxRoughness, tmpvar_30).w * 16.0);
  specular_28 = tmpvar_31;
  mediump vec4 tmpvar_32;
  tmpvar_32.w = 1.0;
  tmpvar_32.xyz = ((diffColor_19 + (specular_28 * 
    mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_16, vec3(_Metallic))
  )) * (tmpvar_1 * tmpvar_21));
  c_13.xyz = tmpvar_32.xyz;
  c_13.w = alpha_18;
  c_3.xyz = c_13.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 44 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xyz;
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform lowp vec4 _LightColor0;
uniform highp samplerCube _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp vec3 tmpvar_5;
  lowp float tmpvar_6;
  lowp vec3 lightDir_7;
  highp vec3 tmpvar_8;
  tmpvar_8 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_7 = tmpvar_8;
  lowp vec4 tmpvar_9;
  tmpvar_9 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_5 = tmpvar_9.xyz;
  tmpvar_6 = tmpvar_9.w;
  highp float tmpvar_10;
  tmpvar_10 = (texture2D (_LightTextureB0, vec2(dot (xlv_TEXCOORD3, xlv_TEXCOORD3))).w * textureCube (_LightTexture0, xlv_TEXCOORD3).w);
  atten_4 = tmpvar_10;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_7;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_11;
  mediump vec4 c_12;
  highp vec3 tmpvar_13;
  tmpvar_13 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_14;
  mediump vec3 albedo_15;
  albedo_15 = tmpvar_5;
  mediump vec3 tmpvar_16;
  tmpvar_16 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_15, vec3(_Metallic));
  tmpvar_14 = (albedo_15 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_11 = tmpvar_14;
  mediump vec3 diffColor_17;
  diffColor_17 = tmpvar_11;
  mediump float alpha_18;
  alpha_18 = tmpvar_6;
  tmpvar_11 = diffColor_17;
  mediump vec3 diffColor_19;
  diffColor_19 = tmpvar_11;
  mediump vec3 color_20;
  highp float specularTerm_21;
  highp float a2_22;
  mediump float roughness_23;
  mediump float perceptualRoughness_24;
  highp vec3 tmpvar_25;
  highp vec3 inVec_26;
  inVec_26 = (tmpvar_2 + normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2)));
  tmpvar_25 = (inVec_26 * inversesqrt(max (0.001, 
    dot (inVec_26, inVec_26)
  )));
  mediump float tmpvar_27;
  highp float tmpvar_28;
  tmpvar_28 = clamp (dot (tmpvar_13, tmpvar_2), 0.0, 1.0);
  tmpvar_27 = tmpvar_28;
  highp float tmpvar_29;
  tmpvar_29 = clamp (dot (tmpvar_13, tmpvar_25), 0.0, 1.0);
  highp float tmpvar_30;
  highp float smoothness_31;
  smoothness_31 = _Glossiness;
  tmpvar_30 = (1.0 - smoothness_31);
  perceptualRoughness_24 = tmpvar_30;
  highp float tmpvar_32;
  highp float perceptualRoughness_33;
  perceptualRoughness_33 = perceptualRoughness_24;
  tmpvar_32 = (perceptualRoughness_33 * perceptualRoughness_33);
  roughness_23 = tmpvar_32;
  mediump float tmpvar_34;
  tmpvar_34 = (roughness_23 * roughness_23);
  a2_22 = tmpvar_34;
  specularTerm_21 = ((roughness_23 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_25), 0.0, 1.0)) * (1.5 + roughness_23))
   * 
    (((tmpvar_29 * tmpvar_29) * (a2_22 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_35;
  tmpvar_35 = clamp (specularTerm_21, 0.0, 100.0);
  specularTerm_21 = tmpvar_35;
  highp vec3 tmpvar_36;
  tmpvar_36 = (((diffColor_19 + 
    (tmpvar_35 * tmpvar_16)
  ) * tmpvar_1) * tmpvar_27);
  color_20 = tmpvar_36;
  mediump vec4 tmpvar_37;
  tmpvar_37.w = 1.0;
  tmpvar_37.xyz = color_20;
  c_12.xyz = tmpvar_37.xyz;
  c_12.w = alpha_18;
  c_3.xyz = c_12.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 44 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xyz;
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform lowp vec4 _LightColor0;
uniform highp samplerCube _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp vec3 tmpvar_5;
  lowp float tmpvar_6;
  lowp vec3 lightDir_7;
  highp vec3 tmpvar_8;
  tmpvar_8 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_7 = tmpvar_8;
  lowp vec4 tmpvar_9;
  tmpvar_9 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_5 = tmpvar_9.xyz;
  tmpvar_6 = tmpvar_9.w;
  highp float tmpvar_10;
  tmpvar_10 = (texture2D (_LightTextureB0, vec2(dot (xlv_TEXCOORD3, xlv_TEXCOORD3))).w * textureCube (_LightTexture0, xlv_TEXCOORD3).w);
  atten_4 = tmpvar_10;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_7;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_11;
  mediump vec4 c_12;
  highp vec3 tmpvar_13;
  tmpvar_13 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_14;
  mediump vec3 albedo_15;
  albedo_15 = tmpvar_5;
  mediump vec3 tmpvar_16;
  tmpvar_16 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_15, vec3(_Metallic));
  tmpvar_14 = (albedo_15 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_11 = tmpvar_14;
  mediump vec3 diffColor_17;
  diffColor_17 = tmpvar_11;
  mediump float alpha_18;
  alpha_18 = tmpvar_6;
  tmpvar_11 = diffColor_17;
  mediump vec3 diffColor_19;
  diffColor_19 = tmpvar_11;
  mediump vec3 color_20;
  highp float specularTerm_21;
  highp float a2_22;
  mediump float roughness_23;
  mediump float perceptualRoughness_24;
  highp vec3 tmpvar_25;
  highp vec3 inVec_26;
  inVec_26 = (tmpvar_2 + normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2)));
  tmpvar_25 = (inVec_26 * inversesqrt(max (0.001, 
    dot (inVec_26, inVec_26)
  )));
  mediump float tmpvar_27;
  highp float tmpvar_28;
  tmpvar_28 = clamp (dot (tmpvar_13, tmpvar_2), 0.0, 1.0);
  tmpvar_27 = tmpvar_28;
  highp float tmpvar_29;
  tmpvar_29 = clamp (dot (tmpvar_13, tmpvar_25), 0.0, 1.0);
  highp float tmpvar_30;
  highp float smoothness_31;
  smoothness_31 = _Glossiness;
  tmpvar_30 = (1.0 - smoothness_31);
  perceptualRoughness_24 = tmpvar_30;
  highp float tmpvar_32;
  highp float perceptualRoughness_33;
  perceptualRoughness_33 = perceptualRoughness_24;
  tmpvar_32 = (perceptualRoughness_33 * perceptualRoughness_33);
  roughness_23 = tmpvar_32;
  mediump float tmpvar_34;
  tmpvar_34 = (roughness_23 * roughness_23);
  a2_22 = tmpvar_34;
  specularTerm_21 = ((roughness_23 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_25), 0.0, 1.0)) * (1.5 + roughness_23))
   * 
    (((tmpvar_29 * tmpvar_29) * (a2_22 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_35;
  tmpvar_35 = clamp (specularTerm_21, 0.0, 100.0);
  specularTerm_21 = tmpvar_35;
  highp vec3 tmpvar_36;
  tmpvar_36 = (((diffColor_19 + 
    (tmpvar_35 * tmpvar_16)
  ) * tmpvar_1) * tmpvar_27);
  color_20 = tmpvar_36;
  mediump vec4 tmpvar_37;
  tmpvar_37.w = 1.0;
  tmpvar_37.xyz = color_20;
  c_12.xyz = tmpvar_37.xyz;
  c_12.w = alpha_18;
  c_3.xyz = c_12.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTextureB0;
uniform highp samplerCube _LightTexture0;
uniform highp sampler2D unity_NHxRoughness;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
float u_xlat5;
float u_xlat10;
float u_xlat15;
mediump float u_xlat16_18;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat15 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat1.xyz = vec3(u_xlat15) * vs_TEXCOORD1.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat1.xyz);
    u_xlat15 = u_xlat15 + u_xlat15;
    u_xlat0.xyz = u_xlat1.xyz * (-vec3(u_xlat15)) + u_xlat0.xyz;
    u_xlat2.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat15 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat2.xyz = vec3(u_xlat15) * u_xlat2.xyz;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat2.xyz);
    u_xlat5 = dot(u_xlat1.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5 = min(max(u_xlat5, 0.0), 1.0);
#else
    u_xlat5 = clamp(u_xlat5, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat1.x = u_xlat0.x * u_xlat0.x;
    u_xlat1.y = (-_Glossiness) + 1.0;
    u_xlat0.x = texture(unity_NHxRoughness, u_xlat1.xy).w;
    u_xlat0.x = u_xlat0.x * 16.0;
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_3.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_3.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_3.xyz = u_xlat0.xxx * u_xlat16_3.xyz;
    u_xlat16_18 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_3.xyz = u_xlat16_1.xyz * vec3(u_xlat16_18) + u_xlat16_3.xyz;
    u_xlat0.xzw = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat0.xzw = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD2.xxx + u_xlat0.xzw;
    u_xlat0.xzw = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD2.zzz + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat0.xzw + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat1.x = dot(u_xlat0.xzw, u_xlat0.xzw);
    u_xlat0.x = texture(_LightTexture0, u_xlat0.xzw).w;
    u_xlat10 = texture(_LightTextureB0, u_xlat1.xx).w;
    u_xlat0.x = u_xlat0.x * u_xlat10;
    u_xlat16_4.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat16_4.xyz = vec3(u_xlat5) * u_xlat16_4.xyz;
    SV_Target0.xyz = u_xlat16_3.xyz * u_xlat16_4.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTextureB0;
uniform highp samplerCube _LightTexture0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
vec3 u_xlat2;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat4;
float u_xlat5;
mediump float u_xlat16_8;
float u_xlat12;
float u_xlat13;
mediump float u_xlat16_13;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat13 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat13 = inversesqrt(u_xlat13);
    u_xlat1.xyz = vec3(u_xlat13) * u_xlat1.xyz;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat12) + u_xlat1.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = max(u_xlat12, 0.00100000005);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    u_xlat12 = dot(u_xlat1.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat12 = min(max(u_xlat12, 0.0), 1.0);
#else
    u_xlat12 = clamp(u_xlat12, 0.0, 1.0);
#endif
    u_xlat12 = max(u_xlat12, 0.319999993);
    u_xlat16_13 = (-_Glossiness) + 1.0;
    u_xlat16_2 = u_xlat16_13 * u_xlat16_13 + 1.5;
    u_xlat16_13 = u_xlat16_13 * u_xlat16_13;
    u_xlat12 = u_xlat12 * u_xlat16_2;
    u_xlat2.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat2.x = inversesqrt(u_xlat2.x);
    u_xlat2.xyz = u_xlat2.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat2.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat4 = dot(u_xlat2.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat4 = min(max(u_xlat4, 0.0), 1.0);
#else
    u_xlat4 = clamp(u_xlat4, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat16_8 = u_xlat16_13 * u_xlat16_13 + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_8 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat12;
    u_xlat0.x = u_xlat16_13 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_3.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_3.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_3.xyz;
    u_xlat16_3.x = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * u_xlat16_3.xxx + u_xlat0.xzw;
    u_xlat1.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD2.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD2.zzz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat13 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat1.x = texture(_LightTexture0, u_xlat1.xyz).w;
    u_xlat5 = texture(_LightTextureB0, vec2(u_xlat13)).w;
    u_xlat1.x = u_xlat1.x * u_xlat5;
    u_xlat16_3.xyz = u_xlat1.xxx * _LightColor0.xyz;
    u_xlat0.xzw = u_xlat0.xzw * u_xlat16_3.xyz;
    u_xlat0.xyz = vec3(u_xlat4) * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTextureB0;
uniform highp samplerCube _LightTexture0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
vec3 u_xlat2;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat4;
float u_xlat5;
mediump float u_xlat16_8;
float u_xlat12;
float u_xlat13;
mediump float u_xlat16_13;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat13 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat13 = inversesqrt(u_xlat13);
    u_xlat1.xyz = vec3(u_xlat13) * u_xlat1.xyz;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat12) + u_xlat1.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = max(u_xlat12, 0.00100000005);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    u_xlat12 = dot(u_xlat1.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat12 = min(max(u_xlat12, 0.0), 1.0);
#else
    u_xlat12 = clamp(u_xlat12, 0.0, 1.0);
#endif
    u_xlat12 = max(u_xlat12, 0.319999993);
    u_xlat16_13 = (-_Glossiness) + 1.0;
    u_xlat16_2 = u_xlat16_13 * u_xlat16_13 + 1.5;
    u_xlat16_13 = u_xlat16_13 * u_xlat16_13;
    u_xlat12 = u_xlat12 * u_xlat16_2;
    u_xlat2.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat2.x = inversesqrt(u_xlat2.x);
    u_xlat2.xyz = u_xlat2.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat2.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat4 = dot(u_xlat2.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat4 = min(max(u_xlat4, 0.0), 1.0);
#else
    u_xlat4 = clamp(u_xlat4, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat16_8 = u_xlat16_13 * u_xlat16_13 + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_8 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat12;
    u_xlat0.x = u_xlat16_13 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_3.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_3.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_3.xyz;
    u_xlat16_3.x = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * u_xlat16_3.xxx + u_xlat0.xzw;
    u_xlat1.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD2.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD2.zzz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat13 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat1.x = texture(_LightTexture0, u_xlat1.xyz).w;
    u_xlat5 = texture(_LightTextureB0, vec2(u_xlat13)).w;
    u_xlat1.x = u_xlat1.x * u_xlat5;
    u_xlat16_3.xyz = u_xlat1.xxx * _LightColor0.xyz;
    u_xlat0.xzw = u_xlat0.xzw * u_xlat16_3.xyz;
    u_xlat0.xyz = vec3(u_xlat4) * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 30 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD3;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xy;
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform highp sampler2D _LightTexture0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp vec3 tmpvar_5;
  lowp float tmpvar_6;
  highp vec3 worldViewDir_7;
  lowp vec3 lightDir_8;
  mediump vec3 tmpvar_9;
  tmpvar_9 = _WorldSpaceLightPos0.xyz;
  lightDir_8 = tmpvar_9;
  worldViewDir_7 = normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2));
  lowp vec4 tmpvar_10;
  tmpvar_10 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_5 = tmpvar_10.xyz;
  tmpvar_6 = tmpvar_10.w;
  highp float tmpvar_11;
  tmpvar_11 = texture2D (_LightTexture0, xlv_TEXCOORD3).w;
  atten_4 = tmpvar_11;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_8;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_12;
  mediump vec4 c_13;
  highp vec3 tmpvar_14;
  tmpvar_14 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_15;
  mediump vec3 albedo_16;
  albedo_16 = tmpvar_5;
  tmpvar_15 = (albedo_16 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_12 = tmpvar_15;
  mediump vec3 diffColor_17;
  diffColor_17 = tmpvar_12;
  mediump float alpha_18;
  alpha_18 = tmpvar_6;
  tmpvar_12 = diffColor_17;
  mediump vec3 diffColor_19;
  diffColor_19 = tmpvar_12;
  mediump vec2 rlPow4AndFresnelTerm_20;
  mediump float tmpvar_21;
  highp float tmpvar_22;
  tmpvar_22 = clamp (dot (tmpvar_14, tmpvar_2), 0.0, 1.0);
  tmpvar_21 = tmpvar_22;
  mediump float tmpvar_23;
  highp float tmpvar_24;
  tmpvar_24 = clamp (dot (tmpvar_14, worldViewDir_7), 0.0, 1.0);
  tmpvar_23 = tmpvar_24;
  highp vec2 tmpvar_25;
  tmpvar_25.x = dot ((worldViewDir_7 - (2.0 * 
    (dot (tmpvar_14, worldViewDir_7) * tmpvar_14)
  )), tmpvar_2);
  tmpvar_25.y = (1.0 - tmpvar_23);
  highp vec2 tmpvar_26;
  tmpvar_26 = ((tmpvar_25 * tmpvar_25) * (tmpvar_25 * tmpvar_25));
  rlPow4AndFresnelTerm_20 = tmpvar_26;
  mediump float tmpvar_27;
  tmpvar_27 = rlPow4AndFresnelTerm_20.x;
  mediump float specular_28;
  highp float smoothness_29;
  smoothness_29 = _Glossiness;
  highp vec2 tmpvar_30;
  tmpvar_30.x = tmpvar_27;
  tmpvar_30.y = (1.0 - smoothness_29);
  highp float tmpvar_31;
  tmpvar_31 = (texture2D (unity_NHxRoughness, tmpvar_30).w * 16.0);
  specular_28 = tmpvar_31;
  mediump vec4 tmpvar_32;
  tmpvar_32.w = 1.0;
  tmpvar_32.xyz = ((diffColor_19 + (specular_28 * 
    mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_16, vec3(_Metallic))
  )) * (tmpvar_1 * tmpvar_21));
  c_13.xyz = tmpvar_32.xyz;
  c_13.w = alpha_18;
  c_3.xyz = c_13.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 40 math, 2 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD3;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xy;
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _LightTexture0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp vec3 tmpvar_5;
  lowp float tmpvar_6;
  lowp vec3 lightDir_7;
  mediump vec3 tmpvar_8;
  tmpvar_8 = _WorldSpaceLightPos0.xyz;
  lightDir_7 = tmpvar_8;
  lowp vec4 tmpvar_9;
  tmpvar_9 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_5 = tmpvar_9.xyz;
  tmpvar_6 = tmpvar_9.w;
  highp float tmpvar_10;
  tmpvar_10 = texture2D (_LightTexture0, xlv_TEXCOORD3).w;
  atten_4 = tmpvar_10;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_7;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_11;
  mediump vec4 c_12;
  highp vec3 tmpvar_13;
  tmpvar_13 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_14;
  mediump vec3 albedo_15;
  albedo_15 = tmpvar_5;
  mediump vec3 tmpvar_16;
  tmpvar_16 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_15, vec3(_Metallic));
  tmpvar_14 = (albedo_15 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_11 = tmpvar_14;
  mediump vec3 diffColor_17;
  diffColor_17 = tmpvar_11;
  mediump float alpha_18;
  alpha_18 = tmpvar_6;
  tmpvar_11 = diffColor_17;
  mediump vec3 diffColor_19;
  diffColor_19 = tmpvar_11;
  mediump vec3 color_20;
  highp float specularTerm_21;
  highp float a2_22;
  mediump float roughness_23;
  mediump float perceptualRoughness_24;
  highp vec3 tmpvar_25;
  highp vec3 inVec_26;
  inVec_26 = (tmpvar_2 + normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2)));
  tmpvar_25 = (inVec_26 * inversesqrt(max (0.001, 
    dot (inVec_26, inVec_26)
  )));
  mediump float tmpvar_27;
  highp float tmpvar_28;
  tmpvar_28 = clamp (dot (tmpvar_13, tmpvar_2), 0.0, 1.0);
  tmpvar_27 = tmpvar_28;
  highp float tmpvar_29;
  tmpvar_29 = clamp (dot (tmpvar_13, tmpvar_25), 0.0, 1.0);
  highp float tmpvar_30;
  highp float smoothness_31;
  smoothness_31 = _Glossiness;
  tmpvar_30 = (1.0 - smoothness_31);
  perceptualRoughness_24 = tmpvar_30;
  highp float tmpvar_32;
  highp float perceptualRoughness_33;
  perceptualRoughness_33 = perceptualRoughness_24;
  tmpvar_32 = (perceptualRoughness_33 * perceptualRoughness_33);
  roughness_23 = tmpvar_32;
  mediump float tmpvar_34;
  tmpvar_34 = (roughness_23 * roughness_23);
  a2_22 = tmpvar_34;
  specularTerm_21 = ((roughness_23 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_25), 0.0, 1.0)) * (1.5 + roughness_23))
   * 
    (((tmpvar_29 * tmpvar_29) * (a2_22 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_35;
  tmpvar_35 = clamp (specularTerm_21, 0.0, 100.0);
  specularTerm_21 = tmpvar_35;
  highp vec3 tmpvar_36;
  tmpvar_36 = (((diffColor_19 + 
    (tmpvar_35 * tmpvar_16)
  ) * tmpvar_1) * tmpvar_27);
  color_20 = tmpvar_36;
  mediump vec4 tmpvar_37;
  tmpvar_37.w = 1.0;
  tmpvar_37.xyz = color_20;
  c_12.xyz = tmpvar_37.xyz;
  c_12.w = alpha_18;
  c_3.xyz = c_12.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 40 math, 2 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD3;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xy;
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _LightTexture0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp vec3 tmpvar_5;
  lowp float tmpvar_6;
  lowp vec3 lightDir_7;
  mediump vec3 tmpvar_8;
  tmpvar_8 = _WorldSpaceLightPos0.xyz;
  lightDir_7 = tmpvar_8;
  lowp vec4 tmpvar_9;
  tmpvar_9 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_5 = tmpvar_9.xyz;
  tmpvar_6 = tmpvar_9.w;
  highp float tmpvar_10;
  tmpvar_10 = texture2D (_LightTexture0, xlv_TEXCOORD3).w;
  atten_4 = tmpvar_10;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_7;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_11;
  mediump vec4 c_12;
  highp vec3 tmpvar_13;
  tmpvar_13 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_14;
  mediump vec3 albedo_15;
  albedo_15 = tmpvar_5;
  mediump vec3 tmpvar_16;
  tmpvar_16 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_15, vec3(_Metallic));
  tmpvar_14 = (albedo_15 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_11 = tmpvar_14;
  mediump vec3 diffColor_17;
  diffColor_17 = tmpvar_11;
  mediump float alpha_18;
  alpha_18 = tmpvar_6;
  tmpvar_11 = diffColor_17;
  mediump vec3 diffColor_19;
  diffColor_19 = tmpvar_11;
  mediump vec3 color_20;
  highp float specularTerm_21;
  highp float a2_22;
  mediump float roughness_23;
  mediump float perceptualRoughness_24;
  highp vec3 tmpvar_25;
  highp vec3 inVec_26;
  inVec_26 = (tmpvar_2 + normalize((_WorldSpaceCameraPos - xlv_TEXCOORD2)));
  tmpvar_25 = (inVec_26 * inversesqrt(max (0.001, 
    dot (inVec_26, inVec_26)
  )));
  mediump float tmpvar_27;
  highp float tmpvar_28;
  tmpvar_28 = clamp (dot (tmpvar_13, tmpvar_2), 0.0, 1.0);
  tmpvar_27 = tmpvar_28;
  highp float tmpvar_29;
  tmpvar_29 = clamp (dot (tmpvar_13, tmpvar_25), 0.0, 1.0);
  highp float tmpvar_30;
  highp float smoothness_31;
  smoothness_31 = _Glossiness;
  tmpvar_30 = (1.0 - smoothness_31);
  perceptualRoughness_24 = tmpvar_30;
  highp float tmpvar_32;
  highp float perceptualRoughness_33;
  perceptualRoughness_33 = perceptualRoughness_24;
  tmpvar_32 = (perceptualRoughness_33 * perceptualRoughness_33);
  roughness_23 = tmpvar_32;
  mediump float tmpvar_34;
  tmpvar_34 = (roughness_23 * roughness_23);
  a2_22 = tmpvar_34;
  specularTerm_21 = ((roughness_23 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_25), 0.0, 1.0)) * (1.5 + roughness_23))
   * 
    (((tmpvar_29 * tmpvar_29) * (a2_22 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_35;
  tmpvar_35 = clamp (specularTerm_21, 0.0, 100.0);
  specularTerm_21 = tmpvar_35;
  highp vec3 tmpvar_36;
  tmpvar_36 = (((diffColor_19 + 
    (tmpvar_35 * tmpvar_16)
  ) * tmpvar_1) * tmpvar_27);
  color_20 = tmpvar_36;
  mediump vec4 tmpvar_37;
  tmpvar_37.w = 1.0;
  tmpvar_37.xyz = color_20;
  c_12.xyz = tmpvar_37.xyz;
  c_12.w = alpha_18;
  c_3.xyz = c_12.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D unity_NHxRoughness;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec3 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat12;
mediump float u_xlat16_14;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    u_xlat12 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = vec3(u_xlat12) * vs_TEXCOORD1.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat1.xyz);
    u_xlat12 = u_xlat12 + u_xlat12;
    u_xlat0.xyz = u_xlat1.xyz * (-vec3(u_xlat12)) + u_xlat0.xyz;
    u_xlat12 = dot(u_xlat1.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat12 = min(max(u_xlat12, 0.0), 1.0);
#else
    u_xlat12 = clamp(u_xlat12, 0.0, 1.0);
#endif
    u_xlat0.x = dot(u_xlat0.xyz, _WorldSpaceLightPos0.xyz);
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.y = (-_Glossiness) + 1.0;
    u_xlat0.x = texture(unity_NHxRoughness, u_xlat0.xy).w;
    u_xlat0.x = u_xlat0.x * 16.0;
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_2.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_2.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_2.xyz = u_xlat0.xxx * u_xlat16_2.xyz;
    u_xlat16_14 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_2.xyz = u_xlat16_1.xyz * vec3(u_xlat16_14) + u_xlat16_2.xyz;
    u_xlat0.xy = vs_TEXCOORD2.yy * hlslcc_mtx4x4unity_WorldToLight[1].xy;
    u_xlat0.xy = hlslcc_mtx4x4unity_WorldToLight[0].xy * vs_TEXCOORD2.xx + u_xlat0.xy;
    u_xlat0.xy = hlslcc_mtx4x4unity_WorldToLight[2].xy * vs_TEXCOORD2.zz + u_xlat0.xy;
    u_xlat0.xy = u_xlat0.xy + hlslcc_mtx4x4unity_WorldToLight[3].xy;
    u_xlat0.x = texture(_LightTexture0, u_xlat0.xy).w;
    u_xlat16_3.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat16_3.xyz = vec3(u_xlat12) * u_xlat16_3.xyz;
    SV_Target0.xyz = u_xlat16_2.xyz * u_xlat16_3.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec2 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
float u_xlat3;
vec3 u_xlat4;
mediump float u_xlat16_4;
mediump float u_xlat16_6;
float u_xlat9;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat9 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat9 = inversesqrt(u_xlat9);
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat9) + _WorldSpaceLightPos0.xyz;
    u_xlat9 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat9 = max(u_xlat9, 0.00100000005);
    u_xlat9 = inversesqrt(u_xlat9);
    u_xlat0.xyz = vec3(u_xlat9) * u_xlat0.xyz;
    u_xlat9 = dot(_WorldSpaceLightPos0.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat9 = min(max(u_xlat9, 0.0), 1.0);
#else
    u_xlat9 = clamp(u_xlat9, 0.0, 1.0);
#endif
    u_xlat9 = max(u_xlat9, 0.319999993);
    u_xlat16_1.x = (-_Glossiness) + 1.0;
    u_xlat16_4 = u_xlat16_1.x * u_xlat16_1.x + 1.5;
    u_xlat16_1.x = u_xlat16_1.x * u_xlat16_1.x;
    u_xlat9 = u_xlat9 * u_xlat16_4;
    u_xlat4.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat4.x = inversesqrt(u_xlat4.x);
    u_xlat4.xyz = u_xlat4.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat4.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat3 = dot(u_xlat4.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat3 = min(max(u_xlat3, 0.0), 1.0);
#else
    u_xlat3 = clamp(u_xlat3, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat16_6 = u_xlat16_1.x * u_xlat16_1.x + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_6 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat9;
    u_xlat0.x = u_xlat16_1.x / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_2.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_2.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_2.xyz;
    u_xlat16_2.x = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * u_xlat16_2.xxx + u_xlat0.xzw;
    u_xlat1.xy = vs_TEXCOORD2.yy * hlslcc_mtx4x4unity_WorldToLight[1].xy;
    u_xlat1.xy = hlslcc_mtx4x4unity_WorldToLight[0].xy * vs_TEXCOORD2.xx + u_xlat1.xy;
    u_xlat1.xy = hlslcc_mtx4x4unity_WorldToLight[2].xy * vs_TEXCOORD2.zz + u_xlat1.xy;
    u_xlat1.xy = u_xlat1.xy + hlslcc_mtx4x4unity_WorldToLight[3].xy;
    u_xlat1.x = texture(_LightTexture0, u_xlat1.xy).w;
    u_xlat16_2.xyz = u_xlat1.xxx * _LightColor0.xyz;
    u_xlat0.xzw = u_xlat0.xzw * u_xlat16_2.xyz;
    u_xlat0.xyz = vec3(u_xlat3) * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec2 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
float u_xlat3;
vec3 u_xlat4;
mediump float u_xlat16_4;
mediump float u_xlat16_6;
float u_xlat9;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat9 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat9 = inversesqrt(u_xlat9);
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat9) + _WorldSpaceLightPos0.xyz;
    u_xlat9 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat9 = max(u_xlat9, 0.00100000005);
    u_xlat9 = inversesqrt(u_xlat9);
    u_xlat0.xyz = vec3(u_xlat9) * u_xlat0.xyz;
    u_xlat9 = dot(_WorldSpaceLightPos0.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat9 = min(max(u_xlat9, 0.0), 1.0);
#else
    u_xlat9 = clamp(u_xlat9, 0.0, 1.0);
#endif
    u_xlat9 = max(u_xlat9, 0.319999993);
    u_xlat16_1.x = (-_Glossiness) + 1.0;
    u_xlat16_4 = u_xlat16_1.x * u_xlat16_1.x + 1.5;
    u_xlat16_1.x = u_xlat16_1.x * u_xlat16_1.x;
    u_xlat9 = u_xlat9 * u_xlat16_4;
    u_xlat4.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat4.x = inversesqrt(u_xlat4.x);
    u_xlat4.xyz = u_xlat4.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat4.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat3 = dot(u_xlat4.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat3 = min(max(u_xlat3, 0.0), 1.0);
#else
    u_xlat3 = clamp(u_xlat3, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat16_6 = u_xlat16_1.x * u_xlat16_1.x + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_6 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat9;
    u_xlat0.x = u_xlat16_1.x / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_2.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_2.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_2.xyz;
    u_xlat16_2.x = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * u_xlat16_2.xxx + u_xlat0.xzw;
    u_xlat1.xy = vs_TEXCOORD2.yy * hlslcc_mtx4x4unity_WorldToLight[1].xy;
    u_xlat1.xy = hlslcc_mtx4x4unity_WorldToLight[0].xy * vs_TEXCOORD2.xx + u_xlat1.xy;
    u_xlat1.xy = hlslcc_mtx4x4unity_WorldToLight[2].xy * vs_TEXCOORD2.zz + u_xlat1.xy;
    u_xlat1.xy = u_xlat1.xy + hlslcc_mtx4x4unity_WorldToLight[3].xy;
    u_xlat1.x = texture(_LightTexture0, u_xlat1.xy).w;
    u_xlat16_2.xyz = u_xlat1.xxx * _LightColor0.xyz;
    u_xlat0.xzw = u_xlat0.xzw * u_xlat16_2.xyz;
    u_xlat0.xyz = vec3(u_xlat3) * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

//////////////////////////////////////////////////////
Keywords set in this variant: SPOT SHADOWS_DEPTH 
-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 53 math, 5 textures, 1 branches
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3);
  xlv_TEXCOORD4 = (unity_WorldToShadow[0] * tmpvar_3);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform highp sampler2D _ShadowMapTexture;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  mediump float realtimeShadowAttenuation_13;
  highp vec4 v_14;
  v_14.x = unity_MatrixV[0].z;
  v_14.y = unity_MatrixV[1].z;
  v_14.z = unity_MatrixV[2].z;
  v_14.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_15;
  tmpvar_15 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = clamp (((
    mix (dot (tmpvar_9, v_14.xyz), sqrt(dot (tmpvar_15, tmpvar_15)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_16 = tmpvar_17;
  realtimeShadowAttenuation_13 = 1.0;
  lowp float tmpvar_18;
  highp vec4 tmpvar_19;
  tmpvar_19 = texture2DProj (_ShadowMapTexture, xlv_TEXCOORD4);
  mediump float tmpvar_20;
  if ((tmpvar_19.x < (xlv_TEXCOORD4.z / xlv_TEXCOORD4.w))) {
    tmpvar_20 = _LightShadowData.x;
  } else {
    tmpvar_20 = 1.0;
  };
  tmpvar_18 = tmpvar_20;
  realtimeShadowAttenuation_13 = tmpvar_18;
  mediump float tmpvar_21;
  tmpvar_21 = mix (realtimeShadowAttenuation_13, 1.0, tmpvar_16);
  shadow_5 = tmpvar_21;
  lowp float tmpvar_22;
  highp vec4 tmpvar_23;
  tmpvar_23 = texture2D (_LightTexture0, ((xlv_TEXCOORD3.xy / xlv_TEXCOORD3.w) + 0.5));
  tmpvar_22 = tmpvar_23.w;
  lowp float tmpvar_24;
  highp vec4 tmpvar_25;
  tmpvar_25 = texture2D (_LightTextureB0, vec2(dot (xlv_TEXCOORD3.xyz, xlv_TEXCOORD3.xyz)));
  tmpvar_24 = tmpvar_25.w;
  highp float tmpvar_26;
  tmpvar_26 = (((
    float((xlv_TEXCOORD3.z > 0.0))
   * tmpvar_22) * tmpvar_24) * shadow_5);
  atten_4 = tmpvar_26;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_27;
  mediump vec4 c_28;
  highp vec3 tmpvar_29;
  tmpvar_29 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_30;
  mediump vec3 albedo_31;
  albedo_31 = tmpvar_10;
  tmpvar_30 = (albedo_31 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_27 = tmpvar_30;
  mediump vec3 diffColor_32;
  diffColor_32 = tmpvar_27;
  mediump float alpha_33;
  alpha_33 = tmpvar_11;
  tmpvar_27 = diffColor_32;
  mediump vec3 diffColor_34;
  diffColor_34 = tmpvar_27;
  mediump vec2 rlPow4AndFresnelTerm_35;
  mediump float tmpvar_36;
  highp float tmpvar_37;
  tmpvar_37 = clamp (dot (tmpvar_29, tmpvar_2), 0.0, 1.0);
  tmpvar_36 = tmpvar_37;
  mediump float tmpvar_38;
  highp float tmpvar_39;
  tmpvar_39 = clamp (dot (tmpvar_29, tmpvar_8), 0.0, 1.0);
  tmpvar_38 = tmpvar_39;
  highp vec2 tmpvar_40;
  tmpvar_40.x = dot ((tmpvar_8 - (2.0 * 
    (dot (tmpvar_29, tmpvar_8) * tmpvar_29)
  )), tmpvar_2);
  tmpvar_40.y = (1.0 - tmpvar_38);
  highp vec2 tmpvar_41;
  tmpvar_41 = ((tmpvar_40 * tmpvar_40) * (tmpvar_40 * tmpvar_40));
  rlPow4AndFresnelTerm_35 = tmpvar_41;
  mediump float tmpvar_42;
  tmpvar_42 = rlPow4AndFresnelTerm_35.x;
  mediump float specular_43;
  highp float smoothness_44;
  smoothness_44 = _Glossiness;
  highp vec2 tmpvar_45;
  tmpvar_45.x = tmpvar_42;
  tmpvar_45.y = (1.0 - smoothness_44);
  highp float tmpvar_46;
  tmpvar_46 = (texture2D (unity_NHxRoughness, tmpvar_45).w * 16.0);
  specular_43 = tmpvar_46;
  mediump vec4 tmpvar_47;
  tmpvar_47.w = 1.0;
  tmpvar_47.xyz = ((diffColor_34 + (specular_43 * 
    mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_31, vec3(_Metallic))
  )) * (tmpvar_1 * tmpvar_36));
  c_28.xyz = tmpvar_47.xyz;
  c_28.w = alpha_33;
  c_3.xyz = c_28.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 63 math, 4 textures, 1 branches
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3);
  xlv_TEXCOORD4 = (unity_WorldToShadow[0] * tmpvar_3);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _ShadowMapTexture;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  mediump float realtimeShadowAttenuation_13;
  highp vec4 v_14;
  v_14.x = unity_MatrixV[0].z;
  v_14.y = unity_MatrixV[1].z;
  v_14.z = unity_MatrixV[2].z;
  v_14.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_15;
  tmpvar_15 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = clamp (((
    mix (dot (tmpvar_9, v_14.xyz), sqrt(dot (tmpvar_15, tmpvar_15)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_16 = tmpvar_17;
  realtimeShadowAttenuation_13 = 1.0;
  lowp float tmpvar_18;
  highp vec4 tmpvar_19;
  tmpvar_19 = texture2DProj (_ShadowMapTexture, xlv_TEXCOORD4);
  mediump float tmpvar_20;
  if ((tmpvar_19.x < (xlv_TEXCOORD4.z / xlv_TEXCOORD4.w))) {
    tmpvar_20 = _LightShadowData.x;
  } else {
    tmpvar_20 = 1.0;
  };
  tmpvar_18 = tmpvar_20;
  realtimeShadowAttenuation_13 = tmpvar_18;
  mediump float tmpvar_21;
  tmpvar_21 = mix (realtimeShadowAttenuation_13, 1.0, tmpvar_16);
  shadow_5 = tmpvar_21;
  lowp float tmpvar_22;
  highp vec4 tmpvar_23;
  tmpvar_23 = texture2D (_LightTexture0, ((xlv_TEXCOORD3.xy / xlv_TEXCOORD3.w) + 0.5));
  tmpvar_22 = tmpvar_23.w;
  lowp float tmpvar_24;
  highp vec4 tmpvar_25;
  tmpvar_25 = texture2D (_LightTextureB0, vec2(dot (xlv_TEXCOORD3.xyz, xlv_TEXCOORD3.xyz)));
  tmpvar_24 = tmpvar_25.w;
  highp float tmpvar_26;
  tmpvar_26 = (((
    float((xlv_TEXCOORD3.z > 0.0))
   * tmpvar_22) * tmpvar_24) * shadow_5);
  atten_4 = tmpvar_26;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_27;
  mediump vec4 c_28;
  highp vec3 tmpvar_29;
  tmpvar_29 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_30;
  mediump vec3 albedo_31;
  albedo_31 = tmpvar_10;
  mediump vec3 tmpvar_32;
  tmpvar_32 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_31, vec3(_Metallic));
  tmpvar_30 = (albedo_31 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_27 = tmpvar_30;
  mediump vec3 diffColor_33;
  diffColor_33 = tmpvar_27;
  mediump float alpha_34;
  alpha_34 = tmpvar_11;
  tmpvar_27 = diffColor_33;
  mediump vec3 diffColor_35;
  diffColor_35 = tmpvar_27;
  mediump vec3 color_36;
  highp float specularTerm_37;
  highp float a2_38;
  mediump float roughness_39;
  mediump float perceptualRoughness_40;
  highp vec3 tmpvar_41;
  highp vec3 inVec_42;
  inVec_42 = (tmpvar_2 + tmpvar_8);
  tmpvar_41 = (inVec_42 * inversesqrt(max (0.001, 
    dot (inVec_42, inVec_42)
  )));
  mediump float tmpvar_43;
  highp float tmpvar_44;
  tmpvar_44 = clamp (dot (tmpvar_29, tmpvar_2), 0.0, 1.0);
  tmpvar_43 = tmpvar_44;
  highp float tmpvar_45;
  tmpvar_45 = clamp (dot (tmpvar_29, tmpvar_41), 0.0, 1.0);
  highp float tmpvar_46;
  highp float smoothness_47;
  smoothness_47 = _Glossiness;
  tmpvar_46 = (1.0 - smoothness_47);
  perceptualRoughness_40 = tmpvar_46;
  highp float tmpvar_48;
  highp float perceptualRoughness_49;
  perceptualRoughness_49 = perceptualRoughness_40;
  tmpvar_48 = (perceptualRoughness_49 * perceptualRoughness_49);
  roughness_39 = tmpvar_48;
  mediump float tmpvar_50;
  tmpvar_50 = (roughness_39 * roughness_39);
  a2_38 = tmpvar_50;
  specularTerm_37 = ((roughness_39 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_41), 0.0, 1.0)) * (1.5 + roughness_39))
   * 
    (((tmpvar_45 * tmpvar_45) * (a2_38 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_51;
  tmpvar_51 = clamp (specularTerm_37, 0.0, 100.0);
  specularTerm_37 = tmpvar_51;
  highp vec3 tmpvar_52;
  tmpvar_52 = (((diffColor_35 + 
    (tmpvar_51 * tmpvar_32)
  ) * tmpvar_1) * tmpvar_43);
  color_36 = tmpvar_52;
  mediump vec4 tmpvar_53;
  tmpvar_53.w = 1.0;
  tmpvar_53.xyz = color_36;
  c_28.xyz = tmpvar_53.xyz;
  c_28.w = alpha_34;
  c_3.xyz = c_28.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 63 math, 4 textures, 1 branches
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3);
  xlv_TEXCOORD4 = (unity_WorldToShadow[0] * tmpvar_3);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _ShadowMapTexture;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  mediump float realtimeShadowAttenuation_13;
  highp vec4 v_14;
  v_14.x = unity_MatrixV[0].z;
  v_14.y = unity_MatrixV[1].z;
  v_14.z = unity_MatrixV[2].z;
  v_14.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_15;
  tmpvar_15 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = clamp (((
    mix (dot (tmpvar_9, v_14.xyz), sqrt(dot (tmpvar_15, tmpvar_15)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_16 = tmpvar_17;
  realtimeShadowAttenuation_13 = 1.0;
  lowp float tmpvar_18;
  highp vec4 tmpvar_19;
  tmpvar_19 = texture2DProj (_ShadowMapTexture, xlv_TEXCOORD4);
  mediump float tmpvar_20;
  if ((tmpvar_19.x < (xlv_TEXCOORD4.z / xlv_TEXCOORD4.w))) {
    tmpvar_20 = _LightShadowData.x;
  } else {
    tmpvar_20 = 1.0;
  };
  tmpvar_18 = tmpvar_20;
  realtimeShadowAttenuation_13 = tmpvar_18;
  mediump float tmpvar_21;
  tmpvar_21 = mix (realtimeShadowAttenuation_13, 1.0, tmpvar_16);
  shadow_5 = tmpvar_21;
  lowp float tmpvar_22;
  highp vec4 tmpvar_23;
  tmpvar_23 = texture2D (_LightTexture0, ((xlv_TEXCOORD3.xy / xlv_TEXCOORD3.w) + 0.5));
  tmpvar_22 = tmpvar_23.w;
  lowp float tmpvar_24;
  highp vec4 tmpvar_25;
  tmpvar_25 = texture2D (_LightTextureB0, vec2(dot (xlv_TEXCOORD3.xyz, xlv_TEXCOORD3.xyz)));
  tmpvar_24 = tmpvar_25.w;
  highp float tmpvar_26;
  tmpvar_26 = (((
    float((xlv_TEXCOORD3.z > 0.0))
   * tmpvar_22) * tmpvar_24) * shadow_5);
  atten_4 = tmpvar_26;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_27;
  mediump vec4 c_28;
  highp vec3 tmpvar_29;
  tmpvar_29 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_30;
  mediump vec3 albedo_31;
  albedo_31 = tmpvar_10;
  mediump vec3 tmpvar_32;
  tmpvar_32 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_31, vec3(_Metallic));
  tmpvar_30 = (albedo_31 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_27 = tmpvar_30;
  mediump vec3 diffColor_33;
  diffColor_33 = tmpvar_27;
  mediump float alpha_34;
  alpha_34 = tmpvar_11;
  tmpvar_27 = diffColor_33;
  mediump vec3 diffColor_35;
  diffColor_35 = tmpvar_27;
  mediump vec3 color_36;
  highp float specularTerm_37;
  highp float a2_38;
  mediump float roughness_39;
  mediump float perceptualRoughness_40;
  highp vec3 tmpvar_41;
  highp vec3 inVec_42;
  inVec_42 = (tmpvar_2 + tmpvar_8);
  tmpvar_41 = (inVec_42 * inversesqrt(max (0.001, 
    dot (inVec_42, inVec_42)
  )));
  mediump float tmpvar_43;
  highp float tmpvar_44;
  tmpvar_44 = clamp (dot (tmpvar_29, tmpvar_2), 0.0, 1.0);
  tmpvar_43 = tmpvar_44;
  highp float tmpvar_45;
  tmpvar_45 = clamp (dot (tmpvar_29, tmpvar_41), 0.0, 1.0);
  highp float tmpvar_46;
  highp float smoothness_47;
  smoothness_47 = _Glossiness;
  tmpvar_46 = (1.0 - smoothness_47);
  perceptualRoughness_40 = tmpvar_46;
  highp float tmpvar_48;
  highp float perceptualRoughness_49;
  perceptualRoughness_49 = perceptualRoughness_40;
  tmpvar_48 = (perceptualRoughness_49 * perceptualRoughness_49);
  roughness_39 = tmpvar_48;
  mediump float tmpvar_50;
  tmpvar_50 = (roughness_39 * roughness_39);
  a2_38 = tmpvar_50;
  specularTerm_37 = ((roughness_39 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_41), 0.0, 1.0)) * (1.5 + roughness_39))
   * 
    (((tmpvar_45 * tmpvar_45) * (a2_38 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_51;
  tmpvar_51 = clamp (specularTerm_37, 0.0, 100.0);
  specularTerm_37 = tmpvar_51;
  highp vec3 tmpvar_52;
  tmpvar_52 = (((diffColor_35 + 
    (tmpvar_51 * tmpvar_32)
  ) * tmpvar_1) * tmpvar_43);
  color_36 = tmpvar_52;
  mediump vec4 tmpvar_53;
  tmpvar_53.w = 1.0;
  tmpvar_53.xyz = color_36;
  c_28.xyz = tmpvar_53.xyz;
  c_28.w = alpha_34;
  c_3.xyz = c_28.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump float u_xlat16_0;
lowp float u_xlat10_0;
vec3 u_xlat1;
bool u_xlatb1;
mediump vec3 u_xlat16_2;
vec3 u_xlat3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump vec3 u_xlat16_5;
lowp vec3 u_xlat10_5;
mediump float u_xlat16_7;
float u_xlat10;
float u_xlat15;
mediump float u_xlat16_17;
void main()
{
    u_xlat0 = vs_TEXCOORD2.yyyy * hlslcc_mtx4x4unity_WorldToLight[1];
    u_xlat0 = hlslcc_mtx4x4unity_WorldToLight[0] * vs_TEXCOORD2.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_WorldToLight[2] * vs_TEXCOORD2.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_WorldToLight[3];
    u_xlat1.xy = u_xlat0.xy / u_xlat0.ww;
    u_xlat1.xy = u_xlat1.xy + vec2(0.5, 0.5);
    u_xlat15 = texture(_LightTexture0, u_xlat1.xy).w;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0<u_xlat0.z);
#else
    u_xlatb1 = 0.0<u_xlat0.z;
#endif
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = texture(_LightTextureB0, u_xlat0.xx).w;
    u_xlat16_2.x = (u_xlatb1) ? 1.0 : 0.0;
    u_xlat16_2.x = u_xlat15 * u_xlat16_2.x;
    u_xlat16_2.x = u_xlat0.x * u_xlat16_2.x;
    u_xlat0 = vs_TEXCOORD2.yyyy * hlslcc_mtx4x4unity_WorldToShadow[1];
    u_xlat0 = hlslcc_mtx4x4unity_WorldToShadow[0] * vs_TEXCOORD2.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_WorldToShadow[2] * vs_TEXCOORD2.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_WorldToShadow[3];
    u_xlat0.xyz = u_xlat0.xyz / u_xlat0.www;
    vec3 txVec0 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat10_0 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_5.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_0 = u_xlat10_0 * u_xlat16_5.x + _LightShadowData.x;
    u_xlat16_7 = (-u_xlat16_0) + 1.0;
    u_xlat5.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat5.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat5.x = sqrt(u_xlat5.x);
    u_xlat1.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat3.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat10 = dot(u_xlat3.xyz, u_xlat1.xyz);
    u_xlat5.x = (-u_xlat10) + u_xlat5.x;
    u_xlat5.x = unity_ShadowFadeCenterAndType.w * u_xlat5.x + u_xlat10;
    u_xlat5.x = u_xlat5.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat16_7 = u_xlat5.x * u_xlat16_7 + u_xlat16_0;
    u_xlat16_2.x = u_xlat16_7 * u_xlat16_2.x;
    u_xlat16_2.xyz = u_xlat16_2.xxx * _LightColor0.xyz;
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat15 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat1.xyz = vec3(u_xlat15) * vs_TEXCOORD1.xyz;
    u_xlat15 = dot(u_xlat1.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat15 = min(max(u_xlat15, 0.0), 1.0);
#else
    u_xlat15 = clamp(u_xlat15, 0.0, 1.0);
#endif
    u_xlat16_2.xyz = vec3(u_xlat15) * u_xlat16_2.xyz;
    u_xlat15 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat3.xyz = vec3(u_xlat15) * u_xlat3.xyz;
    u_xlat15 = dot(u_xlat3.xyz, u_xlat1.xyz);
    u_xlat15 = u_xlat15 + u_xlat15;
    u_xlat1.xyz = u_xlat1.xyz * (-vec3(u_xlat15)) + u_xlat3.xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat0.xyz);
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.y = (-_Glossiness) + 1.0;
    u_xlat0.x = texture(unity_NHxRoughness, u_xlat0.xy).w;
    u_xlat0.x = u_xlat0.x * 16.0;
    u_xlat10_5.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_5.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_5.xyz = u_xlat10_5.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_4.xyz = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_17 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_4.xyz = u_xlat16_5.xyz * vec3(u_xlat16_17) + u_xlat16_4.xyz;
    SV_Target0.xyz = u_xlat16_2.xyz * u_xlat16_4.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump float u_xlat16_0;
lowp float u_xlat10_0;
vec3 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
bool u_xlatb1;
mediump vec3 u_xlat16_2;
vec3 u_xlat3;
mediump float u_xlat16_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump float u_xlat16_5;
mediump float u_xlat16_7;
float u_xlat10;
float u_xlat15;
mediump float u_xlat16_15;
mediump float u_xlat16_16;
mediump float u_xlat16_17;
void main()
{
    u_xlat0 = vs_TEXCOORD2.yyyy * hlslcc_mtx4x4unity_WorldToLight[1];
    u_xlat0 = hlslcc_mtx4x4unity_WorldToLight[0] * vs_TEXCOORD2.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_WorldToLight[2] * vs_TEXCOORD2.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_WorldToLight[3];
    u_xlat1.xy = u_xlat0.xy / u_xlat0.ww;
    u_xlat1.xy = u_xlat1.xy + vec2(0.5, 0.5);
    u_xlat15 = texture(_LightTexture0, u_xlat1.xy).w;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0<u_xlat0.z);
#else
    u_xlatb1 = 0.0<u_xlat0.z;
#endif
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = texture(_LightTextureB0, u_xlat0.xx).w;
    u_xlat16_2.x = (u_xlatb1) ? 1.0 : 0.0;
    u_xlat16_2.x = u_xlat15 * u_xlat16_2.x;
    u_xlat16_2.x = u_xlat0.x * u_xlat16_2.x;
    u_xlat0 = vs_TEXCOORD2.yyyy * hlslcc_mtx4x4unity_WorldToShadow[1];
    u_xlat0 = hlslcc_mtx4x4unity_WorldToShadow[0] * vs_TEXCOORD2.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_WorldToShadow[2] * vs_TEXCOORD2.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_WorldToShadow[3];
    u_xlat0.xyz = u_xlat0.xyz / u_xlat0.www;
    vec3 txVec0 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat10_0 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_5 = (-_LightShadowData.x) + 1.0;
    u_xlat16_0 = u_xlat10_0 * u_xlat16_5 + _LightShadowData.x;
    u_xlat16_7 = (-u_xlat16_0) + 1.0;
    u_xlat5.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat5.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat5.x = sqrt(u_xlat5.x);
    u_xlat1.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat3.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat10 = dot(u_xlat3.xyz, u_xlat1.xyz);
    u_xlat5.x = (-u_xlat10) + u_xlat5.x;
    u_xlat5.x = unity_ShadowFadeCenterAndType.w * u_xlat5.x + u_xlat10;
    u_xlat5.x = u_xlat5.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat16_7 = u_xlat5.x * u_xlat16_7 + u_xlat16_0;
    u_xlat16_2.x = u_xlat16_7 * u_xlat16_2.x;
    u_xlat16_2.xyz = u_xlat16_2.xxx * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat5.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat1.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat1.x = inversesqrt(u_xlat1.x);
    u_xlat5.xyz = u_xlat5.xyz * u_xlat1.xxx;
    u_xlat1.xyz = u_xlat3.xyz * u_xlat0.xxx + u_xlat5.xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat0.x = max(u_xlat0.x, 0.00100000005);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat1.xyz = u_xlat0.xxx * u_xlat1.xyz;
    u_xlat0.x = dot(u_xlat5.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat0.x = max(u_xlat0.x, 0.319999993);
    u_xlat16_16 = (-_Glossiness) + 1.0;
    u_xlat16_3 = u_xlat16_16 * u_xlat16_16 + 1.5;
    u_xlat16_16 = u_xlat16_16 * u_xlat16_16;
    u_xlat0.x = u_xlat0.x * u_xlat16_3;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat1.x = dot(u_xlat3.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat1.x = min(max(u_xlat1.x, 0.0), 1.0);
#else
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
#endif
    u_xlat5.x = dot(u_xlat3.xyz, u_xlat5.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat10 = u_xlat1.x * u_xlat1.x;
    u_xlat16_15 = u_xlat16_16 * u_xlat16_16 + -1.0;
    u_xlat10 = u_xlat10 * u_xlat16_15 + 1.00001001;
    u_xlat0.x = u_xlat10 * u_xlat0.x;
    u_xlat0.x = u_xlat16_16 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_17 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * vec3(u_xlat16_17) + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat16_2.xyz * u_xlat0.xzw;
    u_xlat0.xyz = u_xlat5.xxx * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump float u_xlat16_0;
lowp float u_xlat10_0;
vec3 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
bool u_xlatb1;
mediump vec3 u_xlat16_2;
vec3 u_xlat3;
mediump float u_xlat16_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump float u_xlat16_5;
mediump float u_xlat16_7;
float u_xlat10;
float u_xlat15;
mediump float u_xlat16_15;
mediump float u_xlat16_16;
mediump float u_xlat16_17;
void main()
{
    u_xlat0 = vs_TEXCOORD2.yyyy * hlslcc_mtx4x4unity_WorldToLight[1];
    u_xlat0 = hlslcc_mtx4x4unity_WorldToLight[0] * vs_TEXCOORD2.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_WorldToLight[2] * vs_TEXCOORD2.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_WorldToLight[3];
    u_xlat1.xy = u_xlat0.xy / u_xlat0.ww;
    u_xlat1.xy = u_xlat1.xy + vec2(0.5, 0.5);
    u_xlat15 = texture(_LightTexture0, u_xlat1.xy).w;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0<u_xlat0.z);
#else
    u_xlatb1 = 0.0<u_xlat0.z;
#endif
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = texture(_LightTextureB0, u_xlat0.xx).w;
    u_xlat16_2.x = (u_xlatb1) ? 1.0 : 0.0;
    u_xlat16_2.x = u_xlat15 * u_xlat16_2.x;
    u_xlat16_2.x = u_xlat0.x * u_xlat16_2.x;
    u_xlat0 = vs_TEXCOORD2.yyyy * hlslcc_mtx4x4unity_WorldToShadow[1];
    u_xlat0 = hlslcc_mtx4x4unity_WorldToShadow[0] * vs_TEXCOORD2.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_WorldToShadow[2] * vs_TEXCOORD2.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_WorldToShadow[3];
    u_xlat0.xyz = u_xlat0.xyz / u_xlat0.www;
    vec3 txVec0 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat10_0 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_5 = (-_LightShadowData.x) + 1.0;
    u_xlat16_0 = u_xlat10_0 * u_xlat16_5 + _LightShadowData.x;
    u_xlat16_7 = (-u_xlat16_0) + 1.0;
    u_xlat5.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat5.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat5.x = sqrt(u_xlat5.x);
    u_xlat1.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat3.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat10 = dot(u_xlat3.xyz, u_xlat1.xyz);
    u_xlat5.x = (-u_xlat10) + u_xlat5.x;
    u_xlat5.x = unity_ShadowFadeCenterAndType.w * u_xlat5.x + u_xlat10;
    u_xlat5.x = u_xlat5.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat16_7 = u_xlat5.x * u_xlat16_7 + u_xlat16_0;
    u_xlat16_2.x = u_xlat16_7 * u_xlat16_2.x;
    u_xlat16_2.xyz = u_xlat16_2.xxx * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat5.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat1.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat1.x = inversesqrt(u_xlat1.x);
    u_xlat5.xyz = u_xlat5.xyz * u_xlat1.xxx;
    u_xlat1.xyz = u_xlat3.xyz * u_xlat0.xxx + u_xlat5.xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat0.x = max(u_xlat0.x, 0.00100000005);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat1.xyz = u_xlat0.xxx * u_xlat1.xyz;
    u_xlat0.x = dot(u_xlat5.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat0.x = max(u_xlat0.x, 0.319999993);
    u_xlat16_16 = (-_Glossiness) + 1.0;
    u_xlat16_3 = u_xlat16_16 * u_xlat16_16 + 1.5;
    u_xlat16_16 = u_xlat16_16 * u_xlat16_16;
    u_xlat0.x = u_xlat0.x * u_xlat16_3;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat1.x = dot(u_xlat3.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat1.x = min(max(u_xlat1.x, 0.0), 1.0);
#else
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
#endif
    u_xlat5.x = dot(u_xlat3.xyz, u_xlat5.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat10 = u_xlat1.x * u_xlat1.x;
    u_xlat16_15 = u_xlat16_16 * u_xlat16_16 + -1.0;
    u_xlat10 = u_xlat10 * u_xlat16_15 + 1.00001001;
    u_xlat0.x = u_xlat10 * u_xlat0.x;
    u_xlat0.x = u_xlat16_16 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_17 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * vec3(u_xlat16_17) + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat16_2.xyz * u_xlat0.xzw;
    u_xlat0.xyz = u_xlat5.xxx * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

//////////////////////////////////////////////////////
Keywords set in this variant: SPOT SHADOWS_DEPTH SHADOWS_SOFT 
-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 61 math, 8 textures, 4 branches
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3);
  xlv_TEXCOORD4 = (unity_WorldToShadow[0] * tmpvar_3);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform highp sampler2D _ShadowMapTexture;
uniform highp vec4 _ShadowOffsets[4];
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  mediump float realtimeShadowAttenuation_13;
  highp vec4 v_14;
  v_14.x = unity_MatrixV[0].z;
  v_14.y = unity_MatrixV[1].z;
  v_14.z = unity_MatrixV[2].z;
  v_14.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_15;
  tmpvar_15 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = clamp (((
    mix (dot (tmpvar_9, v_14.xyz), sqrt(dot (tmpvar_15, tmpvar_15)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_16 = tmpvar_17;
  realtimeShadowAttenuation_13 = 1.0;
  lowp float tmpvar_18;
  highp vec4 shadowVals_19;
  highp vec3 tmpvar_20;
  tmpvar_20 = (xlv_TEXCOORD4.xyz / xlv_TEXCOORD4.w);
  shadowVals_19.x = texture2D (_ShadowMapTexture, (tmpvar_20.xy + _ShadowOffsets[0].xy)).x;
  shadowVals_19.y = texture2D (_ShadowMapTexture, (tmpvar_20.xy + _ShadowOffsets[1].xy)).x;
  shadowVals_19.z = texture2D (_ShadowMapTexture, (tmpvar_20.xy + _ShadowOffsets[2].xy)).x;
  shadowVals_19.w = texture2D (_ShadowMapTexture, (tmpvar_20.xy + _ShadowOffsets[3].xy)).x;
  bvec4 tmpvar_21;
  tmpvar_21 = lessThan (shadowVals_19, tmpvar_20.zzzz);
  mediump vec4 tmpvar_22;
  tmpvar_22 = _LightShadowData.xxxx;
  mediump float tmpvar_23;
  if (tmpvar_21.x) {
    tmpvar_23 = tmpvar_22.x;
  } else {
    tmpvar_23 = 1.0;
  };
  mediump float tmpvar_24;
  if (tmpvar_21.y) {
    tmpvar_24 = tmpvar_22.y;
  } else {
    tmpvar_24 = 1.0;
  };
  mediump float tmpvar_25;
  if (tmpvar_21.z) {
    tmpvar_25 = tmpvar_22.z;
  } else {
    tmpvar_25 = 1.0;
  };
  mediump float tmpvar_26;
  if (tmpvar_21.w) {
    tmpvar_26 = tmpvar_22.w;
  } else {
    tmpvar_26 = 1.0;
  };
  mediump vec4 tmpvar_27;
  tmpvar_27.x = tmpvar_23;
  tmpvar_27.y = tmpvar_24;
  tmpvar_27.z = tmpvar_25;
  tmpvar_27.w = tmpvar_26;
  mediump float tmpvar_28;
  tmpvar_28 = dot (tmpvar_27, vec4(0.25, 0.25, 0.25, 0.25));
  tmpvar_18 = tmpvar_28;
  realtimeShadowAttenuation_13 = tmpvar_18;
  mediump float tmpvar_29;
  tmpvar_29 = mix (realtimeShadowAttenuation_13, 1.0, tmpvar_16);
  shadow_5 = tmpvar_29;
  lowp float tmpvar_30;
  highp vec4 tmpvar_31;
  tmpvar_31 = texture2D (_LightTexture0, ((xlv_TEXCOORD3.xy / xlv_TEXCOORD3.w) + 0.5));
  tmpvar_30 = tmpvar_31.w;
  lowp float tmpvar_32;
  highp vec4 tmpvar_33;
  tmpvar_33 = texture2D (_LightTextureB0, vec2(dot (xlv_TEXCOORD3.xyz, xlv_TEXCOORD3.xyz)));
  tmpvar_32 = tmpvar_33.w;
  highp float tmpvar_34;
  tmpvar_34 = (((
    float((xlv_TEXCOORD3.z > 0.0))
   * tmpvar_30) * tmpvar_32) * shadow_5);
  atten_4 = tmpvar_34;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_35;
  mediump vec4 c_36;
  highp vec3 tmpvar_37;
  tmpvar_37 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_38;
  mediump vec3 albedo_39;
  albedo_39 = tmpvar_10;
  tmpvar_38 = (albedo_39 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_35 = tmpvar_38;
  mediump vec3 diffColor_40;
  diffColor_40 = tmpvar_35;
  mediump float alpha_41;
  alpha_41 = tmpvar_11;
  tmpvar_35 = diffColor_40;
  mediump vec3 diffColor_42;
  diffColor_42 = tmpvar_35;
  mediump vec2 rlPow4AndFresnelTerm_43;
  mediump float tmpvar_44;
  highp float tmpvar_45;
  tmpvar_45 = clamp (dot (tmpvar_37, tmpvar_2), 0.0, 1.0);
  tmpvar_44 = tmpvar_45;
  mediump float tmpvar_46;
  highp float tmpvar_47;
  tmpvar_47 = clamp (dot (tmpvar_37, tmpvar_8), 0.0, 1.0);
  tmpvar_46 = tmpvar_47;
  highp vec2 tmpvar_48;
  tmpvar_48.x = dot ((tmpvar_8 - (2.0 * 
    (dot (tmpvar_37, tmpvar_8) * tmpvar_37)
  )), tmpvar_2);
  tmpvar_48.y = (1.0 - tmpvar_46);
  highp vec2 tmpvar_49;
  tmpvar_49 = ((tmpvar_48 * tmpvar_48) * (tmpvar_48 * tmpvar_48));
  rlPow4AndFresnelTerm_43 = tmpvar_49;
  mediump float tmpvar_50;
  tmpvar_50 = rlPow4AndFresnelTerm_43.x;
  mediump float specular_51;
  highp float smoothness_52;
  smoothness_52 = _Glossiness;
  highp vec2 tmpvar_53;
  tmpvar_53.x = tmpvar_50;
  tmpvar_53.y = (1.0 - smoothness_52);
  highp float tmpvar_54;
  tmpvar_54 = (texture2D (unity_NHxRoughness, tmpvar_53).w * 16.0);
  specular_51 = tmpvar_54;
  mediump vec4 tmpvar_55;
  tmpvar_55.w = 1.0;
  tmpvar_55.xyz = ((diffColor_42 + (specular_51 * 
    mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_39, vec3(_Metallic))
  )) * (tmpvar_1 * tmpvar_44));
  c_36.xyz = tmpvar_55.xyz;
  c_36.w = alpha_41;
  c_3.xyz = c_36.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 71 math, 7 textures, 4 branches
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3);
  xlv_TEXCOORD4 = (unity_WorldToShadow[0] * tmpvar_3);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _ShadowMapTexture;
uniform highp vec4 _ShadowOffsets[4];
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  mediump float realtimeShadowAttenuation_13;
  highp vec4 v_14;
  v_14.x = unity_MatrixV[0].z;
  v_14.y = unity_MatrixV[1].z;
  v_14.z = unity_MatrixV[2].z;
  v_14.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_15;
  tmpvar_15 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = clamp (((
    mix (dot (tmpvar_9, v_14.xyz), sqrt(dot (tmpvar_15, tmpvar_15)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_16 = tmpvar_17;
  realtimeShadowAttenuation_13 = 1.0;
  lowp float tmpvar_18;
  highp vec4 shadowVals_19;
  highp vec3 tmpvar_20;
  tmpvar_20 = (xlv_TEXCOORD4.xyz / xlv_TEXCOORD4.w);
  shadowVals_19.x = texture2D (_ShadowMapTexture, (tmpvar_20.xy + _ShadowOffsets[0].xy)).x;
  shadowVals_19.y = texture2D (_ShadowMapTexture, (tmpvar_20.xy + _ShadowOffsets[1].xy)).x;
  shadowVals_19.z = texture2D (_ShadowMapTexture, (tmpvar_20.xy + _ShadowOffsets[2].xy)).x;
  shadowVals_19.w = texture2D (_ShadowMapTexture, (tmpvar_20.xy + _ShadowOffsets[3].xy)).x;
  bvec4 tmpvar_21;
  tmpvar_21 = lessThan (shadowVals_19, tmpvar_20.zzzz);
  mediump vec4 tmpvar_22;
  tmpvar_22 = _LightShadowData.xxxx;
  mediump float tmpvar_23;
  if (tmpvar_21.x) {
    tmpvar_23 = tmpvar_22.x;
  } else {
    tmpvar_23 = 1.0;
  };
  mediump float tmpvar_24;
  if (tmpvar_21.y) {
    tmpvar_24 = tmpvar_22.y;
  } else {
    tmpvar_24 = 1.0;
  };
  mediump float tmpvar_25;
  if (tmpvar_21.z) {
    tmpvar_25 = tmpvar_22.z;
  } else {
    tmpvar_25 = 1.0;
  };
  mediump float tmpvar_26;
  if (tmpvar_21.w) {
    tmpvar_26 = tmpvar_22.w;
  } else {
    tmpvar_26 = 1.0;
  };
  mediump vec4 tmpvar_27;
  tmpvar_27.x = tmpvar_23;
  tmpvar_27.y = tmpvar_24;
  tmpvar_27.z = tmpvar_25;
  tmpvar_27.w = tmpvar_26;
  mediump float tmpvar_28;
  tmpvar_28 = dot (tmpvar_27, vec4(0.25, 0.25, 0.25, 0.25));
  tmpvar_18 = tmpvar_28;
  realtimeShadowAttenuation_13 = tmpvar_18;
  mediump float tmpvar_29;
  tmpvar_29 = mix (realtimeShadowAttenuation_13, 1.0, tmpvar_16);
  shadow_5 = tmpvar_29;
  lowp float tmpvar_30;
  highp vec4 tmpvar_31;
  tmpvar_31 = texture2D (_LightTexture0, ((xlv_TEXCOORD3.xy / xlv_TEXCOORD3.w) + 0.5));
  tmpvar_30 = tmpvar_31.w;
  lowp float tmpvar_32;
  highp vec4 tmpvar_33;
  tmpvar_33 = texture2D (_LightTextureB0, vec2(dot (xlv_TEXCOORD3.xyz, xlv_TEXCOORD3.xyz)));
  tmpvar_32 = tmpvar_33.w;
  highp float tmpvar_34;
  tmpvar_34 = (((
    float((xlv_TEXCOORD3.z > 0.0))
   * tmpvar_30) * tmpvar_32) * shadow_5);
  atten_4 = tmpvar_34;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_35;
  mediump vec4 c_36;
  highp vec3 tmpvar_37;
  tmpvar_37 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_38;
  mediump vec3 albedo_39;
  albedo_39 = tmpvar_10;
  mediump vec3 tmpvar_40;
  tmpvar_40 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_39, vec3(_Metallic));
  tmpvar_38 = (albedo_39 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_35 = tmpvar_38;
  mediump vec3 diffColor_41;
  diffColor_41 = tmpvar_35;
  mediump float alpha_42;
  alpha_42 = tmpvar_11;
  tmpvar_35 = diffColor_41;
  mediump vec3 diffColor_43;
  diffColor_43 = tmpvar_35;
  mediump vec3 color_44;
  highp float specularTerm_45;
  highp float a2_46;
  mediump float roughness_47;
  mediump float perceptualRoughness_48;
  highp vec3 tmpvar_49;
  highp vec3 inVec_50;
  inVec_50 = (tmpvar_2 + tmpvar_8);
  tmpvar_49 = (inVec_50 * inversesqrt(max (0.001, 
    dot (inVec_50, inVec_50)
  )));
  mediump float tmpvar_51;
  highp float tmpvar_52;
  tmpvar_52 = clamp (dot (tmpvar_37, tmpvar_2), 0.0, 1.0);
  tmpvar_51 = tmpvar_52;
  highp float tmpvar_53;
  tmpvar_53 = clamp (dot (tmpvar_37, tmpvar_49), 0.0, 1.0);
  highp float tmpvar_54;
  highp float smoothness_55;
  smoothness_55 = _Glossiness;
  tmpvar_54 = (1.0 - smoothness_55);
  perceptualRoughness_48 = tmpvar_54;
  highp float tmpvar_56;
  highp float perceptualRoughness_57;
  perceptualRoughness_57 = perceptualRoughness_48;
  tmpvar_56 = (perceptualRoughness_57 * perceptualRoughness_57);
  roughness_47 = tmpvar_56;
  mediump float tmpvar_58;
  tmpvar_58 = (roughness_47 * roughness_47);
  a2_46 = tmpvar_58;
  specularTerm_45 = ((roughness_47 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_49), 0.0, 1.0)) * (1.5 + roughness_47))
   * 
    (((tmpvar_53 * tmpvar_53) * (a2_46 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_59;
  tmpvar_59 = clamp (specularTerm_45, 0.0, 100.0);
  specularTerm_45 = tmpvar_59;
  highp vec3 tmpvar_60;
  tmpvar_60 = (((diffColor_43 + 
    (tmpvar_59 * tmpvar_40)
  ) * tmpvar_1) * tmpvar_51);
  color_44 = tmpvar_60;
  mediump vec4 tmpvar_61;
  tmpvar_61.w = 1.0;
  tmpvar_61.xyz = color_44;
  c_36.xyz = tmpvar_61.xyz;
  c_36.w = alpha_42;
  c_3.xyz = c_36.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 71 math, 7 textures, 4 branches
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3);
  xlv_TEXCOORD4 = (unity_WorldToShadow[0] * tmpvar_3);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _ShadowMapTexture;
uniform highp vec4 _ShadowOffsets[4];
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  mediump float realtimeShadowAttenuation_13;
  highp vec4 v_14;
  v_14.x = unity_MatrixV[0].z;
  v_14.y = unity_MatrixV[1].z;
  v_14.z = unity_MatrixV[2].z;
  v_14.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_15;
  tmpvar_15 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = clamp (((
    mix (dot (tmpvar_9, v_14.xyz), sqrt(dot (tmpvar_15, tmpvar_15)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_16 = tmpvar_17;
  realtimeShadowAttenuation_13 = 1.0;
  lowp float tmpvar_18;
  highp vec4 shadowVals_19;
  highp vec3 tmpvar_20;
  tmpvar_20 = (xlv_TEXCOORD4.xyz / xlv_TEXCOORD4.w);
  shadowVals_19.x = texture2D (_ShadowMapTexture, (tmpvar_20.xy + _ShadowOffsets[0].xy)).x;
  shadowVals_19.y = texture2D (_ShadowMapTexture, (tmpvar_20.xy + _ShadowOffsets[1].xy)).x;
  shadowVals_19.z = texture2D (_ShadowMapTexture, (tmpvar_20.xy + _ShadowOffsets[2].xy)).x;
  shadowVals_19.w = texture2D (_ShadowMapTexture, (tmpvar_20.xy + _ShadowOffsets[3].xy)).x;
  bvec4 tmpvar_21;
  tmpvar_21 = lessThan (shadowVals_19, tmpvar_20.zzzz);
  mediump vec4 tmpvar_22;
  tmpvar_22 = _LightShadowData.xxxx;
  mediump float tmpvar_23;
  if (tmpvar_21.x) {
    tmpvar_23 = tmpvar_22.x;
  } else {
    tmpvar_23 = 1.0;
  };
  mediump float tmpvar_24;
  if (tmpvar_21.y) {
    tmpvar_24 = tmpvar_22.y;
  } else {
    tmpvar_24 = 1.0;
  };
  mediump float tmpvar_25;
  if (tmpvar_21.z) {
    tmpvar_25 = tmpvar_22.z;
  } else {
    tmpvar_25 = 1.0;
  };
  mediump float tmpvar_26;
  if (tmpvar_21.w) {
    tmpvar_26 = tmpvar_22.w;
  } else {
    tmpvar_26 = 1.0;
  };
  mediump vec4 tmpvar_27;
  tmpvar_27.x = tmpvar_23;
  tmpvar_27.y = tmpvar_24;
  tmpvar_27.z = tmpvar_25;
  tmpvar_27.w = tmpvar_26;
  mediump float tmpvar_28;
  tmpvar_28 = dot (tmpvar_27, vec4(0.25, 0.25, 0.25, 0.25));
  tmpvar_18 = tmpvar_28;
  realtimeShadowAttenuation_13 = tmpvar_18;
  mediump float tmpvar_29;
  tmpvar_29 = mix (realtimeShadowAttenuation_13, 1.0, tmpvar_16);
  shadow_5 = tmpvar_29;
  lowp float tmpvar_30;
  highp vec4 tmpvar_31;
  tmpvar_31 = texture2D (_LightTexture0, ((xlv_TEXCOORD3.xy / xlv_TEXCOORD3.w) + 0.5));
  tmpvar_30 = tmpvar_31.w;
  lowp float tmpvar_32;
  highp vec4 tmpvar_33;
  tmpvar_33 = texture2D (_LightTextureB0, vec2(dot (xlv_TEXCOORD3.xyz, xlv_TEXCOORD3.xyz)));
  tmpvar_32 = tmpvar_33.w;
  highp float tmpvar_34;
  tmpvar_34 = (((
    float((xlv_TEXCOORD3.z > 0.0))
   * tmpvar_30) * tmpvar_32) * shadow_5);
  atten_4 = tmpvar_34;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_35;
  mediump vec4 c_36;
  highp vec3 tmpvar_37;
  tmpvar_37 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_38;
  mediump vec3 albedo_39;
  albedo_39 = tmpvar_10;
  mediump vec3 tmpvar_40;
  tmpvar_40 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_39, vec3(_Metallic));
  tmpvar_38 = (albedo_39 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_35 = tmpvar_38;
  mediump vec3 diffColor_41;
  diffColor_41 = tmpvar_35;
  mediump float alpha_42;
  alpha_42 = tmpvar_11;
  tmpvar_35 = diffColor_41;
  mediump vec3 diffColor_43;
  diffColor_43 = tmpvar_35;
  mediump vec3 color_44;
  highp float specularTerm_45;
  highp float a2_46;
  mediump float roughness_47;
  mediump float perceptualRoughness_48;
  highp vec3 tmpvar_49;
  highp vec3 inVec_50;
  inVec_50 = (tmpvar_2 + tmpvar_8);
  tmpvar_49 = (inVec_50 * inversesqrt(max (0.001, 
    dot (inVec_50, inVec_50)
  )));
  mediump float tmpvar_51;
  highp float tmpvar_52;
  tmpvar_52 = clamp (dot (tmpvar_37, tmpvar_2), 0.0, 1.0);
  tmpvar_51 = tmpvar_52;
  highp float tmpvar_53;
  tmpvar_53 = clamp (dot (tmpvar_37, tmpvar_49), 0.0, 1.0);
  highp float tmpvar_54;
  highp float smoothness_55;
  smoothness_55 = _Glossiness;
  tmpvar_54 = (1.0 - smoothness_55);
  perceptualRoughness_48 = tmpvar_54;
  highp float tmpvar_56;
  highp float perceptualRoughness_57;
  perceptualRoughness_57 = perceptualRoughness_48;
  tmpvar_56 = (perceptualRoughness_57 * perceptualRoughness_57);
  roughness_47 = tmpvar_56;
  mediump float tmpvar_58;
  tmpvar_58 = (roughness_47 * roughness_47);
  a2_46 = tmpvar_58;
  specularTerm_45 = ((roughness_47 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_49), 0.0, 1.0)) * (1.5 + roughness_47))
   * 
    (((tmpvar_53 * tmpvar_53) * (a2_46 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_59;
  tmpvar_59 = clamp (specularTerm_45, 0.0, 100.0);
  specularTerm_45 = tmpvar_59;
  highp vec3 tmpvar_60;
  tmpvar_60 = (((diffColor_43 + 
    (tmpvar_59 * tmpvar_40)
  ) * tmpvar_1) * tmpvar_51);
  color_44 = tmpvar_60;
  mediump vec4 tmpvar_61;
  tmpvar_61.w = 1.0;
  tmpvar_61.xyz = color_44;
  c_36.xyz = tmpvar_61.xyz;
  c_36.w = alpha_42;
  c_3.xyz = c_36.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 _ShadowOffsets[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec4 u_xlat1;
bool u_xlatb1;
mediump vec3 u_xlat16_2;
vec3 u_xlat3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump vec3 u_xlat16_5;
lowp vec3 u_xlat10_5;
mediump float u_xlat16_7;
float u_xlat10;
float u_xlat15;
mediump float u_xlat16_17;
void main()
{
    u_xlat0 = vs_TEXCOORD2.yyyy * hlslcc_mtx4x4unity_WorldToLight[1];
    u_xlat0 = hlslcc_mtx4x4unity_WorldToLight[0] * vs_TEXCOORD2.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_WorldToLight[2] * vs_TEXCOORD2.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_WorldToLight[3];
    u_xlat1.xy = u_xlat0.xy / u_xlat0.ww;
    u_xlat1.xy = u_xlat1.xy + vec2(0.5, 0.5);
    u_xlat15 = texture(_LightTexture0, u_xlat1.xy).w;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0<u_xlat0.z);
#else
    u_xlatb1 = 0.0<u_xlat0.z;
#endif
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = texture(_LightTextureB0, u_xlat0.xx).w;
    u_xlat16_2.x = (u_xlatb1) ? 1.0 : 0.0;
    u_xlat16_2.x = u_xlat15 * u_xlat16_2.x;
    u_xlat16_2.x = u_xlat0.x * u_xlat16_2.x;
    u_xlat0 = vs_TEXCOORD2.yyyy * hlslcc_mtx4x4unity_WorldToShadow[1];
    u_xlat0 = hlslcc_mtx4x4unity_WorldToShadow[0] * vs_TEXCOORD2.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_WorldToShadow[2] * vs_TEXCOORD2.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_WorldToShadow[3];
    u_xlat0.xyz = u_xlat0.xyz / u_xlat0.www;
    u_xlat1.xyz = u_xlat0.xyz + _ShadowOffsets[0].xyz;
    vec3 txVec0 = vec3(u_xlat1.xy,u_xlat1.z);
    u_xlat1.x = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat3.xyz = u_xlat0.xyz + _ShadowOffsets[1].xyz;
    vec3 txVec1 = vec3(u_xlat3.xy,u_xlat3.z);
    u_xlat1.y = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec1, 0.0);
    u_xlat3.xyz = u_xlat0.xyz + _ShadowOffsets[2].xyz;
    u_xlat0.xyz = u_xlat0.xyz + _ShadowOffsets[3].xyz;
    vec3 txVec2 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat1.w = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec2, 0.0);
    vec3 txVec3 = vec3(u_xlat3.xy,u_xlat3.z);
    u_xlat1.z = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec3, 0.0);
    u_xlat0.x = dot(u_xlat1, vec4(0.25, 0.25, 0.25, 0.25));
    u_xlat16_5.x = (-_LightShadowData.x) + 1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_5.x + _LightShadowData.x;
    u_xlat16_7 = (-u_xlat0.x) + 1.0;
    u_xlat5.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat5.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat5.x = sqrt(u_xlat5.x);
    u_xlat1.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat3.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat10 = dot(u_xlat3.xyz, u_xlat1.xyz);
    u_xlat5.x = (-u_xlat10) + u_xlat5.x;
    u_xlat5.x = unity_ShadowFadeCenterAndType.w * u_xlat5.x + u_xlat10;
    u_xlat5.x = u_xlat5.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat16_7 = u_xlat5.x * u_xlat16_7 + u_xlat0.x;
    u_xlat16_2.x = u_xlat16_7 * u_xlat16_2.x;
    u_xlat16_2.xyz = u_xlat16_2.xxx * _LightColor0.xyz;
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat15 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat1.xyz = vec3(u_xlat15) * vs_TEXCOORD1.xyz;
    u_xlat15 = dot(u_xlat1.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat15 = min(max(u_xlat15, 0.0), 1.0);
#else
    u_xlat15 = clamp(u_xlat15, 0.0, 1.0);
#endif
    u_xlat16_2.xyz = vec3(u_xlat15) * u_xlat16_2.xyz;
    u_xlat15 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat3.xyz = vec3(u_xlat15) * u_xlat3.xyz;
    u_xlat15 = dot(u_xlat3.xyz, u_xlat1.xyz);
    u_xlat15 = u_xlat15 + u_xlat15;
    u_xlat1.xyz = u_xlat1.xyz * (-vec3(u_xlat15)) + u_xlat3.xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat0.xyz);
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.y = (-_Glossiness) + 1.0;
    u_xlat0.x = texture(unity_NHxRoughness, u_xlat0.xy).w;
    u_xlat0.x = u_xlat0.x * 16.0;
    u_xlat10_5.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_5.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_5.xyz = u_xlat10_5.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_4.xyz = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_17 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_4.xyz = u_xlat16_5.xyz * vec3(u_xlat16_17) + u_xlat16_4.xyz;
    SV_Target0.xyz = u_xlat16_2.xyz * u_xlat16_4.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 _ShadowOffsets[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec4 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
bool u_xlatb1;
mediump vec3 u_xlat16_2;
vec3 u_xlat3;
mediump float u_xlat16_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump float u_xlat16_5;
mediump float u_xlat16_7;
float u_xlat10;
float u_xlat15;
mediump float u_xlat16_15;
mediump float u_xlat16_16;
mediump float u_xlat16_17;
void main()
{
    u_xlat0 = vs_TEXCOORD2.yyyy * hlslcc_mtx4x4unity_WorldToLight[1];
    u_xlat0 = hlslcc_mtx4x4unity_WorldToLight[0] * vs_TEXCOORD2.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_WorldToLight[2] * vs_TEXCOORD2.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_WorldToLight[3];
    u_xlat1.xy = u_xlat0.xy / u_xlat0.ww;
    u_xlat1.xy = u_xlat1.xy + vec2(0.5, 0.5);
    u_xlat15 = texture(_LightTexture0, u_xlat1.xy).w;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0<u_xlat0.z);
#else
    u_xlatb1 = 0.0<u_xlat0.z;
#endif
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = texture(_LightTextureB0, u_xlat0.xx).w;
    u_xlat16_2.x = (u_xlatb1) ? 1.0 : 0.0;
    u_xlat16_2.x = u_xlat15 * u_xlat16_2.x;
    u_xlat16_2.x = u_xlat0.x * u_xlat16_2.x;
    u_xlat0 = vs_TEXCOORD2.yyyy * hlslcc_mtx4x4unity_WorldToShadow[1];
    u_xlat0 = hlslcc_mtx4x4unity_WorldToShadow[0] * vs_TEXCOORD2.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_WorldToShadow[2] * vs_TEXCOORD2.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_WorldToShadow[3];
    u_xlat0.xyz = u_xlat0.xyz / u_xlat0.www;
    u_xlat1.xyz = u_xlat0.xyz + _ShadowOffsets[0].xyz;
    vec3 txVec0 = vec3(u_xlat1.xy,u_xlat1.z);
    u_xlat1.x = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat3.xyz = u_xlat0.xyz + _ShadowOffsets[1].xyz;
    vec3 txVec1 = vec3(u_xlat3.xy,u_xlat3.z);
    u_xlat1.y = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec1, 0.0);
    u_xlat3.xyz = u_xlat0.xyz + _ShadowOffsets[2].xyz;
    u_xlat0.xyz = u_xlat0.xyz + _ShadowOffsets[3].xyz;
    vec3 txVec2 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat1.w = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec2, 0.0);
    vec3 txVec3 = vec3(u_xlat3.xy,u_xlat3.z);
    u_xlat1.z = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec3, 0.0);
    u_xlat0.x = dot(u_xlat1, vec4(0.25, 0.25, 0.25, 0.25));
    u_xlat16_5 = (-_LightShadowData.x) + 1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_5 + _LightShadowData.x;
    u_xlat16_7 = (-u_xlat0.x) + 1.0;
    u_xlat5.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat5.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat5.x = sqrt(u_xlat5.x);
    u_xlat1.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat3.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat10 = dot(u_xlat3.xyz, u_xlat1.xyz);
    u_xlat5.x = (-u_xlat10) + u_xlat5.x;
    u_xlat5.x = unity_ShadowFadeCenterAndType.w * u_xlat5.x + u_xlat10;
    u_xlat5.x = u_xlat5.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat16_7 = u_xlat5.x * u_xlat16_7 + u_xlat0.x;
    u_xlat16_2.x = u_xlat16_7 * u_xlat16_2.x;
    u_xlat16_2.xyz = u_xlat16_2.xxx * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat5.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat1.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat1.x = inversesqrt(u_xlat1.x);
    u_xlat5.xyz = u_xlat5.xyz * u_xlat1.xxx;
    u_xlat1.xyz = u_xlat3.xyz * u_xlat0.xxx + u_xlat5.xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat0.x = max(u_xlat0.x, 0.00100000005);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat1.xyz = u_xlat0.xxx * u_xlat1.xyz;
    u_xlat0.x = dot(u_xlat5.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat0.x = max(u_xlat0.x, 0.319999993);
    u_xlat16_16 = (-_Glossiness) + 1.0;
    u_xlat16_3 = u_xlat16_16 * u_xlat16_16 + 1.5;
    u_xlat16_16 = u_xlat16_16 * u_xlat16_16;
    u_xlat0.x = u_xlat0.x * u_xlat16_3;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat1.x = dot(u_xlat3.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat1.x = min(max(u_xlat1.x, 0.0), 1.0);
#else
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
#endif
    u_xlat5.x = dot(u_xlat3.xyz, u_xlat5.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat10 = u_xlat1.x * u_xlat1.x;
    u_xlat16_15 = u_xlat16_16 * u_xlat16_16 + -1.0;
    u_xlat10 = u_xlat10 * u_xlat16_15 + 1.00001001;
    u_xlat0.x = u_xlat10 * u_xlat0.x;
    u_xlat0.x = u_xlat16_16 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_17 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * vec3(u_xlat16_17) + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat16_2.xyz * u_xlat0.xzw;
    u_xlat0.xyz = u_xlat5.xxx * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 _ShadowOffsets[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec4 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
bool u_xlatb1;
mediump vec3 u_xlat16_2;
vec3 u_xlat3;
mediump float u_xlat16_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump float u_xlat16_5;
mediump float u_xlat16_7;
float u_xlat10;
float u_xlat15;
mediump float u_xlat16_15;
mediump float u_xlat16_16;
mediump float u_xlat16_17;
void main()
{
    u_xlat0 = vs_TEXCOORD2.yyyy * hlslcc_mtx4x4unity_WorldToLight[1];
    u_xlat0 = hlslcc_mtx4x4unity_WorldToLight[0] * vs_TEXCOORD2.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_WorldToLight[2] * vs_TEXCOORD2.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_WorldToLight[3];
    u_xlat1.xy = u_xlat0.xy / u_xlat0.ww;
    u_xlat1.xy = u_xlat1.xy + vec2(0.5, 0.5);
    u_xlat15 = texture(_LightTexture0, u_xlat1.xy).w;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(0.0<u_xlat0.z);
#else
    u_xlatb1 = 0.0<u_xlat0.z;
#endif
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = texture(_LightTextureB0, u_xlat0.xx).w;
    u_xlat16_2.x = (u_xlatb1) ? 1.0 : 0.0;
    u_xlat16_2.x = u_xlat15 * u_xlat16_2.x;
    u_xlat16_2.x = u_xlat0.x * u_xlat16_2.x;
    u_xlat0 = vs_TEXCOORD2.yyyy * hlslcc_mtx4x4unity_WorldToShadow[1];
    u_xlat0 = hlslcc_mtx4x4unity_WorldToShadow[0] * vs_TEXCOORD2.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_WorldToShadow[2] * vs_TEXCOORD2.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_WorldToShadow[3];
    u_xlat0.xyz = u_xlat0.xyz / u_xlat0.www;
    u_xlat1.xyz = u_xlat0.xyz + _ShadowOffsets[0].xyz;
    vec3 txVec0 = vec3(u_xlat1.xy,u_xlat1.z);
    u_xlat1.x = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat3.xyz = u_xlat0.xyz + _ShadowOffsets[1].xyz;
    vec3 txVec1 = vec3(u_xlat3.xy,u_xlat3.z);
    u_xlat1.y = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec1, 0.0);
    u_xlat3.xyz = u_xlat0.xyz + _ShadowOffsets[2].xyz;
    u_xlat0.xyz = u_xlat0.xyz + _ShadowOffsets[3].xyz;
    vec3 txVec2 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat1.w = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec2, 0.0);
    vec3 txVec3 = vec3(u_xlat3.xy,u_xlat3.z);
    u_xlat1.z = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec3, 0.0);
    u_xlat0.x = dot(u_xlat1, vec4(0.25, 0.25, 0.25, 0.25));
    u_xlat16_5 = (-_LightShadowData.x) + 1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_5 + _LightShadowData.x;
    u_xlat16_7 = (-u_xlat0.x) + 1.0;
    u_xlat5.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat5.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat5.x = sqrt(u_xlat5.x);
    u_xlat1.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat3.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat10 = dot(u_xlat3.xyz, u_xlat1.xyz);
    u_xlat5.x = (-u_xlat10) + u_xlat5.x;
    u_xlat5.x = unity_ShadowFadeCenterAndType.w * u_xlat5.x + u_xlat10;
    u_xlat5.x = u_xlat5.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat16_7 = u_xlat5.x * u_xlat16_7 + u_xlat0.x;
    u_xlat16_2.x = u_xlat16_7 * u_xlat16_2.x;
    u_xlat16_2.xyz = u_xlat16_2.xxx * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat5.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat1.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat1.x = inversesqrt(u_xlat1.x);
    u_xlat5.xyz = u_xlat5.xyz * u_xlat1.xxx;
    u_xlat1.xyz = u_xlat3.xyz * u_xlat0.xxx + u_xlat5.xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat0.x = max(u_xlat0.x, 0.00100000005);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat1.xyz = u_xlat0.xxx * u_xlat1.xyz;
    u_xlat0.x = dot(u_xlat5.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat0.x = max(u_xlat0.x, 0.319999993);
    u_xlat16_16 = (-_Glossiness) + 1.0;
    u_xlat16_3 = u_xlat16_16 * u_xlat16_16 + 1.5;
    u_xlat16_16 = u_xlat16_16 * u_xlat16_16;
    u_xlat0.x = u_xlat0.x * u_xlat16_3;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat1.x = dot(u_xlat3.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat1.x = min(max(u_xlat1.x, 0.0), 1.0);
#else
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
#endif
    u_xlat5.x = dot(u_xlat3.xyz, u_xlat5.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat10 = u_xlat1.x * u_xlat1.x;
    u_xlat16_15 = u_xlat16_16 * u_xlat16_16 + -1.0;
    u_xlat10 = u_xlat10 * u_xlat16_15 + 1.00001001;
    u_xlat0.x = u_xlat10 * u_xlat0.x;
    u_xlat0.x = u_xlat16_16 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_17 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * vec3(u_xlat16_17) + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat16_2.xyz * u_xlat0.xzw;
    u_xlat0.xyz = u_xlat5.xxx * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 42 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD4 = (unity_WorldToShadow[0] * tmpvar_3);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform highp sampler2D _ShadowMapTexture;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp vec3 lightDir_5;
  mediump vec3 tmpvar_6;
  tmpvar_6 = _WorldSpaceLightPos0.xyz;
  lightDir_5 = tmpvar_6;
  highp vec3 tmpvar_7;
  highp vec3 tmpvar_8;
  tmpvar_8 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_7 = normalize(tmpvar_8);
  lowp vec3 tmpvar_9;
  lowp float tmpvar_10;
  lowp vec4 tmpvar_11;
  tmpvar_11 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_9 = tmpvar_11.xyz;
  tmpvar_10 = tmpvar_11.w;
  mediump float realtimeShadowAttenuation_12;
  highp vec4 v_13;
  v_13.x = unity_MatrixV[0].z;
  v_13.y = unity_MatrixV[1].z;
  v_13.z = unity_MatrixV[2].z;
  v_13.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_14;
  tmpvar_14 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_15;
  highp float tmpvar_16;
  tmpvar_16 = clamp (((
    mix (dot (tmpvar_8, v_13.xyz), sqrt(dot (tmpvar_14, tmpvar_14)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_15 = tmpvar_16;
  lowp float tmpvar_17;
  highp float lightShadowDataX_18;
  mediump float tmpvar_19;
  tmpvar_19 = _LightShadowData.x;
  lightShadowDataX_18 = tmpvar_19;
  highp float tmpvar_20;
  tmpvar_20 = max (float((texture2D (_ShadowMapTexture, xlv_TEXCOORD4.xy).x > xlv_TEXCOORD4.z)), lightShadowDataX_18);
  tmpvar_17 = tmpvar_20;
  realtimeShadowAttenuation_12 = tmpvar_17;
  mediump float tmpvar_21;
  tmpvar_21 = mix (realtimeShadowAttenuation_12, 1.0, tmpvar_15);
  atten_4 = tmpvar_21;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_5;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_22;
  mediump vec4 c_23;
  highp vec3 tmpvar_24;
  tmpvar_24 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_25;
  mediump vec3 albedo_26;
  albedo_26 = tmpvar_9;
  tmpvar_25 = (albedo_26 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_22 = tmpvar_25;
  mediump vec3 diffColor_27;
  diffColor_27 = tmpvar_22;
  mediump float alpha_28;
  alpha_28 = tmpvar_10;
  tmpvar_22 = diffColor_27;
  mediump vec3 diffColor_29;
  diffColor_29 = tmpvar_22;
  mediump vec2 rlPow4AndFresnelTerm_30;
  mediump float tmpvar_31;
  highp float tmpvar_32;
  tmpvar_32 = clamp (dot (tmpvar_24, tmpvar_2), 0.0, 1.0);
  tmpvar_31 = tmpvar_32;
  mediump float tmpvar_33;
  highp float tmpvar_34;
  tmpvar_34 = clamp (dot (tmpvar_24, tmpvar_7), 0.0, 1.0);
  tmpvar_33 = tmpvar_34;
  highp vec2 tmpvar_35;
  tmpvar_35.x = dot ((tmpvar_7 - (2.0 * 
    (dot (tmpvar_24, tmpvar_7) * tmpvar_24)
  )), tmpvar_2);
  tmpvar_35.y = (1.0 - tmpvar_33);
  highp vec2 tmpvar_36;
  tmpvar_36 = ((tmpvar_35 * tmpvar_35) * (tmpvar_35 * tmpvar_35));
  rlPow4AndFresnelTerm_30 = tmpvar_36;
  mediump float tmpvar_37;
  tmpvar_37 = rlPow4AndFresnelTerm_30.x;
  mediump float specular_38;
  highp float smoothness_39;
  smoothness_39 = _Glossiness;
  highp vec2 tmpvar_40;
  tmpvar_40.x = tmpvar_37;
  tmpvar_40.y = (1.0 - smoothness_39);
  highp float tmpvar_41;
  tmpvar_41 = (texture2D (unity_NHxRoughness, tmpvar_40).w * 16.0);
  specular_38 = tmpvar_41;
  mediump vec4 tmpvar_42;
  tmpvar_42.w = 1.0;
  tmpvar_42.xyz = ((diffColor_29 + (specular_38 * 
    mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_26, vec3(_Metallic))
  )) * (tmpvar_1 * tmpvar_31));
  c_23.xyz = tmpvar_42.xyz;
  c_23.w = alpha_28;
  c_3.xyz = c_23.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 52 math, 2 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD4 = (unity_WorldToShadow[0] * tmpvar_3);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _ShadowMapTexture;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp vec3 lightDir_5;
  mediump vec3 tmpvar_6;
  tmpvar_6 = _WorldSpaceLightPos0.xyz;
  lightDir_5 = tmpvar_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  lowp vec3 tmpvar_8;
  lowp float tmpvar_9;
  lowp vec4 tmpvar_10;
  tmpvar_10 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_8 = tmpvar_10.xyz;
  tmpvar_9 = tmpvar_10.w;
  mediump float realtimeShadowAttenuation_11;
  highp vec4 v_12;
  v_12.x = unity_MatrixV[0].z;
  v_12.y = unity_MatrixV[1].z;
  v_12.z = unity_MatrixV[2].z;
  v_12.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_13;
  tmpvar_13 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_14;
  highp float tmpvar_15;
  tmpvar_15 = clamp (((
    mix (dot (tmpvar_7, v_12.xyz), sqrt(dot (tmpvar_13, tmpvar_13)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_14 = tmpvar_15;
  lowp float tmpvar_16;
  highp float lightShadowDataX_17;
  mediump float tmpvar_18;
  tmpvar_18 = _LightShadowData.x;
  lightShadowDataX_17 = tmpvar_18;
  highp float tmpvar_19;
  tmpvar_19 = max (float((texture2D (_ShadowMapTexture, xlv_TEXCOORD4.xy).x > xlv_TEXCOORD4.z)), lightShadowDataX_17);
  tmpvar_16 = tmpvar_19;
  realtimeShadowAttenuation_11 = tmpvar_16;
  mediump float tmpvar_20;
  tmpvar_20 = mix (realtimeShadowAttenuation_11, 1.0, tmpvar_14);
  atten_4 = tmpvar_20;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_5;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_21;
  mediump vec4 c_22;
  highp vec3 tmpvar_23;
  tmpvar_23 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_24;
  mediump vec3 albedo_25;
  albedo_25 = tmpvar_8;
  mediump vec3 tmpvar_26;
  tmpvar_26 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_25, vec3(_Metallic));
  tmpvar_24 = (albedo_25 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_21 = tmpvar_24;
  mediump vec3 diffColor_27;
  diffColor_27 = tmpvar_21;
  mediump float alpha_28;
  alpha_28 = tmpvar_9;
  tmpvar_21 = diffColor_27;
  mediump vec3 diffColor_29;
  diffColor_29 = tmpvar_21;
  mediump vec3 color_30;
  highp float specularTerm_31;
  highp float a2_32;
  mediump float roughness_33;
  mediump float perceptualRoughness_34;
  highp vec3 tmpvar_35;
  highp vec3 inVec_36;
  inVec_36 = (tmpvar_2 + normalize(tmpvar_7));
  tmpvar_35 = (inVec_36 * inversesqrt(max (0.001, 
    dot (inVec_36, inVec_36)
  )));
  mediump float tmpvar_37;
  highp float tmpvar_38;
  tmpvar_38 = clamp (dot (tmpvar_23, tmpvar_2), 0.0, 1.0);
  tmpvar_37 = tmpvar_38;
  highp float tmpvar_39;
  tmpvar_39 = clamp (dot (tmpvar_23, tmpvar_35), 0.0, 1.0);
  highp float tmpvar_40;
  highp float smoothness_41;
  smoothness_41 = _Glossiness;
  tmpvar_40 = (1.0 - smoothness_41);
  perceptualRoughness_34 = tmpvar_40;
  highp float tmpvar_42;
  highp float perceptualRoughness_43;
  perceptualRoughness_43 = perceptualRoughness_34;
  tmpvar_42 = (perceptualRoughness_43 * perceptualRoughness_43);
  roughness_33 = tmpvar_42;
  mediump float tmpvar_44;
  tmpvar_44 = (roughness_33 * roughness_33);
  a2_32 = tmpvar_44;
  specularTerm_31 = ((roughness_33 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_35), 0.0, 1.0)) * (1.5 + roughness_33))
   * 
    (((tmpvar_39 * tmpvar_39) * (a2_32 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_45;
  tmpvar_45 = clamp (specularTerm_31, 0.0, 100.0);
  specularTerm_31 = tmpvar_45;
  highp vec3 tmpvar_46;
  tmpvar_46 = (((diffColor_29 + 
    (tmpvar_45 * tmpvar_26)
  ) * tmpvar_1) * tmpvar_37);
  color_30 = tmpvar_46;
  mediump vec4 tmpvar_47;
  tmpvar_47.w = 1.0;
  tmpvar_47.xyz = color_30;
  c_22.xyz = tmpvar_47.xyz;
  c_22.w = alpha_28;
  c_3.xyz = c_22.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 52 math, 2 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD4 = (unity_WorldToShadow[0] * tmpvar_3);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _ShadowMapTexture;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp vec3 lightDir_5;
  mediump vec3 tmpvar_6;
  tmpvar_6 = _WorldSpaceLightPos0.xyz;
  lightDir_5 = tmpvar_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  lowp vec3 tmpvar_8;
  lowp float tmpvar_9;
  lowp vec4 tmpvar_10;
  tmpvar_10 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_8 = tmpvar_10.xyz;
  tmpvar_9 = tmpvar_10.w;
  mediump float realtimeShadowAttenuation_11;
  highp vec4 v_12;
  v_12.x = unity_MatrixV[0].z;
  v_12.y = unity_MatrixV[1].z;
  v_12.z = unity_MatrixV[2].z;
  v_12.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_13;
  tmpvar_13 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_14;
  highp float tmpvar_15;
  tmpvar_15 = clamp (((
    mix (dot (tmpvar_7, v_12.xyz), sqrt(dot (tmpvar_13, tmpvar_13)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_14 = tmpvar_15;
  lowp float tmpvar_16;
  highp float lightShadowDataX_17;
  mediump float tmpvar_18;
  tmpvar_18 = _LightShadowData.x;
  lightShadowDataX_17 = tmpvar_18;
  highp float tmpvar_19;
  tmpvar_19 = max (float((texture2D (_ShadowMapTexture, xlv_TEXCOORD4.xy).x > xlv_TEXCOORD4.z)), lightShadowDataX_17);
  tmpvar_16 = tmpvar_19;
  realtimeShadowAttenuation_11 = tmpvar_16;
  mediump float tmpvar_20;
  tmpvar_20 = mix (realtimeShadowAttenuation_11, 1.0, tmpvar_14);
  atten_4 = tmpvar_20;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_5;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_21;
  mediump vec4 c_22;
  highp vec3 tmpvar_23;
  tmpvar_23 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_24;
  mediump vec3 albedo_25;
  albedo_25 = tmpvar_8;
  mediump vec3 tmpvar_26;
  tmpvar_26 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_25, vec3(_Metallic));
  tmpvar_24 = (albedo_25 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_21 = tmpvar_24;
  mediump vec3 diffColor_27;
  diffColor_27 = tmpvar_21;
  mediump float alpha_28;
  alpha_28 = tmpvar_9;
  tmpvar_21 = diffColor_27;
  mediump vec3 diffColor_29;
  diffColor_29 = tmpvar_21;
  mediump vec3 color_30;
  highp float specularTerm_31;
  highp float a2_32;
  mediump float roughness_33;
  mediump float perceptualRoughness_34;
  highp vec3 tmpvar_35;
  highp vec3 inVec_36;
  inVec_36 = (tmpvar_2 + normalize(tmpvar_7));
  tmpvar_35 = (inVec_36 * inversesqrt(max (0.001, 
    dot (inVec_36, inVec_36)
  )));
  mediump float tmpvar_37;
  highp float tmpvar_38;
  tmpvar_38 = clamp (dot (tmpvar_23, tmpvar_2), 0.0, 1.0);
  tmpvar_37 = tmpvar_38;
  highp float tmpvar_39;
  tmpvar_39 = clamp (dot (tmpvar_23, tmpvar_35), 0.0, 1.0);
  highp float tmpvar_40;
  highp float smoothness_41;
  smoothness_41 = _Glossiness;
  tmpvar_40 = (1.0 - smoothness_41);
  perceptualRoughness_34 = tmpvar_40;
  highp float tmpvar_42;
  highp float perceptualRoughness_43;
  perceptualRoughness_43 = perceptualRoughness_34;
  tmpvar_42 = (perceptualRoughness_43 * perceptualRoughness_43);
  roughness_33 = tmpvar_42;
  mediump float tmpvar_44;
  tmpvar_44 = (roughness_33 * roughness_33);
  a2_32 = tmpvar_44;
  specularTerm_31 = ((roughness_33 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_35), 0.0, 1.0)) * (1.5 + roughness_33))
   * 
    (((tmpvar_39 * tmpvar_39) * (a2_32 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_45;
  tmpvar_45 = clamp (specularTerm_31, 0.0, 100.0);
  specularTerm_31 = tmpvar_45;
  highp vec3 tmpvar_46;
  tmpvar_46 = (((diffColor_29 + 
    (tmpvar_45 * tmpvar_26)
  ) * tmpvar_1) * tmpvar_37);
  color_30 = tmpvar_46;
  mediump vec4 tmpvar_47;
  tmpvar_47.w = 1.0;
  tmpvar_47.xyz = color_30;
  c_22.xyz = tmpvar_47.xyz;
  c_22.w = alpha_28;
  c_3.xyz = c_22.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec3 u_xlat1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
float u_xlat5;
lowp float u_xlat10_5;
mediump vec3 u_xlat16_6;
lowp vec3 u_xlat10_6;
float u_xlat15;
mediump float u_xlat16_18;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat1.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat15 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat2.xyz = vec3(u_xlat15) * vs_TEXCOORD1.xyz;
    u_xlat15 = dot(u_xlat1.xyz, u_xlat2.xyz);
    u_xlat15 = u_xlat15 + u_xlat15;
    u_xlat1.xyz = u_xlat2.xyz * (-vec3(u_xlat15)) + u_xlat1.xyz;
    u_xlat15 = dot(u_xlat2.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat15 = min(max(u_xlat15, 0.0), 1.0);
#else
    u_xlat15 = clamp(u_xlat15, 0.0, 1.0);
#endif
    u_xlat1.x = dot(u_xlat1.xyz, _WorldSpaceLightPos0.xyz);
    u_xlat1.x = u_xlat1.x * u_xlat1.x;
    u_xlat1.x = u_xlat1.x * u_xlat1.x;
    u_xlat1.y = (-_Glossiness) + 1.0;
    u_xlat1.x = texture(unity_NHxRoughness, u_xlat1.xy).w;
    u_xlat1.x = u_xlat1.x * 16.0;
    u_xlat10_6.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_6.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_6.xyz = u_xlat10_6.xyz * _Color.xyz;
    u_xlat16_3.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_3.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_3.xyz = u_xlat1.xxx * u_xlat16_3.xyz;
    u_xlat16_18 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_3.xyz = u_xlat16_6.xyz * vec3(u_xlat16_18) + u_xlat16_3.xyz;
    u_xlat1.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat1.xyz);
    u_xlat1.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat5 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat5 = sqrt(u_xlat5);
    u_xlat5 = (-u_xlat0.x) + u_xlat5;
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat5 + u_xlat0.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat1.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * vs_TEXCOORD2.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * vs_TEXCOORD2.zzz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    vec3 txVec0 = vec3(u_xlat1.xy,u_xlat1.z);
    u_xlat10_5 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_18 = (-_LightShadowData.x) + 1.0;
    u_xlat16_18 = u_xlat10_5 * u_xlat16_18 + _LightShadowData.x;
    u_xlat16_4.x = (-u_xlat16_18) + 1.0;
    u_xlat16_18 = u_xlat0.x * u_xlat16_4.x + u_xlat16_18;
    u_xlat16_4.xyz = vec3(u_xlat16_18) * _LightColor0.xyz;
    u_xlat16_4.xyz = vec3(u_xlat15) * u_xlat16_4.xyz;
    SV_Target0.xyz = u_xlat16_3.xyz * u_xlat16_4.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec4 u_xlat1;
mediump vec4 u_xlat16_1;
lowp vec4 u_xlat10_1;
vec3 u_xlat2;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
vec3 u_xlat4;
lowp float u_xlat10_4;
float u_xlat5;
mediump float u_xlat16_7;
mediump float u_xlat16_9;
float u_xlat12;
mediump float u_xlat16_13;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = u_xlat0.xyz * vec3(u_xlat12) + _WorldSpaceLightPos0.xyz;
    u_xlat12 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat12 = max(u_xlat12, 0.00100000005);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = vec3(u_xlat12) * u_xlat1.xyz;
    u_xlat12 = dot(_WorldSpaceLightPos0.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat12 = min(max(u_xlat12, 0.0), 1.0);
#else
    u_xlat12 = clamp(u_xlat12, 0.0, 1.0);
#endif
    u_xlat12 = max(u_xlat12, 0.319999993);
    u_xlat16_13 = (-_Glossiness) + 1.0;
    u_xlat16_2 = u_xlat16_13 * u_xlat16_13 + 1.5;
    u_xlat16_13 = u_xlat16_13 * u_xlat16_13;
    u_xlat12 = u_xlat12 * u_xlat16_2;
    u_xlat2.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat2.x = inversesqrt(u_xlat2.x);
    u_xlat2.xyz = u_xlat2.xxx * vs_TEXCOORD1.xyz;
    u_xlat1.x = dot(u_xlat2.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat1.x = min(max(u_xlat1.x, 0.0), 1.0);
#else
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
#endif
    u_xlat5 = dot(u_xlat2.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5 = min(max(u_xlat5, 0.0), 1.0);
#else
    u_xlat5 = clamp(u_xlat5, 0.0, 1.0);
#endif
    u_xlat1.x = u_xlat1.x * u_xlat1.x;
    u_xlat16_9 = u_xlat16_13 * u_xlat16_13 + -1.0;
    u_xlat1.x = u_xlat1.x * u_xlat16_9 + 1.00001001;
    u_xlat12 = u_xlat12 * u_xlat1.x;
    u_xlat12 = u_xlat16_13 / u_xlat12;
    u_xlat12 = u_xlat12 + -9.99999975e-05;
    u_xlat12 = max(u_xlat12, 0.0);
    u_xlat12 = min(u_xlat12, 100.0);
    u_xlat10_1.xzw = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_1.xzw * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xzw = u_xlat10_1.xzw * _Color.xyz;
    u_xlat16_3.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_3.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat2.xyz = vec3(u_xlat12) * u_xlat16_3.xyz;
    u_xlat16_3.x = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat1.xzw = u_xlat16_1.xzw * u_xlat16_3.xxx + u_xlat2.xyz;
    u_xlat2.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat2.xyz);
    u_xlat4.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat4.x = dot(u_xlat4.xyz, u_xlat4.xyz);
    u_xlat4.x = sqrt(u_xlat4.x);
    u_xlat4.x = (-u_xlat0.x) + u_xlat4.x;
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat4.x + u_xlat0.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat4.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat4.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * vs_TEXCOORD2.xxx + u_xlat4.xyz;
    u_xlat4.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * vs_TEXCOORD2.zzz + u_xlat4.xyz;
    u_xlat4.xyz = u_xlat4.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    vec3 txVec0 = vec3(u_xlat4.xy,u_xlat4.z);
    u_xlat10_4 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_3.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_3.x = u_xlat10_4 * u_xlat16_3.x + _LightShadowData.x;
    u_xlat16_7 = (-u_xlat16_3.x) + 1.0;
    u_xlat16_3.x = u_xlat0.x * u_xlat16_7 + u_xlat16_3.x;
    u_xlat16_3.xyz = u_xlat16_3.xxx * _LightColor0.xyz;
    u_xlat0.xyz = u_xlat1.xzw * u_xlat16_3.xyz;
    u_xlat0.xyz = vec3(u_xlat5) * u_xlat0.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec4 u_xlat1;
mediump vec4 u_xlat16_1;
lowp vec4 u_xlat10_1;
vec3 u_xlat2;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
vec3 u_xlat4;
lowp float u_xlat10_4;
float u_xlat5;
mediump float u_xlat16_7;
mediump float u_xlat16_9;
float u_xlat12;
mediump float u_xlat16_13;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = u_xlat0.xyz * vec3(u_xlat12) + _WorldSpaceLightPos0.xyz;
    u_xlat12 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat12 = max(u_xlat12, 0.00100000005);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = vec3(u_xlat12) * u_xlat1.xyz;
    u_xlat12 = dot(_WorldSpaceLightPos0.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat12 = min(max(u_xlat12, 0.0), 1.0);
#else
    u_xlat12 = clamp(u_xlat12, 0.0, 1.0);
#endif
    u_xlat12 = max(u_xlat12, 0.319999993);
    u_xlat16_13 = (-_Glossiness) + 1.0;
    u_xlat16_2 = u_xlat16_13 * u_xlat16_13 + 1.5;
    u_xlat16_13 = u_xlat16_13 * u_xlat16_13;
    u_xlat12 = u_xlat12 * u_xlat16_2;
    u_xlat2.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat2.x = inversesqrt(u_xlat2.x);
    u_xlat2.xyz = u_xlat2.xxx * vs_TEXCOORD1.xyz;
    u_xlat1.x = dot(u_xlat2.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat1.x = min(max(u_xlat1.x, 0.0), 1.0);
#else
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
#endif
    u_xlat5 = dot(u_xlat2.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5 = min(max(u_xlat5, 0.0), 1.0);
#else
    u_xlat5 = clamp(u_xlat5, 0.0, 1.0);
#endif
    u_xlat1.x = u_xlat1.x * u_xlat1.x;
    u_xlat16_9 = u_xlat16_13 * u_xlat16_13 + -1.0;
    u_xlat1.x = u_xlat1.x * u_xlat16_9 + 1.00001001;
    u_xlat12 = u_xlat12 * u_xlat1.x;
    u_xlat12 = u_xlat16_13 / u_xlat12;
    u_xlat12 = u_xlat12 + -9.99999975e-05;
    u_xlat12 = max(u_xlat12, 0.0);
    u_xlat12 = min(u_xlat12, 100.0);
    u_xlat10_1.xzw = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_1.xzw * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xzw = u_xlat10_1.xzw * _Color.xyz;
    u_xlat16_3.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_3.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat2.xyz = vec3(u_xlat12) * u_xlat16_3.xyz;
    u_xlat16_3.x = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat1.xzw = u_xlat16_1.xzw * u_xlat16_3.xxx + u_xlat2.xyz;
    u_xlat2.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat2.xyz);
    u_xlat4.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat4.x = dot(u_xlat4.xyz, u_xlat4.xyz);
    u_xlat4.x = sqrt(u_xlat4.x);
    u_xlat4.x = (-u_xlat0.x) + u_xlat4.x;
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat4.x + u_xlat0.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat4.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat4.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * vs_TEXCOORD2.xxx + u_xlat4.xyz;
    u_xlat4.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * vs_TEXCOORD2.zzz + u_xlat4.xyz;
    u_xlat4.xyz = u_xlat4.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    vec3 txVec0 = vec3(u_xlat4.xy,u_xlat4.z);
    u_xlat10_4 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_3.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_3.x = u_xlat10_4 * u_xlat16_3.x + _LightShadowData.x;
    u_xlat16_7 = (-u_xlat16_3.x) + 1.0;
    u_xlat16_3.x = u_xlat0.x * u_xlat16_7 + u_xlat16_3.x;
    u_xlat16_3.xyz = u_xlat16_3.xxx * _LightColor0.xyz;
    u_xlat0.xyz = u_xlat1.xzw * u_xlat16_3.xyz;
    u_xlat0.xyz = vec3(u_xlat5) * u_xlat0.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

//////////////////////////////////////////////////////
Keywords set in this variant: DIRECTIONAL_COOKIE SHADOWS_SCREEN 
-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 43 math, 4 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xy;
  xlv_TEXCOORD4 = (unity_WorldToShadow[0] * tmpvar_3);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform highp sampler2D _ShadowMapTexture;
uniform highp sampler2D _LightTexture0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  mediump vec3 tmpvar_7;
  tmpvar_7 = _WorldSpaceLightPos0.xyz;
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  mediump float realtimeShadowAttenuation_13;
  highp vec4 v_14;
  v_14.x = unity_MatrixV[0].z;
  v_14.y = unity_MatrixV[1].z;
  v_14.z = unity_MatrixV[2].z;
  v_14.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_15;
  tmpvar_15 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_16;
  highp float tmpvar_17;
  tmpvar_17 = clamp (((
    mix (dot (tmpvar_9, v_14.xyz), sqrt(dot (tmpvar_15, tmpvar_15)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_16 = tmpvar_17;
  lowp float tmpvar_18;
  highp float lightShadowDataX_19;
  mediump float tmpvar_20;
  tmpvar_20 = _LightShadowData.x;
  lightShadowDataX_19 = tmpvar_20;
  highp float tmpvar_21;
  tmpvar_21 = max (float((texture2D (_ShadowMapTexture, xlv_TEXCOORD4.xy).x > xlv_TEXCOORD4.z)), lightShadowDataX_19);
  tmpvar_18 = tmpvar_21;
  realtimeShadowAttenuation_13 = tmpvar_18;
  mediump float tmpvar_22;
  tmpvar_22 = mix (realtimeShadowAttenuation_13, 1.0, tmpvar_16);
  shadow_5 = tmpvar_22;
  highp float tmpvar_23;
  tmpvar_23 = (texture2D (_LightTexture0, xlv_TEXCOORD3).w * shadow_5);
  atten_4 = tmpvar_23;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_24;
  mediump vec4 c_25;
  highp vec3 tmpvar_26;
  tmpvar_26 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_27;
  mediump vec3 albedo_28;
  albedo_28 = tmpvar_10;
  tmpvar_27 = (albedo_28 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_24 = tmpvar_27;
  mediump vec3 diffColor_29;
  diffColor_29 = tmpvar_24;
  mediump float alpha_30;
  alpha_30 = tmpvar_11;
  tmpvar_24 = diffColor_29;
  mediump vec3 diffColor_31;
  diffColor_31 = tmpvar_24;
  mediump vec2 rlPow4AndFresnelTerm_32;
  mediump float tmpvar_33;
  highp float tmpvar_34;
  tmpvar_34 = clamp (dot (tmpvar_26, tmpvar_2), 0.0, 1.0);
  tmpvar_33 = tmpvar_34;
  mediump float tmpvar_35;
  highp float tmpvar_36;
  tmpvar_36 = clamp (dot (tmpvar_26, tmpvar_8), 0.0, 1.0);
  tmpvar_35 = tmpvar_36;
  highp vec2 tmpvar_37;
  tmpvar_37.x = dot ((tmpvar_8 - (2.0 * 
    (dot (tmpvar_26, tmpvar_8) * tmpvar_26)
  )), tmpvar_2);
  tmpvar_37.y = (1.0 - tmpvar_35);
  highp vec2 tmpvar_38;
  tmpvar_38 = ((tmpvar_37 * tmpvar_37) * (tmpvar_37 * tmpvar_37));
  rlPow4AndFresnelTerm_32 = tmpvar_38;
  mediump float tmpvar_39;
  tmpvar_39 = rlPow4AndFresnelTerm_32.x;
  mediump float specular_40;
  highp float smoothness_41;
  smoothness_41 = _Glossiness;
  highp vec2 tmpvar_42;
  tmpvar_42.x = tmpvar_39;
  tmpvar_42.y = (1.0 - smoothness_41);
  highp float tmpvar_43;
  tmpvar_43 = (texture2D (unity_NHxRoughness, tmpvar_42).w * 16.0);
  specular_40 = tmpvar_43;
  mediump vec4 tmpvar_44;
  tmpvar_44.w = 1.0;
  tmpvar_44.xyz = ((diffColor_31 + (specular_40 * 
    mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_28, vec3(_Metallic))
  )) * (tmpvar_1 * tmpvar_33));
  c_25.xyz = tmpvar_44.xyz;
  c_25.w = alpha_30;
  c_3.xyz = c_25.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 53 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xy;
  xlv_TEXCOORD4 = (unity_WorldToShadow[0] * tmpvar_3);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _ShadowMapTexture;
uniform highp sampler2D _LightTexture0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  mediump vec3 tmpvar_7;
  tmpvar_7 = _WorldSpaceLightPos0.xyz;
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  tmpvar_8 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  lowp vec3 tmpvar_9;
  lowp float tmpvar_10;
  lowp vec4 tmpvar_11;
  tmpvar_11 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_9 = tmpvar_11.xyz;
  tmpvar_10 = tmpvar_11.w;
  mediump float realtimeShadowAttenuation_12;
  highp vec4 v_13;
  v_13.x = unity_MatrixV[0].z;
  v_13.y = unity_MatrixV[1].z;
  v_13.z = unity_MatrixV[2].z;
  v_13.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_14;
  tmpvar_14 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_15;
  highp float tmpvar_16;
  tmpvar_16 = clamp (((
    mix (dot (tmpvar_8, v_13.xyz), sqrt(dot (tmpvar_14, tmpvar_14)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_15 = tmpvar_16;
  lowp float tmpvar_17;
  highp float lightShadowDataX_18;
  mediump float tmpvar_19;
  tmpvar_19 = _LightShadowData.x;
  lightShadowDataX_18 = tmpvar_19;
  highp float tmpvar_20;
  tmpvar_20 = max (float((texture2D (_ShadowMapTexture, xlv_TEXCOORD4.xy).x > xlv_TEXCOORD4.z)), lightShadowDataX_18);
  tmpvar_17 = tmpvar_20;
  realtimeShadowAttenuation_12 = tmpvar_17;
  mediump float tmpvar_21;
  tmpvar_21 = mix (realtimeShadowAttenuation_12, 1.0, tmpvar_15);
  shadow_5 = tmpvar_21;
  highp float tmpvar_22;
  tmpvar_22 = (texture2D (_LightTexture0, xlv_TEXCOORD3).w * shadow_5);
  atten_4 = tmpvar_22;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_23;
  mediump vec4 c_24;
  highp vec3 tmpvar_25;
  tmpvar_25 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_26;
  mediump vec3 albedo_27;
  albedo_27 = tmpvar_9;
  mediump vec3 tmpvar_28;
  tmpvar_28 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_27, vec3(_Metallic));
  tmpvar_26 = (albedo_27 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_23 = tmpvar_26;
  mediump vec3 diffColor_29;
  diffColor_29 = tmpvar_23;
  mediump float alpha_30;
  alpha_30 = tmpvar_10;
  tmpvar_23 = diffColor_29;
  mediump vec3 diffColor_31;
  diffColor_31 = tmpvar_23;
  mediump vec3 color_32;
  highp float specularTerm_33;
  highp float a2_34;
  mediump float roughness_35;
  mediump float perceptualRoughness_36;
  highp vec3 tmpvar_37;
  highp vec3 inVec_38;
  inVec_38 = (tmpvar_2 + normalize(tmpvar_8));
  tmpvar_37 = (inVec_38 * inversesqrt(max (0.001, 
    dot (inVec_38, inVec_38)
  )));
  mediump float tmpvar_39;
  highp float tmpvar_40;
  tmpvar_40 = clamp (dot (tmpvar_25, tmpvar_2), 0.0, 1.0);
  tmpvar_39 = tmpvar_40;
  highp float tmpvar_41;
  tmpvar_41 = clamp (dot (tmpvar_25, tmpvar_37), 0.0, 1.0);
  highp float tmpvar_42;
  highp float smoothness_43;
  smoothness_43 = _Glossiness;
  tmpvar_42 = (1.0 - smoothness_43);
  perceptualRoughness_36 = tmpvar_42;
  highp float tmpvar_44;
  highp float perceptualRoughness_45;
  perceptualRoughness_45 = perceptualRoughness_36;
  tmpvar_44 = (perceptualRoughness_45 * perceptualRoughness_45);
  roughness_35 = tmpvar_44;
  mediump float tmpvar_46;
  tmpvar_46 = (roughness_35 * roughness_35);
  a2_34 = tmpvar_46;
  specularTerm_33 = ((roughness_35 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_37), 0.0, 1.0)) * (1.5 + roughness_35))
   * 
    (((tmpvar_41 * tmpvar_41) * (a2_34 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_47;
  tmpvar_47 = clamp (specularTerm_33, 0.0, 100.0);
  specularTerm_33 = tmpvar_47;
  highp vec3 tmpvar_48;
  tmpvar_48 = (((diffColor_31 + 
    (tmpvar_47 * tmpvar_28)
  ) * tmpvar_1) * tmpvar_39);
  color_32 = tmpvar_48;
  mediump vec4 tmpvar_49;
  tmpvar_49.w = 1.0;
  tmpvar_49.xyz = color_32;
  c_24.xyz = tmpvar_49.xyz;
  c_24.w = alpha_30;
  c_3.xyz = c_24.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 53 math, 3 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_WorldToShadow[4];
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xy;
  xlv_TEXCOORD4 = (unity_WorldToShadow[0] * tmpvar_3);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform mediump vec4 _WorldSpaceLightPos0;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D _ShadowMapTexture;
uniform highp sampler2D _LightTexture0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec2 xlv_TEXCOORD3;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  mediump vec3 tmpvar_7;
  tmpvar_7 = _WorldSpaceLightPos0.xyz;
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  tmpvar_8 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  lowp vec3 tmpvar_9;
  lowp float tmpvar_10;
  lowp vec4 tmpvar_11;
  tmpvar_11 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_9 = tmpvar_11.xyz;
  tmpvar_10 = tmpvar_11.w;
  mediump float realtimeShadowAttenuation_12;
  highp vec4 v_13;
  v_13.x = unity_MatrixV[0].z;
  v_13.y = unity_MatrixV[1].z;
  v_13.z = unity_MatrixV[2].z;
  v_13.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_14;
  tmpvar_14 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_15;
  highp float tmpvar_16;
  tmpvar_16 = clamp (((
    mix (dot (tmpvar_8, v_13.xyz), sqrt(dot (tmpvar_14, tmpvar_14)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_15 = tmpvar_16;
  lowp float tmpvar_17;
  highp float lightShadowDataX_18;
  mediump float tmpvar_19;
  tmpvar_19 = _LightShadowData.x;
  lightShadowDataX_18 = tmpvar_19;
  highp float tmpvar_20;
  tmpvar_20 = max (float((texture2D (_ShadowMapTexture, xlv_TEXCOORD4.xy).x > xlv_TEXCOORD4.z)), lightShadowDataX_18);
  tmpvar_17 = tmpvar_20;
  realtimeShadowAttenuation_12 = tmpvar_17;
  mediump float tmpvar_21;
  tmpvar_21 = mix (realtimeShadowAttenuation_12, 1.0, tmpvar_15);
  shadow_5 = tmpvar_21;
  highp float tmpvar_22;
  tmpvar_22 = (texture2D (_LightTexture0, xlv_TEXCOORD3).w * shadow_5);
  atten_4 = tmpvar_22;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_23;
  mediump vec4 c_24;
  highp vec3 tmpvar_25;
  tmpvar_25 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_26;
  mediump vec3 albedo_27;
  albedo_27 = tmpvar_9;
  mediump vec3 tmpvar_28;
  tmpvar_28 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_27, vec3(_Metallic));
  tmpvar_26 = (albedo_27 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_23 = tmpvar_26;
  mediump vec3 diffColor_29;
  diffColor_29 = tmpvar_23;
  mediump float alpha_30;
  alpha_30 = tmpvar_10;
  tmpvar_23 = diffColor_29;
  mediump vec3 diffColor_31;
  diffColor_31 = tmpvar_23;
  mediump vec3 color_32;
  highp float specularTerm_33;
  highp float a2_34;
  mediump float roughness_35;
  mediump float perceptualRoughness_36;
  highp vec3 tmpvar_37;
  highp vec3 inVec_38;
  inVec_38 = (tmpvar_2 + normalize(tmpvar_8));
  tmpvar_37 = (inVec_38 * inversesqrt(max (0.001, 
    dot (inVec_38, inVec_38)
  )));
  mediump float tmpvar_39;
  highp float tmpvar_40;
  tmpvar_40 = clamp (dot (tmpvar_25, tmpvar_2), 0.0, 1.0);
  tmpvar_39 = tmpvar_40;
  highp float tmpvar_41;
  tmpvar_41 = clamp (dot (tmpvar_25, tmpvar_37), 0.0, 1.0);
  highp float tmpvar_42;
  highp float smoothness_43;
  smoothness_43 = _Glossiness;
  tmpvar_42 = (1.0 - smoothness_43);
  perceptualRoughness_36 = tmpvar_42;
  highp float tmpvar_44;
  highp float perceptualRoughness_45;
  perceptualRoughness_45 = perceptualRoughness_36;
  tmpvar_44 = (perceptualRoughness_45 * perceptualRoughness_45);
  roughness_35 = tmpvar_44;
  mediump float tmpvar_46;
  tmpvar_46 = (roughness_35 * roughness_35);
  a2_34 = tmpvar_46;
  specularTerm_33 = ((roughness_35 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_37), 0.0, 1.0)) * (1.5 + roughness_35))
   * 
    (((tmpvar_41 * tmpvar_41) * (a2_34 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_47;
  tmpvar_47 = clamp (specularTerm_33, 0.0, 100.0);
  specularTerm_33 = tmpvar_47;
  highp vec3 tmpvar_48;
  tmpvar_48 = (((diffColor_31 + 
    (tmpvar_47 * tmpvar_28)
  ) * tmpvar_1) * tmpvar_39);
  color_32 = tmpvar_48;
  mediump vec4 tmpvar_49;
  tmpvar_49.w = 1.0;
  tmpvar_49.xyz = color_32;
  c_24.xyz = tmpvar_49.xyz;
  c_24.w = alpha_30;
  c_3.xyz = c_24.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec3 u_xlat1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
float u_xlat5;
lowp float u_xlat10_5;
mediump vec3 u_xlat16_6;
lowp vec3 u_xlat10_6;
float u_xlat15;
mediump float u_xlat16_18;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat1.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat15 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat2.xyz = vec3(u_xlat15) * vs_TEXCOORD1.xyz;
    u_xlat15 = dot(u_xlat1.xyz, u_xlat2.xyz);
    u_xlat15 = u_xlat15 + u_xlat15;
    u_xlat1.xyz = u_xlat2.xyz * (-vec3(u_xlat15)) + u_xlat1.xyz;
    u_xlat15 = dot(u_xlat2.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat15 = min(max(u_xlat15, 0.0), 1.0);
#else
    u_xlat15 = clamp(u_xlat15, 0.0, 1.0);
#endif
    u_xlat1.x = dot(u_xlat1.xyz, _WorldSpaceLightPos0.xyz);
    u_xlat1.x = u_xlat1.x * u_xlat1.x;
    u_xlat1.x = u_xlat1.x * u_xlat1.x;
    u_xlat1.y = (-_Glossiness) + 1.0;
    u_xlat1.x = texture(unity_NHxRoughness, u_xlat1.xy).w;
    u_xlat1.x = u_xlat1.x * 16.0;
    u_xlat10_6.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_6.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_6.xyz = u_xlat10_6.xyz * _Color.xyz;
    u_xlat16_3.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_3.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_3.xyz = u_xlat1.xxx * u_xlat16_3.xyz;
    u_xlat16_18 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_3.xyz = u_xlat16_6.xyz * vec3(u_xlat16_18) + u_xlat16_3.xyz;
    u_xlat1.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat1.xyz);
    u_xlat1.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat5 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat5 = sqrt(u_xlat5);
    u_xlat5 = (-u_xlat0.x) + u_xlat5;
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat5 + u_xlat0.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat1.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * vs_TEXCOORD2.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * vs_TEXCOORD2.zzz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    vec3 txVec0 = vec3(u_xlat1.xy,u_xlat1.z);
    u_xlat10_5 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_18 = (-_LightShadowData.x) + 1.0;
    u_xlat16_18 = u_xlat10_5 * u_xlat16_18 + _LightShadowData.x;
    u_xlat16_4.x = (-u_xlat16_18) + 1.0;
    u_xlat16_18 = u_xlat0.x * u_xlat16_4.x + u_xlat16_18;
    u_xlat0.xy = vs_TEXCOORD2.yy * hlslcc_mtx4x4unity_WorldToLight[1].xy;
    u_xlat0.xy = hlslcc_mtx4x4unity_WorldToLight[0].xy * vs_TEXCOORD2.xx + u_xlat0.xy;
    u_xlat0.xy = hlslcc_mtx4x4unity_WorldToLight[2].xy * vs_TEXCOORD2.zz + u_xlat0.xy;
    u_xlat0.xy = u_xlat0.xy + hlslcc_mtx4x4unity_WorldToLight[3].xy;
    u_xlat0.x = texture(_LightTexture0, u_xlat0.xy).w;
    u_xlat0.x = u_xlat16_18 * u_xlat0.x;
    u_xlat16_4.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat16_4.xyz = vec3(u_xlat15) * u_xlat16_4.xyz;
    SV_Target0.xyz = u_xlat16_3.xyz * u_xlat16_4.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec4 u_xlat1;
mediump vec4 u_xlat16_1;
lowp vec4 u_xlat10_1;
vec3 u_xlat2;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
vec3 u_xlat4;
lowp float u_xlat10_4;
float u_xlat5;
mediump float u_xlat16_7;
mediump float u_xlat16_9;
float u_xlat12;
mediump float u_xlat16_13;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = u_xlat0.xyz * vec3(u_xlat12) + _WorldSpaceLightPos0.xyz;
    u_xlat12 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat12 = max(u_xlat12, 0.00100000005);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = vec3(u_xlat12) * u_xlat1.xyz;
    u_xlat12 = dot(_WorldSpaceLightPos0.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat12 = min(max(u_xlat12, 0.0), 1.0);
#else
    u_xlat12 = clamp(u_xlat12, 0.0, 1.0);
#endif
    u_xlat12 = max(u_xlat12, 0.319999993);
    u_xlat16_13 = (-_Glossiness) + 1.0;
    u_xlat16_2 = u_xlat16_13 * u_xlat16_13 + 1.5;
    u_xlat16_13 = u_xlat16_13 * u_xlat16_13;
    u_xlat12 = u_xlat12 * u_xlat16_2;
    u_xlat2.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat2.x = inversesqrt(u_xlat2.x);
    u_xlat2.xyz = u_xlat2.xxx * vs_TEXCOORD1.xyz;
    u_xlat1.x = dot(u_xlat2.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat1.x = min(max(u_xlat1.x, 0.0), 1.0);
#else
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
#endif
    u_xlat5 = dot(u_xlat2.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5 = min(max(u_xlat5, 0.0), 1.0);
#else
    u_xlat5 = clamp(u_xlat5, 0.0, 1.0);
#endif
    u_xlat1.x = u_xlat1.x * u_xlat1.x;
    u_xlat16_9 = u_xlat16_13 * u_xlat16_13 + -1.0;
    u_xlat1.x = u_xlat1.x * u_xlat16_9 + 1.00001001;
    u_xlat12 = u_xlat12 * u_xlat1.x;
    u_xlat12 = u_xlat16_13 / u_xlat12;
    u_xlat12 = u_xlat12 + -9.99999975e-05;
    u_xlat12 = max(u_xlat12, 0.0);
    u_xlat12 = min(u_xlat12, 100.0);
    u_xlat10_1.xzw = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_1.xzw * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xzw = u_xlat10_1.xzw * _Color.xyz;
    u_xlat16_3.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_3.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat2.xyz = vec3(u_xlat12) * u_xlat16_3.xyz;
    u_xlat16_3.x = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat1.xzw = u_xlat16_1.xzw * u_xlat16_3.xxx + u_xlat2.xyz;
    u_xlat2.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat2.xyz);
    u_xlat4.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat4.x = dot(u_xlat4.xyz, u_xlat4.xyz);
    u_xlat4.x = sqrt(u_xlat4.x);
    u_xlat4.x = (-u_xlat0.x) + u_xlat4.x;
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat4.x + u_xlat0.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat4.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat4.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * vs_TEXCOORD2.xxx + u_xlat4.xyz;
    u_xlat4.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * vs_TEXCOORD2.zzz + u_xlat4.xyz;
    u_xlat4.xyz = u_xlat4.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    vec3 txVec0 = vec3(u_xlat4.xy,u_xlat4.z);
    u_xlat10_4 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_3.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_3.x = u_xlat10_4 * u_xlat16_3.x + _LightShadowData.x;
    u_xlat16_7 = (-u_xlat16_3.x) + 1.0;
    u_xlat16_3.x = u_xlat0.x * u_xlat16_7 + u_xlat16_3.x;
    u_xlat0.xy = vs_TEXCOORD2.yy * hlslcc_mtx4x4unity_WorldToLight[1].xy;
    u_xlat0.xy = hlslcc_mtx4x4unity_WorldToLight[0].xy * vs_TEXCOORD2.xx + u_xlat0.xy;
    u_xlat0.xy = hlslcc_mtx4x4unity_WorldToLight[2].xy * vs_TEXCOORD2.zz + u_xlat0.xy;
    u_xlat0.xy = u_xlat0.xy + hlslcc_mtx4x4unity_WorldToLight[3].xy;
    u_xlat0.x = texture(_LightTexture0, u_xlat0.xy).w;
    u_xlat0.x = u_xlat16_3.x * u_xlat0.x;
    u_xlat16_3.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat0.xyz = u_xlat1.xzw * u_xlat16_3.xyz;
    u_xlat0.xyz = vec3(u_xlat5) * u_xlat0.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform lowp sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp sampler2D _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec4 u_xlat1;
mediump vec4 u_xlat16_1;
lowp vec4 u_xlat10_1;
vec3 u_xlat2;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
vec3 u_xlat4;
lowp float u_xlat10_4;
float u_xlat5;
mediump float u_xlat16_7;
mediump float u_xlat16_9;
float u_xlat12;
mediump float u_xlat16_13;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = u_xlat0.xyz * vec3(u_xlat12) + _WorldSpaceLightPos0.xyz;
    u_xlat12 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat12 = max(u_xlat12, 0.00100000005);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = vec3(u_xlat12) * u_xlat1.xyz;
    u_xlat12 = dot(_WorldSpaceLightPos0.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat12 = min(max(u_xlat12, 0.0), 1.0);
#else
    u_xlat12 = clamp(u_xlat12, 0.0, 1.0);
#endif
    u_xlat12 = max(u_xlat12, 0.319999993);
    u_xlat16_13 = (-_Glossiness) + 1.0;
    u_xlat16_2 = u_xlat16_13 * u_xlat16_13 + 1.5;
    u_xlat16_13 = u_xlat16_13 * u_xlat16_13;
    u_xlat12 = u_xlat12 * u_xlat16_2;
    u_xlat2.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat2.x = inversesqrt(u_xlat2.x);
    u_xlat2.xyz = u_xlat2.xxx * vs_TEXCOORD1.xyz;
    u_xlat1.x = dot(u_xlat2.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat1.x = min(max(u_xlat1.x, 0.0), 1.0);
#else
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
#endif
    u_xlat5 = dot(u_xlat2.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5 = min(max(u_xlat5, 0.0), 1.0);
#else
    u_xlat5 = clamp(u_xlat5, 0.0, 1.0);
#endif
    u_xlat1.x = u_xlat1.x * u_xlat1.x;
    u_xlat16_9 = u_xlat16_13 * u_xlat16_13 + -1.0;
    u_xlat1.x = u_xlat1.x * u_xlat16_9 + 1.00001001;
    u_xlat12 = u_xlat12 * u_xlat1.x;
    u_xlat12 = u_xlat16_13 / u_xlat12;
    u_xlat12 = u_xlat12 + -9.99999975e-05;
    u_xlat12 = max(u_xlat12, 0.0);
    u_xlat12 = min(u_xlat12, 100.0);
    u_xlat10_1.xzw = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_1.xzw * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xzw = u_xlat10_1.xzw * _Color.xyz;
    u_xlat16_3.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_3.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat2.xyz = vec3(u_xlat12) * u_xlat16_3.xyz;
    u_xlat16_3.x = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat1.xzw = u_xlat16_1.xzw * u_xlat16_3.xxx + u_xlat2.xyz;
    u_xlat2.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat2.xyz);
    u_xlat4.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat4.x = dot(u_xlat4.xyz, u_xlat4.xyz);
    u_xlat4.x = sqrt(u_xlat4.x);
    u_xlat4.x = (-u_xlat0.x) + u_xlat4.x;
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat4.x + u_xlat0.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat4.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
    u_xlat4.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * vs_TEXCOORD2.xxx + u_xlat4.xyz;
    u_xlat4.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * vs_TEXCOORD2.zzz + u_xlat4.xyz;
    u_xlat4.xyz = u_xlat4.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
    vec3 txVec0 = vec3(u_xlat4.xy,u_xlat4.z);
    u_xlat10_4 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat16_3.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_3.x = u_xlat10_4 * u_xlat16_3.x + _LightShadowData.x;
    u_xlat16_7 = (-u_xlat16_3.x) + 1.0;
    u_xlat16_3.x = u_xlat0.x * u_xlat16_7 + u_xlat16_3.x;
    u_xlat0.xy = vs_TEXCOORD2.yy * hlslcc_mtx4x4unity_WorldToLight[1].xy;
    u_xlat0.xy = hlslcc_mtx4x4unity_WorldToLight[0].xy * vs_TEXCOORD2.xx + u_xlat0.xy;
    u_xlat0.xy = hlslcc_mtx4x4unity_WorldToLight[2].xy * vs_TEXCOORD2.zz + u_xlat0.xy;
    u_xlat0.xy = u_xlat0.xy + hlslcc_mtx4x4unity_WorldToLight[3].xy;
    u_xlat0.x = texture(_LightTexture0, u_xlat0.xy).w;
    u_xlat0.x = u_xlat16_3.x * u_xlat0.x;
    u_xlat16_3.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat0.xyz = u_xlat1.xzw * u_xlat16_3.xyz;
    u_xlat0.xyz = vec3(u_xlat5) * u_xlat0.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

//////////////////////////////////////////////////////
Keywords set in this variant: POINT SHADOWS_CUBE 
-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 53 math, 4 textures, 1 branches
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp vec4 _LightPositionRange;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
varying highp vec3 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xyz;
  xlv_TEXCOORD4 = (tmpvar_3.xyz - _LightPositionRange.xyz);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _LightPositionRange;
uniform highp vec4 _LightProjectionParams;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp samplerCube _ShadowMapTexture;
uniform highp sampler2D _LightTexture0;
uniform highp mat4 unity_WorldToLight;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  highp vec4 tmpvar_13;
  tmpvar_13.w = 1.0;
  tmpvar_13.xyz = xlv_TEXCOORD2;
  highp vec3 tmpvar_14;
  tmpvar_14 = (unity_WorldToLight * tmpvar_13).xyz;
  highp vec4 v_15;
  v_15.x = unity_MatrixV[0].z;
  v_15.y = unity_MatrixV[1].z;
  v_15.z = unity_MatrixV[2].z;
  v_15.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_16;
  tmpvar_16 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_17;
  highp float tmpvar_18;
  tmpvar_18 = clamp (((
    mix (dot (tmpvar_9, v_15.xyz), sqrt(dot (tmpvar_16, tmpvar_16)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_17 = tmpvar_18;
  highp vec3 vec_19;
  vec_19 = (xlv_TEXCOORD2 - _LightPositionRange.xyz);
  mediump float shadowVal_20;
  highp float mydist_21;
  mydist_21 = ((sqrt(
    dot (vec_19, vec_19)
  ) * _LightPositionRange.w) * _LightProjectionParams.w);
  lowp vec4 tmpvar_22;
  tmpvar_22 = textureCube (_ShadowMapTexture, vec_19);
  highp vec4 vals_23;
  vals_23 = tmpvar_22;
  highp float tmpvar_24;
  tmpvar_24 = dot (vals_23, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  shadowVal_20 = tmpvar_24;
  mediump float tmpvar_25;
  if ((shadowVal_20 < mydist_21)) {
    tmpvar_25 = _LightShadowData.x;
  } else {
    tmpvar_25 = 1.0;
  };
  mediump float tmpvar_26;
  tmpvar_26 = mix (tmpvar_25, 1.0, tmpvar_17);
  shadow_5 = tmpvar_26;
  highp float tmpvar_27;
  tmpvar_27 = (texture2D (_LightTexture0, vec2(dot (tmpvar_14, tmpvar_14))).w * shadow_5);
  atten_4 = tmpvar_27;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_28;
  mediump vec4 c_29;
  highp vec3 tmpvar_30;
  tmpvar_30 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_31;
  mediump vec3 albedo_32;
  albedo_32 = tmpvar_10;
  tmpvar_31 = (albedo_32 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_28 = tmpvar_31;
  mediump vec3 diffColor_33;
  diffColor_33 = tmpvar_28;
  mediump float alpha_34;
  alpha_34 = tmpvar_11;
  tmpvar_28 = diffColor_33;
  mediump vec3 diffColor_35;
  diffColor_35 = tmpvar_28;
  mediump vec2 rlPow4AndFresnelTerm_36;
  mediump float tmpvar_37;
  highp float tmpvar_38;
  tmpvar_38 = clamp (dot (tmpvar_30, tmpvar_2), 0.0, 1.0);
  tmpvar_37 = tmpvar_38;
  mediump float tmpvar_39;
  highp float tmpvar_40;
  tmpvar_40 = clamp (dot (tmpvar_30, tmpvar_8), 0.0, 1.0);
  tmpvar_39 = tmpvar_40;
  highp vec2 tmpvar_41;
  tmpvar_41.x = dot ((tmpvar_8 - (2.0 * 
    (dot (tmpvar_30, tmpvar_8) * tmpvar_30)
  )), tmpvar_2);
  tmpvar_41.y = (1.0 - tmpvar_39);
  highp vec2 tmpvar_42;
  tmpvar_42 = ((tmpvar_41 * tmpvar_41) * (tmpvar_41 * tmpvar_41));
  rlPow4AndFresnelTerm_36 = tmpvar_42;
  mediump float tmpvar_43;
  tmpvar_43 = rlPow4AndFresnelTerm_36.x;
  mediump float specular_44;
  highp float smoothness_45;
  smoothness_45 = _Glossiness;
  highp vec2 tmpvar_46;
  tmpvar_46.x = tmpvar_43;
  tmpvar_46.y = (1.0 - smoothness_45);
  highp float tmpvar_47;
  tmpvar_47 = (texture2D (unity_NHxRoughness, tmpvar_46).w * 16.0);
  specular_44 = tmpvar_47;
  mediump vec4 tmpvar_48;
  tmpvar_48.w = 1.0;
  tmpvar_48.xyz = ((diffColor_35 + (specular_44 * 
    mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_32, vec3(_Metallic))
  )) * (tmpvar_1 * tmpvar_37));
  c_29.xyz = tmpvar_48.xyz;
  c_29.w = alpha_34;
  c_3.xyz = c_29.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 63 math, 3 textures, 1 branches
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp vec4 _LightPositionRange;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
varying highp vec3 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xyz;
  xlv_TEXCOORD4 = (tmpvar_3.xyz - _LightPositionRange.xyz);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _LightPositionRange;
uniform highp vec4 _LightProjectionParams;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform lowp samplerCube _ShadowMapTexture;
uniform highp sampler2D _LightTexture0;
uniform highp mat4 unity_WorldToLight;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  highp vec4 tmpvar_13;
  tmpvar_13.w = 1.0;
  tmpvar_13.xyz = xlv_TEXCOORD2;
  highp vec3 tmpvar_14;
  tmpvar_14 = (unity_WorldToLight * tmpvar_13).xyz;
  highp vec4 v_15;
  v_15.x = unity_MatrixV[0].z;
  v_15.y = unity_MatrixV[1].z;
  v_15.z = unity_MatrixV[2].z;
  v_15.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_16;
  tmpvar_16 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_17;
  highp float tmpvar_18;
  tmpvar_18 = clamp (((
    mix (dot (tmpvar_9, v_15.xyz), sqrt(dot (tmpvar_16, tmpvar_16)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_17 = tmpvar_18;
  highp vec3 vec_19;
  vec_19 = (xlv_TEXCOORD2 - _LightPositionRange.xyz);
  mediump float shadowVal_20;
  highp float mydist_21;
  mydist_21 = ((sqrt(
    dot (vec_19, vec_19)
  ) * _LightPositionRange.w) * _LightProjectionParams.w);
  lowp vec4 tmpvar_22;
  tmpvar_22 = textureCube (_ShadowMapTexture, vec_19);
  highp vec4 vals_23;
  vals_23 = tmpvar_22;
  highp float tmpvar_24;
  tmpvar_24 = dot (vals_23, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  shadowVal_20 = tmpvar_24;
  mediump float tmpvar_25;
  if ((shadowVal_20 < mydist_21)) {
    tmpvar_25 = _LightShadowData.x;
  } else {
    tmpvar_25 = 1.0;
  };
  mediump float tmpvar_26;
  tmpvar_26 = mix (tmpvar_25, 1.0, tmpvar_17);
  shadow_5 = tmpvar_26;
  highp float tmpvar_27;
  tmpvar_27 = (texture2D (_LightTexture0, vec2(dot (tmpvar_14, tmpvar_14))).w * shadow_5);
  atten_4 = tmpvar_27;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_28;
  mediump vec4 c_29;
  highp vec3 tmpvar_30;
  tmpvar_30 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_31;
  mediump vec3 albedo_32;
  albedo_32 = tmpvar_10;
  mediump vec3 tmpvar_33;
  tmpvar_33 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_32, vec3(_Metallic));
  tmpvar_31 = (albedo_32 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_28 = tmpvar_31;
  mediump vec3 diffColor_34;
  diffColor_34 = tmpvar_28;
  mediump float alpha_35;
  alpha_35 = tmpvar_11;
  tmpvar_28 = diffColor_34;
  mediump vec3 diffColor_36;
  diffColor_36 = tmpvar_28;
  mediump vec3 color_37;
  highp float specularTerm_38;
  highp float a2_39;
  mediump float roughness_40;
  mediump float perceptualRoughness_41;
  highp vec3 tmpvar_42;
  highp vec3 inVec_43;
  inVec_43 = (tmpvar_2 + tmpvar_8);
  tmpvar_42 = (inVec_43 * inversesqrt(max (0.001, 
    dot (inVec_43, inVec_43)
  )));
  mediump float tmpvar_44;
  highp float tmpvar_45;
  tmpvar_45 = clamp (dot (tmpvar_30, tmpvar_2), 0.0, 1.0);
  tmpvar_44 = tmpvar_45;
  highp float tmpvar_46;
  tmpvar_46 = clamp (dot (tmpvar_30, tmpvar_42), 0.0, 1.0);
  highp float tmpvar_47;
  highp float smoothness_48;
  smoothness_48 = _Glossiness;
  tmpvar_47 = (1.0 - smoothness_48);
  perceptualRoughness_41 = tmpvar_47;
  highp float tmpvar_49;
  highp float perceptualRoughness_50;
  perceptualRoughness_50 = perceptualRoughness_41;
  tmpvar_49 = (perceptualRoughness_50 * perceptualRoughness_50);
  roughness_40 = tmpvar_49;
  mediump float tmpvar_51;
  tmpvar_51 = (roughness_40 * roughness_40);
  a2_39 = tmpvar_51;
  specularTerm_38 = ((roughness_40 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_42), 0.0, 1.0)) * (1.5 + roughness_40))
   * 
    (((tmpvar_46 * tmpvar_46) * (a2_39 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_52;
  tmpvar_52 = clamp (specularTerm_38, 0.0, 100.0);
  specularTerm_38 = tmpvar_52;
  highp vec3 tmpvar_53;
  tmpvar_53 = (((diffColor_36 + 
    (tmpvar_52 * tmpvar_33)
  ) * tmpvar_1) * tmpvar_44);
  color_37 = tmpvar_53;
  mediump vec4 tmpvar_54;
  tmpvar_54.w = 1.0;
  tmpvar_54.xyz = color_37;
  c_29.xyz = tmpvar_54.xyz;
  c_29.w = alpha_35;
  c_3.xyz = c_29.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 63 math, 3 textures, 1 branches
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp vec4 _LightPositionRange;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
varying highp vec3 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xyz;
  xlv_TEXCOORD4 = (tmpvar_3.xyz - _LightPositionRange.xyz);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _LightPositionRange;
uniform highp vec4 _LightProjectionParams;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform lowp samplerCube _ShadowMapTexture;
uniform highp sampler2D _LightTexture0;
uniform highp mat4 unity_WorldToLight;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  highp vec4 tmpvar_13;
  tmpvar_13.w = 1.0;
  tmpvar_13.xyz = xlv_TEXCOORD2;
  highp vec3 tmpvar_14;
  tmpvar_14 = (unity_WorldToLight * tmpvar_13).xyz;
  highp vec4 v_15;
  v_15.x = unity_MatrixV[0].z;
  v_15.y = unity_MatrixV[1].z;
  v_15.z = unity_MatrixV[2].z;
  v_15.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_16;
  tmpvar_16 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_17;
  highp float tmpvar_18;
  tmpvar_18 = clamp (((
    mix (dot (tmpvar_9, v_15.xyz), sqrt(dot (tmpvar_16, tmpvar_16)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_17 = tmpvar_18;
  highp vec3 vec_19;
  vec_19 = (xlv_TEXCOORD2 - _LightPositionRange.xyz);
  mediump float shadowVal_20;
  highp float mydist_21;
  mydist_21 = ((sqrt(
    dot (vec_19, vec_19)
  ) * _LightPositionRange.w) * _LightProjectionParams.w);
  lowp vec4 tmpvar_22;
  tmpvar_22 = textureCube (_ShadowMapTexture, vec_19);
  highp vec4 vals_23;
  vals_23 = tmpvar_22;
  highp float tmpvar_24;
  tmpvar_24 = dot (vals_23, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  shadowVal_20 = tmpvar_24;
  mediump float tmpvar_25;
  if ((shadowVal_20 < mydist_21)) {
    tmpvar_25 = _LightShadowData.x;
  } else {
    tmpvar_25 = 1.0;
  };
  mediump float tmpvar_26;
  tmpvar_26 = mix (tmpvar_25, 1.0, tmpvar_17);
  shadow_5 = tmpvar_26;
  highp float tmpvar_27;
  tmpvar_27 = (texture2D (_LightTexture0, vec2(dot (tmpvar_14, tmpvar_14))).w * shadow_5);
  atten_4 = tmpvar_27;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_28;
  mediump vec4 c_29;
  highp vec3 tmpvar_30;
  tmpvar_30 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_31;
  mediump vec3 albedo_32;
  albedo_32 = tmpvar_10;
  mediump vec3 tmpvar_33;
  tmpvar_33 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_32, vec3(_Metallic));
  tmpvar_31 = (albedo_32 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_28 = tmpvar_31;
  mediump vec3 diffColor_34;
  diffColor_34 = tmpvar_28;
  mediump float alpha_35;
  alpha_35 = tmpvar_11;
  tmpvar_28 = diffColor_34;
  mediump vec3 diffColor_36;
  diffColor_36 = tmpvar_28;
  mediump vec3 color_37;
  highp float specularTerm_38;
  highp float a2_39;
  mediump float roughness_40;
  mediump float perceptualRoughness_41;
  highp vec3 tmpvar_42;
  highp vec3 inVec_43;
  inVec_43 = (tmpvar_2 + tmpvar_8);
  tmpvar_42 = (inVec_43 * inversesqrt(max (0.001, 
    dot (inVec_43, inVec_43)
  )));
  mediump float tmpvar_44;
  highp float tmpvar_45;
  tmpvar_45 = clamp (dot (tmpvar_30, tmpvar_2), 0.0, 1.0);
  tmpvar_44 = tmpvar_45;
  highp float tmpvar_46;
  tmpvar_46 = clamp (dot (tmpvar_30, tmpvar_42), 0.0, 1.0);
  highp float tmpvar_47;
  highp float smoothness_48;
  smoothness_48 = _Glossiness;
  tmpvar_47 = (1.0 - smoothness_48);
  perceptualRoughness_41 = tmpvar_47;
  highp float tmpvar_49;
  highp float perceptualRoughness_50;
  perceptualRoughness_50 = perceptualRoughness_41;
  tmpvar_49 = (perceptualRoughness_50 * perceptualRoughness_50);
  roughness_40 = tmpvar_49;
  mediump float tmpvar_51;
  tmpvar_51 = (roughness_40 * roughness_40);
  a2_39 = tmpvar_51;
  specularTerm_38 = ((roughness_40 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_42), 0.0, 1.0)) * (1.5 + roughness_40))
   * 
    (((tmpvar_46 * tmpvar_46) * (a2_39 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_52;
  tmpvar_52 = clamp (specularTerm_38, 0.0, 100.0);
  specularTerm_38 = tmpvar_52;
  highp vec3 tmpvar_53;
  tmpvar_53 = (((diffColor_36 + 
    (tmpvar_52 * tmpvar_33)
  ) * tmpvar_1) * tmpvar_44);
  color_37 = tmpvar_53;
  mediump vec4 tmpvar_54;
  tmpvar_54.w = 1.0;
  tmpvar_54.xyz = color_37;
  c_29.xyz = tmpvar_54.xyz;
  c_29.w = alpha_35;
  c_3.xyz = c_29.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4.xyz = vec3(0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 _LightPositionRange;
uniform 	vec4 _LightProjectionParams;
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp samplerCubeShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp samplerCube _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec3 u_xlat1;
vec3 u_xlat2;
vec3 u_xlat3;
mediump vec3 u_xlat16_4;
mediump vec3 u_xlat16_5;
vec3 u_xlat6;
lowp float u_xlat10_6;
float u_xlat7;
mediump vec3 u_xlat16_7;
lowp vec3 u_xlat10_7;
float u_xlat18;
mediump float u_xlat16_22;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat18 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat18 = inversesqrt(u_xlat18);
    u_xlat1.xyz = vec3(u_xlat18) * u_xlat0.xyz;
    u_xlat18 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat18 = inversesqrt(u_xlat18);
    u_xlat2.xyz = vec3(u_xlat18) * vs_TEXCOORD1.xyz;
    u_xlat18 = dot(u_xlat1.xyz, u_xlat2.xyz);
    u_xlat18 = u_xlat18 + u_xlat18;
    u_xlat1.xyz = u_xlat2.xyz * (-vec3(u_xlat18)) + u_xlat1.xyz;
    u_xlat3.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat18 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat18 = inversesqrt(u_xlat18);
    u_xlat3.xyz = vec3(u_xlat18) * u_xlat3.xyz;
    u_xlat18 = dot(u_xlat1.xyz, u_xlat3.xyz);
    u_xlat1.x = dot(u_xlat2.xyz, u_xlat3.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat1.x = min(max(u_xlat1.x, 0.0), 1.0);
#else
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
#endif
    u_xlat18 = u_xlat18 * u_xlat18;
    u_xlat2.x = u_xlat18 * u_xlat18;
    u_xlat2.y = (-_Glossiness) + 1.0;
    u_xlat18 = texture(unity_NHxRoughness, u_xlat2.xy).w;
    u_xlat18 = u_xlat18 * 16.0;
    u_xlat10_7.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_7.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_7.xyz = u_xlat10_7.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_4.xyz = vec3(u_xlat18) * u_xlat16_4.xyz;
    u_xlat16_22 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_4.xyz = u_xlat16_7.xyz * vec3(u_xlat16_22) + u_xlat16_4.xyz;
    u_xlat2.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat2.xyz);
    u_xlat6.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat6.x = dot(u_xlat6.xyz, u_xlat6.xyz);
    u_xlat6.x = sqrt(u_xlat6.x);
    u_xlat6.x = (-u_xlat0.x) + u_xlat6.x;
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat6.x + u_xlat0.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat6.xyz = vs_TEXCOORD2.xyz + (-_LightPositionRange.xyz);
    u_xlat7 = max(abs(u_xlat6.y), abs(u_xlat6.x));
    u_xlat7 = max(abs(u_xlat6.z), u_xlat7);
    u_xlat7 = u_xlat7 + (-_LightProjectionParams.z);
    u_xlat7 = max(u_xlat7, 9.99999975e-06);
    u_xlat7 = u_xlat7 * _LightProjectionParams.w;
    u_xlat7 = _LightProjectionParams.y / u_xlat7;
    u_xlat7 = u_xlat7 + (-_LightProjectionParams.x);
    vec4 txVec0 = vec4(u_xlat6.xyz,u_xlat7);
    u_xlat10_6 = texture(hlslcc_zcmp_ShadowMapTexture, txVec0);
    u_xlat16_22 = (-_LightShadowData.x) + 1.0;
    u_xlat16_22 = u_xlat10_6 * u_xlat16_22 + _LightShadowData.x;
    u_xlat16_5.x = (-u_xlat16_22) + 1.0;
    u_xlat16_22 = u_xlat0.x * u_xlat16_5.x + u_xlat16_22;
    u_xlat0.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD2.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD2.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = texture(_LightTexture0, u_xlat0.xx).w;
    u_xlat0.x = u_xlat16_22 * u_xlat0.x;
    u_xlat16_5.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat16_5.xyz = u_xlat1.xxx * u_xlat16_5.xyz;
    SV_Target0.xyz = u_xlat16_4.xyz * u_xlat16_5.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4.xyz = vec3(0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 _LightPositionRange;
uniform 	vec4 _LightProjectionParams;
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform lowp samplerCubeShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp samplerCube _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
lowp float u_xlat10_0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_2;
lowp vec3 u_xlat10_2;
vec3 u_xlat3;
mediump float u_xlat16_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump float u_xlat16_6;
mediump float u_xlat16_10;
float u_xlat15;
mediump float u_xlat16_16;
float u_xlat17;
mediump float u_xlat16_17;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-_LightPositionRange.xyz);
    u_xlat15 = max(abs(u_xlat0.y), abs(u_xlat0.x));
    u_xlat15 = max(abs(u_xlat0.z), u_xlat15);
    u_xlat15 = u_xlat15 + (-_LightProjectionParams.z);
    u_xlat15 = max(u_xlat15, 9.99999975e-06);
    u_xlat15 = u_xlat15 * _LightProjectionParams.w;
    u_xlat15 = _LightProjectionParams.y / u_xlat15;
    u_xlat15 = u_xlat15 + (-_LightProjectionParams.x);
    vec4 txVec0 = vec4(u_xlat0.xyz,u_xlat15);
    u_xlat10_0 = texture(hlslcc_zcmp_ShadowMapTexture, txVec0);
    u_xlat16_1.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_1.x = u_xlat10_0 * u_xlat16_1.x + _LightShadowData.x;
    u_xlat16_6 = (-u_xlat16_1.x) + 1.0;
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat2.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat5.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat2.x = dot(u_xlat5.xyz, u_xlat2.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat2.x);
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat0.x + u_xlat2.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_1.x = u_xlat0.x * u_xlat16_6 + u_xlat16_1.x;
    u_xlat2.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat2.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD2.xxx + u_xlat2.xyz;
    u_xlat2.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD2.zzz + u_xlat2.xyz;
    u_xlat2.xyz = u_xlat2.xyz + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat0.x = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat0.x = texture(_LightTexture0, u_xlat0.xx).w;
    u_xlat0.x = u_xlat16_1.x * u_xlat0.x;
    u_xlat16_1.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat2.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat17 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat17 = inversesqrt(u_xlat17);
    u_xlat2.xyz = vec3(u_xlat17) * u_xlat2.xyz;
    u_xlat0.xyz = u_xlat5.xyz * u_xlat0.xxx + u_xlat2.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = max(u_xlat15, 0.00100000005);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat15 = dot(u_xlat2.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat15 = min(max(u_xlat15, 0.0), 1.0);
#else
    u_xlat15 = clamp(u_xlat15, 0.0, 1.0);
#endif
    u_xlat15 = max(u_xlat15, 0.319999993);
    u_xlat16_17 = (-_Glossiness) + 1.0;
    u_xlat16_3 = u_xlat16_17 * u_xlat16_17 + 1.5;
    u_xlat16_17 = u_xlat16_17 * u_xlat16_17;
    u_xlat15 = u_xlat15 * u_xlat16_3;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat3.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat5.x = dot(u_xlat3.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat16_10 = u_xlat16_17 * u_xlat16_17 + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_10 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat15;
    u_xlat0.x = u_xlat16_17 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_2.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_2.xyz = u_xlat10_2.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_16 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_2.xyz * vec3(u_xlat16_16) + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat16_1.xyz * u_xlat0.xzw;
    u_xlat0.xyz = u_xlat5.xxx * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4.xyz = vec3(0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 _LightPositionRange;
uniform 	vec4 _LightProjectionParams;
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform lowp samplerCubeShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp samplerCube _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
lowp float u_xlat10_0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_2;
lowp vec3 u_xlat10_2;
vec3 u_xlat3;
mediump float u_xlat16_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump float u_xlat16_6;
mediump float u_xlat16_10;
float u_xlat15;
mediump float u_xlat16_16;
float u_xlat17;
mediump float u_xlat16_17;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-_LightPositionRange.xyz);
    u_xlat15 = max(abs(u_xlat0.y), abs(u_xlat0.x));
    u_xlat15 = max(abs(u_xlat0.z), u_xlat15);
    u_xlat15 = u_xlat15 + (-_LightProjectionParams.z);
    u_xlat15 = max(u_xlat15, 9.99999975e-06);
    u_xlat15 = u_xlat15 * _LightProjectionParams.w;
    u_xlat15 = _LightProjectionParams.y / u_xlat15;
    u_xlat15 = u_xlat15 + (-_LightProjectionParams.x);
    vec4 txVec0 = vec4(u_xlat0.xyz,u_xlat15);
    u_xlat10_0 = texture(hlslcc_zcmp_ShadowMapTexture, txVec0);
    u_xlat16_1.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_1.x = u_xlat10_0 * u_xlat16_1.x + _LightShadowData.x;
    u_xlat16_6 = (-u_xlat16_1.x) + 1.0;
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat2.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat5.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat2.x = dot(u_xlat5.xyz, u_xlat2.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat2.x);
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat0.x + u_xlat2.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_1.x = u_xlat0.x * u_xlat16_6 + u_xlat16_1.x;
    u_xlat2.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat2.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD2.xxx + u_xlat2.xyz;
    u_xlat2.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD2.zzz + u_xlat2.xyz;
    u_xlat2.xyz = u_xlat2.xyz + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat0.x = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat0.x = texture(_LightTexture0, u_xlat0.xx).w;
    u_xlat0.x = u_xlat16_1.x * u_xlat0.x;
    u_xlat16_1.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat2.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat17 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat17 = inversesqrt(u_xlat17);
    u_xlat2.xyz = vec3(u_xlat17) * u_xlat2.xyz;
    u_xlat0.xyz = u_xlat5.xyz * u_xlat0.xxx + u_xlat2.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = max(u_xlat15, 0.00100000005);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat15 = dot(u_xlat2.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat15 = min(max(u_xlat15, 0.0), 1.0);
#else
    u_xlat15 = clamp(u_xlat15, 0.0, 1.0);
#endif
    u_xlat15 = max(u_xlat15, 0.319999993);
    u_xlat16_17 = (-_Glossiness) + 1.0;
    u_xlat16_3 = u_xlat16_17 * u_xlat16_17 + 1.5;
    u_xlat16_17 = u_xlat16_17 * u_xlat16_17;
    u_xlat15 = u_xlat15 * u_xlat16_3;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat3.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat5.x = dot(u_xlat3.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat16_10 = u_xlat16_17 * u_xlat16_17 + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_10 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat15;
    u_xlat0.x = u_xlat16_17 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_2.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_2.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_2.xyz = u_xlat10_2.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_16 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_2.xyz * vec3(u_xlat16_16) + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat16_1.xyz * u_xlat0.xzw;
    u_xlat0.xyz = u_xlat5.xxx * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

//////////////////////////////////////////////////////
Keywords set in this variant: POINT SHADOWS_CUBE SHADOWS_SOFT 
-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 68 math, 7 textures, 4 branches
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp vec4 _LightPositionRange;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
varying highp vec3 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xyz;
  xlv_TEXCOORD4 = (tmpvar_3.xyz - _LightPositionRange.xyz);
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _LightPositionRange;
uniform highp vec4 _LightProjectionParams;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp samplerCube _ShadowMapTexture;
uniform highp sampler2D _LightTexture0;
uniform highp mat4 unity_WorldToLight;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  highp vec4 tmpvar_13;
  tmpvar_13.w = 1.0;
  tmpvar_13.xyz = xlv_TEXCOORD2;
  highp vec3 tmpvar_14;
  tmpvar_14 = (unity_WorldToLight * tmpvar_13).xyz;
  highp vec4 v_15;
  v_15.x = unity_MatrixV[0].z;
  v_15.y = unity_MatrixV[1].z;
  v_15.z = unity_MatrixV[2].z;
  v_15.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_16;
  tmpvar_16 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_17;
  highp float tmpvar_18;
  tmpvar_18 = clamp (((
    mix (dot (tmpvar_9, v_15.xyz), sqrt(dot (tmpvar_16, tmpvar_16)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_17 = tmpvar_18;
  highp vec3 vec_19;
  vec_19 = (xlv_TEXCOORD2 - _LightPositionRange.xyz);
  highp vec4 shadowVals_20;
  highp float mydist_21;
  mydist_21 = ((sqrt(
    dot (vec_19, vec_19)
  ) * _LightPositionRange.w) * _LightProjectionParams.w);
  highp vec4 tmpvar_22;
  tmpvar_22.w = 0.0;
  tmpvar_22.xyz = (vec_19 + vec3(0.0078125, 0.0078125, 0.0078125));
  highp vec4 tmpvar_23;
  lowp vec4 tmpvar_24;
  tmpvar_24 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_22.xyz, 0.0);
  tmpvar_23 = tmpvar_24;
  shadowVals_20.x = dot (tmpvar_23, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  highp vec4 tmpvar_25;
  tmpvar_25.w = 0.0;
  tmpvar_25.xyz = (vec_19 + vec3(-0.0078125, -0.0078125, 0.0078125));
  highp vec4 tmpvar_26;
  lowp vec4 tmpvar_27;
  tmpvar_27 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_25.xyz, 0.0);
  tmpvar_26 = tmpvar_27;
  shadowVals_20.y = dot (tmpvar_26, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  highp vec4 tmpvar_28;
  tmpvar_28.w = 0.0;
  tmpvar_28.xyz = (vec_19 + vec3(-0.0078125, 0.0078125, -0.0078125));
  highp vec4 tmpvar_29;
  lowp vec4 tmpvar_30;
  tmpvar_30 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_28.xyz, 0.0);
  tmpvar_29 = tmpvar_30;
  shadowVals_20.z = dot (tmpvar_29, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  highp vec4 tmpvar_31;
  tmpvar_31.w = 0.0;
  tmpvar_31.xyz = (vec_19 + vec3(0.0078125, -0.0078125, -0.0078125));
  highp vec4 tmpvar_32;
  lowp vec4 tmpvar_33;
  tmpvar_33 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_31.xyz, 0.0);
  tmpvar_32 = tmpvar_33;
  shadowVals_20.w = dot (tmpvar_32, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  bvec4 tmpvar_34;
  tmpvar_34 = lessThan (shadowVals_20, vec4(mydist_21));
  mediump vec4 tmpvar_35;
  tmpvar_35 = _LightShadowData.xxxx;
  mediump float tmpvar_36;
  if (tmpvar_34.x) {
    tmpvar_36 = tmpvar_35.x;
  } else {
    tmpvar_36 = 1.0;
  };
  mediump float tmpvar_37;
  if (tmpvar_34.y) {
    tmpvar_37 = tmpvar_35.y;
  } else {
    tmpvar_37 = 1.0;
  };
  mediump float tmpvar_38;
  if (tmpvar_34.z) {
    tmpvar_38 = tmpvar_35.z;
  } else {
    tmpvar_38 = 1.0;
  };
  mediump float tmpvar_39;
  if (tmpvar_34.w) {
    tmpvar_39 = tmpvar_35.w;
  } else {
    tmpvar_39 = 1.0;
  };
  mediump vec4 tmpvar_40;
  tmpvar_40.x = tmpvar_36;
  tmpvar_40.y = tmpvar_37;
  tmpvar_40.z = tmpvar_38;
  tmpvar_40.w = tmpvar_39;
  mediump float tmpvar_41;
  tmpvar_41 = mix (dot (tmpvar_40, vec4(0.25, 0.25, 0.25, 0.25)), 1.0, tmpvar_17);
  shadow_5 = tmpvar_41;
  highp float tmpvar_42;
  tmpvar_42 = (texture2D (_LightTexture0, vec2(dot (tmpvar_14, tmpvar_14))).w * shadow_5);
  atten_4 = tmpvar_42;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_43;
  mediump vec4 c_44;
  highp vec3 tmpvar_45;
  tmpvar_45 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_46;
  mediump vec3 albedo_47;
  albedo_47 = tmpvar_10;
  tmpvar_46 = (albedo_47 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_43 = tmpvar_46;
  mediump vec3 diffColor_48;
  diffColor_48 = tmpvar_43;
  mediump float alpha_49;
  alpha_49 = tmpvar_11;
  tmpvar_43 = diffColor_48;
  mediump vec3 diffColor_50;
  diffColor_50 = tmpvar_43;
  mediump vec2 rlPow4AndFresnelTerm_51;
  mediump float tmpvar_52;
  highp float tmpvar_53;
  tmpvar_53 = clamp (dot (tmpvar_45, tmpvar_2), 0.0, 1.0);
  tmpvar_52 = tmpvar_53;
  mediump float tmpvar_54;
  highp float tmpvar_55;
  tmpvar_55 = clamp (dot (tmpvar_45, tmpvar_8), 0.0, 1.0);
  tmpvar_54 = tmpvar_55;
  highp vec2 tmpvar_56;
  tmpvar_56.x = dot ((tmpvar_8 - (2.0 * 
    (dot (tmpvar_45, tmpvar_8) * tmpvar_45)
  )), tmpvar_2);
  tmpvar_56.y = (1.0 - tmpvar_54);
  highp vec2 tmpvar_57;
  tmpvar_57 = ((tmpvar_56 * tmpvar_56) * (tmpvar_56 * tmpvar_56));
  rlPow4AndFresnelTerm_51 = tmpvar_57;
  mediump float tmpvar_58;
  tmpvar_58 = rlPow4AndFresnelTerm_51.x;
  mediump float specular_59;
  highp float smoothness_60;
  smoothness_60 = _Glossiness;
  highp vec2 tmpvar_61;
  tmpvar_61.x = tmpvar_58;
  tmpvar_61.y = (1.0 - smoothness_60);
  highp float tmpvar_62;
  tmpvar_62 = (texture2D (unity_NHxRoughness, tmpvar_61).w * 16.0);
  specular_59 = tmpvar_62;
  mediump vec4 tmpvar_63;
  tmpvar_63.w = 1.0;
  tmpvar_63.xyz = ((diffColor_50 + (specular_59 * 
    mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_47, vec3(_Metallic))
  )) * (tmpvar_1 * tmpvar_52));
  c_44.xyz = tmpvar_63.xyz;
  c_44.w = alpha_49;
  c_3.xyz = c_44.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 78 math, 6 textures, 4 branches
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp vec4 _LightPositionRange;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
varying highp vec3 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xyz;
  xlv_TEXCOORD4 = (tmpvar_3.xyz - _LightPositionRange.xyz);
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _LightPositionRange;
uniform highp vec4 _LightProjectionParams;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform lowp samplerCube _ShadowMapTexture;
uniform highp sampler2D _LightTexture0;
uniform highp mat4 unity_WorldToLight;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  highp vec4 tmpvar_13;
  tmpvar_13.w = 1.0;
  tmpvar_13.xyz = xlv_TEXCOORD2;
  highp vec3 tmpvar_14;
  tmpvar_14 = (unity_WorldToLight * tmpvar_13).xyz;
  highp vec4 v_15;
  v_15.x = unity_MatrixV[0].z;
  v_15.y = unity_MatrixV[1].z;
  v_15.z = unity_MatrixV[2].z;
  v_15.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_16;
  tmpvar_16 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_17;
  highp float tmpvar_18;
  tmpvar_18 = clamp (((
    mix (dot (tmpvar_9, v_15.xyz), sqrt(dot (tmpvar_16, tmpvar_16)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_17 = tmpvar_18;
  highp vec3 vec_19;
  vec_19 = (xlv_TEXCOORD2 - _LightPositionRange.xyz);
  highp vec4 shadowVals_20;
  highp float mydist_21;
  mydist_21 = ((sqrt(
    dot (vec_19, vec_19)
  ) * _LightPositionRange.w) * _LightProjectionParams.w);
  highp vec4 tmpvar_22;
  tmpvar_22.w = 0.0;
  tmpvar_22.xyz = (vec_19 + vec3(0.0078125, 0.0078125, 0.0078125));
  highp vec4 tmpvar_23;
  lowp vec4 tmpvar_24;
  tmpvar_24 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_22.xyz, 0.0);
  tmpvar_23 = tmpvar_24;
  shadowVals_20.x = dot (tmpvar_23, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  highp vec4 tmpvar_25;
  tmpvar_25.w = 0.0;
  tmpvar_25.xyz = (vec_19 + vec3(-0.0078125, -0.0078125, 0.0078125));
  highp vec4 tmpvar_26;
  lowp vec4 tmpvar_27;
  tmpvar_27 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_25.xyz, 0.0);
  tmpvar_26 = tmpvar_27;
  shadowVals_20.y = dot (tmpvar_26, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  highp vec4 tmpvar_28;
  tmpvar_28.w = 0.0;
  tmpvar_28.xyz = (vec_19 + vec3(-0.0078125, 0.0078125, -0.0078125));
  highp vec4 tmpvar_29;
  lowp vec4 tmpvar_30;
  tmpvar_30 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_28.xyz, 0.0);
  tmpvar_29 = tmpvar_30;
  shadowVals_20.z = dot (tmpvar_29, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  highp vec4 tmpvar_31;
  tmpvar_31.w = 0.0;
  tmpvar_31.xyz = (vec_19 + vec3(0.0078125, -0.0078125, -0.0078125));
  highp vec4 tmpvar_32;
  lowp vec4 tmpvar_33;
  tmpvar_33 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_31.xyz, 0.0);
  tmpvar_32 = tmpvar_33;
  shadowVals_20.w = dot (tmpvar_32, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  bvec4 tmpvar_34;
  tmpvar_34 = lessThan (shadowVals_20, vec4(mydist_21));
  mediump vec4 tmpvar_35;
  tmpvar_35 = _LightShadowData.xxxx;
  mediump float tmpvar_36;
  if (tmpvar_34.x) {
    tmpvar_36 = tmpvar_35.x;
  } else {
    tmpvar_36 = 1.0;
  };
  mediump float tmpvar_37;
  if (tmpvar_34.y) {
    tmpvar_37 = tmpvar_35.y;
  } else {
    tmpvar_37 = 1.0;
  };
  mediump float tmpvar_38;
  if (tmpvar_34.z) {
    tmpvar_38 = tmpvar_35.z;
  } else {
    tmpvar_38 = 1.0;
  };
  mediump float tmpvar_39;
  if (tmpvar_34.w) {
    tmpvar_39 = tmpvar_35.w;
  } else {
    tmpvar_39 = 1.0;
  };
  mediump vec4 tmpvar_40;
  tmpvar_40.x = tmpvar_36;
  tmpvar_40.y = tmpvar_37;
  tmpvar_40.z = tmpvar_38;
  tmpvar_40.w = tmpvar_39;
  mediump float tmpvar_41;
  tmpvar_41 = mix (dot (tmpvar_40, vec4(0.25, 0.25, 0.25, 0.25)), 1.0, tmpvar_17);
  shadow_5 = tmpvar_41;
  highp float tmpvar_42;
  tmpvar_42 = (texture2D (_LightTexture0, vec2(dot (tmpvar_14, tmpvar_14))).w * shadow_5);
  atten_4 = tmpvar_42;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_43;
  mediump vec4 c_44;
  highp vec3 tmpvar_45;
  tmpvar_45 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_46;
  mediump vec3 albedo_47;
  albedo_47 = tmpvar_10;
  mediump vec3 tmpvar_48;
  tmpvar_48 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_47, vec3(_Metallic));
  tmpvar_46 = (albedo_47 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_43 = tmpvar_46;
  mediump vec3 diffColor_49;
  diffColor_49 = tmpvar_43;
  mediump float alpha_50;
  alpha_50 = tmpvar_11;
  tmpvar_43 = diffColor_49;
  mediump vec3 diffColor_51;
  diffColor_51 = tmpvar_43;
  mediump vec3 color_52;
  highp float specularTerm_53;
  highp float a2_54;
  mediump float roughness_55;
  mediump float perceptualRoughness_56;
  highp vec3 tmpvar_57;
  highp vec3 inVec_58;
  inVec_58 = (tmpvar_2 + tmpvar_8);
  tmpvar_57 = (inVec_58 * inversesqrt(max (0.001, 
    dot (inVec_58, inVec_58)
  )));
  mediump float tmpvar_59;
  highp float tmpvar_60;
  tmpvar_60 = clamp (dot (tmpvar_45, tmpvar_2), 0.0, 1.0);
  tmpvar_59 = tmpvar_60;
  highp float tmpvar_61;
  tmpvar_61 = clamp (dot (tmpvar_45, tmpvar_57), 0.0, 1.0);
  highp float tmpvar_62;
  highp float smoothness_63;
  smoothness_63 = _Glossiness;
  tmpvar_62 = (1.0 - smoothness_63);
  perceptualRoughness_56 = tmpvar_62;
  highp float tmpvar_64;
  highp float perceptualRoughness_65;
  perceptualRoughness_65 = perceptualRoughness_56;
  tmpvar_64 = (perceptualRoughness_65 * perceptualRoughness_65);
  roughness_55 = tmpvar_64;
  mediump float tmpvar_66;
  tmpvar_66 = (roughness_55 * roughness_55);
  a2_54 = tmpvar_66;
  specularTerm_53 = ((roughness_55 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_57), 0.0, 1.0)) * (1.5 + roughness_55))
   * 
    (((tmpvar_61 * tmpvar_61) * (a2_54 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_67;
  tmpvar_67 = clamp (specularTerm_53, 0.0, 100.0);
  specularTerm_53 = tmpvar_67;
  highp vec3 tmpvar_68;
  tmpvar_68 = (((diffColor_51 + 
    (tmpvar_67 * tmpvar_48)
  ) * tmpvar_1) * tmpvar_59);
  color_52 = tmpvar_68;
  mediump vec4 tmpvar_69;
  tmpvar_69.w = 1.0;
  tmpvar_69.xyz = color_52;
  c_44.xyz = tmpvar_69.xyz;
  c_44.w = alpha_50;
  c_3.xyz = c_44.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 78 math, 6 textures, 4 branches
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp vec4 _LightPositionRange;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
varying highp vec3 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xyz;
  xlv_TEXCOORD4 = (tmpvar_3.xyz - _LightPositionRange.xyz);
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _LightPositionRange;
uniform highp vec4 _LightProjectionParams;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform lowp samplerCube _ShadowMapTexture;
uniform highp sampler2D _LightTexture0;
uniform highp mat4 unity_WorldToLight;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  highp vec4 tmpvar_13;
  tmpvar_13.w = 1.0;
  tmpvar_13.xyz = xlv_TEXCOORD2;
  highp vec3 tmpvar_14;
  tmpvar_14 = (unity_WorldToLight * tmpvar_13).xyz;
  highp vec4 v_15;
  v_15.x = unity_MatrixV[0].z;
  v_15.y = unity_MatrixV[1].z;
  v_15.z = unity_MatrixV[2].z;
  v_15.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_16;
  tmpvar_16 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_17;
  highp float tmpvar_18;
  tmpvar_18 = clamp (((
    mix (dot (tmpvar_9, v_15.xyz), sqrt(dot (tmpvar_16, tmpvar_16)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_17 = tmpvar_18;
  highp vec3 vec_19;
  vec_19 = (xlv_TEXCOORD2 - _LightPositionRange.xyz);
  highp vec4 shadowVals_20;
  highp float mydist_21;
  mydist_21 = ((sqrt(
    dot (vec_19, vec_19)
  ) * _LightPositionRange.w) * _LightProjectionParams.w);
  highp vec4 tmpvar_22;
  tmpvar_22.w = 0.0;
  tmpvar_22.xyz = (vec_19 + vec3(0.0078125, 0.0078125, 0.0078125));
  highp vec4 tmpvar_23;
  lowp vec4 tmpvar_24;
  tmpvar_24 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_22.xyz, 0.0);
  tmpvar_23 = tmpvar_24;
  shadowVals_20.x = dot (tmpvar_23, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  highp vec4 tmpvar_25;
  tmpvar_25.w = 0.0;
  tmpvar_25.xyz = (vec_19 + vec3(-0.0078125, -0.0078125, 0.0078125));
  highp vec4 tmpvar_26;
  lowp vec4 tmpvar_27;
  tmpvar_27 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_25.xyz, 0.0);
  tmpvar_26 = tmpvar_27;
  shadowVals_20.y = dot (tmpvar_26, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  highp vec4 tmpvar_28;
  tmpvar_28.w = 0.0;
  tmpvar_28.xyz = (vec_19 + vec3(-0.0078125, 0.0078125, -0.0078125));
  highp vec4 tmpvar_29;
  lowp vec4 tmpvar_30;
  tmpvar_30 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_28.xyz, 0.0);
  tmpvar_29 = tmpvar_30;
  shadowVals_20.z = dot (tmpvar_29, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  highp vec4 tmpvar_31;
  tmpvar_31.w = 0.0;
  tmpvar_31.xyz = (vec_19 + vec3(0.0078125, -0.0078125, -0.0078125));
  highp vec4 tmpvar_32;
  lowp vec4 tmpvar_33;
  tmpvar_33 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_31.xyz, 0.0);
  tmpvar_32 = tmpvar_33;
  shadowVals_20.w = dot (tmpvar_32, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  bvec4 tmpvar_34;
  tmpvar_34 = lessThan (shadowVals_20, vec4(mydist_21));
  mediump vec4 tmpvar_35;
  tmpvar_35 = _LightShadowData.xxxx;
  mediump float tmpvar_36;
  if (tmpvar_34.x) {
    tmpvar_36 = tmpvar_35.x;
  } else {
    tmpvar_36 = 1.0;
  };
  mediump float tmpvar_37;
  if (tmpvar_34.y) {
    tmpvar_37 = tmpvar_35.y;
  } else {
    tmpvar_37 = 1.0;
  };
  mediump float tmpvar_38;
  if (tmpvar_34.z) {
    tmpvar_38 = tmpvar_35.z;
  } else {
    tmpvar_38 = 1.0;
  };
  mediump float tmpvar_39;
  if (tmpvar_34.w) {
    tmpvar_39 = tmpvar_35.w;
  } else {
    tmpvar_39 = 1.0;
  };
  mediump vec4 tmpvar_40;
  tmpvar_40.x = tmpvar_36;
  tmpvar_40.y = tmpvar_37;
  tmpvar_40.z = tmpvar_38;
  tmpvar_40.w = tmpvar_39;
  mediump float tmpvar_41;
  tmpvar_41 = mix (dot (tmpvar_40, vec4(0.25, 0.25, 0.25, 0.25)), 1.0, tmpvar_17);
  shadow_5 = tmpvar_41;
  highp float tmpvar_42;
  tmpvar_42 = (texture2D (_LightTexture0, vec2(dot (tmpvar_14, tmpvar_14))).w * shadow_5);
  atten_4 = tmpvar_42;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_43;
  mediump vec4 c_44;
  highp vec3 tmpvar_45;
  tmpvar_45 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_46;
  mediump vec3 albedo_47;
  albedo_47 = tmpvar_10;
  mediump vec3 tmpvar_48;
  tmpvar_48 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_47, vec3(_Metallic));
  tmpvar_46 = (albedo_47 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_43 = tmpvar_46;
  mediump vec3 diffColor_49;
  diffColor_49 = tmpvar_43;
  mediump float alpha_50;
  alpha_50 = tmpvar_11;
  tmpvar_43 = diffColor_49;
  mediump vec3 diffColor_51;
  diffColor_51 = tmpvar_43;
  mediump vec3 color_52;
  highp float specularTerm_53;
  highp float a2_54;
  mediump float roughness_55;
  mediump float perceptualRoughness_56;
  highp vec3 tmpvar_57;
  highp vec3 inVec_58;
  inVec_58 = (tmpvar_2 + tmpvar_8);
  tmpvar_57 = (inVec_58 * inversesqrt(max (0.001, 
    dot (inVec_58, inVec_58)
  )));
  mediump float tmpvar_59;
  highp float tmpvar_60;
  tmpvar_60 = clamp (dot (tmpvar_45, tmpvar_2), 0.0, 1.0);
  tmpvar_59 = tmpvar_60;
  highp float tmpvar_61;
  tmpvar_61 = clamp (dot (tmpvar_45, tmpvar_57), 0.0, 1.0);
  highp float tmpvar_62;
  highp float smoothness_63;
  smoothness_63 = _Glossiness;
  tmpvar_62 = (1.0 - smoothness_63);
  perceptualRoughness_56 = tmpvar_62;
  highp float tmpvar_64;
  highp float perceptualRoughness_65;
  perceptualRoughness_65 = perceptualRoughness_56;
  tmpvar_64 = (perceptualRoughness_65 * perceptualRoughness_65);
  roughness_55 = tmpvar_64;
  mediump float tmpvar_66;
  tmpvar_66 = (roughness_55 * roughness_55);
  a2_54 = tmpvar_66;
  specularTerm_53 = ((roughness_55 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_57), 0.0, 1.0)) * (1.5 + roughness_55))
   * 
    (((tmpvar_61 * tmpvar_61) * (a2_54 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_67;
  tmpvar_67 = clamp (specularTerm_53, 0.0, 100.0);
  specularTerm_53 = tmpvar_67;
  highp vec3 tmpvar_68;
  tmpvar_68 = (((diffColor_51 + 
    (tmpvar_67 * tmpvar_48)
  ) * tmpvar_1) * tmpvar_59);
  color_52 = tmpvar_68;
  mediump vec4 tmpvar_69;
  tmpvar_69.w = 1.0;
  tmpvar_69.xyz = color_52;
  c_44.xyz = tmpvar_69.xyz;
  c_44.w = alpha_50;
  c_3.xyz = c_44.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4.xyz = vec3(0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 _LightPositionRange;
uniform 	vec4 _LightProjectionParams;
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp samplerCubeShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp samplerCube _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec4 u_xlat1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump vec3 u_xlat16_5;
lowp vec3 u_xlat10_5;
mediump float u_xlat16_8;
float u_xlat15;
mediump float u_xlat16_18;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-_LightPositionRange.xyz);
    u_xlat15 = max(abs(u_xlat0.y), abs(u_xlat0.x));
    u_xlat15 = max(abs(u_xlat0.z), u_xlat15);
    u_xlat15 = u_xlat15 + (-_LightProjectionParams.z);
    u_xlat15 = max(u_xlat15, 9.99999975e-06);
    u_xlat15 = u_xlat15 * _LightProjectionParams.w;
    u_xlat15 = _LightProjectionParams.y / u_xlat15;
    u_xlat15 = u_xlat15 + (-_LightProjectionParams.x);
    u_xlat1.xyz = u_xlat0.xyz + vec3(0.0078125, 0.0078125, 0.0078125);
    vec4 txVec0 = vec4(u_xlat1.xyz,u_xlat15);
    u_xlat1.x = texture(hlslcc_zcmp_ShadowMapTexture, txVec0);
    u_xlat2.xyz = u_xlat0.xyz + vec3(-0.0078125, -0.0078125, 0.0078125);
    vec4 txVec1 = vec4(u_xlat2.xyz,u_xlat15);
    u_xlat1.y = texture(hlslcc_zcmp_ShadowMapTexture, txVec1);
    u_xlat2.xyz = u_xlat0.xyz + vec3(-0.0078125, 0.0078125, -0.0078125);
    u_xlat0.xyz = u_xlat0.xyz + vec3(0.0078125, -0.0078125, -0.0078125);
    vec4 txVec2 = vec4(u_xlat0.xyz,u_xlat15);
    u_xlat1.w = texture(hlslcc_zcmp_ShadowMapTexture, txVec2);
    vec4 txVec3 = vec4(u_xlat2.xyz,u_xlat15);
    u_xlat1.z = texture(hlslcc_zcmp_ShadowMapTexture, txVec3);
    u_xlat0.x = dot(u_xlat1, vec4(0.25, 0.25, 0.25, 0.25));
    u_xlat16_3.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_3.x = u_xlat0.x * u_xlat16_3.x + _LightShadowData.x;
    u_xlat16_8 = (-u_xlat16_3.x) + 1.0;
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat1.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat5.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat1.x = dot(u_xlat5.xyz, u_xlat1.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat1.x);
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat0.x + u_xlat1.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_3.x = u_xlat0.x * u_xlat16_8 + u_xlat16_3.x;
    u_xlat1.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD2.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD2.zzz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat0.x = texture(_LightTexture0, u_xlat0.xx).w;
    u_xlat0.x = u_xlat16_3.x * u_xlat0.x;
    u_xlat16_3.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat1.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat1.xyz = u_xlat0.xxx * u_xlat1.xyz;
    u_xlat0.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat2.xyz = u_xlat0.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat2.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_3.xyz = u_xlat0.xxx * u_xlat16_3.xyz;
    u_xlat0.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx * u_xlat5.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat2.xyz);
    u_xlat15 = u_xlat15 + u_xlat15;
    u_xlat0.xyz = u_xlat2.xyz * (-vec3(u_xlat15)) + u_xlat0.xyz;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat1.xyz);
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.y = (-_Glossiness) + 1.0;
    u_xlat0.x = texture(unity_NHxRoughness, u_xlat0.xy).w;
    u_xlat0.x = u_xlat0.x * 16.0;
    u_xlat10_5.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_5.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_5.xyz = u_xlat10_5.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_4.xyz = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_18 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_4.xyz = u_xlat16_5.xyz * vec3(u_xlat16_18) + u_xlat16_4.xyz;
    SV_Target0.xyz = u_xlat16_3.xyz * u_xlat16_4.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4.xyz = vec3(0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 _LightPositionRange;
uniform 	vec4 _LightProjectionParams;
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform lowp samplerCubeShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp samplerCube _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec4 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
vec3 u_xlat2;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump float u_xlat16_8;
mediump float u_xlat16_10;
float u_xlat15;
float u_xlat16;
mediump float u_xlat16_16;
mediump float u_xlat16_18;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-_LightPositionRange.xyz);
    u_xlat15 = max(abs(u_xlat0.y), abs(u_xlat0.x));
    u_xlat15 = max(abs(u_xlat0.z), u_xlat15);
    u_xlat15 = u_xlat15 + (-_LightProjectionParams.z);
    u_xlat15 = max(u_xlat15, 9.99999975e-06);
    u_xlat15 = u_xlat15 * _LightProjectionParams.w;
    u_xlat15 = _LightProjectionParams.y / u_xlat15;
    u_xlat15 = u_xlat15 + (-_LightProjectionParams.x);
    u_xlat1.xyz = u_xlat0.xyz + vec3(0.0078125, 0.0078125, 0.0078125);
    vec4 txVec0 = vec4(u_xlat1.xyz,u_xlat15);
    u_xlat1.x = texture(hlslcc_zcmp_ShadowMapTexture, txVec0);
    u_xlat2.xyz = u_xlat0.xyz + vec3(-0.0078125, -0.0078125, 0.0078125);
    vec4 txVec1 = vec4(u_xlat2.xyz,u_xlat15);
    u_xlat1.y = texture(hlslcc_zcmp_ShadowMapTexture, txVec1);
    u_xlat2.xyz = u_xlat0.xyz + vec3(-0.0078125, 0.0078125, -0.0078125);
    u_xlat0.xyz = u_xlat0.xyz + vec3(0.0078125, -0.0078125, -0.0078125);
    vec4 txVec2 = vec4(u_xlat0.xyz,u_xlat15);
    u_xlat1.w = texture(hlslcc_zcmp_ShadowMapTexture, txVec2);
    vec4 txVec3 = vec4(u_xlat2.xyz,u_xlat15);
    u_xlat1.z = texture(hlslcc_zcmp_ShadowMapTexture, txVec3);
    u_xlat0.x = dot(u_xlat1, vec4(0.25, 0.25, 0.25, 0.25));
    u_xlat16_3.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_3.x = u_xlat0.x * u_xlat16_3.x + _LightShadowData.x;
    u_xlat16_8 = (-u_xlat16_3.x) + 1.0;
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat1.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat5.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat1.x = dot(u_xlat5.xyz, u_xlat1.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat1.x);
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat0.x + u_xlat1.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_3.x = u_xlat0.x * u_xlat16_8 + u_xlat16_3.x;
    u_xlat1.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD2.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD2.zzz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat0.x = texture(_LightTexture0, u_xlat0.xx).w;
    u_xlat0.x = u_xlat16_3.x * u_xlat0.x;
    u_xlat16_3.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat1.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat16 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat16 = inversesqrt(u_xlat16);
    u_xlat1.xyz = vec3(u_xlat16) * u_xlat1.xyz;
    u_xlat0.xyz = u_xlat5.xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = max(u_xlat15, 0.00100000005);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat15 = dot(u_xlat1.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat15 = min(max(u_xlat15, 0.0), 1.0);
#else
    u_xlat15 = clamp(u_xlat15, 0.0, 1.0);
#endif
    u_xlat15 = max(u_xlat15, 0.319999993);
    u_xlat16_16 = (-_Glossiness) + 1.0;
    u_xlat16_2 = u_xlat16_16 * u_xlat16_16 + 1.5;
    u_xlat16_16 = u_xlat16_16 * u_xlat16_16;
    u_xlat15 = u_xlat15 * u_xlat16_2;
    u_xlat2.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat2.x = inversesqrt(u_xlat2.x);
    u_xlat2.xyz = u_xlat2.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat2.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat5.x = dot(u_xlat2.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat16_10 = u_xlat16_16 * u_xlat16_16 + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_10 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat15;
    u_xlat0.x = u_xlat16_16 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_18 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * vec3(u_xlat16_18) + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat16_3.xyz * u_xlat0.xzw;
    u_xlat0.xyz = u_xlat5.xxx * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4.xyz = vec3(0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 _LightPositionRange;
uniform 	vec4 _LightProjectionParams;
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTexture0;
uniform lowp samplerCubeShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp samplerCube _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec4 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
vec3 u_xlat2;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump float u_xlat16_8;
mediump float u_xlat16_10;
float u_xlat15;
float u_xlat16;
mediump float u_xlat16_16;
mediump float u_xlat16_18;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-_LightPositionRange.xyz);
    u_xlat15 = max(abs(u_xlat0.y), abs(u_xlat0.x));
    u_xlat15 = max(abs(u_xlat0.z), u_xlat15);
    u_xlat15 = u_xlat15 + (-_LightProjectionParams.z);
    u_xlat15 = max(u_xlat15, 9.99999975e-06);
    u_xlat15 = u_xlat15 * _LightProjectionParams.w;
    u_xlat15 = _LightProjectionParams.y / u_xlat15;
    u_xlat15 = u_xlat15 + (-_LightProjectionParams.x);
    u_xlat1.xyz = u_xlat0.xyz + vec3(0.0078125, 0.0078125, 0.0078125);
    vec4 txVec0 = vec4(u_xlat1.xyz,u_xlat15);
    u_xlat1.x = texture(hlslcc_zcmp_ShadowMapTexture, txVec0);
    u_xlat2.xyz = u_xlat0.xyz + vec3(-0.0078125, -0.0078125, 0.0078125);
    vec4 txVec1 = vec4(u_xlat2.xyz,u_xlat15);
    u_xlat1.y = texture(hlslcc_zcmp_ShadowMapTexture, txVec1);
    u_xlat2.xyz = u_xlat0.xyz + vec3(-0.0078125, 0.0078125, -0.0078125);
    u_xlat0.xyz = u_xlat0.xyz + vec3(0.0078125, -0.0078125, -0.0078125);
    vec4 txVec2 = vec4(u_xlat0.xyz,u_xlat15);
    u_xlat1.w = texture(hlslcc_zcmp_ShadowMapTexture, txVec2);
    vec4 txVec3 = vec4(u_xlat2.xyz,u_xlat15);
    u_xlat1.z = texture(hlslcc_zcmp_ShadowMapTexture, txVec3);
    u_xlat0.x = dot(u_xlat1, vec4(0.25, 0.25, 0.25, 0.25));
    u_xlat16_3.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_3.x = u_xlat0.x * u_xlat16_3.x + _LightShadowData.x;
    u_xlat16_8 = (-u_xlat16_3.x) + 1.0;
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat1.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat5.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat1.x = dot(u_xlat5.xyz, u_xlat1.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat1.x);
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat0.x + u_xlat1.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_3.x = u_xlat0.x * u_xlat16_8 + u_xlat16_3.x;
    u_xlat1.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD2.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD2.zzz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat0.x = texture(_LightTexture0, u_xlat0.xx).w;
    u_xlat0.x = u_xlat16_3.x * u_xlat0.x;
    u_xlat16_3.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat1.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat16 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat16 = inversesqrt(u_xlat16);
    u_xlat1.xyz = vec3(u_xlat16) * u_xlat1.xyz;
    u_xlat0.xyz = u_xlat5.xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = max(u_xlat15, 0.00100000005);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat15 = dot(u_xlat1.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat15 = min(max(u_xlat15, 0.0), 1.0);
#else
    u_xlat15 = clamp(u_xlat15, 0.0, 1.0);
#endif
    u_xlat15 = max(u_xlat15, 0.319999993);
    u_xlat16_16 = (-_Glossiness) + 1.0;
    u_xlat16_2 = u_xlat16_16 * u_xlat16_16 + 1.5;
    u_xlat16_16 = u_xlat16_16 * u_xlat16_16;
    u_xlat15 = u_xlat15 * u_xlat16_2;
    u_xlat2.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat2.x = inversesqrt(u_xlat2.x);
    u_xlat2.xyz = u_xlat2.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat2.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat5.x = dot(u_xlat2.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat16_10 = u_xlat16_16 * u_xlat16_16 + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_10 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat15;
    u_xlat0.x = u_xlat16_16 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_18 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * vec3(u_xlat16_18) + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat16_3.xyz * u_xlat0.xzw;
    u_xlat0.xyz = u_xlat5.xxx * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 52 math, 5 textures, 1 branches
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp vec4 _LightPositionRange;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
varying highp vec3 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xyz;
  xlv_TEXCOORD4 = (tmpvar_3.xyz - _LightPositionRange.xyz);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _LightPositionRange;
uniform highp vec4 _LightProjectionParams;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp samplerCube _ShadowMapTexture;
uniform highp samplerCube _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  highp vec4 v_13;
  v_13.x = unity_MatrixV[0].z;
  v_13.y = unity_MatrixV[1].z;
  v_13.z = unity_MatrixV[2].z;
  v_13.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_14;
  tmpvar_14 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_15;
  highp float tmpvar_16;
  tmpvar_16 = clamp (((
    mix (dot (tmpvar_9, v_13.xyz), sqrt(dot (tmpvar_14, tmpvar_14)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_15 = tmpvar_16;
  highp vec3 vec_17;
  vec_17 = (xlv_TEXCOORD2 - _LightPositionRange.xyz);
  mediump float shadowVal_18;
  highp float mydist_19;
  mydist_19 = ((sqrt(
    dot (vec_17, vec_17)
  ) * _LightPositionRange.w) * _LightProjectionParams.w);
  lowp vec4 tmpvar_20;
  tmpvar_20 = textureCube (_ShadowMapTexture, vec_17);
  highp vec4 vals_21;
  vals_21 = tmpvar_20;
  highp float tmpvar_22;
  tmpvar_22 = dot (vals_21, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  shadowVal_18 = tmpvar_22;
  mediump float tmpvar_23;
  if ((shadowVal_18 < mydist_19)) {
    tmpvar_23 = _LightShadowData.x;
  } else {
    tmpvar_23 = 1.0;
  };
  mediump float tmpvar_24;
  tmpvar_24 = mix (tmpvar_23, 1.0, tmpvar_15);
  shadow_5 = tmpvar_24;
  highp float tmpvar_25;
  tmpvar_25 = ((texture2D (_LightTextureB0, vec2(dot (xlv_TEXCOORD3, xlv_TEXCOORD3))).w * textureCube (_LightTexture0, xlv_TEXCOORD3).w) * shadow_5);
  atten_4 = tmpvar_25;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_26;
  mediump vec4 c_27;
  highp vec3 tmpvar_28;
  tmpvar_28 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_29;
  mediump vec3 albedo_30;
  albedo_30 = tmpvar_10;
  tmpvar_29 = (albedo_30 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_26 = tmpvar_29;
  mediump vec3 diffColor_31;
  diffColor_31 = tmpvar_26;
  mediump float alpha_32;
  alpha_32 = tmpvar_11;
  tmpvar_26 = diffColor_31;
  mediump vec3 diffColor_33;
  diffColor_33 = tmpvar_26;
  mediump vec2 rlPow4AndFresnelTerm_34;
  mediump float tmpvar_35;
  highp float tmpvar_36;
  tmpvar_36 = clamp (dot (tmpvar_28, tmpvar_2), 0.0, 1.0);
  tmpvar_35 = tmpvar_36;
  mediump float tmpvar_37;
  highp float tmpvar_38;
  tmpvar_38 = clamp (dot (tmpvar_28, tmpvar_8), 0.0, 1.0);
  tmpvar_37 = tmpvar_38;
  highp vec2 tmpvar_39;
  tmpvar_39.x = dot ((tmpvar_8 - (2.0 * 
    (dot (tmpvar_28, tmpvar_8) * tmpvar_28)
  )), tmpvar_2);
  tmpvar_39.y = (1.0 - tmpvar_37);
  highp vec2 tmpvar_40;
  tmpvar_40 = ((tmpvar_39 * tmpvar_39) * (tmpvar_39 * tmpvar_39));
  rlPow4AndFresnelTerm_34 = tmpvar_40;
  mediump float tmpvar_41;
  tmpvar_41 = rlPow4AndFresnelTerm_34.x;
  mediump float specular_42;
  highp float smoothness_43;
  smoothness_43 = _Glossiness;
  highp vec2 tmpvar_44;
  tmpvar_44.x = tmpvar_41;
  tmpvar_44.y = (1.0 - smoothness_43);
  highp float tmpvar_45;
  tmpvar_45 = (texture2D (unity_NHxRoughness, tmpvar_44).w * 16.0);
  specular_42 = tmpvar_45;
  mediump vec4 tmpvar_46;
  tmpvar_46.w = 1.0;
  tmpvar_46.xyz = ((diffColor_33 + (specular_42 * 
    mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_30, vec3(_Metallic))
  )) * (tmpvar_1 * tmpvar_35));
  c_27.xyz = tmpvar_46.xyz;
  c_27.w = alpha_32;
  c_3.xyz = c_27.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 62 math, 4 textures, 1 branches
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp vec4 _LightPositionRange;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
varying highp vec3 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xyz;
  xlv_TEXCOORD4 = (tmpvar_3.xyz - _LightPositionRange.xyz);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _LightPositionRange;
uniform highp vec4 _LightProjectionParams;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform lowp samplerCube _ShadowMapTexture;
uniform highp samplerCube _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  highp vec4 v_13;
  v_13.x = unity_MatrixV[0].z;
  v_13.y = unity_MatrixV[1].z;
  v_13.z = unity_MatrixV[2].z;
  v_13.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_14;
  tmpvar_14 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_15;
  highp float tmpvar_16;
  tmpvar_16 = clamp (((
    mix (dot (tmpvar_9, v_13.xyz), sqrt(dot (tmpvar_14, tmpvar_14)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_15 = tmpvar_16;
  highp vec3 vec_17;
  vec_17 = (xlv_TEXCOORD2 - _LightPositionRange.xyz);
  mediump float shadowVal_18;
  highp float mydist_19;
  mydist_19 = ((sqrt(
    dot (vec_17, vec_17)
  ) * _LightPositionRange.w) * _LightProjectionParams.w);
  lowp vec4 tmpvar_20;
  tmpvar_20 = textureCube (_ShadowMapTexture, vec_17);
  highp vec4 vals_21;
  vals_21 = tmpvar_20;
  highp float tmpvar_22;
  tmpvar_22 = dot (vals_21, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  shadowVal_18 = tmpvar_22;
  mediump float tmpvar_23;
  if ((shadowVal_18 < mydist_19)) {
    tmpvar_23 = _LightShadowData.x;
  } else {
    tmpvar_23 = 1.0;
  };
  mediump float tmpvar_24;
  tmpvar_24 = mix (tmpvar_23, 1.0, tmpvar_15);
  shadow_5 = tmpvar_24;
  highp float tmpvar_25;
  tmpvar_25 = ((texture2D (_LightTextureB0, vec2(dot (xlv_TEXCOORD3, xlv_TEXCOORD3))).w * textureCube (_LightTexture0, xlv_TEXCOORD3).w) * shadow_5);
  atten_4 = tmpvar_25;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_26;
  mediump vec4 c_27;
  highp vec3 tmpvar_28;
  tmpvar_28 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_29;
  mediump vec3 albedo_30;
  albedo_30 = tmpvar_10;
  mediump vec3 tmpvar_31;
  tmpvar_31 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_30, vec3(_Metallic));
  tmpvar_29 = (albedo_30 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_26 = tmpvar_29;
  mediump vec3 diffColor_32;
  diffColor_32 = tmpvar_26;
  mediump float alpha_33;
  alpha_33 = tmpvar_11;
  tmpvar_26 = diffColor_32;
  mediump vec3 diffColor_34;
  diffColor_34 = tmpvar_26;
  mediump vec3 color_35;
  highp float specularTerm_36;
  highp float a2_37;
  mediump float roughness_38;
  mediump float perceptualRoughness_39;
  highp vec3 tmpvar_40;
  highp vec3 inVec_41;
  inVec_41 = (tmpvar_2 + tmpvar_8);
  tmpvar_40 = (inVec_41 * inversesqrt(max (0.001, 
    dot (inVec_41, inVec_41)
  )));
  mediump float tmpvar_42;
  highp float tmpvar_43;
  tmpvar_43 = clamp (dot (tmpvar_28, tmpvar_2), 0.0, 1.0);
  tmpvar_42 = tmpvar_43;
  highp float tmpvar_44;
  tmpvar_44 = clamp (dot (tmpvar_28, tmpvar_40), 0.0, 1.0);
  highp float tmpvar_45;
  highp float smoothness_46;
  smoothness_46 = _Glossiness;
  tmpvar_45 = (1.0 - smoothness_46);
  perceptualRoughness_39 = tmpvar_45;
  highp float tmpvar_47;
  highp float perceptualRoughness_48;
  perceptualRoughness_48 = perceptualRoughness_39;
  tmpvar_47 = (perceptualRoughness_48 * perceptualRoughness_48);
  roughness_38 = tmpvar_47;
  mediump float tmpvar_49;
  tmpvar_49 = (roughness_38 * roughness_38);
  a2_37 = tmpvar_49;
  specularTerm_36 = ((roughness_38 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_40), 0.0, 1.0)) * (1.5 + roughness_38))
   * 
    (((tmpvar_44 * tmpvar_44) * (a2_37 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_50;
  tmpvar_50 = clamp (specularTerm_36, 0.0, 100.0);
  specularTerm_36 = tmpvar_50;
  highp vec3 tmpvar_51;
  tmpvar_51 = (((diffColor_34 + 
    (tmpvar_50 * tmpvar_31)
  ) * tmpvar_1) * tmpvar_42);
  color_35 = tmpvar_51;
  mediump vec4 tmpvar_52;
  tmpvar_52.w = 1.0;
  tmpvar_52.xyz = color_35;
  c_27.xyz = tmpvar_52.xyz;
  c_27.w = alpha_33;
  c_3.xyz = c_27.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 62 math, 4 textures, 1 branches
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp vec4 _LightPositionRange;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
varying highp vec3 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xyz;
  xlv_TEXCOORD4 = (tmpvar_3.xyz - _LightPositionRange.xyz);
}


#endif
#ifdef FRAGMENT
uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _LightPositionRange;
uniform highp vec4 _LightProjectionParams;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform lowp samplerCube _ShadowMapTexture;
uniform highp samplerCube _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  highp vec4 v_13;
  v_13.x = unity_MatrixV[0].z;
  v_13.y = unity_MatrixV[1].z;
  v_13.z = unity_MatrixV[2].z;
  v_13.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_14;
  tmpvar_14 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_15;
  highp float tmpvar_16;
  tmpvar_16 = clamp (((
    mix (dot (tmpvar_9, v_13.xyz), sqrt(dot (tmpvar_14, tmpvar_14)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_15 = tmpvar_16;
  highp vec3 vec_17;
  vec_17 = (xlv_TEXCOORD2 - _LightPositionRange.xyz);
  mediump float shadowVal_18;
  highp float mydist_19;
  mydist_19 = ((sqrt(
    dot (vec_17, vec_17)
  ) * _LightPositionRange.w) * _LightProjectionParams.w);
  lowp vec4 tmpvar_20;
  tmpvar_20 = textureCube (_ShadowMapTexture, vec_17);
  highp vec4 vals_21;
  vals_21 = tmpvar_20;
  highp float tmpvar_22;
  tmpvar_22 = dot (vals_21, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  shadowVal_18 = tmpvar_22;
  mediump float tmpvar_23;
  if ((shadowVal_18 < mydist_19)) {
    tmpvar_23 = _LightShadowData.x;
  } else {
    tmpvar_23 = 1.0;
  };
  mediump float tmpvar_24;
  tmpvar_24 = mix (tmpvar_23, 1.0, tmpvar_15);
  shadow_5 = tmpvar_24;
  highp float tmpvar_25;
  tmpvar_25 = ((texture2D (_LightTextureB0, vec2(dot (xlv_TEXCOORD3, xlv_TEXCOORD3))).w * textureCube (_LightTexture0, xlv_TEXCOORD3).w) * shadow_5);
  atten_4 = tmpvar_25;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_26;
  mediump vec4 c_27;
  highp vec3 tmpvar_28;
  tmpvar_28 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_29;
  mediump vec3 albedo_30;
  albedo_30 = tmpvar_10;
  mediump vec3 tmpvar_31;
  tmpvar_31 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_30, vec3(_Metallic));
  tmpvar_29 = (albedo_30 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_26 = tmpvar_29;
  mediump vec3 diffColor_32;
  diffColor_32 = tmpvar_26;
  mediump float alpha_33;
  alpha_33 = tmpvar_11;
  tmpvar_26 = diffColor_32;
  mediump vec3 diffColor_34;
  diffColor_34 = tmpvar_26;
  mediump vec3 color_35;
  highp float specularTerm_36;
  highp float a2_37;
  mediump float roughness_38;
  mediump float perceptualRoughness_39;
  highp vec3 tmpvar_40;
  highp vec3 inVec_41;
  inVec_41 = (tmpvar_2 + tmpvar_8);
  tmpvar_40 = (inVec_41 * inversesqrt(max (0.001, 
    dot (inVec_41, inVec_41)
  )));
  mediump float tmpvar_42;
  highp float tmpvar_43;
  tmpvar_43 = clamp (dot (tmpvar_28, tmpvar_2), 0.0, 1.0);
  tmpvar_42 = tmpvar_43;
  highp float tmpvar_44;
  tmpvar_44 = clamp (dot (tmpvar_28, tmpvar_40), 0.0, 1.0);
  highp float tmpvar_45;
  highp float smoothness_46;
  smoothness_46 = _Glossiness;
  tmpvar_45 = (1.0 - smoothness_46);
  perceptualRoughness_39 = tmpvar_45;
  highp float tmpvar_47;
  highp float perceptualRoughness_48;
  perceptualRoughness_48 = perceptualRoughness_39;
  tmpvar_47 = (perceptualRoughness_48 * perceptualRoughness_48);
  roughness_38 = tmpvar_47;
  mediump float tmpvar_49;
  tmpvar_49 = (roughness_38 * roughness_38);
  a2_37 = tmpvar_49;
  specularTerm_36 = ((roughness_38 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_40), 0.0, 1.0)) * (1.5 + roughness_38))
   * 
    (((tmpvar_44 * tmpvar_44) * (a2_37 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_50;
  tmpvar_50 = clamp (specularTerm_36, 0.0, 100.0);
  specularTerm_36 = tmpvar_50;
  highp vec3 tmpvar_51;
  tmpvar_51 = (((diffColor_34 + 
    (tmpvar_50 * tmpvar_31)
  ) * tmpvar_1) * tmpvar_42);
  color_35 = tmpvar_51;
  mediump vec4 tmpvar_52;
  tmpvar_52.w = 1.0;
  tmpvar_52.xyz = color_35;
  c_27.xyz = tmpvar_52.xyz;
  c_27.w = alpha_33;
  c_3.xyz = c_27.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4.xyz = vec3(0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 _LightPositionRange;
uniform 	vec4 _LightProjectionParams;
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTextureB0;
uniform highp samplerCube _LightTexture0;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp samplerCubeShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp samplerCube _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec3 u_xlat1;
mediump vec3 u_xlat16_2;
vec3 u_xlat3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump vec3 u_xlat16_5;
lowp vec3 u_xlat10_5;
mediump float u_xlat16_7;
float u_xlat10;
float u_xlat15;
mediump float u_xlat16_17;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD2.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD2.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = texture(_LightTexture0, u_xlat0.xyz).w;
    u_xlat5.x = texture(_LightTextureB0, vec2(u_xlat15)).w;
    u_xlat0.x = u_xlat0.x * u_xlat5.x;
    u_xlat5.xyz = vs_TEXCOORD2.xyz + (-_LightPositionRange.xyz);
    u_xlat1.x = max(abs(u_xlat5.y), abs(u_xlat5.x));
    u_xlat1.x = max(abs(u_xlat5.z), u_xlat1.x);
    u_xlat1.x = u_xlat1.x + (-_LightProjectionParams.z);
    u_xlat1.x = max(u_xlat1.x, 9.99999975e-06);
    u_xlat1.x = u_xlat1.x * _LightProjectionParams.w;
    u_xlat1.x = _LightProjectionParams.y / u_xlat1.x;
    u_xlat1.x = u_xlat1.x + (-_LightProjectionParams.x);
    vec4 txVec0 = vec4(u_xlat5.xyz,u_xlat1.x);
    u_xlat10_5.x = texture(hlslcc_zcmp_ShadowMapTexture, txVec0);
    u_xlat16_2.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_2.x = u_xlat10_5.x * u_xlat16_2.x + _LightShadowData.x;
    u_xlat16_7 = (-u_xlat16_2.x) + 1.0;
    u_xlat5.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat5.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat5.x = sqrt(u_xlat5.x);
    u_xlat1.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat3.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat10 = dot(u_xlat3.xyz, u_xlat1.xyz);
    u_xlat5.x = (-u_xlat10) + u_xlat5.x;
    u_xlat5.x = unity_ShadowFadeCenterAndType.w * u_xlat5.x + u_xlat10;
    u_xlat5.x = u_xlat5.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat16_2.x = u_xlat5.x * u_xlat16_7 + u_xlat16_2.x;
    u_xlat0.x = u_xlat0.x * u_xlat16_2.x;
    u_xlat16_2.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat15 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat1.xyz = vec3(u_xlat15) * vs_TEXCOORD1.xyz;
    u_xlat15 = dot(u_xlat1.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat15 = min(max(u_xlat15, 0.0), 1.0);
#else
    u_xlat15 = clamp(u_xlat15, 0.0, 1.0);
#endif
    u_xlat16_2.xyz = vec3(u_xlat15) * u_xlat16_2.xyz;
    u_xlat15 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat3.xyz = vec3(u_xlat15) * u_xlat3.xyz;
    u_xlat15 = dot(u_xlat3.xyz, u_xlat1.xyz);
    u_xlat15 = u_xlat15 + u_xlat15;
    u_xlat1.xyz = u_xlat1.xyz * (-vec3(u_xlat15)) + u_xlat3.xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat0.xyz);
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.y = (-_Glossiness) + 1.0;
    u_xlat0.x = texture(unity_NHxRoughness, u_xlat0.xy).w;
    u_xlat0.x = u_xlat0.x * 16.0;
    u_xlat10_5.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_5.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_5.xyz = u_xlat10_5.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_4.xyz = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_17 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_4.xyz = u_xlat16_5.xyz * vec3(u_xlat16_17) + u_xlat16_4.xyz;
    SV_Target0.xyz = u_xlat16_2.xyz * u_xlat16_4.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4.xyz = vec3(0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 _LightPositionRange;
uniform 	vec4 _LightProjectionParams;
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTextureB0;
uniform highp samplerCube _LightTexture0;
uniform lowp samplerCubeShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp samplerCube _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
vec3 u_xlat3;
mediump float u_xlat16_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
lowp float u_xlat10_5;
mediump float u_xlat16_7;
float u_xlat10;
float u_xlat15;
mediump float u_xlat16_15;
mediump float u_xlat16_16;
mediump float u_xlat16_17;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD2.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD2.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = texture(_LightTexture0, u_xlat0.xyz).w;
    u_xlat5.x = texture(_LightTextureB0, vec2(u_xlat15)).w;
    u_xlat0.x = u_xlat0.x * u_xlat5.x;
    u_xlat5.xyz = vs_TEXCOORD2.xyz + (-_LightPositionRange.xyz);
    u_xlat1.x = max(abs(u_xlat5.y), abs(u_xlat5.x));
    u_xlat1.x = max(abs(u_xlat5.z), u_xlat1.x);
    u_xlat1.x = u_xlat1.x + (-_LightProjectionParams.z);
    u_xlat1.x = max(u_xlat1.x, 9.99999975e-06);
    u_xlat1.x = u_xlat1.x * _LightProjectionParams.w;
    u_xlat1.x = _LightProjectionParams.y / u_xlat1.x;
    u_xlat1.x = u_xlat1.x + (-_LightProjectionParams.x);
    vec4 txVec0 = vec4(u_xlat5.xyz,u_xlat1.x);
    u_xlat10_5 = texture(hlslcc_zcmp_ShadowMapTexture, txVec0);
    u_xlat16_2.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_2.x = u_xlat10_5 * u_xlat16_2.x + _LightShadowData.x;
    u_xlat16_7 = (-u_xlat16_2.x) + 1.0;
    u_xlat5.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat5.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat5.x = sqrt(u_xlat5.x);
    u_xlat1.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat3.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat10 = dot(u_xlat3.xyz, u_xlat1.xyz);
    u_xlat5.x = (-u_xlat10) + u_xlat5.x;
    u_xlat5.x = unity_ShadowFadeCenterAndType.w * u_xlat5.x + u_xlat10;
    u_xlat5.x = u_xlat5.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat16_2.x = u_xlat5.x * u_xlat16_7 + u_xlat16_2.x;
    u_xlat0.x = u_xlat0.x * u_xlat16_2.x;
    u_xlat16_2.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat5.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat1.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat1.x = inversesqrt(u_xlat1.x);
    u_xlat5.xyz = u_xlat5.xyz * u_xlat1.xxx;
    u_xlat1.xyz = u_xlat3.xyz * u_xlat0.xxx + u_xlat5.xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat0.x = max(u_xlat0.x, 0.00100000005);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat1.xyz = u_xlat0.xxx * u_xlat1.xyz;
    u_xlat0.x = dot(u_xlat5.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat0.x = max(u_xlat0.x, 0.319999993);
    u_xlat16_16 = (-_Glossiness) + 1.0;
    u_xlat16_3 = u_xlat16_16 * u_xlat16_16 + 1.5;
    u_xlat16_16 = u_xlat16_16 * u_xlat16_16;
    u_xlat0.x = u_xlat0.x * u_xlat16_3;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat1.x = dot(u_xlat3.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat1.x = min(max(u_xlat1.x, 0.0), 1.0);
#else
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
#endif
    u_xlat5.x = dot(u_xlat3.xyz, u_xlat5.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat10 = u_xlat1.x * u_xlat1.x;
    u_xlat16_15 = u_xlat16_16 * u_xlat16_16 + -1.0;
    u_xlat10 = u_xlat10 * u_xlat16_15 + 1.00001001;
    u_xlat0.x = u_xlat10 * u_xlat0.x;
    u_xlat0.x = u_xlat16_16 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_17 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * vec3(u_xlat16_17) + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat16_2.xyz * u_xlat0.xzw;
    u_xlat0.xyz = u_xlat5.xxx * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4.xyz = vec3(0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 _LightPositionRange;
uniform 	vec4 _LightProjectionParams;
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTextureB0;
uniform highp samplerCube _LightTexture0;
uniform lowp samplerCubeShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp samplerCube _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
vec3 u_xlat3;
mediump float u_xlat16_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
lowp float u_xlat10_5;
mediump float u_xlat16_7;
float u_xlat10;
float u_xlat15;
mediump float u_xlat16_15;
mediump float u_xlat16_16;
mediump float u_xlat16_17;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD2.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD2.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = texture(_LightTexture0, u_xlat0.xyz).w;
    u_xlat5.x = texture(_LightTextureB0, vec2(u_xlat15)).w;
    u_xlat0.x = u_xlat0.x * u_xlat5.x;
    u_xlat5.xyz = vs_TEXCOORD2.xyz + (-_LightPositionRange.xyz);
    u_xlat1.x = max(abs(u_xlat5.y), abs(u_xlat5.x));
    u_xlat1.x = max(abs(u_xlat5.z), u_xlat1.x);
    u_xlat1.x = u_xlat1.x + (-_LightProjectionParams.z);
    u_xlat1.x = max(u_xlat1.x, 9.99999975e-06);
    u_xlat1.x = u_xlat1.x * _LightProjectionParams.w;
    u_xlat1.x = _LightProjectionParams.y / u_xlat1.x;
    u_xlat1.x = u_xlat1.x + (-_LightProjectionParams.x);
    vec4 txVec0 = vec4(u_xlat5.xyz,u_xlat1.x);
    u_xlat10_5 = texture(hlslcc_zcmp_ShadowMapTexture, txVec0);
    u_xlat16_2.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_2.x = u_xlat10_5 * u_xlat16_2.x + _LightShadowData.x;
    u_xlat16_7 = (-u_xlat16_2.x) + 1.0;
    u_xlat5.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat5.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat5.x = sqrt(u_xlat5.x);
    u_xlat1.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat3.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat10 = dot(u_xlat3.xyz, u_xlat1.xyz);
    u_xlat5.x = (-u_xlat10) + u_xlat5.x;
    u_xlat5.x = unity_ShadowFadeCenterAndType.w * u_xlat5.x + u_xlat10;
    u_xlat5.x = u_xlat5.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat16_2.x = u_xlat5.x * u_xlat16_7 + u_xlat16_2.x;
    u_xlat0.x = u_xlat0.x * u_xlat16_2.x;
    u_xlat16_2.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat5.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat1.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat1.x = inversesqrt(u_xlat1.x);
    u_xlat5.xyz = u_xlat5.xyz * u_xlat1.xxx;
    u_xlat1.xyz = u_xlat3.xyz * u_xlat0.xxx + u_xlat5.xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat0.x = max(u_xlat0.x, 0.00100000005);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat1.xyz = u_xlat0.xxx * u_xlat1.xyz;
    u_xlat0.x = dot(u_xlat5.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat0.x = max(u_xlat0.x, 0.319999993);
    u_xlat16_16 = (-_Glossiness) + 1.0;
    u_xlat16_3 = u_xlat16_16 * u_xlat16_16 + 1.5;
    u_xlat16_16 = u_xlat16_16 * u_xlat16_16;
    u_xlat0.x = u_xlat0.x * u_xlat16_3;
    u_xlat3.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat3.xxx * vs_TEXCOORD1.xyz;
    u_xlat1.x = dot(u_xlat3.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat1.x = min(max(u_xlat1.x, 0.0), 1.0);
#else
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
#endif
    u_xlat5.x = dot(u_xlat3.xyz, u_xlat5.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat10 = u_xlat1.x * u_xlat1.x;
    u_xlat16_15 = u_xlat16_16 * u_xlat16_16 + -1.0;
    u_xlat10 = u_xlat10 * u_xlat16_15 + 1.00001001;
    u_xlat0.x = u_xlat10 * u_xlat0.x;
    u_xlat0.x = u_xlat16_16 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_17 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * vec3(u_xlat16_17) + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat16_2.xyz * u_xlat0.xzw;
    u_xlat0.xyz = u_xlat5.xxx * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

//////////////////////////////////////////////////////
Keywords set in this variant: POINT_COOKIE SHADOWS_CUBE SHADOWS_SOFT 
-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 67 math, 8 textures, 4 branches
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp vec4 _LightPositionRange;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
varying highp vec3 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xyz;
  xlv_TEXCOORD4 = (tmpvar_3.xyz - _LightPositionRange.xyz);
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _LightPositionRange;
uniform highp vec4 _LightProjectionParams;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp samplerCube _ShadowMapTexture;
uniform highp samplerCube _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  highp vec4 v_13;
  v_13.x = unity_MatrixV[0].z;
  v_13.y = unity_MatrixV[1].z;
  v_13.z = unity_MatrixV[2].z;
  v_13.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_14;
  tmpvar_14 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_15;
  highp float tmpvar_16;
  tmpvar_16 = clamp (((
    mix (dot (tmpvar_9, v_13.xyz), sqrt(dot (tmpvar_14, tmpvar_14)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_15 = tmpvar_16;
  highp vec3 vec_17;
  vec_17 = (xlv_TEXCOORD2 - _LightPositionRange.xyz);
  highp vec4 shadowVals_18;
  highp float mydist_19;
  mydist_19 = ((sqrt(
    dot (vec_17, vec_17)
  ) * _LightPositionRange.w) * _LightProjectionParams.w);
  highp vec4 tmpvar_20;
  tmpvar_20.w = 0.0;
  tmpvar_20.xyz = (vec_17 + vec3(0.0078125, 0.0078125, 0.0078125));
  highp vec4 tmpvar_21;
  lowp vec4 tmpvar_22;
  tmpvar_22 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_20.xyz, 0.0);
  tmpvar_21 = tmpvar_22;
  shadowVals_18.x = dot (tmpvar_21, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  highp vec4 tmpvar_23;
  tmpvar_23.w = 0.0;
  tmpvar_23.xyz = (vec_17 + vec3(-0.0078125, -0.0078125, 0.0078125));
  highp vec4 tmpvar_24;
  lowp vec4 tmpvar_25;
  tmpvar_25 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_23.xyz, 0.0);
  tmpvar_24 = tmpvar_25;
  shadowVals_18.y = dot (tmpvar_24, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  highp vec4 tmpvar_26;
  tmpvar_26.w = 0.0;
  tmpvar_26.xyz = (vec_17 + vec3(-0.0078125, 0.0078125, -0.0078125));
  highp vec4 tmpvar_27;
  lowp vec4 tmpvar_28;
  tmpvar_28 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_26.xyz, 0.0);
  tmpvar_27 = tmpvar_28;
  shadowVals_18.z = dot (tmpvar_27, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  highp vec4 tmpvar_29;
  tmpvar_29.w = 0.0;
  tmpvar_29.xyz = (vec_17 + vec3(0.0078125, -0.0078125, -0.0078125));
  highp vec4 tmpvar_30;
  lowp vec4 tmpvar_31;
  tmpvar_31 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_29.xyz, 0.0);
  tmpvar_30 = tmpvar_31;
  shadowVals_18.w = dot (tmpvar_30, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  bvec4 tmpvar_32;
  tmpvar_32 = lessThan (shadowVals_18, vec4(mydist_19));
  mediump vec4 tmpvar_33;
  tmpvar_33 = _LightShadowData.xxxx;
  mediump float tmpvar_34;
  if (tmpvar_32.x) {
    tmpvar_34 = tmpvar_33.x;
  } else {
    tmpvar_34 = 1.0;
  };
  mediump float tmpvar_35;
  if (tmpvar_32.y) {
    tmpvar_35 = tmpvar_33.y;
  } else {
    tmpvar_35 = 1.0;
  };
  mediump float tmpvar_36;
  if (tmpvar_32.z) {
    tmpvar_36 = tmpvar_33.z;
  } else {
    tmpvar_36 = 1.0;
  };
  mediump float tmpvar_37;
  if (tmpvar_32.w) {
    tmpvar_37 = tmpvar_33.w;
  } else {
    tmpvar_37 = 1.0;
  };
  mediump vec4 tmpvar_38;
  tmpvar_38.x = tmpvar_34;
  tmpvar_38.y = tmpvar_35;
  tmpvar_38.z = tmpvar_36;
  tmpvar_38.w = tmpvar_37;
  mediump float tmpvar_39;
  tmpvar_39 = mix (dot (tmpvar_38, vec4(0.25, 0.25, 0.25, 0.25)), 1.0, tmpvar_15);
  shadow_5 = tmpvar_39;
  highp float tmpvar_40;
  tmpvar_40 = ((texture2D (_LightTextureB0, vec2(dot (xlv_TEXCOORD3, xlv_TEXCOORD3))).w * textureCube (_LightTexture0, xlv_TEXCOORD3).w) * shadow_5);
  atten_4 = tmpvar_40;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_41;
  mediump vec4 c_42;
  highp vec3 tmpvar_43;
  tmpvar_43 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_44;
  mediump vec3 albedo_45;
  albedo_45 = tmpvar_10;
  tmpvar_44 = (albedo_45 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_41 = tmpvar_44;
  mediump vec3 diffColor_46;
  diffColor_46 = tmpvar_41;
  mediump float alpha_47;
  alpha_47 = tmpvar_11;
  tmpvar_41 = diffColor_46;
  mediump vec3 diffColor_48;
  diffColor_48 = tmpvar_41;
  mediump vec2 rlPow4AndFresnelTerm_49;
  mediump float tmpvar_50;
  highp float tmpvar_51;
  tmpvar_51 = clamp (dot (tmpvar_43, tmpvar_2), 0.0, 1.0);
  tmpvar_50 = tmpvar_51;
  mediump float tmpvar_52;
  highp float tmpvar_53;
  tmpvar_53 = clamp (dot (tmpvar_43, tmpvar_8), 0.0, 1.0);
  tmpvar_52 = tmpvar_53;
  highp vec2 tmpvar_54;
  tmpvar_54.x = dot ((tmpvar_8 - (2.0 * 
    (dot (tmpvar_43, tmpvar_8) * tmpvar_43)
  )), tmpvar_2);
  tmpvar_54.y = (1.0 - tmpvar_52);
  highp vec2 tmpvar_55;
  tmpvar_55 = ((tmpvar_54 * tmpvar_54) * (tmpvar_54 * tmpvar_54));
  rlPow4AndFresnelTerm_49 = tmpvar_55;
  mediump float tmpvar_56;
  tmpvar_56 = rlPow4AndFresnelTerm_49.x;
  mediump float specular_57;
  highp float smoothness_58;
  smoothness_58 = _Glossiness;
  highp vec2 tmpvar_59;
  tmpvar_59.x = tmpvar_56;
  tmpvar_59.y = (1.0 - smoothness_58);
  highp float tmpvar_60;
  tmpvar_60 = (texture2D (unity_NHxRoughness, tmpvar_59).w * 16.0);
  specular_57 = tmpvar_60;
  mediump vec4 tmpvar_61;
  tmpvar_61.w = 1.0;
  tmpvar_61.xyz = ((diffColor_48 + (specular_57 * 
    mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_45, vec3(_Metallic))
  )) * (tmpvar_1 * tmpvar_50));
  c_42.xyz = tmpvar_61.xyz;
  c_42.w = alpha_47;
  c_3.xyz = c_42.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 77 math, 7 textures, 4 branches
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp vec4 _LightPositionRange;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
varying highp vec3 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xyz;
  xlv_TEXCOORD4 = (tmpvar_3.xyz - _LightPositionRange.xyz);
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _LightPositionRange;
uniform highp vec4 _LightProjectionParams;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform lowp samplerCube _ShadowMapTexture;
uniform highp samplerCube _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  highp vec4 v_13;
  v_13.x = unity_MatrixV[0].z;
  v_13.y = unity_MatrixV[1].z;
  v_13.z = unity_MatrixV[2].z;
  v_13.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_14;
  tmpvar_14 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_15;
  highp float tmpvar_16;
  tmpvar_16 = clamp (((
    mix (dot (tmpvar_9, v_13.xyz), sqrt(dot (tmpvar_14, tmpvar_14)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_15 = tmpvar_16;
  highp vec3 vec_17;
  vec_17 = (xlv_TEXCOORD2 - _LightPositionRange.xyz);
  highp vec4 shadowVals_18;
  highp float mydist_19;
  mydist_19 = ((sqrt(
    dot (vec_17, vec_17)
  ) * _LightPositionRange.w) * _LightProjectionParams.w);
  highp vec4 tmpvar_20;
  tmpvar_20.w = 0.0;
  tmpvar_20.xyz = (vec_17 + vec3(0.0078125, 0.0078125, 0.0078125));
  highp vec4 tmpvar_21;
  lowp vec4 tmpvar_22;
  tmpvar_22 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_20.xyz, 0.0);
  tmpvar_21 = tmpvar_22;
  shadowVals_18.x = dot (tmpvar_21, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  highp vec4 tmpvar_23;
  tmpvar_23.w = 0.0;
  tmpvar_23.xyz = (vec_17 + vec3(-0.0078125, -0.0078125, 0.0078125));
  highp vec4 tmpvar_24;
  lowp vec4 tmpvar_25;
  tmpvar_25 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_23.xyz, 0.0);
  tmpvar_24 = tmpvar_25;
  shadowVals_18.y = dot (tmpvar_24, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  highp vec4 tmpvar_26;
  tmpvar_26.w = 0.0;
  tmpvar_26.xyz = (vec_17 + vec3(-0.0078125, 0.0078125, -0.0078125));
  highp vec4 tmpvar_27;
  lowp vec4 tmpvar_28;
  tmpvar_28 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_26.xyz, 0.0);
  tmpvar_27 = tmpvar_28;
  shadowVals_18.z = dot (tmpvar_27, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  highp vec4 tmpvar_29;
  tmpvar_29.w = 0.0;
  tmpvar_29.xyz = (vec_17 + vec3(0.0078125, -0.0078125, -0.0078125));
  highp vec4 tmpvar_30;
  lowp vec4 tmpvar_31;
  tmpvar_31 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_29.xyz, 0.0);
  tmpvar_30 = tmpvar_31;
  shadowVals_18.w = dot (tmpvar_30, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  bvec4 tmpvar_32;
  tmpvar_32 = lessThan (shadowVals_18, vec4(mydist_19));
  mediump vec4 tmpvar_33;
  tmpvar_33 = _LightShadowData.xxxx;
  mediump float tmpvar_34;
  if (tmpvar_32.x) {
    tmpvar_34 = tmpvar_33.x;
  } else {
    tmpvar_34 = 1.0;
  };
  mediump float tmpvar_35;
  if (tmpvar_32.y) {
    tmpvar_35 = tmpvar_33.y;
  } else {
    tmpvar_35 = 1.0;
  };
  mediump float tmpvar_36;
  if (tmpvar_32.z) {
    tmpvar_36 = tmpvar_33.z;
  } else {
    tmpvar_36 = 1.0;
  };
  mediump float tmpvar_37;
  if (tmpvar_32.w) {
    tmpvar_37 = tmpvar_33.w;
  } else {
    tmpvar_37 = 1.0;
  };
  mediump vec4 tmpvar_38;
  tmpvar_38.x = tmpvar_34;
  tmpvar_38.y = tmpvar_35;
  tmpvar_38.z = tmpvar_36;
  tmpvar_38.w = tmpvar_37;
  mediump float tmpvar_39;
  tmpvar_39 = mix (dot (tmpvar_38, vec4(0.25, 0.25, 0.25, 0.25)), 1.0, tmpvar_15);
  shadow_5 = tmpvar_39;
  highp float tmpvar_40;
  tmpvar_40 = ((texture2D (_LightTextureB0, vec2(dot (xlv_TEXCOORD3, xlv_TEXCOORD3))).w * textureCube (_LightTexture0, xlv_TEXCOORD3).w) * shadow_5);
  atten_4 = tmpvar_40;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_41;
  mediump vec4 c_42;
  highp vec3 tmpvar_43;
  tmpvar_43 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_44;
  mediump vec3 albedo_45;
  albedo_45 = tmpvar_10;
  mediump vec3 tmpvar_46;
  tmpvar_46 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_45, vec3(_Metallic));
  tmpvar_44 = (albedo_45 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_41 = tmpvar_44;
  mediump vec3 diffColor_47;
  diffColor_47 = tmpvar_41;
  mediump float alpha_48;
  alpha_48 = tmpvar_11;
  tmpvar_41 = diffColor_47;
  mediump vec3 diffColor_49;
  diffColor_49 = tmpvar_41;
  mediump vec3 color_50;
  highp float specularTerm_51;
  highp float a2_52;
  mediump float roughness_53;
  mediump float perceptualRoughness_54;
  highp vec3 tmpvar_55;
  highp vec3 inVec_56;
  inVec_56 = (tmpvar_2 + tmpvar_8);
  tmpvar_55 = (inVec_56 * inversesqrt(max (0.001, 
    dot (inVec_56, inVec_56)
  )));
  mediump float tmpvar_57;
  highp float tmpvar_58;
  tmpvar_58 = clamp (dot (tmpvar_43, tmpvar_2), 0.0, 1.0);
  tmpvar_57 = tmpvar_58;
  highp float tmpvar_59;
  tmpvar_59 = clamp (dot (tmpvar_43, tmpvar_55), 0.0, 1.0);
  highp float tmpvar_60;
  highp float smoothness_61;
  smoothness_61 = _Glossiness;
  tmpvar_60 = (1.0 - smoothness_61);
  perceptualRoughness_54 = tmpvar_60;
  highp float tmpvar_62;
  highp float perceptualRoughness_63;
  perceptualRoughness_63 = perceptualRoughness_54;
  tmpvar_62 = (perceptualRoughness_63 * perceptualRoughness_63);
  roughness_53 = tmpvar_62;
  mediump float tmpvar_64;
  tmpvar_64 = (roughness_53 * roughness_53);
  a2_52 = tmpvar_64;
  specularTerm_51 = ((roughness_53 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_55), 0.0, 1.0)) * (1.5 + roughness_53))
   * 
    (((tmpvar_59 * tmpvar_59) * (a2_52 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_65;
  tmpvar_65 = clamp (specularTerm_51, 0.0, 100.0);
  specularTerm_51 = tmpvar_65;
  highp vec3 tmpvar_66;
  tmpvar_66 = (((diffColor_49 + 
    (tmpvar_65 * tmpvar_46)
  ) * tmpvar_1) * tmpvar_57);
  color_50 = tmpvar_66;
  mediump vec4 tmpvar_67;
  tmpvar_67.w = 1.0;
  tmpvar_67.xyz = color_50;
  c_42.xyz = tmpvar_67.xyz;
  c_42.w = alpha_48;
  c_3.xyz = c_42.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 77 math, 7 textures, 4 branches
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp vec4 _LightPositionRange;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp mat4 unity_WorldToLight;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
varying highp vec3 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  tmpvar_1.w = 1.0;
  tmpvar_1.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_2;
  tmpvar_2[0] = unity_WorldToObject[0].xyz;
  tmpvar_2[1] = unity_WorldToObject[1].xyz;
  tmpvar_2[2] = unity_WorldToObject[2].xyz;
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_1));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_2));
  highp vec4 tmpvar_3;
  tmpvar_3 = (unity_ObjectToWorld * _glesVertex);
  xlv_TEXCOORD2 = tmpvar_3.xyz;
  xlv_TEXCOORD3 = (unity_WorldToLight * tmpvar_3).xyz;
  xlv_TEXCOORD4 = (tmpvar_3.xyz - _LightPositionRange.xyz);
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_shader_texture_lod : enable
lowp vec4 impl_low_textureCubeLodEXT(lowp samplerCube sampler, highp vec3 coord, mediump float lod)
{
#if defined(GL_EXT_shader_texture_lod)
	return textureCubeLodEXT(sampler, coord, lod);
#else
	return textureCube(sampler, coord, lod);
#endif
}

uniform highp vec3 _WorldSpaceCameraPos;
uniform highp vec4 _WorldSpaceLightPos0;
uniform highp vec4 _LightPositionRange;
uniform highp vec4 _LightProjectionParams;
uniform mediump vec4 _LightShadowData;
uniform highp vec4 unity_ShadowFadeCenterAndType;
uniform highp mat4 unity_MatrixV;
uniform lowp vec4 _LightColor0;
uniform lowp samplerCube _ShadowMapTexture;
uniform highp samplerCube _LightTexture0;
uniform highp sampler2D _LightTextureB0;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec3 xlv_TEXCOORD3;
void main ()
{
  mediump vec3 tmpvar_1;
  mediump vec3 tmpvar_2;
  lowp vec4 c_3;
  lowp float atten_4;
  lowp float shadow_5;
  lowp vec3 lightDir_6;
  highp vec3 tmpvar_7;
  tmpvar_7 = normalize((_WorldSpaceLightPos0.xyz - xlv_TEXCOORD2));
  lightDir_6 = tmpvar_7;
  highp vec3 tmpvar_8;
  highp vec3 tmpvar_9;
  tmpvar_9 = (_WorldSpaceCameraPos - xlv_TEXCOORD2);
  tmpvar_8 = normalize(tmpvar_9);
  lowp vec3 tmpvar_10;
  lowp float tmpvar_11;
  lowp vec4 tmpvar_12;
  tmpvar_12 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color);
  tmpvar_10 = tmpvar_12.xyz;
  tmpvar_11 = tmpvar_12.w;
  highp vec4 v_13;
  v_13.x = unity_MatrixV[0].z;
  v_13.y = unity_MatrixV[1].z;
  v_13.z = unity_MatrixV[2].z;
  v_13.w = unity_MatrixV[3].z;
  highp vec3 tmpvar_14;
  tmpvar_14 = (xlv_TEXCOORD2 - unity_ShadowFadeCenterAndType.xyz);
  mediump float tmpvar_15;
  highp float tmpvar_16;
  tmpvar_16 = clamp (((
    mix (dot (tmpvar_9, v_13.xyz), sqrt(dot (tmpvar_14, tmpvar_14)), unity_ShadowFadeCenterAndType.w)
   * _LightShadowData.z) + _LightShadowData.w), 0.0, 1.0);
  tmpvar_15 = tmpvar_16;
  highp vec3 vec_17;
  vec_17 = (xlv_TEXCOORD2 - _LightPositionRange.xyz);
  highp vec4 shadowVals_18;
  highp float mydist_19;
  mydist_19 = ((sqrt(
    dot (vec_17, vec_17)
  ) * _LightPositionRange.w) * _LightProjectionParams.w);
  highp vec4 tmpvar_20;
  tmpvar_20.w = 0.0;
  tmpvar_20.xyz = (vec_17 + vec3(0.0078125, 0.0078125, 0.0078125));
  highp vec4 tmpvar_21;
  lowp vec4 tmpvar_22;
  tmpvar_22 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_20.xyz, 0.0);
  tmpvar_21 = tmpvar_22;
  shadowVals_18.x = dot (tmpvar_21, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  highp vec4 tmpvar_23;
  tmpvar_23.w = 0.0;
  tmpvar_23.xyz = (vec_17 + vec3(-0.0078125, -0.0078125, 0.0078125));
  highp vec4 tmpvar_24;
  lowp vec4 tmpvar_25;
  tmpvar_25 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_23.xyz, 0.0);
  tmpvar_24 = tmpvar_25;
  shadowVals_18.y = dot (tmpvar_24, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  highp vec4 tmpvar_26;
  tmpvar_26.w = 0.0;
  tmpvar_26.xyz = (vec_17 + vec3(-0.0078125, 0.0078125, -0.0078125));
  highp vec4 tmpvar_27;
  lowp vec4 tmpvar_28;
  tmpvar_28 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_26.xyz, 0.0);
  tmpvar_27 = tmpvar_28;
  shadowVals_18.z = dot (tmpvar_27, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  highp vec4 tmpvar_29;
  tmpvar_29.w = 0.0;
  tmpvar_29.xyz = (vec_17 + vec3(0.0078125, -0.0078125, -0.0078125));
  highp vec4 tmpvar_30;
  lowp vec4 tmpvar_31;
  tmpvar_31 = impl_low_textureCubeLodEXT (_ShadowMapTexture, tmpvar_29.xyz, 0.0);
  tmpvar_30 = tmpvar_31;
  shadowVals_18.w = dot (tmpvar_30, vec4(1.0, 0.003921569, 1.53787e-05, 6.030863e-08));
  bvec4 tmpvar_32;
  tmpvar_32 = lessThan (shadowVals_18, vec4(mydist_19));
  mediump vec4 tmpvar_33;
  tmpvar_33 = _LightShadowData.xxxx;
  mediump float tmpvar_34;
  if (tmpvar_32.x) {
    tmpvar_34 = tmpvar_33.x;
  } else {
    tmpvar_34 = 1.0;
  };
  mediump float tmpvar_35;
  if (tmpvar_32.y) {
    tmpvar_35 = tmpvar_33.y;
  } else {
    tmpvar_35 = 1.0;
  };
  mediump float tmpvar_36;
  if (tmpvar_32.z) {
    tmpvar_36 = tmpvar_33.z;
  } else {
    tmpvar_36 = 1.0;
  };
  mediump float tmpvar_37;
  if (tmpvar_32.w) {
    tmpvar_37 = tmpvar_33.w;
  } else {
    tmpvar_37 = 1.0;
  };
  mediump vec4 tmpvar_38;
  tmpvar_38.x = tmpvar_34;
  tmpvar_38.y = tmpvar_35;
  tmpvar_38.z = tmpvar_36;
  tmpvar_38.w = tmpvar_37;
  mediump float tmpvar_39;
  tmpvar_39 = mix (dot (tmpvar_38, vec4(0.25, 0.25, 0.25, 0.25)), 1.0, tmpvar_15);
  shadow_5 = tmpvar_39;
  highp float tmpvar_40;
  tmpvar_40 = ((texture2D (_LightTextureB0, vec2(dot (xlv_TEXCOORD3, xlv_TEXCOORD3))).w * textureCube (_LightTexture0, xlv_TEXCOORD3).w) * shadow_5);
  atten_4 = tmpvar_40;
  tmpvar_1 = _LightColor0.xyz;
  tmpvar_2 = lightDir_6;
  tmpvar_1 = (tmpvar_1 * atten_4);
  lowp vec3 tmpvar_41;
  mediump vec4 c_42;
  highp vec3 tmpvar_43;
  tmpvar_43 = normalize(xlv_TEXCOORD1);
  mediump vec3 tmpvar_44;
  mediump vec3 albedo_45;
  albedo_45 = tmpvar_10;
  mediump vec3 tmpvar_46;
  tmpvar_46 = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_45, vec3(_Metallic));
  tmpvar_44 = (albedo_45 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_41 = tmpvar_44;
  mediump vec3 diffColor_47;
  diffColor_47 = tmpvar_41;
  mediump float alpha_48;
  alpha_48 = tmpvar_11;
  tmpvar_41 = diffColor_47;
  mediump vec3 diffColor_49;
  diffColor_49 = tmpvar_41;
  mediump vec3 color_50;
  highp float specularTerm_51;
  highp float a2_52;
  mediump float roughness_53;
  mediump float perceptualRoughness_54;
  highp vec3 tmpvar_55;
  highp vec3 inVec_56;
  inVec_56 = (tmpvar_2 + tmpvar_8);
  tmpvar_55 = (inVec_56 * inversesqrt(max (0.001, 
    dot (inVec_56, inVec_56)
  )));
  mediump float tmpvar_57;
  highp float tmpvar_58;
  tmpvar_58 = clamp (dot (tmpvar_43, tmpvar_2), 0.0, 1.0);
  tmpvar_57 = tmpvar_58;
  highp float tmpvar_59;
  tmpvar_59 = clamp (dot (tmpvar_43, tmpvar_55), 0.0, 1.0);
  highp float tmpvar_60;
  highp float smoothness_61;
  smoothness_61 = _Glossiness;
  tmpvar_60 = (1.0 - smoothness_61);
  perceptualRoughness_54 = tmpvar_60;
  highp float tmpvar_62;
  highp float perceptualRoughness_63;
  perceptualRoughness_63 = perceptualRoughness_54;
  tmpvar_62 = (perceptualRoughness_63 * perceptualRoughness_63);
  roughness_53 = tmpvar_62;
  mediump float tmpvar_64;
  tmpvar_64 = (roughness_53 * roughness_53);
  a2_52 = tmpvar_64;
  specularTerm_51 = ((roughness_53 / (
    (max (0.32, clamp (dot (tmpvar_2, tmpvar_55), 0.0, 1.0)) * (1.5 + roughness_53))
   * 
    (((tmpvar_59 * tmpvar_59) * (a2_52 - 1.0)) + 1.00001)
  )) - 0.0001);
  highp float tmpvar_65;
  tmpvar_65 = clamp (specularTerm_51, 0.0, 100.0);
  specularTerm_51 = tmpvar_65;
  highp vec3 tmpvar_66;
  tmpvar_66 = (((diffColor_49 + 
    (tmpvar_65 * tmpvar_46)
  ) * tmpvar_1) * tmpvar_57);
  color_50 = tmpvar_66;
  mediump vec4 tmpvar_67;
  tmpvar_67.w = 1.0;
  tmpvar_67.xyz = color_50;
  c_42.xyz = tmpvar_67.xyz;
  c_42.w = alpha_48;
  c_3.xyz = c_42.xyz;
  c_3.w = 1.0;
  gl_FragData[0] = c_3;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4.xyz = vec3(0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 _LightPositionRange;
uniform 	vec4 _LightProjectionParams;
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTextureB0;
uniform highp samplerCube _LightTexture0;
uniform highp sampler2D unity_NHxRoughness;
uniform lowp samplerCubeShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp samplerCube _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
vec4 u_xlat1;
vec3 u_xlat2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump vec3 u_xlat16_5;
lowp vec3 u_xlat10_5;
mediump float u_xlat16_8;
float u_xlat15;
mediump float u_xlat16_18;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-_LightPositionRange.xyz);
    u_xlat15 = max(abs(u_xlat0.y), abs(u_xlat0.x));
    u_xlat15 = max(abs(u_xlat0.z), u_xlat15);
    u_xlat15 = u_xlat15 + (-_LightProjectionParams.z);
    u_xlat15 = max(u_xlat15, 9.99999975e-06);
    u_xlat15 = u_xlat15 * _LightProjectionParams.w;
    u_xlat15 = _LightProjectionParams.y / u_xlat15;
    u_xlat15 = u_xlat15 + (-_LightProjectionParams.x);
    u_xlat1.xyz = u_xlat0.xyz + vec3(0.0078125, 0.0078125, 0.0078125);
    vec4 txVec0 = vec4(u_xlat1.xyz,u_xlat15);
    u_xlat1.x = texture(hlslcc_zcmp_ShadowMapTexture, txVec0);
    u_xlat2.xyz = u_xlat0.xyz + vec3(-0.0078125, -0.0078125, 0.0078125);
    vec4 txVec1 = vec4(u_xlat2.xyz,u_xlat15);
    u_xlat1.y = texture(hlslcc_zcmp_ShadowMapTexture, txVec1);
    u_xlat2.xyz = u_xlat0.xyz + vec3(-0.0078125, 0.0078125, -0.0078125);
    u_xlat0.xyz = u_xlat0.xyz + vec3(0.0078125, -0.0078125, -0.0078125);
    vec4 txVec2 = vec4(u_xlat0.xyz,u_xlat15);
    u_xlat1.w = texture(hlslcc_zcmp_ShadowMapTexture, txVec2);
    vec4 txVec3 = vec4(u_xlat2.xyz,u_xlat15);
    u_xlat1.z = texture(hlslcc_zcmp_ShadowMapTexture, txVec3);
    u_xlat0.x = dot(u_xlat1, vec4(0.25, 0.25, 0.25, 0.25));
    u_xlat16_3.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_3.x = u_xlat0.x * u_xlat16_3.x + _LightShadowData.x;
    u_xlat16_8 = (-u_xlat16_3.x) + 1.0;
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat1.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat5.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat1.x = dot(u_xlat5.xyz, u_xlat1.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat1.x);
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat0.x + u_xlat1.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_3.x = u_xlat0.x * u_xlat16_8 + u_xlat16_3.x;
    u_xlat1.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD2.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD2.zzz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat1.x = texture(_LightTexture0, u_xlat1.xyz).w;
    u_xlat0.x = texture(_LightTextureB0, u_xlat0.xx).w;
    u_xlat0.x = u_xlat1.x * u_xlat0.x;
    u_xlat0.x = u_xlat16_3.x * u_xlat0.x;
    u_xlat16_3.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat1.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat1.xyz = u_xlat0.xxx * u_xlat1.xyz;
    u_xlat0.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat2.xyz = u_xlat0.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat2.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_3.xyz = u_xlat0.xxx * u_xlat16_3.xyz;
    u_xlat0.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx * u_xlat5.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat2.xyz);
    u_xlat15 = u_xlat15 + u_xlat15;
    u_xlat0.xyz = u_xlat2.xyz * (-vec3(u_xlat15)) + u_xlat0.xyz;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat1.xyz);
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.y = (-_Glossiness) + 1.0;
    u_xlat0.x = texture(unity_NHxRoughness, u_xlat0.xy).w;
    u_xlat0.x = u_xlat0.x * 16.0;
    u_xlat10_5.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_5.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_5.xyz = u_xlat10_5.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_4.xyz = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_18 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat16_4.xyz = u_xlat16_5.xyz * vec3(u_xlat16_18) + u_xlat16_4.xyz;
    SV_Target0.xyz = u_xlat16_3.xyz * u_xlat16_4.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4.xyz = vec3(0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 _LightPositionRange;
uniform 	vec4 _LightProjectionParams;
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTextureB0;
uniform highp samplerCube _LightTexture0;
uniform lowp samplerCubeShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp samplerCube _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec4 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
vec3 u_xlat2;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump float u_xlat16_8;
mediump float u_xlat16_10;
float u_xlat15;
float u_xlat16;
mediump float u_xlat16_16;
mediump float u_xlat16_18;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-_LightPositionRange.xyz);
    u_xlat15 = max(abs(u_xlat0.y), abs(u_xlat0.x));
    u_xlat15 = max(abs(u_xlat0.z), u_xlat15);
    u_xlat15 = u_xlat15 + (-_LightProjectionParams.z);
    u_xlat15 = max(u_xlat15, 9.99999975e-06);
    u_xlat15 = u_xlat15 * _LightProjectionParams.w;
    u_xlat15 = _LightProjectionParams.y / u_xlat15;
    u_xlat15 = u_xlat15 + (-_LightProjectionParams.x);
    u_xlat1.xyz = u_xlat0.xyz + vec3(0.0078125, 0.0078125, 0.0078125);
    vec4 txVec0 = vec4(u_xlat1.xyz,u_xlat15);
    u_xlat1.x = texture(hlslcc_zcmp_ShadowMapTexture, txVec0);
    u_xlat2.xyz = u_xlat0.xyz + vec3(-0.0078125, -0.0078125, 0.0078125);
    vec4 txVec1 = vec4(u_xlat2.xyz,u_xlat15);
    u_xlat1.y = texture(hlslcc_zcmp_ShadowMapTexture, txVec1);
    u_xlat2.xyz = u_xlat0.xyz + vec3(-0.0078125, 0.0078125, -0.0078125);
    u_xlat0.xyz = u_xlat0.xyz + vec3(0.0078125, -0.0078125, -0.0078125);
    vec4 txVec2 = vec4(u_xlat0.xyz,u_xlat15);
    u_xlat1.w = texture(hlslcc_zcmp_ShadowMapTexture, txVec2);
    vec4 txVec3 = vec4(u_xlat2.xyz,u_xlat15);
    u_xlat1.z = texture(hlslcc_zcmp_ShadowMapTexture, txVec3);
    u_xlat0.x = dot(u_xlat1, vec4(0.25, 0.25, 0.25, 0.25));
    u_xlat16_3.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_3.x = u_xlat0.x * u_xlat16_3.x + _LightShadowData.x;
    u_xlat16_8 = (-u_xlat16_3.x) + 1.0;
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat1.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat5.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat1.x = dot(u_xlat5.xyz, u_xlat1.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat1.x);
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat0.x + u_xlat1.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_3.x = u_xlat0.x * u_xlat16_8 + u_xlat16_3.x;
    u_xlat1.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD2.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD2.zzz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat1.x = texture(_LightTexture0, u_xlat1.xyz).w;
    u_xlat0.x = texture(_LightTextureB0, u_xlat0.xx).w;
    u_xlat0.x = u_xlat1.x * u_xlat0.x;
    u_xlat0.x = u_xlat16_3.x * u_xlat0.x;
    u_xlat16_3.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat1.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat16 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat16 = inversesqrt(u_xlat16);
    u_xlat1.xyz = vec3(u_xlat16) * u_xlat1.xyz;
    u_xlat0.xyz = u_xlat5.xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = max(u_xlat15, 0.00100000005);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat15 = dot(u_xlat1.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat15 = min(max(u_xlat15, 0.0), 1.0);
#else
    u_xlat15 = clamp(u_xlat15, 0.0, 1.0);
#endif
    u_xlat15 = max(u_xlat15, 0.319999993);
    u_xlat16_16 = (-_Glossiness) + 1.0;
    u_xlat16_2 = u_xlat16_16 * u_xlat16_16 + 1.5;
    u_xlat16_16 = u_xlat16_16 * u_xlat16_16;
    u_xlat15 = u_xlat15 * u_xlat16_2;
    u_xlat2.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat2.x = inversesqrt(u_xlat2.x);
    u_xlat2.xyz = u_xlat2.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat2.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat5.x = dot(u_xlat2.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat16_10 = u_xlat16_16 * u_xlat16_16 + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_10 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat15;
    u_xlat0.x = u_xlat16_16 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_18 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * vec3(u_xlat16_18) + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat16_3.xyz * u_xlat0.xzw;
    u_xlat0.xyz = u_xlat5.xxx * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4.xyz = vec3(0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 _LightPositionRange;
uniform 	vec4 _LightProjectionParams;
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
uniform highp sampler2D _LightTextureB0;
uniform highp samplerCube _LightTexture0;
uniform lowp samplerCubeShadow hlslcc_zcmp_ShadowMapTexture;
uniform lowp samplerCube _ShadowMapTexture;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
vec4 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
vec3 u_xlat2;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
mediump float u_xlat16_8;
mediump float u_xlat16_10;
float u_xlat15;
float u_xlat16;
mediump float u_xlat16_16;
mediump float u_xlat16_18;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-_LightPositionRange.xyz);
    u_xlat15 = max(abs(u_xlat0.y), abs(u_xlat0.x));
    u_xlat15 = max(abs(u_xlat0.z), u_xlat15);
    u_xlat15 = u_xlat15 + (-_LightProjectionParams.z);
    u_xlat15 = max(u_xlat15, 9.99999975e-06);
    u_xlat15 = u_xlat15 * _LightProjectionParams.w;
    u_xlat15 = _LightProjectionParams.y / u_xlat15;
    u_xlat15 = u_xlat15 + (-_LightProjectionParams.x);
    u_xlat1.xyz = u_xlat0.xyz + vec3(0.0078125, 0.0078125, 0.0078125);
    vec4 txVec0 = vec4(u_xlat1.xyz,u_xlat15);
    u_xlat1.x = texture(hlslcc_zcmp_ShadowMapTexture, txVec0);
    u_xlat2.xyz = u_xlat0.xyz + vec3(-0.0078125, -0.0078125, 0.0078125);
    vec4 txVec1 = vec4(u_xlat2.xyz,u_xlat15);
    u_xlat1.y = texture(hlslcc_zcmp_ShadowMapTexture, txVec1);
    u_xlat2.xyz = u_xlat0.xyz + vec3(-0.0078125, 0.0078125, -0.0078125);
    u_xlat0.xyz = u_xlat0.xyz + vec3(0.0078125, -0.0078125, -0.0078125);
    vec4 txVec2 = vec4(u_xlat0.xyz,u_xlat15);
    u_xlat1.w = texture(hlslcc_zcmp_ShadowMapTexture, txVec2);
    vec4 txVec3 = vec4(u_xlat2.xyz,u_xlat15);
    u_xlat1.z = texture(hlslcc_zcmp_ShadowMapTexture, txVec3);
    u_xlat0.x = dot(u_xlat1, vec4(0.25, 0.25, 0.25, 0.25));
    u_xlat16_3.x = (-_LightShadowData.x) + 1.0;
    u_xlat16_3.x = u_xlat0.x * u_xlat16_3.x + _LightShadowData.x;
    u_xlat16_8 = (-u_xlat16_3.x) + 1.0;
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat1.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat1.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat1.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat5.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat1.x = dot(u_xlat5.xyz, u_xlat1.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat1.x);
    u_xlat0.x = unity_ShadowFadeCenterAndType.w * u_xlat0.x + u_xlat1.x;
    u_xlat0.x = u_xlat0.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat16_3.x = u_xlat0.x * u_xlat16_8 + u_xlat16_3.x;
    u_xlat1.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD2.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD2.zzz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat1.x = texture(_LightTexture0, u_xlat1.xyz).w;
    u_xlat0.x = texture(_LightTextureB0, u_xlat0.xx).w;
    u_xlat0.x = u_xlat1.x * u_xlat0.x;
    u_xlat0.x = u_xlat16_3.x * u_xlat0.x;
    u_xlat16_3.xyz = u_xlat0.xxx * _LightColor0.xyz;
    u_xlat0.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat1.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat16 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat16 = inversesqrt(u_xlat16);
    u_xlat1.xyz = vec3(u_xlat16) * u_xlat1.xyz;
    u_xlat0.xyz = u_xlat5.xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = max(u_xlat15, 0.00100000005);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat15 = dot(u_xlat1.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat15 = min(max(u_xlat15, 0.0), 1.0);
#else
    u_xlat15 = clamp(u_xlat15, 0.0, 1.0);
#endif
    u_xlat15 = max(u_xlat15, 0.319999993);
    u_xlat16_16 = (-_Glossiness) + 1.0;
    u_xlat16_2 = u_xlat16_16 * u_xlat16_16 + 1.5;
    u_xlat16_16 = u_xlat16_16 * u_xlat16_16;
    u_xlat15 = u_xlat15 * u_xlat16_2;
    u_xlat2.x = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat2.x = inversesqrt(u_xlat2.x);
    u_xlat2.xyz = u_xlat2.xxx * vs_TEXCOORD1.xyz;
    u_xlat0.x = dot(u_xlat2.xyz, u_xlat0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat5.x = dot(u_xlat2.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat16_10 = u_xlat16_16 * u_xlat16_16 + -1.0;
    u_xlat0.x = u_xlat0.x * u_xlat16_10 + 1.00001001;
    u_xlat0.x = u_xlat0.x * u_xlat15;
    u_xlat0.x = u_xlat16_16 / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -9.99999975e-05;
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = min(u_xlat0.x, 100.0);
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_4.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_1.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_4.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat0.xzw = u_xlat0.xxx * u_xlat16_4.xyz;
    u_xlat16_18 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat0.xzw = u_xlat16_1.xyz * vec3(u_xlat16_18) + u_xlat0.xzw;
    u_xlat0.xzw = u_xlat16_3.xyz * u_xlat0.xzw;
    u_xlat0.xyz = u_xlat5.xxx * u_xlat0.xzw;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

 }


 // Stats for Vertex shader:
 //         gles: 18 avg math (11..23), 1 texture
 Pass {
  Name "DEFERRED"
  Tags { "LIGHTMODE"="DEFERRED" "RenderType"="Opaque" }
  //////////////////////////////////
  //                              //
  //      Compiled programs       //
  //                              //
  //////////////////////////////////
//////////////////////////////////////////////////////
No keywords set in this variant.
-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 11 math, 1 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  tmpvar_2.w = 1.0;
  tmpvar_2.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_3;
  tmpvar_3[0] = unity_WorldToObject[0].xyz;
  tmpvar_3[1] = unity_WorldToObject[1].xyz;
  tmpvar_3[2] = unity_WorldToObject[2].xyz;
  tmpvar_1.zw = vec2(0.0, 0.0);
  tmpvar_1.xy = vec2(0.0, 0.0);
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_2));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_3));
  xlv_TEXCOORD2 = (unity_ObjectToWorld * _glesVertex).xyz;
  xlv_TEXCOORD4 = tmpvar_1;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_draw_buffers : enable
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
void main ()
{
  mediump vec4 outEmission_1;
  lowp vec3 tmpvar_2;
  tmpvar_2 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color).xyz;
  lowp vec3 tmpvar_3;
  mediump vec3 tmpvar_4;
  mediump vec3 tmpvar_5;
  mediump vec3 albedo_6;
  albedo_6 = tmpvar_2;
  tmpvar_5 = (albedo_6 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_3 = tmpvar_5;
  tmpvar_4 = tmpvar_3;
  mediump vec4 outGBuffer2_7;
  mediump vec4 tmpvar_8;
  tmpvar_8.xyz = tmpvar_4;
  tmpvar_8.w = 1.0;
  mediump vec4 tmpvar_9;
  tmpvar_9.xyz = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_6, vec3(_Metallic));
  tmpvar_9.w = _Glossiness;
  highp vec4 tmpvar_10;
  tmpvar_10.w = 1.0;
  tmpvar_10.xyz = ((xlv_TEXCOORD1 * 0.5) + 0.5);
  outGBuffer2_7 = tmpvar_10;
  outEmission_1.w = 1.0;
  outEmission_1.xyz = vec3(1.0, 1.0, 1.0);
  gl_FragData[0] = tmpvar_8;
  gl_FragData[1] = tmpvar_9;
  gl_FragData[2] = outGBuffer2_7;
  gl_FragData[3] = outEmission_1;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 11 math, 1 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  tmpvar_2.w = 1.0;
  tmpvar_2.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_3;
  tmpvar_3[0] = unity_WorldToObject[0].xyz;
  tmpvar_3[1] = unity_WorldToObject[1].xyz;
  tmpvar_3[2] = unity_WorldToObject[2].xyz;
  tmpvar_1.zw = vec2(0.0, 0.0);
  tmpvar_1.xy = vec2(0.0, 0.0);
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_2));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_3));
  xlv_TEXCOORD2 = (unity_ObjectToWorld * _glesVertex).xyz;
  xlv_TEXCOORD4 = tmpvar_1;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_draw_buffers : enable
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
void main ()
{
  mediump vec4 outEmission_1;
  lowp vec3 tmpvar_2;
  tmpvar_2 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color).xyz;
  lowp vec3 tmpvar_3;
  mediump vec3 tmpvar_4;
  mediump vec3 tmpvar_5;
  mediump vec3 albedo_6;
  albedo_6 = tmpvar_2;
  tmpvar_5 = (albedo_6 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_3 = tmpvar_5;
  tmpvar_4 = tmpvar_3;
  mediump vec4 outGBuffer2_7;
  mediump vec4 tmpvar_8;
  tmpvar_8.xyz = tmpvar_4;
  tmpvar_8.w = 1.0;
  mediump vec4 tmpvar_9;
  tmpvar_9.xyz = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_6, vec3(_Metallic));
  tmpvar_9.w = _Glossiness;
  highp vec4 tmpvar_10;
  tmpvar_10.w = 1.0;
  tmpvar_10.xyz = ((xlv_TEXCOORD1 * 0.5) + 0.5);
  outGBuffer2_7 = tmpvar_10;
  outEmission_1.w = 1.0;
  outEmission_1.xyz = vec3(1.0, 1.0, 1.0);
  gl_FragData[0] = tmpvar_8;
  gl_FragData[1] = tmpvar_9;
  gl_FragData[2] = outGBuffer2_7;
  gl_FragData[3] = outEmission_1;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 11 math, 1 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD4;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  tmpvar_2.w = 1.0;
  tmpvar_2.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_3;
  tmpvar_3[0] = unity_WorldToObject[0].xyz;
  tmpvar_3[1] = unity_WorldToObject[1].xyz;
  tmpvar_3[2] = unity_WorldToObject[2].xyz;
  tmpvar_1.zw = vec2(0.0, 0.0);
  tmpvar_1.xy = vec2(0.0, 0.0);
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_2));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = normalize((_glesNormal * tmpvar_3));
  xlv_TEXCOORD2 = (unity_ObjectToWorld * _glesVertex).xyz;
  xlv_TEXCOORD4 = tmpvar_1;
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_draw_buffers : enable
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
void main ()
{
  mediump vec4 outEmission_1;
  lowp vec3 tmpvar_2;
  tmpvar_2 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color).xyz;
  lowp vec3 tmpvar_3;
  mediump vec3 tmpvar_4;
  mediump vec3 tmpvar_5;
  mediump vec3 albedo_6;
  albedo_6 = tmpvar_2;
  tmpvar_5 = (albedo_6 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_3 = tmpvar_5;
  tmpvar_4 = tmpvar_3;
  mediump vec4 outGBuffer2_7;
  mediump vec4 tmpvar_8;
  tmpvar_8.xyz = tmpvar_4;
  tmpvar_8.w = 1.0;
  mediump vec4 tmpvar_9;
  tmpvar_9.xyz = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_6, vec3(_Metallic));
  tmpvar_9.w = _Glossiness;
  highp vec4 tmpvar_10;
  tmpvar_10.w = 1.0;
  tmpvar_10.xyz = ((xlv_TEXCOORD1 * 0.5) + 0.5);
  outGBuffer2_7 = tmpvar_10;
  outEmission_1.w = 1.0;
  outEmission_1.xyz = vec3(1.0, 1.0, 1.0);
  gl_FragData[0] = tmpvar_8;
  gl_FragData[1] = tmpvar_9;
  gl_FragData[2] = outGBuffer2_7;
  gl_FragData[3] = outEmission_1;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
layout(location = 1) out mediump vec4 SV_Target1;
layout(location = 2) out mediump vec4 SV_Target2;
layout(location = 3) out mediump vec4 SV_Target3;
vec4 u_xlat0;
mediump float u_xlat16_0;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
mediump vec3 u_xlat16_3;
void main()
{
    u_xlat16_0 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_3.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    SV_Target1.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_3.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    SV_Target0.xyz = vec3(u_xlat16_0) * u_xlat16_2.xyz;
    SV_Target0.w = 1.0;
    SV_Target1.w = _Glossiness;
    u_xlat0.xyz = vs_TEXCOORD1.xyz * vec3(0.5, 0.5, 0.5) + vec3(0.5, 0.5, 0.5);
    u_xlat0.w = 1.0;
    SV_Target2 = u_xlat0;
    SV_Target3 = vec4(1.0, 1.0, 1.0, 1.0);
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
layout(location = 1) out mediump vec4 SV_Target1;
layout(location = 2) out mediump vec4 SV_Target2;
layout(location = 3) out mediump vec4 SV_Target3;
vec4 u_xlat0;
mediump float u_xlat16_0;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
mediump vec3 u_xlat16_3;
void main()
{
    u_xlat16_0 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_3.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    SV_Target1.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_3.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    SV_Target0.xyz = vec3(u_xlat16_0) * u_xlat16_2.xyz;
    SV_Target0.w = 1.0;
    SV_Target1.w = _Glossiness;
    u_xlat0.xyz = vs_TEXCOORD1.xyz * vec3(0.5, 0.5, 0.5) + vec3(0.5, 0.5, 0.5);
    u_xlat0.w = 1.0;
    SV_Target2 = u_xlat0;
    SV_Target3 = vec4(1.0, 1.0, 1.0, 1.0);
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    vs_TEXCOORD1.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
layout(location = 1) out mediump vec4 SV_Target1;
layout(location = 2) out mediump vec4 SV_Target2;
layout(location = 3) out mediump vec4 SV_Target3;
vec4 u_xlat0;
mediump float u_xlat16_0;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
mediump vec3 u_xlat16_3;
void main()
{
    u_xlat16_0 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_3.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    SV_Target1.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_3.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    SV_Target0.xyz = vec3(u_xlat16_0) * u_xlat16_2.xyz;
    SV_Target0.w = 1.0;
    SV_Target1.w = _Glossiness;
    u_xlat0.xyz = vs_TEXCOORD1.xyz * vec3(0.5, 0.5, 0.5) + vec3(0.5, 0.5, 0.5);
    u_xlat0.w = 1.0;
    SV_Target2 = u_xlat0;
    SV_Target3 = vec4(1.0, 1.0, 1.0, 1.0);
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

//////////////////////////////////////////////////////
Keywords set in this variant: LIGHTPROBE_SH 
-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 23 math, 1 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform mediump vec4 unity_SHBr;
uniform mediump vec4 unity_SHBg;
uniform mediump vec4 unity_SHBb;
uniform mediump vec4 unity_SHC;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD4;
varying mediump vec3 xlv_TEXCOORD5;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  tmpvar_2.w = 1.0;
  tmpvar_2.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_3;
  tmpvar_3[0] = unity_WorldToObject[0].xyz;
  tmpvar_3[1] = unity_WorldToObject[1].xyz;
  tmpvar_3[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_4;
  tmpvar_4 = normalize((_glesNormal * tmpvar_3));
  tmpvar_1.zw = vec2(0.0, 0.0);
  tmpvar_1.xy = vec2(0.0, 0.0);
  mediump vec3 normal_5;
  normal_5 = tmpvar_4;
  mediump vec3 x1_6;
  mediump vec4 tmpvar_7;
  tmpvar_7 = (normal_5.xyzz * normal_5.yzzx);
  x1_6.x = dot (unity_SHBr, tmpvar_7);
  x1_6.y = dot (unity_SHBg, tmpvar_7);
  x1_6.z = dot (unity_SHBb, tmpvar_7);
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_2));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = tmpvar_4;
  xlv_TEXCOORD2 = (unity_ObjectToWorld * _glesVertex).xyz;
  xlv_TEXCOORD4 = tmpvar_1;
  xlv_TEXCOORD5 = (x1_6 + (unity_SHC.xyz * (
    (normal_5.x * normal_5.x)
   - 
    (normal_5.y * normal_5.y)
  )));
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_draw_buffers : enable
uniform mediump vec4 unity_SHAr;
uniform mediump vec4 unity_SHAg;
uniform mediump vec4 unity_SHAb;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying mediump vec3 xlv_TEXCOORD5;
void main ()
{
  mediump vec4 outEmission_1;
  lowp vec3 tmpvar_2;
  tmpvar_2 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color).xyz;
  mediump vec3 normalWorld_3;
  normalWorld_3 = xlv_TEXCOORD1;
  mediump vec4 tmpvar_4;
  tmpvar_4.w = 1.0;
  tmpvar_4.xyz = normalWorld_3;
  mediump vec3 x_5;
  x_5.x = dot (unity_SHAr, tmpvar_4);
  x_5.y = dot (unity_SHAg, tmpvar_4);
  x_5.z = dot (unity_SHAb, tmpvar_4);
  lowp vec3 tmpvar_6;
  mediump vec3 tmpvar_7;
  mediump vec3 tmpvar_8;
  mediump vec3 albedo_9;
  albedo_9 = tmpvar_2;
  tmpvar_8 = (albedo_9 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_6 = tmpvar_8;
  mediump vec3 diffColor_10;
  diffColor_10 = tmpvar_6;
  mediump vec3 color_11;
  color_11 = (max ((
    (1.055 * pow (max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD5 + x_5)), vec3(0.4166667, 0.4166667, 0.4166667)))
   - 0.055), vec3(0.0, 0.0, 0.0)) * diffColor_10);
  tmpvar_7 = tmpvar_6;
  mediump vec4 outGBuffer2_12;
  mediump vec4 tmpvar_13;
  tmpvar_13.xyz = tmpvar_7;
  tmpvar_13.w = 1.0;
  mediump vec4 tmpvar_14;
  tmpvar_14.xyz = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_9, vec3(_Metallic));
  tmpvar_14.w = _Glossiness;
  highp vec4 tmpvar_15;
  tmpvar_15.w = 1.0;
  tmpvar_15.xyz = ((xlv_TEXCOORD1 * 0.5) + 0.5);
  outGBuffer2_12 = tmpvar_15;
  mediump vec4 tmpvar_16;
  tmpvar_16.w = 1.0;
  tmpvar_16.xyz = color_11;
  outEmission_1.w = tmpvar_16.w;
  outEmission_1.xyz = exp2(-(color_11));
  gl_FragData[0] = tmpvar_13;
  gl_FragData[1] = tmpvar_14;
  gl_FragData[2] = outGBuffer2_12;
  gl_FragData[3] = outEmission_1;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 23 math, 1 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform mediump vec4 unity_SHBr;
uniform mediump vec4 unity_SHBg;
uniform mediump vec4 unity_SHBb;
uniform mediump vec4 unity_SHC;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD4;
varying mediump vec3 xlv_TEXCOORD5;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  tmpvar_2.w = 1.0;
  tmpvar_2.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_3;
  tmpvar_3[0] = unity_WorldToObject[0].xyz;
  tmpvar_3[1] = unity_WorldToObject[1].xyz;
  tmpvar_3[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_4;
  tmpvar_4 = normalize((_glesNormal * tmpvar_3));
  tmpvar_1.zw = vec2(0.0, 0.0);
  tmpvar_1.xy = vec2(0.0, 0.0);
  mediump vec3 normal_5;
  normal_5 = tmpvar_4;
  mediump vec3 x1_6;
  mediump vec4 tmpvar_7;
  tmpvar_7 = (normal_5.xyzz * normal_5.yzzx);
  x1_6.x = dot (unity_SHBr, tmpvar_7);
  x1_6.y = dot (unity_SHBg, tmpvar_7);
  x1_6.z = dot (unity_SHBb, tmpvar_7);
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_2));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = tmpvar_4;
  xlv_TEXCOORD2 = (unity_ObjectToWorld * _glesVertex).xyz;
  xlv_TEXCOORD4 = tmpvar_1;
  xlv_TEXCOORD5 = (x1_6 + (unity_SHC.xyz * (
    (normal_5.x * normal_5.x)
   - 
    (normal_5.y * normal_5.y)
  )));
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_draw_buffers : enable
uniform mediump vec4 unity_SHAr;
uniform mediump vec4 unity_SHAg;
uniform mediump vec4 unity_SHAb;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying mediump vec3 xlv_TEXCOORD5;
void main ()
{
  mediump vec4 outEmission_1;
  lowp vec3 tmpvar_2;
  tmpvar_2 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color).xyz;
  mediump vec3 normalWorld_3;
  normalWorld_3 = xlv_TEXCOORD1;
  mediump vec3 tmpvar_4;
  mediump vec4 tmpvar_5;
  tmpvar_5.w = 1.0;
  tmpvar_5.xyz = normalWorld_3;
  mediump vec3 x_6;
  x_6.x = dot (unity_SHAr, tmpvar_5);
  x_6.y = dot (unity_SHAg, tmpvar_5);
  x_6.z = dot (unity_SHAb, tmpvar_5);
  tmpvar_4 = max (((1.055 * 
    pow (max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD5 + x_6)), vec3(0.4166667, 0.4166667, 0.4166667))
  ) - 0.055), vec3(0.0, 0.0, 0.0));
  lowp vec3 tmpvar_7;
  mediump vec3 tmpvar_8;
  mediump vec3 tmpvar_9;
  mediump vec3 albedo_10;
  albedo_10 = tmpvar_2;
  tmpvar_9 = (albedo_10 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_7 = tmpvar_9;
  mediump vec3 diffColor_11;
  diffColor_11 = tmpvar_7;
  mediump vec3 color_12;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_4 * diffColor_11);
  color_12 = tmpvar_13;
  tmpvar_8 = tmpvar_7;
  mediump vec4 outGBuffer2_14;
  mediump vec4 tmpvar_15;
  tmpvar_15.xyz = tmpvar_8;
  tmpvar_15.w = 1.0;
  mediump vec4 tmpvar_16;
  tmpvar_16.xyz = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_10, vec3(_Metallic));
  tmpvar_16.w = _Glossiness;
  highp vec4 tmpvar_17;
  tmpvar_17.w = 1.0;
  tmpvar_17.xyz = ((xlv_TEXCOORD1 * 0.5) + 0.5);
  outGBuffer2_14 = tmpvar_17;
  mediump vec4 tmpvar_18;
  tmpvar_18.w = 1.0;
  tmpvar_18.xyz = color_12;
  outEmission_1.w = tmpvar_18.w;
  outEmission_1.xyz = exp2(-(color_12));
  gl_FragData[0] = tmpvar_15;
  gl_FragData[1] = tmpvar_16;
  gl_FragData[2] = outGBuffer2_14;
  gl_FragData[3] = outEmission_1;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 23 math, 1 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform mediump vec4 unity_SHBr;
uniform mediump vec4 unity_SHBg;
uniform mediump vec4 unity_SHBb;
uniform mediump vec4 unity_SHC;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD4;
varying mediump vec3 xlv_TEXCOORD5;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  tmpvar_2.w = 1.0;
  tmpvar_2.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_3;
  tmpvar_3[0] = unity_WorldToObject[0].xyz;
  tmpvar_3[1] = unity_WorldToObject[1].xyz;
  tmpvar_3[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_4;
  tmpvar_4 = normalize((_glesNormal * tmpvar_3));
  tmpvar_1.zw = vec2(0.0, 0.0);
  tmpvar_1.xy = vec2(0.0, 0.0);
  mediump vec3 normal_5;
  normal_5 = tmpvar_4;
  mediump vec3 x1_6;
  mediump vec4 tmpvar_7;
  tmpvar_7 = (normal_5.xyzz * normal_5.yzzx);
  x1_6.x = dot (unity_SHBr, tmpvar_7);
  x1_6.y = dot (unity_SHBg, tmpvar_7);
  x1_6.z = dot (unity_SHBb, tmpvar_7);
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_2));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = tmpvar_4;
  xlv_TEXCOORD2 = (unity_ObjectToWorld * _glesVertex).xyz;
  xlv_TEXCOORD4 = tmpvar_1;
  xlv_TEXCOORD5 = (x1_6 + (unity_SHC.xyz * (
    (normal_5.x * normal_5.x)
   - 
    (normal_5.y * normal_5.y)
  )));
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_draw_buffers : enable
uniform mediump vec4 unity_SHAr;
uniform mediump vec4 unity_SHAg;
uniform mediump vec4 unity_SHAb;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying mediump vec3 xlv_TEXCOORD5;
void main ()
{
  mediump vec4 outEmission_1;
  lowp vec3 tmpvar_2;
  tmpvar_2 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color).xyz;
  mediump vec3 normalWorld_3;
  normalWorld_3 = xlv_TEXCOORD1;
  mediump vec3 tmpvar_4;
  mediump vec4 tmpvar_5;
  tmpvar_5.w = 1.0;
  tmpvar_5.xyz = normalWorld_3;
  mediump vec3 x_6;
  x_6.x = dot (unity_SHAr, tmpvar_5);
  x_6.y = dot (unity_SHAg, tmpvar_5);
  x_6.z = dot (unity_SHAb, tmpvar_5);
  tmpvar_4 = max (((1.055 * 
    pow (max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD5 + x_6)), vec3(0.4166667, 0.4166667, 0.4166667))
  ) - 0.055), vec3(0.0, 0.0, 0.0));
  lowp vec3 tmpvar_7;
  mediump vec3 tmpvar_8;
  mediump vec3 tmpvar_9;
  mediump vec3 albedo_10;
  albedo_10 = tmpvar_2;
  tmpvar_9 = (albedo_10 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_7 = tmpvar_9;
  mediump vec3 diffColor_11;
  diffColor_11 = tmpvar_7;
  mediump vec3 color_12;
  highp vec3 tmpvar_13;
  tmpvar_13 = (tmpvar_4 * diffColor_11);
  color_12 = tmpvar_13;
  tmpvar_8 = tmpvar_7;
  mediump vec4 outGBuffer2_14;
  mediump vec4 tmpvar_15;
  tmpvar_15.xyz = tmpvar_8;
  tmpvar_15.w = 1.0;
  mediump vec4 tmpvar_16;
  tmpvar_16.xyz = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_10, vec3(_Metallic));
  tmpvar_16.w = _Glossiness;
  highp vec4 tmpvar_17;
  tmpvar_17.w = 1.0;
  tmpvar_17.xyz = ((xlv_TEXCOORD1 * 0.5) + 0.5);
  outGBuffer2_14 = tmpvar_17;
  mediump vec4 tmpvar_18;
  tmpvar_18.w = 1.0;
  tmpvar_18.xyz = color_12;
  outEmission_1.w = tmpvar_18.w;
  outEmission_1.xyz = exp2(-(color_12));
  gl_FragData[0] = tmpvar_15;
  gl_FragData[1] = tmpvar_16;
  gl_FragData[2] = outGBuffer2_14;
  gl_FragData[3] = outEmission_1;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
out mediump vec3 vs_TEXCOORD5;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat12;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    vs_TEXCOORD1.xyz = u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat16_2 = u_xlat0.y * u_xlat0.y;
    u_xlat16_2 = u_xlat0.x * u_xlat0.x + (-u_xlat16_2);
    u_xlat16_0 = u_xlat0.yzzx * u_xlat0.xyzz;
    u_xlat16_3.x = dot(unity_SHBr, u_xlat16_0);
    u_xlat16_3.y = dot(unity_SHBg, u_xlat16_0);
    u_xlat16_3.z = dot(unity_SHBb, u_xlat16_0);
    vs_TEXCOORD5.xyz = unity_SHC.xyz * vec3(u_xlat16_2) + u_xlat16_3.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in mediump vec3 vs_TEXCOORD5;
layout(location = 0) out mediump vec4 SV_Target0;
layout(location = 1) out mediump vec4 SV_Target1;
layout(location = 2) out mediump vec4 SV_Target2;
layout(location = 3) out mediump vec4 SV_Target3;
mediump vec3 u_xlat16_0;
vec4 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
void main()
{
    u_xlat16_0.x = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_4.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    SV_Target1.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_0.xyz = u_xlat16_0.xxx * u_xlat16_2.xyz;
    SV_Target0.xyz = u_xlat16_0.xyz;
    SV_Target0.w = 1.0;
    SV_Target1.w = _Glossiness;
    u_xlat1.xyz = vs_TEXCOORD1.xyz * vec3(0.5, 0.5, 0.5) + vec3(0.5, 0.5, 0.5);
    u_xlat1.w = 1.0;
    SV_Target2 = u_xlat1;
    u_xlat1.xyz = vs_TEXCOORD1.xyz;
    u_xlat1.w = 1.0;
    u_xlat16_3.x = dot(unity_SHAr, u_xlat1);
    u_xlat16_3.y = dot(unity_SHAg, u_xlat1);
    u_xlat16_3.z = dot(unity_SHAb, u_xlat1);
    u_xlat16_3.xyz = u_xlat16_3.xyz + vs_TEXCOORD5.xyz;
    u_xlat16_3.xyz = max(u_xlat16_3.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_1.xyz = log2(u_xlat16_3.xyz);
    u_xlat16_1.xyz = u_xlat16_1.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_1.xyz = exp2(u_xlat16_1.xyz);
    u_xlat16_1.xyz = u_xlat16_1.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_0.xyz = u_xlat16_0.xyz * u_xlat16_1.xyz;
    SV_Target3.xyz = exp2((-u_xlat16_0.xyz));
    SV_Target3.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
out mediump vec3 vs_TEXCOORD5;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat12;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    vs_TEXCOORD1.xyz = u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat16_2 = u_xlat0.y * u_xlat0.y;
    u_xlat16_2 = u_xlat0.x * u_xlat0.x + (-u_xlat16_2);
    u_xlat16_0 = u_xlat0.yzzx * u_xlat0.xyzz;
    u_xlat16_3.x = dot(unity_SHBr, u_xlat16_0);
    u_xlat16_3.y = dot(unity_SHBg, u_xlat16_0);
    u_xlat16_3.z = dot(unity_SHBb, u_xlat16_0);
    vs_TEXCOORD5.xyz = unity_SHC.xyz * vec3(u_xlat16_2) + u_xlat16_3.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in mediump vec3 vs_TEXCOORD5;
layout(location = 0) out mediump vec4 SV_Target0;
layout(location = 1) out mediump vec4 SV_Target1;
layout(location = 2) out mediump vec4 SV_Target2;
layout(location = 3) out mediump vec4 SV_Target3;
mediump vec3 u_xlat16_0;
vec4 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
void main()
{
    u_xlat16_0.x = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_4.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    SV_Target1.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_0.xyz = u_xlat16_0.xxx * u_xlat16_2.xyz;
    SV_Target0.xyz = u_xlat16_0.xyz;
    SV_Target0.w = 1.0;
    SV_Target1.w = _Glossiness;
    u_xlat1.xyz = vs_TEXCOORD1.xyz * vec3(0.5, 0.5, 0.5) + vec3(0.5, 0.5, 0.5);
    u_xlat1.w = 1.0;
    SV_Target2 = u_xlat1;
    u_xlat1.xyz = vs_TEXCOORD1.xyz;
    u_xlat1.w = 1.0;
    u_xlat16_3.x = dot(unity_SHAr, u_xlat1);
    u_xlat16_3.y = dot(unity_SHAg, u_xlat1);
    u_xlat16_3.z = dot(unity_SHAb, u_xlat1);
    u_xlat16_3.xyz = u_xlat16_3.xyz + vs_TEXCOORD5.xyz;
    u_xlat16_3.xyz = max(u_xlat16_3.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_1.xyz = log2(u_xlat16_3.xyz);
    u_xlat16_1.xyz = u_xlat16_1.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_1.xyz = exp2(u_xlat16_1.xyz);
    u_xlat16_1.xyz = u_xlat16_1.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_0.xyz = u_xlat16_0.xyz * u_xlat16_1.xyz;
    SV_Target3.xyz = exp2((-u_xlat16_0.xyz));
    SV_Target3.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
out mediump vec3 vs_TEXCOORD5;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat12;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    vs_TEXCOORD1.xyz = u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat16_2 = u_xlat0.y * u_xlat0.y;
    u_xlat16_2 = u_xlat0.x * u_xlat0.x + (-u_xlat16_2);
    u_xlat16_0 = u_xlat0.yzzx * u_xlat0.xyzz;
    u_xlat16_3.x = dot(unity_SHBr, u_xlat16_0);
    u_xlat16_3.y = dot(unity_SHBg, u_xlat16_0);
    u_xlat16_3.z = dot(unity_SHBb, u_xlat16_0);
    vs_TEXCOORD5.xyz = unity_SHC.xyz * vec3(u_xlat16_2) + u_xlat16_3.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in mediump vec3 vs_TEXCOORD5;
layout(location = 0) out mediump vec4 SV_Target0;
layout(location = 1) out mediump vec4 SV_Target1;
layout(location = 2) out mediump vec4 SV_Target2;
layout(location = 3) out mediump vec4 SV_Target3;
mediump vec3 u_xlat16_0;
vec4 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
void main()
{
    u_xlat16_0.x = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_4.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    SV_Target1.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_0.xyz = u_xlat16_0.xxx * u_xlat16_2.xyz;
    SV_Target0.xyz = u_xlat16_0.xyz;
    SV_Target0.w = 1.0;
    SV_Target1.w = _Glossiness;
    u_xlat1.xyz = vs_TEXCOORD1.xyz * vec3(0.5, 0.5, 0.5) + vec3(0.5, 0.5, 0.5);
    u_xlat1.w = 1.0;
    SV_Target2 = u_xlat1;
    u_xlat1.xyz = vs_TEXCOORD1.xyz;
    u_xlat1.w = 1.0;
    u_xlat16_3.x = dot(unity_SHAr, u_xlat1);
    u_xlat16_3.y = dot(unity_SHAg, u_xlat1);
    u_xlat16_3.z = dot(unity_SHAb, u_xlat1);
    u_xlat16_3.xyz = u_xlat16_3.xyz + vs_TEXCOORD5.xyz;
    u_xlat16_3.xyz = max(u_xlat16_3.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_1.xyz = log2(u_xlat16_3.xyz);
    u_xlat16_1.xyz = u_xlat16_1.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_1.xyz = exp2(u_xlat16_1.xyz);
    u_xlat16_1.xyz = u_xlat16_1.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_0.xyz = u_xlat16_0.xyz * u_xlat16_1.xyz;
    SV_Target3.xyz = exp2((-u_xlat16_0.xyz));
    SV_Target3.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

//////////////////////////////////////////////////////
Keywords set in this variant: LIGHTPROBE_SH UNITY_HDR_ON 
-- Hardware tier variant: Tier 1
-- Vertex shader for "gles":
// Stats: 21 math, 1 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform mediump vec4 unity_SHBr;
uniform mediump vec4 unity_SHBg;
uniform mediump vec4 unity_SHBb;
uniform mediump vec4 unity_SHC;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD4;
varying mediump vec3 xlv_TEXCOORD5;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  tmpvar_2.w = 1.0;
  tmpvar_2.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_3;
  tmpvar_3[0] = unity_WorldToObject[0].xyz;
  tmpvar_3[1] = unity_WorldToObject[1].xyz;
  tmpvar_3[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_4;
  tmpvar_4 = normalize((_glesNormal * tmpvar_3));
  tmpvar_1.zw = vec2(0.0, 0.0);
  tmpvar_1.xy = vec2(0.0, 0.0);
  mediump vec3 normal_5;
  normal_5 = tmpvar_4;
  mediump vec3 x1_6;
  mediump vec4 tmpvar_7;
  tmpvar_7 = (normal_5.xyzz * normal_5.yzzx);
  x1_6.x = dot (unity_SHBr, tmpvar_7);
  x1_6.y = dot (unity_SHBg, tmpvar_7);
  x1_6.z = dot (unity_SHBb, tmpvar_7);
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_2));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = tmpvar_4;
  xlv_TEXCOORD2 = (unity_ObjectToWorld * _glesVertex).xyz;
  xlv_TEXCOORD4 = tmpvar_1;
  xlv_TEXCOORD5 = (x1_6 + (unity_SHC.xyz * (
    (normal_5.x * normal_5.x)
   - 
    (normal_5.y * normal_5.y)
  )));
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_draw_buffers : enable
uniform mediump vec4 unity_SHAr;
uniform mediump vec4 unity_SHAg;
uniform mediump vec4 unity_SHAb;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying mediump vec3 xlv_TEXCOORD5;
void main ()
{
  lowp vec3 tmpvar_1;
  tmpvar_1 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color).xyz;
  mediump vec3 normalWorld_2;
  normalWorld_2 = xlv_TEXCOORD1;
  mediump vec4 tmpvar_3;
  tmpvar_3.w = 1.0;
  tmpvar_3.xyz = normalWorld_2;
  mediump vec3 x_4;
  x_4.x = dot (unity_SHAr, tmpvar_3);
  x_4.y = dot (unity_SHAg, tmpvar_3);
  x_4.z = dot (unity_SHAb, tmpvar_3);
  lowp vec3 tmpvar_5;
  mediump vec3 tmpvar_6;
  mediump vec3 tmpvar_7;
  mediump vec3 albedo_8;
  albedo_8 = tmpvar_1;
  tmpvar_7 = (albedo_8 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_5 = tmpvar_7;
  mediump vec3 diffColor_9;
  diffColor_9 = tmpvar_5;
  tmpvar_6 = tmpvar_5;
  mediump vec4 outGBuffer2_10;
  mediump vec4 tmpvar_11;
  tmpvar_11.xyz = tmpvar_6;
  tmpvar_11.w = 1.0;
  mediump vec4 tmpvar_12;
  tmpvar_12.xyz = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_8, vec3(_Metallic));
  tmpvar_12.w = _Glossiness;
  highp vec4 tmpvar_13;
  tmpvar_13.w = 1.0;
  tmpvar_13.xyz = ((xlv_TEXCOORD1 * 0.5) + 0.5);
  outGBuffer2_10 = tmpvar_13;
  mediump vec4 tmpvar_14;
  tmpvar_14.w = 1.0;
  tmpvar_14.xyz = (max ((
    (1.055 * pow (max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD5 + x_4)), vec3(0.4166667, 0.4166667, 0.4166667)))
   - 0.055), vec3(0.0, 0.0, 0.0)) * diffColor_9);
  gl_FragData[0] = tmpvar_11;
  gl_FragData[1] = tmpvar_12;
  gl_FragData[2] = outGBuffer2_10;
  gl_FragData[3] = tmpvar_14;
}


#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles":
// Stats: 21 math, 1 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform mediump vec4 unity_SHBr;
uniform mediump vec4 unity_SHBg;
uniform mediump vec4 unity_SHBb;
uniform mediump vec4 unity_SHC;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD4;
varying mediump vec3 xlv_TEXCOORD5;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  tmpvar_2.w = 1.0;
  tmpvar_2.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_3;
  tmpvar_3[0] = unity_WorldToObject[0].xyz;
  tmpvar_3[1] = unity_WorldToObject[1].xyz;
  tmpvar_3[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_4;
  tmpvar_4 = normalize((_glesNormal * tmpvar_3));
  tmpvar_1.zw = vec2(0.0, 0.0);
  tmpvar_1.xy = vec2(0.0, 0.0);
  mediump vec3 normal_5;
  normal_5 = tmpvar_4;
  mediump vec3 x1_6;
  mediump vec4 tmpvar_7;
  tmpvar_7 = (normal_5.xyzz * normal_5.yzzx);
  x1_6.x = dot (unity_SHBr, tmpvar_7);
  x1_6.y = dot (unity_SHBg, tmpvar_7);
  x1_6.z = dot (unity_SHBb, tmpvar_7);
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_2));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = tmpvar_4;
  xlv_TEXCOORD2 = (unity_ObjectToWorld * _glesVertex).xyz;
  xlv_TEXCOORD4 = tmpvar_1;
  xlv_TEXCOORD5 = (x1_6 + (unity_SHC.xyz * (
    (normal_5.x * normal_5.x)
   - 
    (normal_5.y * normal_5.y)
  )));
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_draw_buffers : enable
uniform mediump vec4 unity_SHAr;
uniform mediump vec4 unity_SHAg;
uniform mediump vec4 unity_SHAb;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying mediump vec3 xlv_TEXCOORD5;
void main ()
{
  lowp vec3 tmpvar_1;
  tmpvar_1 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color).xyz;
  mediump vec3 normalWorld_2;
  normalWorld_2 = xlv_TEXCOORD1;
  mediump vec3 tmpvar_3;
  mediump vec4 tmpvar_4;
  tmpvar_4.w = 1.0;
  tmpvar_4.xyz = normalWorld_2;
  mediump vec3 x_5;
  x_5.x = dot (unity_SHAr, tmpvar_4);
  x_5.y = dot (unity_SHAg, tmpvar_4);
  x_5.z = dot (unity_SHAb, tmpvar_4);
  tmpvar_3 = max (((1.055 * 
    pow (max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD5 + x_5)), vec3(0.4166667, 0.4166667, 0.4166667))
  ) - 0.055), vec3(0.0, 0.0, 0.0));
  lowp vec3 tmpvar_6;
  mediump vec3 tmpvar_7;
  mediump vec3 tmpvar_8;
  mediump vec3 albedo_9;
  albedo_9 = tmpvar_1;
  tmpvar_8 = (albedo_9 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_6 = tmpvar_8;
  mediump vec3 diffColor_10;
  diffColor_10 = tmpvar_6;
  mediump vec3 color_11;
  highp vec3 tmpvar_12;
  tmpvar_12 = (tmpvar_3 * diffColor_10);
  color_11 = tmpvar_12;
  tmpvar_7 = tmpvar_6;
  mediump vec4 outGBuffer2_13;
  mediump vec4 tmpvar_14;
  tmpvar_14.xyz = tmpvar_7;
  tmpvar_14.w = 1.0;
  mediump vec4 tmpvar_15;
  tmpvar_15.xyz = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_9, vec3(_Metallic));
  tmpvar_15.w = _Glossiness;
  highp vec4 tmpvar_16;
  tmpvar_16.w = 1.0;
  tmpvar_16.xyz = ((xlv_TEXCOORD1 * 0.5) + 0.5);
  outGBuffer2_13 = tmpvar_16;
  mediump vec4 tmpvar_17;
  tmpvar_17.w = 1.0;
  tmpvar_17.xyz = color_11;
  gl_FragData[0] = tmpvar_14;
  gl_FragData[1] = tmpvar_15;
  gl_FragData[2] = outGBuffer2_13;
  gl_FragData[3] = tmpvar_17;
}


#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles":
// Stats: 21 math, 1 textures
Shader Disassembly:
#version 100

#ifdef VERTEX
attribute vec4 _glesVertex;
attribute vec3 _glesNormal;
attribute vec4 _glesMultiTexCoord0;
uniform mediump vec4 unity_SHBr;
uniform mediump vec4 unity_SHBg;
uniform mediump vec4 unity_SHBb;
uniform mediump vec4 unity_SHC;
uniform highp mat4 unity_ObjectToWorld;
uniform highp mat4 unity_WorldToObject;
uniform highp mat4 unity_MatrixVP;
uniform highp vec4 _MainTex_ST;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying highp vec3 xlv_TEXCOORD2;
varying highp vec4 xlv_TEXCOORD4;
varying mediump vec3 xlv_TEXCOORD5;
void main ()
{
  highp vec4 tmpvar_1;
  highp vec4 tmpvar_2;
  tmpvar_2.w = 1.0;
  tmpvar_2.xyz = _glesVertex.xyz;
  highp mat3 tmpvar_3;
  tmpvar_3[0] = unity_WorldToObject[0].xyz;
  tmpvar_3[1] = unity_WorldToObject[1].xyz;
  tmpvar_3[2] = unity_WorldToObject[2].xyz;
  highp vec3 tmpvar_4;
  tmpvar_4 = normalize((_glesNormal * tmpvar_3));
  tmpvar_1.zw = vec2(0.0, 0.0);
  tmpvar_1.xy = vec2(0.0, 0.0);
  mediump vec3 normal_5;
  normal_5 = tmpvar_4;
  mediump vec3 x1_6;
  mediump vec4 tmpvar_7;
  tmpvar_7 = (normal_5.xyzz * normal_5.yzzx);
  x1_6.x = dot (unity_SHBr, tmpvar_7);
  x1_6.y = dot (unity_SHBg, tmpvar_7);
  x1_6.z = dot (unity_SHBb, tmpvar_7);
  gl_Position = (unity_MatrixVP * (unity_ObjectToWorld * tmpvar_2));
  xlv_TEXCOORD0 = ((_glesMultiTexCoord0.xy * _MainTex_ST.xy) + _MainTex_ST.zw);
  xlv_TEXCOORD1 = tmpvar_4;
  xlv_TEXCOORD2 = (unity_ObjectToWorld * _glesVertex).xyz;
  xlv_TEXCOORD4 = tmpvar_1;
  xlv_TEXCOORD5 = (x1_6 + (unity_SHC.xyz * (
    (normal_5.x * normal_5.x)
   - 
    (normal_5.y * normal_5.y)
  )));
}


#endif
#ifdef FRAGMENT
#extension GL_EXT_draw_buffers : enable
uniform mediump vec4 unity_SHAr;
uniform mediump vec4 unity_SHAg;
uniform mediump vec4 unity_SHAb;
uniform sampler2D _MainTex;
uniform mediump float _Glossiness;
uniform mediump float _Metallic;
uniform lowp vec4 _Color;
varying highp vec2 xlv_TEXCOORD0;
varying highp vec3 xlv_TEXCOORD1;
varying mediump vec3 xlv_TEXCOORD5;
void main ()
{
  lowp vec3 tmpvar_1;
  tmpvar_1 = (texture2D (_MainTex, xlv_TEXCOORD0) * _Color).xyz;
  mediump vec3 normalWorld_2;
  normalWorld_2 = xlv_TEXCOORD1;
  mediump vec3 tmpvar_3;
  mediump vec4 tmpvar_4;
  tmpvar_4.w = 1.0;
  tmpvar_4.xyz = normalWorld_2;
  mediump vec3 x_5;
  x_5.x = dot (unity_SHAr, tmpvar_4);
  x_5.y = dot (unity_SHAg, tmpvar_4);
  x_5.z = dot (unity_SHAb, tmpvar_4);
  tmpvar_3 = max (((1.055 * 
    pow (max (vec3(0.0, 0.0, 0.0), (xlv_TEXCOORD5 + x_5)), vec3(0.4166667, 0.4166667, 0.4166667))
  ) - 0.055), vec3(0.0, 0.0, 0.0));
  lowp vec3 tmpvar_6;
  mediump vec3 tmpvar_7;
  mediump vec3 tmpvar_8;
  mediump vec3 albedo_9;
  albedo_9 = tmpvar_1;
  tmpvar_8 = (albedo_9 * (0.7790837 - (_Metallic * 0.7790837)));
  tmpvar_6 = tmpvar_8;
  mediump vec3 diffColor_10;
  diffColor_10 = tmpvar_6;
  mediump vec3 color_11;
  highp vec3 tmpvar_12;
  tmpvar_12 = (tmpvar_3 * diffColor_10);
  color_11 = tmpvar_12;
  tmpvar_7 = tmpvar_6;
  mediump vec4 outGBuffer2_13;
  mediump vec4 tmpvar_14;
  tmpvar_14.xyz = tmpvar_7;
  tmpvar_14.w = 1.0;
  mediump vec4 tmpvar_15;
  tmpvar_15.xyz = mix (vec3(0.2209163, 0.2209163, 0.2209163), albedo_9, vec3(_Metallic));
  tmpvar_15.w = _Glossiness;
  highp vec4 tmpvar_16;
  tmpvar_16.w = 1.0;
  tmpvar_16.xyz = ((xlv_TEXCOORD1 * 0.5) + 0.5);
  outGBuffer2_13 = tmpvar_16;
  mediump vec4 tmpvar_17;
  tmpvar_17.w = 1.0;
  tmpvar_17.xyz = color_11;
  gl_FragData[0] = tmpvar_14;
  gl_FragData[1] = tmpvar_15;
  gl_FragData[2] = outGBuffer2_13;
  gl_FragData[3] = tmpvar_17;
}


#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 1
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
out mediump vec3 vs_TEXCOORD5;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat12;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    vs_TEXCOORD1.xyz = u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat16_2 = u_xlat0.y * u_xlat0.y;
    u_xlat16_2 = u_xlat0.x * u_xlat0.x + (-u_xlat16_2);
    u_xlat16_0 = u_xlat0.yzzx * u_xlat0.xyzz;
    u_xlat16_3.x = dot(unity_SHBr, u_xlat16_0);
    u_xlat16_3.y = dot(unity_SHBg, u_xlat16_0);
    u_xlat16_3.z = dot(unity_SHBb, u_xlat16_0);
    vs_TEXCOORD5.xyz = unity_SHC.xyz * vec3(u_xlat16_2) + u_xlat16_3.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in mediump vec3 vs_TEXCOORD5;
layout(location = 0) out mediump vec4 SV_Target0;
layout(location = 1) out mediump vec4 SV_Target1;
layout(location = 2) out mediump vec4 SV_Target2;
layout(location = 3) out mediump vec4 SV_Target3;
mediump vec3 u_xlat16_0;
vec4 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
void main()
{
    u_xlat16_0.x = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_4.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    SV_Target1.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_0.xyz = u_xlat16_0.xxx * u_xlat16_2.xyz;
    SV_Target0.xyz = u_xlat16_0.xyz;
    SV_Target0.w = 1.0;
    SV_Target1.w = _Glossiness;
    u_xlat1.xyz = vs_TEXCOORD1.xyz * vec3(0.5, 0.5, 0.5) + vec3(0.5, 0.5, 0.5);
    u_xlat1.w = 1.0;
    SV_Target2 = u_xlat1;
    u_xlat1.xyz = vs_TEXCOORD1.xyz;
    u_xlat1.w = 1.0;
    u_xlat16_3.x = dot(unity_SHAr, u_xlat1);
    u_xlat16_3.y = dot(unity_SHAg, u_xlat1);
    u_xlat16_3.z = dot(unity_SHAb, u_xlat1);
    u_xlat16_3.xyz = u_xlat16_3.xyz + vs_TEXCOORD5.xyz;
    u_xlat16_3.xyz = max(u_xlat16_3.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_1.xyz = log2(u_xlat16_3.xyz);
    u_xlat16_1.xyz = u_xlat16_1.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_1.xyz = exp2(u_xlat16_1.xyz);
    u_xlat16_1.xyz = u_xlat16_1.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    SV_Target3.xyz = u_xlat16_0.xyz * u_xlat16_1.xyz;
    SV_Target3.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 1
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 2
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
out mediump vec3 vs_TEXCOORD5;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat12;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    vs_TEXCOORD1.xyz = u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat16_2 = u_xlat0.y * u_xlat0.y;
    u_xlat16_2 = u_xlat0.x * u_xlat0.x + (-u_xlat16_2);
    u_xlat16_0 = u_xlat0.yzzx * u_xlat0.xyzz;
    u_xlat16_3.x = dot(unity_SHBr, u_xlat16_0);
    u_xlat16_3.y = dot(unity_SHBg, u_xlat16_0);
    u_xlat16_3.z = dot(unity_SHBb, u_xlat16_0);
    vs_TEXCOORD5.xyz = unity_SHC.xyz * vec3(u_xlat16_2) + u_xlat16_3.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in mediump vec3 vs_TEXCOORD5;
layout(location = 0) out mediump vec4 SV_Target0;
layout(location = 1) out mediump vec4 SV_Target1;
layout(location = 2) out mediump vec4 SV_Target2;
layout(location = 3) out mediump vec4 SV_Target3;
mediump vec3 u_xlat16_0;
vec4 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
void main()
{
    u_xlat16_0.x = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_4.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    SV_Target1.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_0.xyz = u_xlat16_0.xxx * u_xlat16_2.xyz;
    SV_Target0.xyz = u_xlat16_0.xyz;
    SV_Target0.w = 1.0;
    SV_Target1.w = _Glossiness;
    u_xlat1.xyz = vs_TEXCOORD1.xyz * vec3(0.5, 0.5, 0.5) + vec3(0.5, 0.5, 0.5);
    u_xlat1.w = 1.0;
    SV_Target2 = u_xlat1;
    u_xlat1.xyz = vs_TEXCOORD1.xyz;
    u_xlat1.w = 1.0;
    u_xlat16_3.x = dot(unity_SHAr, u_xlat1);
    u_xlat16_3.y = dot(unity_SHAg, u_xlat1);
    u_xlat16_3.z = dot(unity_SHAb, u_xlat1);
    u_xlat16_3.xyz = u_xlat16_3.xyz + vs_TEXCOORD5.xyz;
    u_xlat16_3.xyz = max(u_xlat16_3.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_1.xyz = log2(u_xlat16_3.xyz);
    u_xlat16_1.xyz = u_xlat16_1.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_1.xyz = exp2(u_xlat16_1.xyz);
    u_xlat16_1.xyz = u_xlat16_1.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    SV_Target3.xyz = u_xlat16_0.xyz * u_xlat16_1.xyz;
    SV_Target3.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 2
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

-- Hardware tier variant: Tier 3
-- Vertex shader for "gles3":
Shader Disassembly:
#ifdef VERTEX
#version 300 es

uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD4;
out mediump vec3 vs_TEXCOORD5;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
float u_xlat12;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    vs_TEXCOORD1.xyz = u_xlat0.xyz;
    vs_TEXCOORD4 = vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat16_2 = u_xlat0.y * u_xlat0.y;
    u_xlat16_2 = u_xlat0.x * u_xlat0.x + (-u_xlat16_2);
    u_xlat16_0 = u_xlat0.yzzx * u_xlat0.xyzz;
    u_xlat16_3.x = dot(unity_SHBr, u_xlat16_0);
    u_xlat16_3.y = dot(unity_SHBg, u_xlat16_0);
    u_xlat16_3.z = dot(unity_SHBb, u_xlat16_0);
    vs_TEXCOORD5.xyz = unity_SHC.xyz * vec3(u_xlat16_2) + u_xlat16_3.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	mediump float _Glossiness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _Color;
uniform lowp sampler2D _MainTex;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in mediump vec3 vs_TEXCOORD5;
layout(location = 0) out mediump vec4 SV_Target0;
layout(location = 1) out mediump vec4 SV_Target1;
layout(location = 2) out mediump vec4 SV_Target2;
layout(location = 3) out mediump vec4 SV_Target3;
mediump vec3 u_xlat16_0;
vec4 u_xlat1;
mediump vec3 u_xlat16_1;
lowp vec3 u_xlat10_1;
mediump vec3 u_xlat16_2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
void main()
{
    u_xlat16_0.x = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat10_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = u_xlat10_1.xyz * _Color.xyz;
    u_xlat16_4.xyz = u_xlat10_1.xyz * _Color.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    SV_Target1.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_4.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_0.xyz = u_xlat16_0.xxx * u_xlat16_2.xyz;
    SV_Target0.xyz = u_xlat16_0.xyz;
    SV_Target0.w = 1.0;
    SV_Target1.w = _Glossiness;
    u_xlat1.xyz = vs_TEXCOORD1.xyz * vec3(0.5, 0.5, 0.5) + vec3(0.5, 0.5, 0.5);
    u_xlat1.w = 1.0;
    SV_Target2 = u_xlat1;
    u_xlat1.xyz = vs_TEXCOORD1.xyz;
    u_xlat1.w = 1.0;
    u_xlat16_3.x = dot(unity_SHAr, u_xlat1);
    u_xlat16_3.y = dot(unity_SHAg, u_xlat1);
    u_xlat16_3.z = dot(unity_SHAb, u_xlat1);
    u_xlat16_3.xyz = u_xlat16_3.xyz + vs_TEXCOORD5.xyz;
    u_xlat16_3.xyz = max(u_xlat16_3.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_1.xyz = log2(u_xlat16_3.xyz);
    u_xlat16_1.xyz = u_xlat16_1.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_1.xyz = exp2(u_xlat16_1.xyz);
    u_xlat16_1.xyz = u_xlat16_1.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    SV_Target3.xyz = u_xlat16_0.xyz * u_xlat16_1.xyz;
    SV_Target3.w = 1.0;
    return;
}

#endif


-- Hardware tier variant: Tier 3
-- Fragment shader for "gles3":
Shader Disassembly:
// All GLSL source is contained within the vertex program

 }
}
Fallback "Diffuse"
}